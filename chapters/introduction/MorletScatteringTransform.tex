Here we discuss the particulars of our implementation of the generalized scattering transform used for 1D sonar classification.
The code can be found on github.com at \href{https://github.com/dsweber2/ScatteringTransform.jl}{ScatteringTransform.jl}.\footnote{\href{https://github.com/dsweber2/ScatteringTransform.jl}{https://github.com/dsweber2/ScatteringTransform.jl}}
We have implemented all of the wavelets found in \Cref{sec:WaveletExamples} from ContinuousWavelets.jl, which are all 1D transforms, as well as shearlets for the 2D case.
For subsampling, we use a very straightforward average pooling, which if the subsampling rate is $\nf {p}{q}$, sums over windows of size $q$, keeping every $p$ entries.

\subsection{Discrete Output}
\label{sec:discrete-output}

One feature of our implementation that is not present in the continuous theory above is a final subsampling after averaging with the father wavelet.
Because the father wavelet is typically approximately zero past some relatively low frequency (in \Cref{fig:Qbeta}, for example, the left-most column is the father wavelet, and it is approximately zero by $\slimsim 40\si{Hz}$ out of the $\slimsim 500\si{Hz}$ frequencies used), including all points in the output is typically highly redundant.
We subsample the output $\contOut[\pat][t]$ to reduce this redundancy; we will denote this further subsampling rate by $\sub'$.

In the discrete case, the input signal $f$ must be evaluated at only a finite set of points $\inTime$ for $j=0,\ldots, N-1$, so define $f_{j} = f[j] \define f\paren{\inTime}$.
Because of the subsampling, the points where either the internal layers $\contIn[\pat]$ or external output $\contOut[\pat]$ are evaluated will not simply be the $\inTime$'s.
Instead, the discretized internal layers are given by the collection of vectors $U = \brak[\big]{\discIn}_{\pat\in\PathSet,m\in\mZ_{0}}$, with the entries of the vector $\discIn$ given by $\discIn[\pat][i] \define \contIn[\pat][\subTime]$, where the continuous output has been sampled at the points $\subTime \define \inTime[j_{i,m}]$ for some subset $\inTime[j_{i,m}]$ of the input points $\inTime$ which keeps every $\slimsim \p{k=1}{m}\sub[k]$th point.
Note that $\subTime[j][0]=\inTime$.
Similarly, the output is given by the collection of vectors $S = \brak[\Big]{\discOut[\pat][i] \define \contOut[\pat][\subTime'] }_{\pat\in\PathSet,m\in\mZ_{0}}$, where the output sample points have been further subsampled, $\subTime' \define \subTime[k_{i}]$, where $k_{i}$ keeps every $\slimsim \sub'$th point.
For the wavelets in layer $m$, we will denote the sampled vector equivalent as $\frEl[m][]{}[i] \define \frEl[m][]{}\paren[\big]{\subTime}$.

We will refer to the \emph{output location $\nf{i}{M}$ (at path $\pat$)} as shorthand for $i$th value in the vector $\discOut[\pat]$, which is of length $M$.
We will omit ``at path $\pat$'' when it is clear from context.

Unless otherwise noted, we will be using a sampling rate of $1000\si{Hz}$, or a sample every millisecond.

% discuss normalization
\subsection{Normalization}\label{sec:normal}
As observed in~\cite{brunaInvariantScatteringConvolution2013}, the magnitude of the coefficients in each layer is frequently orders of magnitude different.
This can be a major issue for convergence for some linear classification methods, which assume that the input is normally distributed and frequently mean zero~\cite{tibshiraniElementsStatisticalLearning2009}.
The mean is usually accounted for through a bias term, but whitening is typically handled as a preprocessing step.
To do this, we normalize each layer separately via $\contOut
[\pat][t]\cdot N_{m}/\norm[\big]{\layerOut}_{2}$ where $N_{m}$ is the total number of paths used in the $m$th layer.
This only applies for classification however, since for interpretation purposes, normalization creates some odd distortions (see \Cref{cha:Interpretation}).

% discuss the plots

\subsection{Example transforms}
\label{sec:example-transforms}
In \Cref{fig:HeaviSineST}, \Cref{fig:HeaviSineDoG2ST}, and \Cref{fig:bumpsST} we have a compressed representation of layers zero, one, and two for the scattering transform of various signals and wavelets.
In all of the figures, the color represents the magnitude of the ST output in the logarithmic scale (base 10) at that path (for the second layer) or time (for the zeroth or first layers).
For the zeroth layer, we have taken the absolute value to be able to plot on the same color scale as the other layers.
The $x$-axis for the zeroth layer is the time, while there is no variation along the $y$-axis, since there is only the averaging function.
It is a heatmap only to facilitate comparison with the output of the other layers.
The first layer is quite similar to the scalograms in \Cref{sec:WaveletExamples}, although the magnitude is not squared to facilitate comparison, and it has been averaged and subsampled.
The second layer is the most complicated to describe.
There are two indices to express each path $\pat=(\ind[2], \ind[1])$, and then for each path there is a time component as well.
To express this, the paths are given along the axes, while at a particular heatmap location, for example $(\ind[2], \ind[1]) = (1.59\si{Hz}, 9.86\si{Hz})$, there is a small subplot where time varies along the $x$-axis, corresponding to the output $\contOut[(\ind[2], \ind[1])](\subTime[i][2]')$.
The color for the second layer refers to the log base 10 of the largest coefficient at that path.
Given the layer of detail in each subplot, these figures are best examined on a digital copy, zooming in as needed.

\begin{figure}[h!]
  \begin{subfigure}{1.0\linewidth}
    \centering
    \includegraphics[width=.7\textwidth]{figures/introduction/HeaviSineFunction.pdf}
  \end{subfigure}
  \begin{subfigure}{1.0\linewidth}
    \centering
    \includegraphics[width=.8\textwidth]{figures/introduction/heavisineScatteredMorlet.png}
  \caption{ST Coefficients of the HeaviSine using Morlet wavelets of mean frequency $\pi$}\label{fig:HeaviSineST}
  \end{subfigure}
  \begin{subfigure}{1.0\linewidth}
    \centering
    \includegraphics[width=.8\textwidth]{figures/introduction/heavisineScatteredDoG2.png}
    \caption{ST Coefficients of the HeaviSine using DoG2 wavelets}\label{fig:HeaviSineDoG2ST}
  \end{subfigure}
\end{figure}


For the HeaviSine function, the zeroth layer in either \Cref{fig:HeaviSineST} or \Cref{fig:HeaviSineDoG2ST} captures the step function from 500--1500\si{ms}.
As may be expected from averaging with the father wavelet, the edges in the first layer are blurred, but the pure sinusoid at 9\si{Hz} comes through clearly.
In the second layer, the two discontinuities show up as peaks in either \Cref{fig:HeaviSineST} or \Cref{fig:HeaviSineDoG2ST} at high first layer frequency, though the largest coefficients are still a result of paths with first layer frequency around $9\si{Hz}$.
These coefficients are roughly uniform in space on the paths near $(21.8,9.48)$.
On the other hand, there are some paths, such as $(5.53\si{Hz}, 15.1\si{Hz})$ or $(3.11\si{Hz}, 28.7\si{Hz})$ where only one of the two peaks shows up prominently.
Attempting to understand what these coordinates mean will be the principal aim of \Cref{cha:Interpretation}.

\begin{figure}[h!]
  \begin{subfigure}{1.0\linewidth}
  \centering
  \includegraphics[width=.8\textwidth]{figures/introduction/bumpsFunction.pdf}
  \end{subfigure}
  \begin{subfigure}{1.0\linewidth}
    \centering
    \includegraphics[width=.8\textwidth]{figures/introduction/bumpsScatteredMorlet.png}
  \end{subfigure}
  \caption{ST Coefficients of the bumps signal using Morlet wavelets of mean frequency $\pi$}\label{fig:bumpsST}
\end{figure}
\begin{figure}[h!]
  \centering
  \includegraphics[width=.8\textwidth]{figures/introduction/bumpsScatteredDoG2.png}
  \caption{ST Coefficients of the bumps signal using DoG2 wavelets}
  \label{fig:bumpsDoG2ST}
\end{figure}


For the bumps function in \Cref{fig:bumpsST}, the zeroth and first layers roughly indicate the locations of discontinuities, with somewhat more clarity as the scale becomes finer at high frequency.
The first layer DoG2 wavelets in \Cref{fig:bumpsDoG2ST} correspond much more roughly to the scalogram in \Cref{fig:bumpsDog2}, due to the blurring of the averaging function and the subsampling.
For the DoG2 wavelets, looking at the individual subplots, the second layer paths can roughly be divided into two kinds.
Paths such as $(3.11\si{Hz}, 9.48\si{Hz})$ which are low frequency in both the first and second layer wavelets, generally consist of a single peak.
On the other hand, paths such as $(32.3\si{Hz}, 150.0\si{Hz})$ which is relatively high frequency in both, consist of two bumps, with the left higher than the right.
These roughly correspond to the collection of bumps between 400--900\si{ms} where there are 7 peaks, and the collection between 1300--1700\si{ms}, where there are only 4 peaks.
Using Morlet wavelets in \Cref{fig:bumpsST}, the same two peaks show up for high frequency in both (such as path $(60.6\si{Hz}, 244.0\si{Hz})$).
There is however much more noise in the signals.

In \Cref{fig:HeaviSineST} and \Cref{fig:bumpsST}, we can see the concentration along decreasing paths where $\ind[2]<\ind[1]$. Mallat proved this property occurs for a fairly restrictive class of wavelets in~\cite[Lemma 2.8]{mallatGroupInvariantScattering2012} and it was empirically observed for Morlet wavelets by Bruna and Mallat in~\cite{brunaInvariantScatteringConvolution2013}.
They observed that the only coefficients with large magnitude for scattering transforms using Morlet wavelets are those where the frequency in the second layer is less than the first layer, or $\ind[2]<\ind[1]$.
For these figures, this means that the majority of the energy is located in the upper left, above the diagonal line where the frequencies are equal; since the second layer is subsampled, this is slightly above the $\ang{45}$ line.
Note however, that this is very much not the case for the real-valued DoG2 wavelets in \Cref{fig:HeaviSineDoG2ST}, where the largest second layer coefficients occur for paths around $(21.8,9.48)$.
The effect is subtler in \cref{fig:bumpsDoG2ST}, but the response around path $(47.0\si{Hz}, 23.4\si{Hz})$ is of the same order of magnitude as that at path $(3.11\si{Hz}, 23.4\si{Hz})$.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Dissertation"
%%% End:
