% background
% - wavelets
% time/frequency interpretation
% shattering
% sonar

% Intro structures: mini outline of the dissertation. May push the sonar problem out too far.



Deep neural networks, and convolutional neural networks in particular, have proven quite effective at discerning hierarchical patterns in large datasets~\cite{lecunDeepLearning2015}.
Some examples include image classification~\cite{krizhevskyImageNetClassificationDeep2012}, face recognition~\cite{sunDeepConvolutionalNetwork2013}, and speech recognition~\cite{dahlImprovingDeepNeural2013}, among many others.
Clearly, something is going very right in the design of CNNs.
However, the principles that account for this success are still somewhat elusive~\cite{mallatUnderstandingDeepConvolutional2016}, as is the construction of systems that work well with few examples.

The scattering transform (ST) was created to remedy these issues.
In 2012, St\'ephane Mallat and Joan Bruna published both theoretical results~\cite{mallatGroupInvariantScattering2012} and numerical implementations~\cite{brunaClassificationScatteringOperators2011} tying together convolutional neural networks and wavelet theory.
They demonstrated that scattering transforms of wavelets and modulus nonlinearities, are translation invariant in the limit of infinite scale, and Lipschitz continuous under non-uniform translation, \ie $T_\tau(f)(x)=f\paren[\big]{x-\tau(x)}$ for $\tau$ with bounded gradient.
Numerically, they achieved state of the art on image and texture classification problems.

More recent work from Wiatowski and B\"olcskei have generalized the Lipschitz continuity result from wavelet transforms to frames, and more importantly, established that increasing the \textit{depth} of the network also leads to translation invariant features~\cite{wiatowskiMathematicalTheoryDeep2018}.
We detail this approach to the scattering transform in \Cref{sec:scat-chap}.
There have been a number of related papers, including a discrete version of Wiatowski's result \cite{wiatowskiDiscreteDeepFeature2016}, and a related method on graphs~\cite{merollaMillionSpikingneuronIntegrated2014}.
There have also been a number of papers using the scattering transform in such problems as fetal heart rate classification~\cite{chudacekScatteringTransformIntrapartum2014}, age estimation from face images~\cite{changLearningFrameworkAge2015}, and voice detection in the presence of transient noise~\cite{dovVoiceActivityDetection2014}.

Throughout this dissertation, we make extensive use of the \href{https://julialang.org/}{Julia programming language}\footnote{\href{https://julialang.org/}{https://julialang.org/}}~\cite{bezansonJuliaFreshApproach2017}, which is designed as both a readable high-level language like Python or MATLAB that maintains the performance results of a lower level compiled language like C or Fortran.
Using Julia and building off of the machine learning and automatic differentiation platform \href{https://fluxml.ai/}{Flux}\footnote{\href{https://fluxml.ai/}{https://fluxml.ai/}}~\cite{innesFluxElegantMachine2018}, we have constructed a differentiable scattering transform \href{https://github.com/dsweber2/ScatteringTransform.jl}{ScatteringTransform.jl} which allows for substituting various frames into the framework easily, along with supporting either multi-threaded or GPU based computing.
This dissertation fits into the tradition of reproducible research, and the code to produce this dissertation, along with all of the figures and results can be found at \href{https://gitlab.com/dsweber2/dissertation}{this gitlab.com repository}\footnote{\href{https://gitlab.com/dsweber2/dissertation}{https://gitlab.com/dsweber2/dissertation}}.

The filters used in the scattering transform have a rich set of interpretations, unlike a CNN.
However, there is still some ambiguity introduced by the presence of the nonlinearities, subsampling, and averaging.
Most previous work to specifically interpret what particular scattering transform output tells us about the input domain consists in determining the scattering transform's invertibility~\cite{cotterVisualizingImprovingScattering2017, anglesGenerativeNetworksInverse2018, waldspurgerPhaseRetrievalWavelet2017a}.
Working in a slightly different direction, in \Cref{cha:Interpretation} we use both a pseudo-inverse in \Cref{sec:synth-coord} and the theoretical properties of wavelets in \Cref{sec:theo-inter-scat-trans} to develop more of an understanding of what information the ST coefficients are capturing.

Building off of Wiatowski and B\"olcskei's result, which work for general frames and not just wavelets specifically, there have been extensions to Gabor systems, with specific proof of the decay properties~\cite{czajaAnalysisTimefrequencyScattering2019}.
There have also been some applied results building off of this work for music signals~\cite{haiderExtractionRhythmicalFeatures2019, bammerInvarianceStabilityGabor2017}.
In a similar vein, in \Cref{cha:shattering-transform} we examine the utility and properties of a scattering transform based off of the shearlet transform~\cite{kutyniokShearletsMultiscaleAnalysis2012}.
In \Cref{sec:theor-prop}, we demonstrate that it has the same theoretical sparsity guarantees as the original shearlet transform, and in \Cref{sec:experiments} we demonstrate that it performs at a comparable level to the 2D Morlet scattering transform implemented in Kymatio~\cite{andreuxKymatioScatteringTransforms2018} on MNIST and FashionMNIST~\cite{xiaoFashionMNISTNovelImage2017}.

The problem of interpreting sonar signals is a difficult one, particularly for smaller objects such as unexploded ordinance (UXOs)~\cite{karglAcousticResponseUnderwater2015}.
Sonar signals offer a rich dataset which has a clear physical generation process that can be simulated~\cite{bremerFastDirectSolver2012} to understand the role of various parameters, such as shape and material properties; we describe the simulation in \Cref{sec:PDE-sig-gen}.
This also allows us to show that the invariants of these classes fit within the framework of the scattering transform in \Cref{sec:geom}.
We test this empirically in \Cref{sec:class-res}, both on synthetic data and real data.
Finally, we use the pseudo-inversion methods developed in \Cref{sec:synth-coord} to interpret the ST coefficients used in a logistic regression classifier.
But before any of that, we review the core tools of harmonic and wavelet analysis.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Dissertation"
%%% End:
