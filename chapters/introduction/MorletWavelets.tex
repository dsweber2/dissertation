A \emph{continuous wavelet transform} is generated through convolutions with scaled and translated copies of a \emph{mother wavelet} $\mother*$.
Any function $\mother*\in\Lp[2][\mR]$ which satisfies
\begin{gather}
  \label{eq:motherDef}
  \integral{-\infty}{\infty}{\mother*(t)}{t} = 0\\
  C_{\mother*} = \integral{-\infty}{\infty}{\frac{\abs{\fhat{\mother*}(\omega)}^{2}}{\abs{\omega}}}{\omega} < \infty \label{eq:calderonCond}
\end{gather}
can be used, though there are plenty of motivations for particular choices~\cite{kaiserFriendlyGuideWavelets1994, daubechiesTenLecturesWavelets1992a, mallatWaveletTourSignal2009}.
The corresponding wavelet transform $\WT$ at scale $s\in \mR^{+}$ and time $t\in\mR$ is given by
\begin{align}
  \label{eq:WTdefinition}
  \WT &\define f\conv \mother*_{s}(t) = \paren[\big]{s}^{-p}\integral{-\infty}{\infty}{f(u)\mother*\paren[\Big]{\frac{u-t}{s}}}{u}\\
  \mother*_{s}(u)&\define \paren[\big]{s}^{-p}\mother*\paren[\big]{\nf{u}{s}},
\end{align}
with $p\in(1,\infty)$ and the convention that $p'=\nf{p}{p-1} = 1$ when $p=\infty$.
Note that the choice of $p$ determines which norm of $\mother*_{s}$ is preserved as the scale $s$ varies~\cite[Section 3.1]{kaiserFriendlyGuideWavelets1994}.\footnote{note that we have chosen the opposite convention for $p$ from \href{https://ucd4ids.github.io/ContinuousWavelets.jl/dev/}{ContinuousWavelets.jl} for ease of presentation and to correspond to~\cite{kaiserFriendlyGuideWavelets1994}.}
The norms preserved by the time and frequency domain are duals, since $\FT\brac[\big]{f(\nf ts)}(\omega) = \abs{s}\fhat{f}(s\omega)$, so we have that for any $s>0$,
\begin{align}\label{eq:normPreserve}
  \norm{\mother*_{s}}_{p} &= \norm{\mother*}_{p},\\
  \norm{\fhat{\mother*_{s}}}_{p'} &= \norm{\fhat{\mother*}}_{p'}.
\end{align}
We will generally choose $p=1$, which fixes the maximum value in the Fourier domain, as this prevents any one frequency from outweighing the others and makes inversion more stable.
One of the nice properties of the Fourier transform outlined above was its relative translation invariance.
While the continuous wavelet transform isn't relatively translation invariant, it is translation covariant, meaning that $\WT[\trans[a] f] = \trans[a] \WT$.
This is not as robust as relative invariance, but will serve as the basis for making an increasingly translation invariant transform with increasing depth in \Cref{sec:scat-chap}.



If $f$ is a real-valued signal, $\fhat{f}(-\omega) = \conj{\fhat{f}(\omega)}$, so in principle we only need either the positive or negative frequencies to fully represent or reconstruct $f$.
It is useful at this point to draw a distinction between two broad class of continuous wavelet transforms, depending on how the transform addresses this redundancy.

\emph{Real wavelets} $\mother*(t)\in \mR$ address this by having exactly the same redundancy as the target signal class.
Then $C_{\mother*}$ from \cref{eq:calderonCond} can be split into two equal halves coming from from the positive and negative halves of the integral, and then for any $f\in\Lp[2][\mR]$ the wavelet transform is invertible, and is given by
\begin{align}
  \label{eq:realInversion}
  f(t) = \int_{-\infty}^{\infty}\int_{0}^{\infty}\WT\mother*\paren[\Big]{\frac{u-t}{s}}~\frac{\dd s~\dd u}{C_{\mother*}s^{3-2p}},
\end{align}
where equality is in a weak sense~\cite[Theorem 3.1]{kaiserFriendlyGuideWavelets1994}.
Real wavelets are better adapted to characterizing discontinuous signals, as will be discussed more thoroughly in \Cref{sec:wavel-with-vanish}.

The second class is \emph{analytic wavelets}, which are complex valued but only non-zero for non-negative frequencies.
Then $\WT = \WT[f_{a}]$, where $f_{a}$ is the \emph{analytic part} of a signal $f$, defined by $\fhat{f_{a}}(\omega) = \characteristic{[0,\infty)}f(\omega)$, where $\characteristic{A}$ is the characteristic function so that for $\omega\in A$, $\characteristic{A}(\omega) = 1$ and $\characteristic{A}(\omega)=0$ otherwise.
The analytic part of a signal can be uniquely\footnote{There are ambiguities created when the amplitude passes through zero, see~\cite{cohenAmbiguityDefinitionAmplitude1999}.} decomposed into an amplitude or \emph{envelope} $\abs[\big]{f_{a}(t)}$ and \emph{instantaneous phase} $\arg\paren[\big]{f_{a}(t)}$.
This allows for a unique definition of \emph{instantaneous frequency} as the derivative of the instantaneous phase~\cite[Section 4.4.2]{mallatWaveletTourSignal2009}\cite{vakmanAnalyticSignalTeagerKaiser1996}.
Analytic wavelets facilitate investigating the instantaneous frequency and amplitude of a function~\cite{lillyAnalyticWaveletTransform2010}.
The reconstruction is quite similar to the real case:
\begin{align}
  \label{eq:analyticInversion}
  f(t) = \int_{-\infty}^{\infty}\int_{0}^{\infty}\Re\paren[\Big]{\WT\mother*\paren[\Big]{\frac{u-t}{s}}}~\frac{\dd s~\dd t}{C_{\mother*}s^{3-2p}},
\end{align}
from~\cite[Section 2.4.8]{daubechiesTenLecturesWavelets1992a}.
There are some further complications dealing with $f$ complex and variations on \cref{eq:calderonCond}; for more on those, see either~\cite[Section 2.4]{daubechiesTenLecturesWavelets1992a}\ or~\cite[Section 3.2]{kaiserFriendlyGuideWavelets1994}.
There is also a useful extension to the discrete case covered in~\cite[Chapter 6]{kaiserFriendlyGuideWavelets1994} or~\cite[Section 11.2]{christensenIntroductionFramesRiesz2016}.

When $s$ and $t$ are discretized, the resulting wavelet transform is an example of the more general class of frame transforms.
A \emph{frame} is a sequence of functions $\brak[\big]{f_{k}}_{k=1}^{\infty} \subset \Lp[2][\mR]$ which satisfy the frame bounds
\begin{align}
  \label{eq:frameBounds}
  A\norm{f}^{2} \leq \s{k=1}{\infty}\abs{\ip{f}{f_{k}}}^{2} \leq B \norm{f}^{2},
\end{align}
for some $A,B > 0$ and any function $f\in\Lp[2][\mR]$; we will reserve ``the'' frame bounds for the optimal such bounds~\cite[Chapter 6]{christensenIntroductionFramesRiesz2016}.
A frame can be thought of as a redundant basis which allows for multiple representations for a function.
This allows for additional features, such as robustness to noise~\cite[Section 5.9]{christensenIntroductionFramesRiesz2016}, increased sparsity of the representation (see \Cref{sec:dist-der-Lipschitz}), and some invariance to transformations by bounded linear operators~\cite[Section 5.3.1]{christensenIntroductionFramesRiesz2016}.
They also allow for a diversity of inversion methods beyond the canonical dual frames found in \cref{eq:analyticInversion}; as we don't use the inversions in this dissertation, we will not go into that theory further, but it can be found in~\cite{christensenIntroductionFramesRiesz2016}.

\subsection{Examples}
\label{sec:WaveletExamples}
\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{figures/introduction/mothers.pdf}
  \caption[lof?]{The various mother wavelets defined in \href{https://ucd4ids.github.io/ContinuousWavelets.jl/dev/}{ContinuousWavelets.jl}\protect\footnotemark} \label{fig:mothers}
\end{figure}
\footnotetext{\href{https://ucd4ids.github.io/ContinuousWavelets.jl/dev/}{https://ucd4ids.github.io/ContinuousWavelets.jl/dev/}}

In \Cref{fig:mothers} we have some example mother wavelets of more general wavelet families, as defined in the \href{https://ucd4ids.github.io/ContinuousWavelets.jl/dev/}{ContinuousWavelets.jl} Julia package.\footnotemark[\value{footnote}]
Only the first two families are (approximately) analytic, while the rest are real.
The first three are described succinctly by Torrence and Compo~\cite{torrencePracticalGuideWavelet1998}.
In the time domain, the \emph{Morlet wavelet family} is a Gaussian that is modulated by a complex exponential with frequency parameter $\omega_{0}$:
\begin{align}
  \label{eq:MorletWavelet}
  \mother*(t) \propto \e^{-\nf{t^{2}}{2}}\paren[\Big]{\e^{i\omega_{0} t} - \kappa} \xrightarrow{\FT} \fhat{\mother*}(\omega) \propto \e^{-\frac 12\paren{\omega-\omega_{0}}^{2}} - \kappa \e^{-\frac 12\paren{\omega}^{2}}
\end{align}
where $\kappa$ is chosen so that $\mother*$ satisfies the zero average condition \cref{eq:motherDef}.
In \Cref{fig:mothers}, $\omega_{0}=\pi$.
$\omega_{0}$ determines the trade-off between high spatial accuracy ($\omega_{0}$ small) and high frequency accuracy ($\omega_{0}$ large).
We have defined $\mother*(t)$ only up to a constant, since the constant will be chosen so that the entire wavelet family satisfies certain frame bounds (usually $B=1$); this convention carries through for the rest of the wavelets.
The Cauchy (aka Paul, or Klauder) wavelets are a classic example with a polynomial rate of decay $m$:
\begin{align}
  \label{eq:PaulDef}
  \mother*(t) = \im^{m}\paren[\Bigg]{\frac{1}{1-\im t}}^{m+1} \xrightarrow{\FT} \fhat{\mother*}(\omega) \propto H(\omega)\omega^{m}\e^{{-\omega}}
\end{align}
where $H(\omega) = \characteristic{[0,\infty)}$ is the Heaviside function.
Finally, the $m$th derivative of Gaussian (or DoG$m$) wavelets are simply that:
\begin{align}
  \label{eq:dogDef}
  \mother*(t) =-\paren[\bigg]{-\frac{\dd}{\dd t}}^{m} \e^{-\nf{t^{2}}{2}} \xrightarrow{\FT} \fhat{\mother*}(\omega) \propto \paren[\big]{\im \omega}^{m} \e^{-\nf{\omega^{2}}{2}}.
\end{align}
That they are derivatives of the Gaussian function will guarantee some nice properties for the decay rate for DoG wavelets, as will be discussed in \Cref{sec:dist-der-Lipschitz}.
Both the Paul wavelets and a complex version of the DoG wavelets can be thought of as special cases of the analytic \emph{Morse wavelets}, which we will not get into here, but are well described by Lilly and Olhede~\cite{olhedeGeneralizedMorseWavelets2002, lillyAnalyticWaveletTransform2010}.
The other seven wavelets are continuous versions of classic orthonormal real wavelets, in this case generated using \href{https://github.com/JuliaDSP/Wavelets.jl}{Wavelets.jl}.
There is no explicit formula for most of them (excluding the Haar wavelet), and they are derived by the Cascade Algorithm from the corresponding quadrature mirror filters~\cite[Section 6.5]{daubechiesTenLecturesWavelets1992a}.
The number in the titles of \Cref{fig:mothers} for these refer to the number $n$ of \emph{vanishing moments}, that is for $m \leq n$ we have that $\ip{t^{m}}{\mother*} = 0$, a property further discussed in \Cref{sec:dist-der-Lipschitz}.
For a more thorough explanation of each, see Daubechies's ``Ten Lectures''~\cite{daubechiesTenLecturesWavelets1992a}, specifically Section 6.4 for the Daubechies, Haar and Symlet wavelets,\footnote{The Haar wavelets are exactly the Daubechies 1 wavelets} Section 8.2 for Coiflets, Section 5.4 for Battle--Lemarie wavelets.
The Beylkin wavelets and Vaidyanathan wavelets can be found in~\cite{wickerhauserAdaptedWaveletAnalysis1994}.

\begin{figure}[h]
  \centering
  \begin{subfigure}{.40\linewidth}
    \includegraphics[width=\linewidth]{figures/introduction/heavisineScalogramDog2.pdf}
    \caption{Scalogram of the HeaviSine function using the DoG2 wavelets\\ }\label{fig:heavisineDog2}
  \end{subfigure}
  \begin{subfigure}{.40\linewidth}
    \includegraphics[width=\linewidth]{figures/introduction/heavisineScalogram.pdf}
    \caption{Scalogram of the HeaviSine function using the Morlet wavelets, $\omega_{0} = \pi$}\label{fig:heavisineMorl}
  \end{subfigure}
  \begin{subfigure}{.40\linewidth}
    \includegraphics[width=\linewidth]{figures/introduction/bumpsScalogramDog2.pdf}
    \caption{Scalogram of the Bumps function using the DoG2 wavelets\\ }\label{fig:bumpsDog2}
  \end{subfigure}
  \begin{subfigure}{.40\linewidth}
    \includegraphics[width=\linewidth]{figures/introduction/bumpsScalogram.pdf}
    \caption{Scalogram of the Bumps function using the Morlet wavelets, $\omega_{0} = \pi$}\label{fig:bumpsMorl}
  \end{subfigure}
  \caption{Various continuous wavelet transforms}\label{fig:cwtExs}
\end{figure}
In \Cref{fig:cwtExs} we have various \emph{scalograms}, which are defined by $\abs{\WT}^{2}$, where the color is on a logarithmic scale.
The $y$-axis, while labeled by increasing frequency, is uniform in the \emph{sampled} scales, or inverse frequency.
We will use this convention for scalograms throughout this dissertation.
In \Cref{fig:heavisineDog2} and \Cref{fig:heavisineMorl}, we have the wavelet transform of the same HeaviSine function in \Cref{fig:decayRates} with the second derivative of Gaussian (DoG2) and Morlet wavelets, respectively.
Both of these are much sparser than the Fourier transform of the same signal was, with the Morlet wavelet having a better concentration on the correct frequency of $8\si{Hz}$.
The discontinuity is visible in both, though very faint for the Morlet wavelet.
On the other hand the bumps function, which is a collection of peaks with different rates of decay, has clearer ``cones of influence'' and spatial localization using the DoG2 wavelets in \Cref{fig:bumpsDog2} than in \Cref{fig:bumpsMorl}.
\Cref{sec:wavelet-decay} will discuss the relationship between the Lipschitz constant and the decay rate of the coefficients.

\FloatBarrier
\subsection{Effective Quality factor calculation}\label{sec:EffQual}
As we move from the ideal continuous case to something we can actually implement in a discrete case, we need to sample the scale parameter $s$ in some manner.
Throughout this section, the \emph{wavelet index} $k\in\brak{1,\ldots,K}$ will denote the discrete set of wavelets used, with resulting scales $s_{1}, \ldots, s_{K}$.
As discussed in~\cite[Chapter 6]{kaiserFriendlyGuideWavelets1994}, the default is to use a uniform sampling of the form $s_{k} = \sigma^{k}$ for $k\in\mZ$; a common rephrasing of this is to consider octaves of the form $2^{\nf{k}{Q}}$ for some choice of \emph{quality factor} $Q$.
A reasonable default, especially for music signals, is to use $Q=8$~\cite{andenDeepScatteringSpectrum2014}.

\begin{wrapfigure}[11]{o}{.5\textwidth}
  \centering
  \includegraphics[width=.5\textwidth]{figures/plotOfLogCentralFrequencies.pdf}
  \caption{An example of the mean log-frequencies for some wavelets, where $\beta=4$ and $Q=8$}\label{fig:sketchWaves}
\end{wrapfigure}

Keeping a fixed number $Q$ of wavelets per octave leads to a denser coverage of the low frequencies than is necessary in many applications, such as audio or sonar processing.
Consequently, one needs to choose some method of reducing the number of such low frequency wavelets.
One common choice is to choose a particular scale of wavelet and then translate this in the frequency domain.
This leads to a collection of wavelets that are no longer a scaling of a single function.

Instead of simply having scales distributed log-linearly as $2^{\nf{k}{Q}}$, we distribute them according to $s_{k} = 2^{a(mk+k_0)^{\nf{1}{\beta}}}$, or according to a $\beta$th-root in the log frequency, as in \Cref{fig:sketchWaves}.
If $\beta$ is 1, then we have a linear relation between the index and the log-frequency, and $Q$ gives exactly the number of wavelets per octave throughout.
As $\beta$ increases, the wavelets skew more and more heavily to high frequencies.
To choose the parameters $a,m$ and $k_0$ for a given frequency skew $\beta$, quality factor $Q$, and number of octaves $\alpha$ covered by the averaging function (see \Cref{sec:aver-funct-mult}), there are a couple of criteria:
\begin{itemize}
  \item The first wavelet is scaled by $2^{\alpha}$, so the curve $a(mk+k_0)^{1/\beta}_{}$ goes through the point $(k,y)=(1,\alpha)$.
  \item The derivative $\frac{\dd y}{\dd k}$ at the last point is $\frac{1}{Q}$, so the ``instantaneous'' number of wavelets $k$ per octave $y$ is $Q$.
  Each type of wavelet has a maximum scaling $2^{N_{Octaves}}$, so the final point $N_w$ satisfies both $y(N_w) = N_{Octaves}$ and $y'(N_w)=\nf{1}{Q}$.
  \item Finally, the spacing $m$ is chosen so that there are exactly $Q$ wavelets in the last octave.
\end{itemize}

As a simple example of the resulting wavelets, see \Cref{fig:Qbeta}; if $N=2^9=512$, $Q=8$, $a=2$, and $\beta=1$, we have 45 wavelets, whereas if $\beta=4$, we have just $21$.

\begin{figure}
  \centering
  \includegraphics[width=.6\textwidth]{figures/QDecreasingBeta0vs1.pdf}
  \caption{A comparison between the resulting Fourier domain wavelets for different values of $\beta$. Note that there are $25$ filters with mean frequency below 50 in the first plot with $\beta = 1$, rather than $2$ in the second where $\beta=4$}\label{fig:Qbeta}
\end{figure}

\subsection{The Averaging Function and Multiresolution Analysis}
\label{sec:aver-funct-mult}

On their own, we need an infinite collection of wavelets to cover the coarse scale as $s \rightarrow \infty$ or $\omega \rightarrow 0$, and the mean (zero frequency) is completely uncovered even in the limit (this is part of the downside of the weak nature of the convergence).
A way of remedying this is to introduce the \emph{father}, or \emph{averaging}, or \emph{scaling wavelet}~\cite[Section 4.3.1]{mallatWaveletTourSignal2009}.
This is specifically designed to cover the low frequency information missed by the mother $\mother*$ at scale $1$:
\begin{align}
  \label{eq:fatherdef}
  \abs{\fhat{\father*}(\omega)}^{2} \define \integral{1}{\infty}{\frac{\abs{\fhat{\mother*}(s\omega)}^{2}}{s^{3-2p}}}{s},
\end{align}
where we allowed to choose the phase with impunity (adapted from~\cite[Section 4.3.1]{mallatWaveletTourSignal2009}).
This allows us to cap off $s$ in either of the inversion formulas \cref{eq:realInversion} or \cref{eq:analyticInversion} to the interval $[0,s_{0}]$.

Through careful choices for the father $\father*$ and $\mother*$ and looking at the subspaces generated by their translates, one can create nested families of orthogonal bases, eventually spanning $\Lp[2][\mR]$, as discussed in Mallat's \emph{Wavelet Tour of Signal Processing}\cite[Chapter 7]{mallatWaveletTourSignal2009}, Daubechies's \emph{Ten Lectures on Wavelets}~\cite[Chapter 5]{daubechiesTenLecturesWavelets1992a}, and Kaiser's \emph{Friendly Guide to Wavelets}~\cite[Chapter 7]{kaiserFriendlyGuideWavelets1994}.
These can be used to create a fast wavelet transform akin to the fast Fourier transform.
Unfortunately, this beautiful theory is for the most part too far afield to spend much time exploring further.
The principal problem with the orthogonal wavelet transforms for our purpose is that they are not even translation covariant.
For finite signals, shifting the input by a single index results in wildly different wavelet coefficients~\cite{saitoMultiresolutionRepresentationsUsing1993}, meaning that they cannot be used as coefficients for signal classification where translation invariance must be guaranteed.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Dissertation"
%%% End:
