One way to extend the Fourier transform to functions $f$ not in $\Lp[1][\mR]$ is to treat $f$ as a tempered distribution, so that $f$ itself is an operator on the nicer class of Schwartz functions $\calS$~\cite{follandFourierAnalysisIts1992}.
A Schwartz function $\testfun\in\calS$ is an infinitely smooth function $\testfun\in\cont[\infty][\mR]$, whose derivatives decay more quickly than any monomial, so $\sup_{x\in\mR}\abs{x^{\alpha}\testfun^{(\beta)}(x)} <\infty$.
Schwartz functions are those functions nice enough so that the Fourier transform and its inverse maps the space to itself, so that $\testfun\in\calS \Leftrightarrow \fhat{\testfun}\in\calS$~\cite{follandFourierAnalysisIts1992}.
This means that we can define a whole host of operations on our distribution $f$ that may not be well defined on the corresponding function by moving that operation from $f$ to $\testfun$.
For the Fourier transform, a function $g\in\Lp[1][\mR]$, satisfies $\ip{\fhat{g}}{\testfun} = \ip{g}{\fhat{\testfun}}$, so for the case of the tempered distribution $f$ with corresponding operator $F[\testfun] = \ip{f}{\testfun}$, we define the Fourier transform of $f$ so that $\fhat{F}[\testfun] = \ip{\fhat{f}}{\testfun} \define \ip{f}{\fhat{\testfun}}$.
For this to be well behaved, we will need that $F$ is of finite order, that is there's some $N\in\mN$ and $C>0$ so that
\begin{align}
  \label{eq:finiteOrder}
  \abs[\big]{\ip{f}{\testfun}} \leq C\s*{\alpha + \beta \leq N}{}\sup_{x}\abs[\Big]{x^{\alpha}\testfun^{(\beta)}(x)},
\end{align}
from~\cite{follandFourierAnalysisIts1992}.
We can use the same idea to take derivatives of general distributions using integration by parts, so the derivative of a distribution $f$ is defined by the relation $\ip{f'}{\testfun}\define - \ip{f}{\testfun'}$.\footnote{This is well defined for a larger class of distributions than just tempered, and only needs the test functions to be smooth instead of Schwartz functions.} For example, the distributional derivative of the step function $\characteristic{(0, \infty)}(x)$ is the delta ``function'', defined by $\int\delta(t)\testfun(t)~\dd t = \testfun(0)$.
In fact, any operator $G$ which has an adjoint $G^{*}$, meaning that $\ip{Gf}{g} = \ip{f}{G^{*}g}$, can effectively be defined to act on a distribution $f$ by applying its adjoint to some class of test functions preserved by $G$.
A couple of significantly simpler examples than the above that are relevant for wavelets are translation $\trans$, which has an adjoint $\trans[-t]$; scaling $S_{s}[f](t)=f(\nf ts)$, which has an adjoint $S_{\nf 1s}$; convolution with a function $\conv g$, which has an adjoint that is also convolution with $\conj{g(-t)}$; and finally multiplication by a smooth function $g$, which is self-adjoint.
\subsection{Wavelets with Vanishing Moments}
\label{sec:wavel-with-vanish}

There is a whole class of wavelets which can be characterized as taking the distributional derivative of the input $f$ they are transforming, that is
\begin{align}
  \label{eq:waveDistDer}
  \WT = \paren{f\conv\mother*_{s}}(t) = \paren{s\partial}^{n}\paren{ f \conv \theta_{s}}(t) = \big\langle\partial^{n}f, \conj{\theta_{s}}\big\rangle.
\end{align}
To make this more precise, we need a couple of preliminary definitions.
The first is fast decay: a function $g$ is said to have \emph{fast decay} if for every $m$, there is some $C_{m}$ so that
\begin{align}
  \label{eq:fastDecay}
  \abs{g(t)} \leq \frac{C_{m}}{1+ \abs{t}^{m}}.
\end{align}
In words, $g$ decays faster at infinity than any rational function.
The second is having $n$ vanishing moments: a mother wavelet $\mother*$ is said to have $n$ \emph{vanishing moments} if
\begin{align}
  \label{eq:vanishingDefinition}
  \integral{-\infty}{\infty}{t^{k}\mother*(t)}{t} = 0
\end{align}
for every $k\in\brak{1, \ldots, n}$. It turns out that for a fast decaying wavelet, having exactly $n$ vanishing moments is equivalent to having a function $\theta$ such that
\begin{align}
  \label{eq:vanishingMoments}
  \mother*(t) = \paren[\bigg]{-\frac{\dd}{\dd t}}^{n} \theta(t),
\end{align}
where $\theta$ is a function with nonzero mean, $\int \theta~\dd t \neq 0$, and corresponds to the father wavelet described in \Cref{sec:aver-funct-mult}~\cite[Theorem 6.2]{mallatWaveletTourSignal2009}.
Probably the most frequently used example in this family is the derivative of Gaussian (DoG) wavelets, where $\theta$ is a Gaussian.
DoG2, the second derivative, is particularly common, as any resulting extreme values of the scalogram characterize regions of high curvature, while zeros are indicators of edges.
For the entire family, the curves defined by $\partial_{t}\paren{\mother*\conv f} = 0$ are guaranteed to be continuous as functions of $s$ and vanish only as $s$ increases~\cite{yuilleScalingTheoremsZero1986}.

\subsection{Wavelet Decay}
\label{sec:wavelet-decay}
In \Cref{sec:Four-Trans} we have described the relation between the differentiability of $f$ and the decay of the Fourier coefficients.
For non-smooth functions it is more appropriate to discuss the Lipschitz exponent $\alpha(f,\nu)$ at a point $\nu$:
\begin{align}
  \label{eq:lipschitz}
  \abs{f(t) - f(\nu)} \leq C_{\alpha,f,\nu} \abs{t-\nu}^{\alpha}
\end{align}
for $t$ in a sufficiently small neighborhood of $\nu$ and some constant $C_{\alpha,f,\nu}$.
If $f$ is $n$ times differentiable at $\nu$, then $\alpha< n+1$, and the minimal constant $C_{\alpha,f,\nu}$ is exactly the $\alpha$th derivative if $\alpha$ an integer.
As might be expected from the properties of the derivative discussed in \Cref{sec:Four-Trans}, if the Fourier transform of a function $f$ satisfies
\begin{align}
  \label{eq:FTLipschitz}
  \integral{-\infty}{\infty}{\abs[\Big]{\fhat{f}(\omega)}\paren[\Big]{1+\abs{\omega}^{\alpha}}}{{\omega}} < \infty
\end{align}
for some $\alpha>0$, then $f$ is globally Lipschitz $\alpha$.
However, since it's global, $\alpha = \min\limits_{\nu\in\mR} \alpha(f,\nu)$, so there may only be a few points where $\alpha$ is small, while the rest of the signal is well behaved and $\alpha(f,\nu)$ is quite large.
As promised, the wavelet transform localizes $\alpha(f,\nu)$ so that the spread of any irregular points is confined to a cone of influence.
Suppose that the mother wavelet has $n$ vanishing moments, and that $f$ is Lipschitz $\alpha(f,\nu)\leq n$ at $\nu$.
Then there is some $A>0$ so that
\begin{align}
  \label{eq:growthBound}
  \abs[\big]{\WT} \leq A s^{\alpha+\nf 12}\paren[\bigg]{1 + \abs[\Big]{\frac{t-\nu}{s}}^{\alpha'}}
\end{align}
with $\alpha=\alpha'$ and conversely, if $f$ satisfies this bound for some $\alpha'$ \emph{strictly less than} $\alpha$, then it is Lipschitz $\alpha$ at $\nu$.
This was originally proved in~\cite{jaffardPointwiseSmoothnessTwomicrolocalization1991}, though it can also be found as Theorem 10.1 in~\cite{jaffardWaveletsToolsScience2001} or Theorem 6.4 in~\cite{mallatWaveletTourSignal2009}.
When combined with a father wavelet, which caps the scale to $s\in(0, s_{0}]$, this gives a characterization of the decay rate of the wavelet coefficients, since $\abs{\WT[f][s][\nu]} \leq As^{\alpha+\nf 12}$.

The set $\setbuild[\big]{(t,s)}{\abs[\big]{\frac{t-\nu}{s}}\leq C}$ is known as the \emph{cone of influence} of the point $\nu$, and the magnitude of $\abs[\big]{\WT}$ within this cone is closely related to the regularity of $\nu$, though the fact that $\alpha$ and $\alpha'$ are not equal in the converse above leaves room for exceptions.
These cones are clearly visible in \Cref{fig:cwtExs}.

Having introduced our sparse and translation covariant transform, it is time to see if we can combine these features in a nonlinear feature extractor.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Dissertation"
%%% End:
