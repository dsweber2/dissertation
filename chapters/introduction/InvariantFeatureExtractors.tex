The Fourier transform is so well known that it hardly needs introduction.
But for the sake of emphasis, we can think of the Fourier transform $\FT$ as a linear operator that takes a function $f\in\Lp[1][\mR]$ and returns another function $\fhat{f}\in\Lp[\infty][\mR]$ through the transform
\begin{align}
  \label{eq:FourierTransform}
  \fhat{f}(\omega) \define \FT(f)(\omega) \define \integral{-\infty}{\infty}{f(x)\e^{-\im\omega x}}{x}.
\end{align}
The resulting complex function $\fhat{f}(\omega) = a(\omega)\e^{\im\phi(\omega)}$, gives the amplitude $a$ and phase $\phi$ of oscillations with frequency $\omega$.
When $\fhat{f}$ is also in $\Lp[1][\mR]$, then inverse is well defined and almost identical, up to a sign and a constant:
\begin{align}
  \label{eq:inverseFourierTransform}
   f(x) = \FT^{-1}\paren[\big]{\fhat{f}}(\omega) = \frac{1}{2\pi}\integral{-\infty}{\infty}{f(\omega)\e^{\im\omega x}}{\omega}.
\end{align}
The Fourier transform simplifies some otherwise complicated operations: the convolution of two functions $f$ and $g$ becomes pointwise multiplication $\fhat{f\conv g} = \fhat{f}\cdot\fhat{g}$, while differentiation becomes multiplication by a monomial $\fhat{\frac{\dd}{\dd x}f} = \im\omega \fhat{f}$, among others, see \cite{follandFourierAnalysisIts1992, steinFourierAnalysisIntroduction2003}.
%There are a number of books devoted to just this transform and its consequences which we will not get into~\cite{follandFourierAnalysisIts1992}.

\begin{wrapfigure}[7]{o}{.4\textwidth}
  \centering
  \includegraphics[width=.4\textwidth]{figures/introduction/CoefficientDecay.pdf}
  \caption{Comparing decay rates}\label{fig:decayRates}
\end{wrapfigure}
This last property ties together the smoothness of $f$ and the decay rate of $\fhat{f}$.
Specifically, if $f$ is up to $k$ times differentiable, then $\omega^{k}\fhat{f}\in\Lp[1][\mR]$ so $\fhat{f}$ must decay at least at a rate of $O\paren[\big]{\abs{\omega}^{-k}}$.
This means that smooth signals are quite sparse in frequency; see for example \Cref{fig:decayRates}, where on a log scale the smooth signal rapidly decays, while an otherwise smooth function with 2 discontinuities decays quite slowly.
A similar result holds for the Fourier series~\cite{follandFourierAnalysisIts1992}.

It is also the canonical example in the family linear feature extractors that are \emph{relatively invariant} to translation, meaning that for the translation operator $\trans[t][f](x)\define f(x-t)$, the price for no longer applying the translation operator is multiplication by a function of translation distance $t$; to be precise, $\FT\paren[\big]{\trans[t](f)}(\omega) = \e^{\im t\cdot\omega}\fhat{f}(\omega)$\cite{amariInvariantStructuresSignal1968, otsuInvariantTheoryLinear1973}.
Any linear feature extractor that is relatively translation invariant can be considered as a linear sum over various frequencies.
The same work by Amari and Otsu demonstrates that the only absolutely invariant \emph{linear} transforms are the moments, which is a very restrictive class.
This motivates our use of the absolute value of the Fourier transform (or AVFT) throughout this dissertation as the simplest example of a nonlinear (relatively) translation invariant transform.

\Cref{fig:decayRates} demonstrates one of the potential pitfalls of using just the Fourier transform; away from the two points of discontinuity, the HeaviSine is a single low frequency sinusoid, yet most of the Fourier domain is plagued by noise at all frequencies.
The issue with the Fourier transform alone is that it only captures global frequency behavior, which is easily derailed by local aberrations.
Capturing both local discontinuities and behavior at different scales is a reason for using wavelet transforms.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Dissertation"
%%% End:
