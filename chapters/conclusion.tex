Throughout this dissertation we have focused on providing interpretations of scattering transform coefficients.
This is necessarily a somewhat subjective process.

As one might hope, the meaning of the scattering transform coefficients is quite dependent on the kind of frame used in their construction.
There is also great variability in the meaning of the different layers, so that very different features can be characterized all within the same transform.
For either kind of wavelet, the zeroth layer coefficients are simply the local average value, while the first layer coefficients correspond directly to averages of the wavelet magnitudes.
The second layer begins to get novel behavior.
For the analytic wavelets, in the case of paths with increasing frequency, the first layer frequency determines the signal instantaneous frequency, while the second layer frequency determines the envelope.
The exact shape of the envelope depends on the wavelet used.
The ST coefficients with real-valued wavelets are somewhat more complex, with the second layer coefficients representing a mixture of several orders of distributional derivatives, all evaluated at the level sets of the first layer derivative (in its full detail, this is \cref{eq:specificAbsoluteValueK}).
In addition to the pseudo-inverses of the particular ST coefficients and the distributional derivative formulation, the gradients for the second layer coefficients in \Cref{sec:gradient-based} increase a given coefficient in the second layer by inducing a signal that locally maximizes the response at that frequency, modulated by the second layer wavelet.
This is similar to the pseudo-inverse of the coordinate for the analytic case, which is somewhat surprising, given that gradient-based methods weren't well suited to actually solving the pseudo-inversion since the sign change induced by the absolute value meant the gradient frequently changes abruptly.

Both our experiments and theoretical work in \Cref{cha:shattering-transform} begin to shed some light on the relevance of the non-linearity chosen for the sparsity of the resulting scattering transform.
Nonlinearities which preserve the decay rate of coefficients as per \Cref{thm:scatSparse} were significantly better at classifying than those which didn't.

The features revealed in the previous chapters allow us to build classifiers for sonar data that represent meaningful features of the input domain.
Using geometric arguments and the synthetic dataset, we have demonstrated that material detection is a considerably simpler problem than shape detection, and that the scattering transform is capable of solving both problems.
For the case of classifying object with varying internal speed of sound, the pseudo-inversion of the depth two classifier emphasizes the difference in frequency as well as the gap between the first and second peaks.
For the case of classifying objects with varying shape, the increased complexity of the sharkfin geometry relative to the triangle leads to higher levels of variability in the tail of the response.

In some respects, the work in this dissertation is just the beginning of an examination into the possible uses of the scattering transform in understanding sonar data.
The dependence on angle, distance, and subtler geometric features are only briefly touched on, and the real signal objects could be de-aggregated into separate classes and the differences between them better characterized.
Further, the third layer and deeper ST coefficients have not been thoroughly examined; if the results for the second layer are any indication, the third layer would nest the second layer signals in another layer of oscillations.



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../Dissertation"
%%% End:
