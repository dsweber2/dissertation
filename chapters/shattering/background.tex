One of the advantage of wavelets in one dimension is that they provide sparse representations of functions which are continuous except at a finite set of discontinuities.
Extending this idea to two and higher dimensions requires some care, since in addition to point discontinuities there are curve discontinuities.
Shearlets are a family of frame transforms designed to efficiently capture both point and curve singularities~\cite{kutyniokShearletsMultiscaleAnalysis2012}.
In this chapter, we implement a generalized scattering transform using the shearlet transform, called by the portmanteau \emph{shattering transform}.

In \Cref{sec:shearlets} we detail the shearlet transform and its sparsity guarantees.
In \Cref{sec:theor-prop} we prove that the shattering transform maintains the sparsity guarantees of shearlets and satisfies the prerequisites for the results of the generalized scattering outlined in \Cref{sec:WaveTheory}~\cite{wiatowskiMathematicalTheoryDeep2018}.
Finally, in \Cref{sec:experiments} we compare the shattering transform using various nonlinearities to the 2D Morlet wavelet transform as implemented in \href{https://www.kymat.io/}{Kymatio}\footnote{https://www.kymat.io/} on the MNIST and Fashion MNIST datasets\cite{andreuxKymatioScatteringTransforms2018, xiaoFashionMNISTNovelImage2017}.
For the shearlet implementation we use \href{https://arsenal9971.github.io/Shearlab.jl/}{Shearlab.jl}\footnote{https://arsenal9971.github.io/Shearlab.jl/}, written by H\'ector Loarca as a Julia implementation of Shearlab3D~\cite{kutyniokShearLab3DFaithful2014}.



\section{Shearlets}
\label{sec:shearlets}

Shearlets generate optimally sparse linear representations of $f\in\Lp[][\mR^{2}]$.
The Shearlet frame is formed by applying shearing matrices $S_{k}$ with $k\in\mZ$, and anisotropic scaling matrices $A_{j,\alpha}$ and $\td A_{j,\alpha}$ to a generator $\psi$, which needs to have some anisotropic structure. These matrices are
\begin{gather}
  S_{k} \define
  \begin{pmatrix}
    1 & ck\\
    0 & 1
  \end{pmatrix},~~
  A_j \define
  \begin{pmatrix}
    2^j & 0\\ 0 &
	2^{\nf {j\alpha}2}
  \end{pmatrix},~~ \textrm{ and }
  \td A_j \define
  \begin{pmatrix}
    2^{\nf{j\alpha}2} & 0\\ 0 &
	2^{j}
  \end{pmatrix}.
\end{gather}
Shearlab does so using a non-separable generator:
\begin{align}
	\fhat{\psi^{(1)}}(\xi_1,\xi_2) \define
P\paren[\bigg]{\tfrac{\xi_1}2,\xi_2} \fhat{\psi_1\cross\phi_1}(\xi_1,\xi_2)
\end{align}
% TODO: check if the directional selectivity of this is along $\xi_1$
where $P$ is a 2D directional filter\footnote{Specifically the third order polynomial defined in~\cite[Table II]{dacunhaNonsubsampledContourletTransform2006}, as discussed in~\cite{kutyniokShearLab3DFaithful2014}.}, $\phi_1$ is a 1D scaling (or averaging) function, $\psi_1$ is a 1D wavelet function, and $\cross$ is the tensor product, so $\psi_1\cross\phi_1(x_{1},x_{2}) = \psi_{1}(x_{1})\phi_{1}(x_{2})$.
This function will have directional selectivity along $\xi_1$.
To capture frequency information concentrated along $\xi_2$ with finitely many elements, it is necessary to introduce a second ``cone'', generated using $\fhat{\psi^{(2)}}(\xi_1,\xi_2) = \fhat{\psi^{(1)}}(\xi_2,\xi_1)$, which swaps the roles of $\xi_1$ and $\xi_2$.

If we choose $\psi_{1}$ and $\phi_{1}$ appropriately to define $\psi =\psi_{1} \cross \phi_{1}$, there is a corresponding averaging function $\phi$ and the resulting shearlet system and discrete shearlet transform $\mathcal{SH}$ are
\begin{gather}
  \psi^{1}_{j,k}(\vect{l},\vect{x}) \define 2^{\nf{(\alpha+2)j}{4}}\psi\paren[\big]{S_k A_j \vect{x}-[l_1c_1~ l_2c_2]\transp}\\
	\psi^{2}_{j,k}(\vect{l},\vect{x}) \define 2^{\nf{(\alpha+2)j}{4}}\psi\paren[\big]{S_k\transp \td A_j [x_{2}~ x_{1}]\transp-[l_1c_2~ l_2c_1]\transp}\\
  \phi(\vect{l},\vect{x}) \define \phi\paren[\big]{\vect{x}-[l_{1}c_{1}~ l_{2}c_{2}]\transp}\\
  \abs{k}<\ceil[\Big]{2^{\nf j2}}, \phantom{a}[l_1~ l_2]\in\mZ^2, \phantom{a}j\geq 0\\
  \mathcal{SH}f\paren[\big]{\vect{l}^{0}, i, \vect{l}, j, k} \define \brak[\Bigg]{\ipll{f(\cdot)}{\phi\paren[\big]{\vect{l}^{0},\cdot}}, \ipll{f(\cdot)}{\psi^{(i)}_{j,k}\paren[\big]{\vect{l},\cdot}}}
\end{gather}
where $i$ is the cone, $j$ is the scale, $k$ is the shearing, $\vect{l}^{0}$ is the translation of the averaging function, $\vect{l}$ is the translation of the wavelet, and $\vect{c}$ is the sampling grid size.
See the Shearlet textbook edited by Kutyniok and Labate~\cite{kutyniokShearletsMultiscaleAnalysis2012} or the ShearLab3D paper~\cite{kutyniokShearLab3DFaithful2014} for further details.

\subsection{Optimal Coefficient Decay rate}\label{sec:decay}

One of the nice properties of the shearlet transform is the coefficients are guaranteed to decay at (almost) the optimal rate for linear transforms of cartoon-like functions~\cite[Theorem 5.5]{kutyniokShearletsMultiscaleAnalysis2012}.
By cartoon-like functions, we mean functions that are mostly smooth, other than jump discontinuities along smooth curves.
More formally, the set of cartoon-like functions are the subset of $\Lp[2][\mR]$ such that $f=f_0+f_1\chi_{B}$ where $f_0,f_1\in\calC^2(\mR^2)$, and $\partial B$ is defined by some piecewise $\calC^2$ curve $\Gamma$ with bounded curvature.

% TODO: do a thorough reading of the Donoho paper
By optimally sparse, we mean that shearlets can represent any cartoon-like function using (up to a log factor) as few coefficients as possible.
For any linear system $G = \brak[\big]{g_i}_{i=0}^{\infty}$ which spans the set of cartoon-like functions, we can write $f=\s{i=0}{\infty}c_i g_i$.
For this sum to converge, $c_{i}$ must decay to zero; the rate at which these coefficients decay to zero is not guaranteed however.
Donoho showed in 2001 that for any particular linear system\footnotemark, there is some cartoon-like function such that $(c_0,c_1,\ldots)\in\lp*[p]$ if and only if $p>\frac{2}{3}$~\cite{donohoSparseComponentsImages2001}.
\footnotetext{To avoid pathological cases, we need the restriction that $G$ can only be reordered up to a distance given by a fixed polynomial. Otherwise such pathological examples as countable dense subsets of $\Lp*[2]$ are allowed, which converge faster but are computationally useless.}
This means that the fastest that the coefficients $c_{i}$ for a linear representation system can decay is $O\paren[\big]{n^{-\nf 32}}$.
There is no known linear system which achieves this bound exactly--- however, shearlets, like curvelets, come within a log term of doing so~\cite[Chaper 5]{kutyniokShearletsMultiscaleAnalysis2012}.
Denoting the shearlet coefficients of $f$ reordered in decreasing order as $c_n^*$, we have that
\begin{equation}
	\abs[\big]{c_n^*} = O\paren[\Big]{n^{-\nf 32}\log(n)^{\nf 32}}.
\end{equation}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Dissertation"
%%% End:
