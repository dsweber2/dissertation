The shattering transform is nonlinear, so the decay rate limits of \Cref{sec:decay} don't apply; however, because each layer is composed of shearlet transforms, as long as the nonlinearity and subsampling don't interfere, we will have that, the shattering coefficients also decay as $O\paren[\big]{n^{-\nf 32}\log(n)^{\nf 32}}$.

In considering this, we need to account for the switch between continuous input $f$ and discrete output $c_{n}$ for the shearlet system $\Frame$.
We arrive at a somewhat awkward construction: for the purposes of output, we discretize the internal layer operator $\layer$ from \cref{eq:layer-operator} before averaging, while for passing to the next layer, we consider it continuous in space but not shearing or scale indices.
In both cases the nonlinearity $\nonlin*$ introduces some additional constraints.
As a continuous operator, $\layer$ produces cartoon-like functions from cartoon-like functions, while as a discrete operator, we need to show that neither $\nonlin*$ nor averaging destroys the decay rate.

First, we deal with the nonlinearity for the discrete $\layer$.
To maintain (or improve) the rate of decay, we need that for any two points $s$ and $t$, for $\nonlin*$ to bring whichever one is closer to zero even closer.
A motivating example of what we want to avoid is $\nonlin*(t)=t^{\nf 12}$; applied to the sequence $\tfrac{1}{n^{2}}$, this nonlinearity ends up giving a decay rate of $\tfrac{1}{n}$.

\clearpage
\begin{lemma}\label{lem:nonlinConds}
  If $\nonlin*$ satisfies
  \begin{itemize}
    \item $\abs{\nonlin*(t)}$ is convex,
    \item if $\abs{t} \leq \abs{s}$, with $t$ and $s$ having the same sign, then $\abs{\nonlin*(t)} \leq \abs{\nonlin*(s)}$,\footnote{this isn't simply increasing, unfortunately. It is increasing on positive numbers $t$ if $\nonlin*(t)$ is positive, or decreasing if it's negative, while for $t<0$ it is decreasing if $\nonlin*(t)$ is positive, or increasing if it's negative.}
    \item $\nonlin*(0)=0$
  \end{itemize}
  and the sequence $c_{n}$ decreases at rate $\abs{c_{n}} \lesssim f(n)$, then the sequence $\abs{\nonlin*(c_{n})}$ also decreases at least as fast: $\abs{\nonlin*(c_{n})} \lesssim f(n)$.
  Here $a\lesssim b$ means $a = O(b)$.
\end{lemma}
\begin{proof}
  W.l.o.g., consider those $c_{n}$ where we have exactly $\abs{c_{n}} \leq f(n)$.
  Any multiplicative constants needed to make this exact can be absorbed into $f$, while the finitely many possible exceptions can be ignored.
  Define $C \define \tfrac{\nonlin*\paren[\big]{f(0)}}{f(0)}$, and choose $t \define \tfrac{f(n)}{f(0)}$.
  Then $t\in [0,1]$, since $f$ is a decreasing function, and the expression $(1-t)0 + t f(0)$ simplifies to $f(n)$. We can use the convexity of $\abs{\nonlin*(t)}$ to say that
  \begin{align}
    \abs[\Big]{\nonlin*\paren[\big]{f(n)}} &= \abs[\bigg]{\nonlin*\paren[\Big]{(1-t)0 + t f(0)}} \leq (1-t)\abs[\big]{\nonlin*(0)} + t\abs[\Big]{\nonlin*\paren[\big]{f(0)}}\\
    &\leq 0 + \frac{f(n)}{f(0)}\abs[\Big]{\nonlin*\paren[\big]{f(0)}} = C f(n) \label{eq:vexity}
  \end{align}
  where we have also used the third assumption that $\nonlin*(0)=0$.
  Then
 \begin{align}
   \label{eq:nonlinDecay}
   \abs{\nonlin*(c_{n})} \leq \abs[\big]{\nonlin*\paren{f(n)}} \leq Cf(n).
 \end{align}
 The first step we have because of the second assumption, that $\abs{c_{n}} \leq \abs{Cf(n)}$, and the second step is just \cref{eq:vexity}.
\end{proof}
Some examples of functions that work are $x^{3}$, $\abs{x}$, or $\ReLU$; examples that will end up being relevant later that aren't allowed are $\tanh$ or other sigmoidal functions, as they all have concave portions (and must to reach a bounded value from below).
It is worth noting that such sigmoidal functions were common in the neural network literature, but have since fallen out of common use.

Next, we deal with the somewhat more awkward case of the averaging.
We show that so long as $\phi$ has compact support, every large coefficient can only change the magnitude of coefficients in its immediate neighborhood, which incurs at most a constant penalty $2L$.

\begin{lemma}[Decay after averaging]\label{lem:aveDec}
  Suppose we have an array $\vect{g}$ defined on $\mZ^{d}$, and a rearrangement of the entries of $\vect{g}$ given by $\pi:\mN \rightarrow \mZ^{d}$ which orders $\vect{g}\brac[\big]{\pi(n)}$ as a monotonically decreasing sequence, and further that they decay at a rate $\abs[\Big]{\vect{g}\brac[\big]{\pi(n)}} \lesssim n^{(-\nf 32)}(\log n)^{\nf 32}$.
  Suppose we have some averaging array $\vect{\phi}$ which is supported on some finite box $[-L, L]^{d}$.
  Then $\vect{\phi} \conv \vect{g}$ has a different rearrangement $\pi^{*}$ so that it too also decays as $\vect{\phi} \conv \vect{g}\brac[\big]{\pi^{*}(n)} \lesssim n^{-\nf 32}\paren[\big]{\log n}^{\nf 32}$.
\end{lemma}
\begin{proof}
  First, some simplifying assumptions and definitions.
  W.l.o.g., assume that $\norm{\vect{\phi}}_{1}=1$; if it isn't, simply consider the array $\frac{\vect{g}}{\norm{\vect{\phi}}_{1}}$, which has the same coefficient order.
  Let the \emph{neighborhood} $N(\vect{i})$ of $\vect{i} \in \mZ^{d}$ be the set of tuples $N\paren{\vect{i}} = \setbuild[\big]{\vect{k}}{\norm[\big]{\vect{i}-\vect{k}}_{\infty}\leq L}$.

  The core idea for constructing $\pi^{*}$ from $\pi$ is to enumerate every point in the neighborhood of $\pi(1)$, then every point in the neighborhood of $\pi(2)$ that wasn't already listed in the neighborhood of $\pi(1)$, and so on.
  This contains the interference from any coefficients increased in the neighborhood of $\pi(1)$ early in the list.
  To that end, let $n_{k}$ be the number of points in the union $\unioni{j=1}{k}N\paren[\big]{\pi(j)}$ (the number of points in the neighborhoods of all the $k$ largest points).
  Then $\pi^{*}$ from $1$ to $n_{2}$ is $N\paren[\big]{\pi(1)}$, while $\pi^{*}$ from $n_{2}+1$ to $n_{3}$ is $N\paren[\big]{\pi(2)}\setminus N\paren[\big]{\pi(1)}$.
  In general, $\pi^{*}$ from $n_{k} +1$ to $n_{k+1}$ enumerates the set $N\paren[\big]{\pi(k)}\setminus\unioni{j=1}{k-1}N\paren[\big]{\pi(j)}$ in order from largest to smallest element.
  By this construction, the number of points added $n_{k+1} - n_{k}$ is at most $(2L)^{d}$, since each neighborhood $N\paren[\big]{\pi(k)}$ has this many points, and we may remove some.
  Note that in this construction, $\pi(k)$ may end up being in the neighborhood of an earlier point such as $\pi(k-5)$, and not in its own neighborhood!
  This won't actually be a problem, as we simply want to guarantee that the elements in the neighborhood are smaller than that at $\pi(k)$.

  To see that $\vect{g}_{\pi(k)}$ is larger than every point in the set $\setbuild{\vect{g}_{\vect{w}}}{\vect{w} \in N\paren[\big]{\pi^{*}(j)}\textrm{ for }j\in [n_{k}+1,n_{k+1}]}$ (the neighborhoods of every point in the range $[n_{k}+1,n_{k+1}]$), assume that there's some point $\vect{i}$ so that $\vect{g}\brac[\big]{\vect{i}} > \vect{g}\brac[\big]{\pi(k)}$.
  This would mean that $\vect{i}$ must come before $\pi(k)$ in the list generated by the original $\pi$ say as the $j$th largest coefficient, so $\vect{i}=\pi(j)$.
  But since every point which has $\vect{i}$ in its neighborhood is also in $\vect{i}$'s own neighborhood, $\pi(k)$ must have already been included as elements of $N\paren[\big]{\pi(j)}$ somewhere between $n_{j}+1$ and $n_{j+1}$.
  So in a proof by contradiction, $\vect{g}\brac{\vect{w}} < \vect{g}\brac{{\pi(k)}}$ for every $\vect{w}\in\setbuild{\vect{w}}{\vect{w}\in N\paren[\big]{\pi^{*}(i)}\textrm{ for }i\in [n_{k}+1,n_{k+1}]}$.

  For convenience define $\vect{h}\brac{\vect{i}} = (\vect{\phi} \conv \vect{g})\brac{\vect{i}}$.
  To get a bound on $\vect{h}\brac{\vect{i}}$ using $\vect{g}$, we use a discrete version of Young's inequality with $p=q=1$, $r=\infty$ and $f_{\vect{j}}=\delta_{\vect{i},\vect{j}}$:~\cite[Section 4.2]{liebAnalysis2001}
  \begin{align}
    \label{eq:hsparsity}
    \abs[\Big]{h\brac{\vect{i}}}
    \leq \abs[\Bigg]{\s*{\vect{l}\in[-L,L]^{d}}{} \delta_{\vect{i},\vect{l}}\vect{\phi}\conv \vect{g}\brac[\big]{\vect{l}}}
    \leq \norm[\big]{\delta_{\vect{i},\vect{j}}}_{1}\norm[\big]{\vect{\phi}}_{1}\norm[\big]{\vect{g}\chi_{N(\vect{i})}}_{\infty}
    \leq \norm[\big]{\vect{g}\chi_{N(\vect{i})}}_{\infty}
  \end{align}
  where $\characteristic{N(\vect{i})}$ is the (discrete) characteristic function of the neighborhood of $\vect{i}$.
  If $\vect{i} = \pi^{*}(k)$ for $k$ between $n_{j}+1 $ and $ n_{j+1}$, then by the construction of $\pi^{*}$ and $n_{j}$ above, the largest magnitude coefficient in $N\paren[\big]{\pi^{*}(k)}$ is $g_{\pi(j)}$. Thus $h_{\pi^{*}(k)} \leq g_{\pi(j)}$.

  Because each neighborhood has at most ${(2L)}^{d}$ elements, asymptotically we can relate $j$ and $k$ through $(2L)^{d}j \leq k \leq (2L)^{d}(j + 1)$, so we have that $k\approx (2L)^{d}j$, or $j\approx (2L)^{-d}k$. Using this and the result from the previous paragraph,
  \begin{align}
    \label{eq:sparsityRes}
    h_{\pi^{*}(k)} &\leq g_{\pi(j)}
    \leq C j^{-\nf 32}(\log j)^{\nf 32}
    = C \frac{\paren[\bigg]{\log(k(2L)^{-d})}^{\nf 32}}{\paren[\big]{k(2L)^{-d}}^{\nf 32}}\\
    &= C (2L)^{\nf{2d}{3}} \frac{\paren[\bigg]{\log(k) -d\log 2L}^{\nf 32}}{\paren[\big]{k}^{\nf 32}} \lesssim k^{(-\nf 32)}(\log k)^{\nf 32}
  \end{align}

\end{proof}
Note that while the proof is for the case of a single array $\vect{g}$, since $\vect{\phi}$ only interacts with a single $\vect{g}$ at a time, it can be extended to the case of a collection of arrays $\brak[\big]{\vect{g}^{\ind*[]}}_{\ind*[]\in\IndexSet[]}$ at the cost of more notation overhead.
It is worth noting that this rearrangement is most likely not monotonically decreasing, so in practice the additional $(2L)^{d}$ factor may not be necessary if $\vect{\phi}$ has the shape of an actual averaging function like \eg, B-splines, Welch, Hann, etc.
We could also extend this result to averaging functions which decay sufficiently quickly, rather than have finite support, but do not do so as a matter of time, as there are shearlet constructions where the averaging wavelet does have compact support.

Finally, we deal with the internal case of continuous $\layer$. It will turn out that the $\layer(f)$ is actually a slight generalization of a cartoon-like function that involves the potential intersection of several boundary curves.
\begin{lemma}\label{lem:contDecay}
  If $\nonlin*$ is piecewise smooth and $f$ is a cartoon-like function, then the shearlet transform of $\layer(f)$ decays as $O(n^{-\nf 32}(\log(n))^{\nf 32})$.
\end{lemma}
\begin{proof}
  Convolution gives the output the smoothness of the smoother of the two functions; consequently, $\frEl\conv f$ for $f$ being cartoon-like results in a smooth function, since the $\frEl$ for shearlets are smooth.
  The only major difficulty is what happens with $\nonlin*$.
  Because it is piecewise smooth, the only difficulties arise on the level sets of $h(\vect{x}) = \nonlin*(\frEl\conv f)(\vect{x})$ for the points of discontinuity of $\nonlin*$ (suppose one is at $t_{0}$; the same ideas work for any additional level-sets).
  By the inverse function theorem\cite[Theorem 9.24]{rudinPrinciplesMathematicalAnalysis1976}, we have that the level set $h(\vect{x})=t_{0}$ is at least $\cC*[2]$ away from points where $\grad h\neq \vect{0}$, and further that at those points that it is the intersection of finitely many $\cC*[2]$ curves.
  Thankfully, the case of piecewise $\cC*[2]$ curves is covered in~\cite[Section 5.1.7]{kutyniokShearletsMultiscaleAnalysis2012}. The proof that not an unreasonable number of coefficients become too large at the point of discontinuity in the curve applies equally well when there are multiple curves intersecting instead of a single curve having a cusp. For brevity, we don't recapitulate the proof here.
\end{proof}

Finally, putting together \Cref{lem:nonlinConds}, \Cref{lem:aveDec}, and \Cref{lem:contDecay}, we have the actual result of this section:
\begin{theorem}[Scattering Sparsity]\label{thm:scatSparse}
  If the nonlinearities $\brak[\big]{\nonlin*}_{m\in\mN}$ satisfy
  \begin{itemize}
    \item $\nonlin*$ is piecewise smooth
    \item $\abs{\nonlin*(t)}$ is convex,
    \item for $\abs{t} \leq \abs{s}$, $\abs{\nonlin*(t)} \leq \abs{\nonlin*(t)}$,
    \item $\nonlin*(0)=0$
  \end{itemize}
  and the averaging functions are compactly supported, then for cartoon functions $f$, the shattering transform output $\allOut[f]$ decays at the rate $O(n^{-\nf 32}(\log(n))^{\nf 32})$.
\end{theorem}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Dissertation"
%%% End:
