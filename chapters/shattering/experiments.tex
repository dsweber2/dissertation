The primary classification results can be found in \Cref{fig:ress}.
We compare the efficacy of various nonlinearities in transform along with the 2D scattering transform as implemented in Kymatio~\cite{andreuxKymatioScatteringTransforms2018} at classification on the MNIST dataset and the Fashion MNIST datasets with an increasing amounts of data~\cite{xiaoFashionMNISTNovelImage2017}.
For classification we used a linear support vector machine.
The final data point in each is trained on 525k examples and the reported test is on the validation set.
Both graphs are on a log-log plot, with the colored regions indicating the standard deviation of the cross validation.
The linear dependence between accuracy and amount of training data fits the rates found for neural networks~\cite{hestnessDeepLearningScaling2017}.
The nonlinearities used are in \Cref{tab:nonlin}.
\begin{figure}[htb]
  \begin{subfigure}{0.49\linewidth}
    \includegraphics[width=\linewidth]{images/shattering/linearMNIST.pdf}
    \caption{{ \small MNIST dataset.}}
  \end{subfigure}
  \begin{subfigure}{0.49\linewidth}
    \includegraphics[width=\linewidth]{images/shattering/linearFashionMNIST.pdf}
    \caption{{ \small Fashion MNIST dataset}}
  \end{subfigure}
  \caption{Relation between the error rate using various nonlinearities and the number of training examples.
    Both the $x$- and $y$-axes are logarithmic.
    The colored bands surrounding each curve represent the cross validation variance.
    As one may expect, the variance is higher with fewer examples.}
  \label{fig:ress}
\end{figure}

The shattering transform with the absolute value is on par with the kymatio transform for the MNIST classifier; somewhat surprisingly, all other nonlinearities lead to a significantly worse classification rate.
On the Fashion MNIST dataset, all of the shattering techniques are significantly more effective when given only 50 examples, but are surpassed by 5000 examples.

First, we may have expected the sparsity of the transform to lead to better classification performance as more features would be concentrated in fewer coefficients.
However, as was mentioned in \Cref{sec:MeetConditions}, both MNIST and FashionMNIST are only $28 \times 28$ pixels.
This led to very large upper and lower frame bounds, and in general a poorly constructed version of the shearlet transform.
Effectively, at this scale, there is very little difference in construction between the shearlets and the 2D Morlet wavelets.
On a larger dataset, we might see the benefit provided by the sparsity of the shearlets more concretely.
\begin{table}[h]
  \centering
  \begin{tabular}{c|l}
    Nonlinearity Name & Function\\
    \hline
    absolute Value & $\nonlin*(t) = \abs{t}$\\
    ReLU & $\nonlin*(t) = \max\brak{t,0}$\\
    Tanh & $\nonlin*(t) = \tfrac{\e^{t}-\e^{-t}}{\e^{t}-\e^{-t}}$\\
    Piecewise Linear & $\nonlin*(t) = \pc{t}{t \geq 0}{\tfrac{t}{2}}{t < 0}$\\
    Softplus & $\nonlin*(t) = \log\paren{1+\e^{t}}$\\
  \end{tabular}
  \caption{The nonlinearities used in the Shattering transform in this section}
  \label{tab:nonlin}
\end{table}

It is worth considering why we have such different results for different nonlinearities.
We postulate that it is primarily due to bounded nature of the tanh, ReLU, and soft-plus.
In the case of ReLU, the value of the function in all negative regions is erased completely.
The absolute value only discards the sign information.
While tanh and soft-plus are theoretically invertible, and so should lose nothing, in practice numerical precision rounds any sufficiently negative output to 0 (in practice, this seems to be around $-16$ for soft-plus and $\pm 8$ for tanh).
So practically, tanh discards both positive and negative values outside of $[-8, 8]$, explaining its somewhat abysmal asymptotic performance.

Why then does ReLU perform well in the case of convolutional neural networks? A possible reason for this is that CNNs use affine, not linear transforms, meaning that the input to the ReLU can be shifted so that informative regions are on the positive side of the zero hyperplane.
In contrast, unmodified scattering transforms aren't affine, and so benefit more from nonlinearities that include information from the negative regions.
% TODO: make tables of results
% TODO: decide on which results
% TODO: resample the image at a higher resolution and run for small number of examples
% TODO: see how well the class still holds under translation, and see how much the norm changes


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Dissertation"
%%% End:
