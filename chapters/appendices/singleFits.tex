\section{Pseudo-inversion of Wavelet Coefficients}
\label{sec:fit-wave-coef}
This appendix serves as a sanity check of the methods developed in \Cref{sec:synth-coord} to confirm that they do actually return the original wavelets when fitting the wavelet coefficients.
Specifically, we are trying to find the input signal that maximizes the discrete wavelet transform defined in \cref{eq:dwt}.
We will discuss a couple of alternatives that demonstrate why we eventually settled on \cref{eq:coordinateFit}.
The first is the analog of \cref{eq:coordinateFit}:
\begin{align}
  \label{eq:fittingWavelet}
  \min_{\vect{f}} \brak[\Bigg]{\regTerm \norm[\Big]{\vect{f}}^{2} - \WT[\vect{f}][\scaleLoc][\tau_i]}
\end{align}
where $\WT[\vect{f}][\scaleLoc][\tau_i]$ is defined in \cref{eq:dwt}, and $\vect{y}[\scaleLoc]=\vect{e}_{\tau_{i}}$, where $\scaleLoc, \tau_{i}$ are the target output location $\tau_{i}$ and scale $\scaleLoc$.
We will specifically be working with the case of the Morlet wavelets with mean frequency $\pi$.
\begin{figure}[h]
  \centering
  \includegraphics[width=.7\textwidth]{figures/appendix/fittingCWT/max/compareDirectWithTargetWavelet.pdf}
  \caption{The result from fitting \cref{eq:fittingWavelet}, which is nearly identitical to the original wavelet.}
  \label{fig:max-compare}
\end{figure}

One somewhat counter-intuitive result is depending on the kind of normalization imposed on the wavelets themselves (the $p$ from \Cref{sec:cont-wavel-transf}), solving \cref{eq:fittingWavelet} can lead to a different coordinate actually being larger.
The reason for this is that the inner product between the wavelets, $\ip{\mother_{\scaleLoc[k]}}{\mother_{\scaleLoc[k']}}$ is determined by the $\lp[2]$ norm, rather than the other $p$ norms, so by maintaining the $\lp[1]$ norm, the $\lp[2]$ norm decreases with scale.
simply because the net $\lp[2]$ mass is larger, which dictates the inner product between the two wavelets.
This comes through somewhat in the case of $p=1$ in the scalogram \Cref{fig:max-compare-scal}, as the scale $\scaleLoc=57.4\si{Hz}$ actually has some larger coefficients than $\scaleLoc=48.2\si{Hz}$ itself, the target frequency.
\begin{figure}[h]
  \centering
  \includegraphics[width=.7\textwidth]{figures/appendix/fittingCWT/max/scalogram.pdf}
  \caption{The Scalogram of the fit result in \Cref{fig:max-compare}. Note that this is also effectively the scalogram of the original wavelet, which is clearly non-orthogonal with the other wavelet scales.}
  \label{fig:max-compare-scal}
\end{figure}

\FloatBarrier
\subsection{Maximizing one coordinate while minimizing others}
\label{sec:penal-minMax}
Because both the CWT and the scattering transform are redundant, non-orthogonal transforms, maximizing one location while minimizing all others can lead to some unexpected behavior.
Explicitly, if we are optimizing
\begin{align}
  \label{eq:fittingMaxMin}
  \min_{\vect{f}} \brak[\Bigg]{\regTerm \norm[\Big]{\vect{f}}^{2} -
  \abs[\Big]{\WT[\vect{f}][\scaleLoc][\tau_i]} +
  \alpha \s{\tau_{i'}\neq \tau_{i},\scaleLoc[k']\neq\scaleLoc}{} \WT[\vect{f}][\scaleLoc[k']][\tau_{i'}]}
\end{align}
for some value of $\alpha$ and $\regTerm$, then several things can potentially go wrong.
If $\alpha$ is chosen too large, then the resulting fit will select frequencies with the narrowest support.
Even with $\alpha$ chosen approximately correctly in \Cref{fig:maxi-mini} and the corresponding scalogram in \cref{fig:max-min-compare-scal}, the wavelet is significantly distorted to minimize the other coefficients.


\begin{figure}[!h]
  \centering
  \includegraphics[width=.6\textwidth]{figures/appendix/fittingCWT/maxMin/compareDirectWithTargetWavelet.pdf}
  \caption{The result of fitting the maximized and minimized \cref{eq:fittingMaxMin} with $\mu=\num{3.3e-8}$ and $\alpha = \num{3.33e-3}$.}
  \label{fig:maxi-mini}
\end{figure}

\begin{figure}[!h]
  \centering
  \includegraphics[width=.6\textwidth]{figures/appendix/fittingCWT/maxMin/scalogram.pdf}
  \caption{The scalogram corresponding to \Cref{fig:maxi-mini}. Compared to \Cref{fig:max-compare-scal}, it is slightly more concentrated around the target location. However because of the non-orthogonal nature of the CWT, this results in a worse fit.}
  \label{fig:max-min-compare-scal}
\end{figure}

\FloatBarrier
\subsection{Fitting the Normalized Coordinate}
\label{sec:probl-with-norm}

While useful for classification, the normalization of the ST coefficients discussed in \Cref{sec:normal} causes the fit to distort far away from the target wavelet.
This is because maximizing the target location necessitates minimizing the other locations, as there is a fixed mass shared between all locations.
Unlike the previous section, it has the advantage of not having an extra parameter $\alpha$ to tune, but it suffers a similar problem of over-optimizing for the exact location of a frequency.
\Cref{fig:normalizing-scalogram} is even more concentrated at the target frequency and location than either \Cref{fig:max-compare-scal} or \Cref{fig:normalizing-compare}, but the resulting fit bears no resemblance to the target.
\begin{figure}[h]
  \centering
  \includegraphics[width=.6\textwidth]{figures/appendix/fittingCWT/normed/compareDirectlyWithTargetWavelet.pdf}
  \caption{The result of fitting the normalized version of \cref{eq:fittingWavelet}. There is very little resemblance between the fit solution and the original wavelets.}
  \label{fig:normalizing-compare}
\end{figure}
\begin{figure}[h]
  \centering
  \includegraphics[width=.6\textwidth]{figures/appendix/fittingCWT/normed/Scalogram.pdf}
  \caption{The scalogram }
  \label{fig:normalizing-scalogram}
\end{figure}

\FloatBarrier
\section{Penalizing Multiple Paths Simultaneously}
\label{sec:penal-other-locat}
In \Cref{sec:single-coordinate} we focused on finding the pseudo-inverse of single coordinates, where $\otherOut[p][y] = \vect{e}_{k}$ for a single $p$, while in either \Cref{sec:fit-results} or \Cref{sec:class-interp} we focus on penalizing entire layers or transforms.
This short appendix provides some examples of fitting just a couple of locations, so $\otherOut[p_{i}][y] = \vect{e}_{k_{i}}$ for $i=1,2$ for two paths $p$ simultaneously.
In \Cref{fig:multi-first-time} we are fitting in the first layer, and have that $p_{1}=120\si{Hz}$ with corresponding output time $35\si{ms}$ and $p_{2}=299\si{Hz}$ with corresponding output time $81\si{ms}$.
These two paths are well separated in both time and frequency, and they are clearly separated in the resulting space figure.
In \Cref{fig:multi-first-time-eq}, the two are weighted equally, while in \Cref{fig:multi-first-time-uneq} we have doubled the weight on the first location, so $y_{35\si{ms}}[120\si{Hz}] = 2$.

\begin{figure}[h]
  \centering
  \begin{subfigure}[h]{0.49\linewidth}
    \includegraphics[width=\textwidth]{figures/interpret/multiFits/morl2Pi/poolBy1.5_outPool8_pNorm2_aveLen-1-12_extraOct0/path((3, 4), (7, 13))Vals(1, 1)/bestExamplemixed.pdf}
    \caption{Fitting in the first layer with equal weights and two distant paths}
    \label{fig:multi-first-time-eq}
  \end{subfigure}
  \begin{subfigure}[h]{0.49\linewidth}
    \includegraphics[width=\textwidth]{figures/interpret/multiFits/morl2Pi/poolBy1.5_outPool8_pNorm2_aveLen-1-12_extraOct0/path((3, 4), (7, 13))Vals(2, 1)/bestExamplemixed.pdf}
    \caption{Fitting in the first layer with unequal weights and two distant paths}
    \label{fig:multi-first-time-uneq}
  \end{subfigure}
    \caption{Fitting multiple paths in the first layer}
    \label{fig:multi-first-time}
\end{figure}

In \Cref{fig:multi-second-time} we are fitting in the second layer, and have that $p_{1}=(121\si{Hz}, 139\si{Hz})$ with corresponding output time $37\si{ms}$ and $p_{2}=(219\si{Hz}, 323\si{Hz})$ with corresponding output time $91\si{ms}$.
These two paths are well separated in both time and frequency, and they are clearly separated in the resulting space figure.
In \Cref{fig:multi-first-time-eq}, the two are weighted equally, while in \Cref{fig:multi-first-time-uneq} we have doubled the weight on the first location, so $y_{91\si{ms}}[(219.0\si{Hz}, 323.0\si{Hz})] = 2$.
As was the case with maximizing single coordinates, another coordinate ends up being larger in the process of maximizing these locations.
The relative weights are less clearly doubled as they were in the first layer.

\begin{figure}[h]
  \centering
  \begin{subfigure}[h]{0.49\linewidth}
    \includegraphics[width=\textwidth]{figures/interpret/multiFits/morl2Pi/poolBy1.5_outPool8_pNorm2_aveLen-1-12_extraOct0/path((2, 5, 5), (5, 12, 14))Vals(1, 1)/bestExamplemixed.pdf}
    \caption{Fitting in the second layer with equal weights and two distant paths}
    \label{fig:multi-second-time-eq}
  \end{subfigure}
  \begin{subfigure}[h]{0.49\linewidth}
    \includegraphics[width=\textwidth]{figures/interpret/multiFits/morl2Pi/poolBy1.5_outPool8_pNorm2_aveLen-1-12_extraOct0/path((2, 5, 5), (5, 12, 14))Vals(1, 2)/bestExamplemixed.pdf}
    \caption{Fitting in the second layer with unequal weights and two distant paths}
    \label{fig:multi-second-time-uneq}
  \end{subfigure}
  \begin{subfigure}[h]{0.49\linewidth}
    \includegraphics[width=\textwidth]{figures/interpret/multiFits/morl2Pi/poolBy1.5_outPool8_pNorm2_aveLen-1-12_extraOct0/path((2, 5, 5), (5, 12, 14))Vals(1, 1)/SecondLayerOutputmixed.pdf}
    \caption{The second layer coefficients of \Cref{fig:multi-second-time-eq}}
    \label{fig:multi-second-time-output-eq}
  \end{subfigure}
  \begin{subfigure}[h]{0.49\linewidth}
    \includegraphics[width=\textwidth]{figures/interpret/multiFits/morl2Pi/poolBy1.5_outPool8_pNorm2_aveLen-1-12_extraOct0/path((2, 5, 5), (5, 12, 14))Vals(1, 2)/SecondLayerOutputmixed.pdf}
    \caption{The second layer coefficients of \Cref{fig:multi-second-time-uneq}}
    \label{fig:multi-second-time-output-uneq}
  \end{subfigure}
    \caption{Fitting multiple paths in the second layer}
    \label{fig:multi-second-time}
\end{figure}

\clearpage
\section{Space Domain Second Layer coefficient pseudo-inversion}
\label{sec:joint-plot-every}


This appendix is the space domain representation of the pseudo-inverses of the second layer coefficients, mostly displayed in scalograms in the main text.
\begin{figure}
  \centering
\includegraphics[width=\linewidth]{figures/interpret/singleFits/morl/poolBy1.5_outPool8_pNorm2_aveLen-1-12_extraOct0/lay2/multiBestPlot.pdf}
\caption{Morlet mean $\pi$}
\label{fig:ApxmorlLay2compare}
\end{figure}
\begin{figure}
  \centering
\includegraphics[width=\linewidth]{figures/interpret/singleFits/morl2Pi/poolBy1.5_outPool8_pNorm2_aveLen-1-12_extraOct0/lay2/multiBestPlot.pdf}
\caption{Morlet mean $2\pi$}
\label{fig:Apxmorl2PiLay2compare}
\end{figure}
\begin{figure}
  \centering
\includegraphics[width=\linewidth]{figures/interpret/singleFits/dog1/poolBy1.5_outPool8_pNorm2_aveLen-1-12_extraOct0/lay2/multiBestPlot.pdf}
\caption{DoG1}
\label{fig:Apxdog1Lay2compare}
\end{figure}
\begin{figure}
  \centering
\includegraphics[width=\linewidth]{figures/interpret/singleFits/dog2/poolBy1.5_outPool8_pNorm2_aveLen-1-12_extraOct0/lay2/multiBestPlot.pdf}
\caption{DoG2}
\label{fig:Apxdog2Lay2compare}
\end{figure}
\FloatBarrier

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Dissertation"
%%% End:
