\section{Distributional Chain-rule derivation for absolute value and its derivatives}
\label{sec:heav-deriv-eval}


In this appendix we find the derivatives of $\abs{f(x)}$, treated as a distribution, as used in \Cref{sec:synth-coord}.
Throughout, let $\varphi$ be a test function, let $f$ a piecewise infinitely differentiable function which has finitely many roots $Z_{f} = \setbuild{x}{f(x)=0}$ and points of discontinuity $D_{f}$, and let $F_{f}=Z_{f}\union D_{f}\union \{-\infty, \infty\}$.
Then for $\abs{x}$, the distributional derivative is defined via integration by parts:
\begin{align}
  \label{eq:absderivative}
  \integral{}{}{\frac{\dd}{\dd x}\abs{f}(x)\varphi(x)}{x} &\define -\integral{}{}{\abs{f}(x)\varphi'(x)}{x} =  \s*{x_{i}\in F_{f}}{}-\integral{x_{i}}{x_{i+1}}{\sgn\paren{f}(x)f(x)\varphi'(x)}{x}\\
  &\phantom{:}= \s*{x_{i}\in F_{f}}{}\paren[\Big]{f(x_{i+1}-)\varphi(x_{i+1}) - f(x_{i}+)\varphi(x_{i}) + \integral{x_{i}}{x_{i+1}}{\sgn\paren{f}f'(x)\varphi(x)}{x}}\\
  &\phantom{:}= \integral{}{}{\sgn\paren{f}f'(x)\varphi(x)}{x} + \s*{x_{i}\in D_{f}}{} f(x_{i}+)\varphi(x_{i}) - f(x_{i}-)\varphi(x_{i})
\end{align}
where we have used that $\sgn(f)(x)$ is a constant between $x_{i}$ and $x_{i+1}$, since outside of these points it is a non-zero smooth function, the fact that at the points $x_{i}\in Z_{f}\setminus D_{f}$ (so zero but not also discontinuous), $f(x_{i})$ is just zero, and the fact that $\varphi(\pm\infty)=0$ because it is a test function.
So in a distributional sense, $\frac{\dd}{\dd x}\abs{f} = \sgn(f)(x)f'$, since the distributional derivative of $f$ includes the sum over points of discontinuities~\cite[Theorem 9.1]{follandFourierAnalysisIts1992}.
For $\sgn$, we know that $\frac{\dd}{\dd x}\sgn(x) = 2\delta(x)$, so
\begin{align}
  \label{eq:sgnderivative}
  \integral{}{}{\frac{\dd}{\dd x}\sgn{f}(x)\varphi(x)}{x} &= \integral{}{}{\sgn(f(x))\varphi'(x)}{x} = \s*{x_{i}\in F_{f}}{}\integral{x_{i}}{x_{i+1}}{\sgn(f(x))\varphi'(x)}{x}\\
  &=\s*{x_{i}\in F_{f}}{}\sgn(f)(x_{i+1}-)\varphi(x_{i+1}) - \sgn(f)(x_{i}+)\varphi(x_{i})\\
\end{align}
To go further, we will need to decompose $Z_{f}\union D_{f}$ into 3 sets: let
\begin{itemize}
  \item $N_{f}=\setbuild{x\in Z_{f}\union D_{f}}{\sgn(f)(x-) > \sgn(f)(x+)}$ (where the sign becomes negative),
  \item  $P_{f}=\setbuild{x\in Z_{f}\union D_{f}}{\sgn(f)(x-) < \sgn(f)(x+)}$ (where the sign becomes positive), and
  \item $S_{f}=\setbuild{x\in Z_{f}\union D_{f}}{\sgn(f)(x-) = \sgn(f)(x+)}$ the sign is the same.
\end{itemize}
Only the first two are really needed, as both $\sgn(f)(x_{i}-)\varphi(x_{i+1})$ and $-\sgn(f)(x_{i}-)\varphi(x_{i+1})$ are equal and opposite for any $x_{i}\in S_{f}$.
Using these and the fact that $\varphi(\pm\infty)=0$, we can rearrange the sum to give
\begin{align}
  \label{eq:sgnderivativeFinal}
  \integral{}{}{\frac{\dd}{\dd x}\sgn{f}(x)\varphi(x)}{x}
  &=\s*{x_{i}\in P_{f}}{}2\varphi(x_{i}) - \s*{x_{i}\in N_{f}}{}2\varphi(x_{i})\\
  \frac{\dd}{\dd x}\sgn{f} &= \s*{x_{i}\in P_{f}}{}2\delta(x-x_{i}) - \s*{x_{i}\in N_{f}}{}2\delta(x-x_{i})
\end{align}
All further derivatives of $\abs{f(x)}$ are now derivatives of delta functions, which are unique distributions that evaluate the derivative of the test function they are operating at, e.g. $\delta'\varphi(x) = -\varphi'(0)$ and so on.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Dissertation"
%%% End:
