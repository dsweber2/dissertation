While finding inputs which most activate a particular path requires some notion of inversion like the previous methods, it differs from them in that we expect the target to not actually be in the image of the scattering transform, so we will need to relax the notion of ``inverse'' somewhat.
In the discrete case as discussed in \Cref{sec:discrete-output}, the image of $\allOut[\vect{f}]$ is a subset of collections of vectors of the form $Y \define \brak[\Big]{\otherOut}_{\pat\in\PathSet, m\in\mZ_{0}}$ where $\otherOut$ has the same shape and size as the output $\discOut$.
Some of these collections $Y$ will correspond to the output of a particular input, but one can easily construct examples that aren't; for example, if any path in the first layer has a negative entry.
For collections $Y = \allOut[\vect{f_{0}}]$ which are in the output, a problem where the original $\vect{f}_{0}$ is an optimum is $\lp*[2]$-minimization:
\begin{align}\label{eq:actualOptim}
  \min_{\vect{f}}\s*{p\in P}{}\norm[\Big]{\discOutX[p][][\vect{f}] - \otherOut[p]}_2^2.
\end{align}
This will be true for arbitrary subsets $P \subseteq \unioni{m=0}{\infty} \PathSet$, with more paths decreasing the possible range of alternative minima.
See \Cref{sec:joint-plot-every} for some examples doing this for several simultaneous target paths.


This formulation of inversion naturally extends to $Y$ not in the image of $\allOut[]$, for which it is finding the closest point $\discOutX[p][][\vect{f}]$ in the image of $\allOut[]$ and its inverse $\vect{f}$.
One issue with doing just the $\lp*[2]$-norm minimization is that if the zeroth layer is not in $P$, then for any $c\in\mR$, $c+\vect{f_{0}}$ is also a minimizer, since for the wavelets of interest, we have that $\fhat{\psi}(0) = 0$.
To deal with this, we impose a regularization term $\regTerm\norm[\big]{\vect{f}}_2$:
\begin{align}
  \min_{\vect{f}}\brak[\Bigg]{\regTerm\norm[\big]{\vect{f}}_2^{2} + \s{p\in P}{}\norm[\Big]{\discOutX[p][][\vect{f}] - \otherOut[p]}_2^2}
\end{align}
A problem with an equivalent set of minima that that frames finding $\discOutX[p][\vect{f}]$ that most aligns with $\otherOut[p]$ is
\begin{align}\label{eq:coordinateFit}
  \min_{\vect{f}}\brak[\Bigg]{\regTerm\norm[\bigg]{\vect{f}}_2^{2} - \s{p\in P}{}\bigg\langle\discOutX[p][][\vect{f}],\otherOut[p]\bigg\rangle}.
\end{align}
We will generally use this formulation of the problem, since it more directly ties the regularization parameter $\regTerm$ to the values of $\discOutX[p][][\vect{f}]$.
In the case of computing the reconstruction of real examples, we can choose $\regTerm$ by minimizing the difference between $\vect{f}$ and the original function $\vect{f}_{0}$.
However, in the case of pseudo-inversion, it is not clear what the correct choice of $\mu$ should be.
In the absence of a reason for a particular choice, we choose $\num{1e-5}$, since this resulted in solutions that have values around 1.

\subsection{Optimization methods}
\label{sec:optimization-methods}

This is a form of regularized nonlinear least squares, where each residual is $r_{p,i}(\vect{f}) = \discOut[p][i][\vect{f}]-\otherOut[p][y][i]$.
The differentiability of $r_{p,i}$ comes down to that of $\discOut[p][][\vect{f}]$, which is almost everywhere differentiable, since $\abs{\cdot}$ is almost everywhere differentiable.
However, in practice several variations on gradient descent or approximate gradient descent, such as BFGS~\cite{nocedalNumericalOptimization2006}, stochastic gradient descent~\cite{duGradientDescentCan2017}, and PDFO~\cite{ragonneauPDFOCrossPlatformInterfaces2020}, all failed to converge in reasonable time frames.
This is most likely because across zero, the gradient flips sign.
In combination with the highly oscillatory nature of the solutions, each gradient step flips the sign at a different time sample, leading to a combinatorial search despite using the gradient information.
In practice this means that gradient or approximate gradient-based methods frequently get stuck around saddle nodes.
As a result, non-gradient-based methods such as \emph{differential evolution} have proven more effective at solving the problem~\cite{priceDifferentialEvolutionPractical2005}.
The specific version we use is the implementation of DE/rand/1/bin in \href{https://github.com/robertfeldt/BlackBoxOptim.jl}{BlackBoxOptim.jl}\footnote{https://github.com/robertfeldt/BlackBoxOptim.jl}, with some extra pre- and post-processing steps.

In general, to minimize a function $h$, evolutionary computation works by iteratively updating a population of candidate agents $i=1,\ldots, P$ for each generation $g$ using some update rule, and only keeping solutions which sufficiently improve the accuracy.
Each candidate $i$ during generation $g$ consists of a candidate solution $\vect{X}^{i,g}$ and various associated parameters.
DE/rand/1/bin's update rule, applied to each candidate $\vect{X}^{i_{0},g}$ in the current population has two steps: first, we take three \emph{other} randomly chosen current candidates $\vect{X}^{i_{1},g}$, $\vect{X}^{i_{2}, g}$, and $\vect{X}^{i_{3},g}$, and create an intermediate vector
\begin{align}
   \vect{V}^{i_{0},g} = \vect{X}^{i_1,g} + F^{i_{0}, g}\cdot(\vect{X}^{i_2,g}-\vect{X}^{i_3,g})\label{eq:DEUpdateRule}
\end{align}
where $F^{i_{0},g}$ is a parameter of the agent $i_{0}$.
Next, with some crossover probability $r^{i_{0},g}$ (which also depends on the agent $i_{0}$), each coordinate of the new candidate is
\begin{align}
  \label{eq:crossover}
  Y^{i_{0},g}_{k} = \pc{
                    X^{i_{0},g}_{k}}{\textrm{with probability $r^{i_{0},g}$}}{
                    V^{i_{0},g}_{k}}{\textrm{with probability $1-r^{i_{0},g}$}}
\end{align}
Finally, the actual objective function $h$ comes into play: if $h(\vect{Y}^{i_{0},g})<h(\vect{X}^{i_{0},g})$, then we replace $\vect{X}^{i_{0},g+1} = \vect{Y}^{i_{0},g}$.
Otherwise, we keep the same candidate $\vect{X}^{i_{0},g+1} = \vect{X}^{i_{0},g}$, and reselect $F^{i_{0}}$ and $r^{i_{0},g}$ from the Cauchy distribution\footnote{The randomization of $F^{i_{0}}$ and $r^{i_{0},g}$ isn't strictly part of the DE/rand/1/bin algorithm, but is a common enough variant, \eg\cite{choiAdaptiveCauchyDifferential2013}.}.

With this context, the somewhat opaque name DE/rand/1/bin refers to \emph{D}ifferential \emph{E}volution, with the base vector $\vect{X}^{i_{0},g}$ in \cref{eq:DEUpdateRule} chosen \emph{rand}omly, with \emph{1} difference added, and the final candidate $\vect{Y}^{i_{0},g}$ chosen so the mutated locations follow a \emph{bin}omial distribution.

We use a couple of adaptations that are particular to solving \cref{eq:coordinateFit}.
\begin{enumerate}
  \item We perform all updates to the  candidates $\vect{X}^{i,g}$ after a discrete cosine transform (DCT).
        Applied directly to the space domain coefficients for \cref{eq:coordinateFit}, both \cref{eq:DEUpdateRule} and \cref{eq:crossover} will frequently break any level of continuity in the candidate solutions, as there is no relation between coordinates.
        By representing the candidates in frequency, mutation will maintain the frequencies used throughout the population.
        As one might expect for so stochastic a method, for any particular run, this may or may not improve convergence, but on average the DCT representation converges faster.

\item We initialize the candidates non-uniformly in each coordinate.
        Given the power-law spacing of the wavelets in frequency, we initialize the candidates with a colored noise distribution, so if $\fhat{X}_{k}$ is the fast Fourier transform of $\vect{X}$, then for positive frequencies $k \geq 0$ the initial value is distributed as
        \begin{align}
          \label{eq:pinkNoiseInit}
          \fhat{X^{i,1}_{k}} \sim\frac{\normal(0,1) + \im \normal(0,1)}{(k+1)^\alpha}
        \end{align}
        while for $k < 0$, $\fhat{X^{i,1}_{k}} = \conj{\fhat{X^{i,1}_{-k}}}$ to maintain $\vect{X}^{i,1}$ real.
        Here for a given signal $\alpha$ is chosen uniformly at random between $.5$ and $3$ to allow for a wide distribution of smoothness.
        In practice, the frequently-used wavelets have minimal support in the very high frequencies, so noise in these frequencies takes a very long time to converge to zero.
        Since we know these should be less represented, we start with their values much smaller, but still non-zero and with varying relative magnitude.
\item We perturb the worst fifth of the population using the same colored noise distribution every 4 minutes to guarantee that the process will eventually converge, in a manner similar to\cite{aliImprovingPerformanceDifferential2011}, though less frequent and independent of the agent.
        Differential evolution without some method of adding noise is also potentially susceptible to being trapped in local minima, which this modification avoids.
        Both $4$ minutes and a fifth of the population were chosen heuristically, with $4$ minutes approximately the time required for a single second layer target to converge.
        Any choice for the fraction perturbed will work, as long as the fraction of the population changed is not too large to disrupt convergence entirely, or not too small to escape local minima.
\end{enumerate}
Finally, because black box optim cannot of some peculiarities of the implementation in \href{https://github.com/robertfeldt/BlackBoxOptim.jl}{BlackBoxOptim.jl}, we adjust \cref{eq:coordinateFit} to an equivalent form (with correct choice of $\regTerm$)
\begin{align}
  \label{eq:bboObjective}
  \min_{\vect{f}}\exp\paren[\Bigg]{\log(1.1)\paren[\bigg]{\regTerm\norm{\vect{f}}_2^{2} - \s{p\in P}{}\ipll{\discOutX[p][][\vect{f}]}{\vect{y}\big[p\big]}}},
\end{align}
where the base $1.1$ is chosen to avoid the objective function growing too quickly.
\subsection{Fitting Single Coordinate}
\label{sec:single-coordinate}
\input{chapters/interpret/singleCoordinate.tex}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Dissertation"
%%% End:
