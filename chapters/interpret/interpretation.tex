As was discussed in \Cref{sec:cont-wavel-transf} and \Cref{sec:dist-der-Lipschitz}, wavelet coefficients have a rich set of interpretations.
For real wavelets, they characterize edges and other non-smooth points in a signal based on the Lipschitz regularity at those points.
For analytic wavelets, they decompose a signal into an amplitude and phase locally in the time domain.

Unfortunately, we don't have as clean an interpretation of ST coefficients.
Because the convolutional layer has well-understood filters makes it significantly easier to interpret than a generic CNN, yet there has been some difficulty in interpreting the coefficients.
Most methods of interpretation to date are best considered as some form of inversion~\cite{cotterVisualizingImprovingScattering2017, anglesGenerativeNetworksInverse2018}.
The work of Cotter and Kingsbury reconstructs the input using the dual frame in each layer with the expense of storing phase (or sign) information during the forward transform~\cite{cotterVisualizingImprovingScattering2017}.
In~\cite{anglesGenerativeNetworksInverse2018}, Angles and Mallat use a generative adversarial network very similar to that of Dosovitskiy et al.\ \cite{dosovitskiyInvertingVisualRepresentations2015} to approximate the inverse of the scattering transform for a specific dataset.
While of broader applicability than Cotter and Kingsbury's work, its focus is more on using ST coefficients to generate plausible images in a broader class, rather than interpreting the meaning of any particular scattering coefficient.
Finally, Waldspurger's phase retrieval methods for recovering the original input from the modulus of the wavelet transform, while they would certainly form a reasonable basis from which to formulate an inversion method, we are unaware of any paper which does so\cite{waldspurgerPhaseRetrievalWavelet2017a}.


In this chapter, we will use both theoretical and experimental methods to try to shed more light on the meaning of the ST coefficients.
As with the original wavelets, there are marked differences between the interpretations of real and analytic wavelets, which recur in every interpretation method we use.

In \Cref{sec:gradient-based}, we examine the gradient of particular coordinates at various signals.
The gradient is perhaps the simplest method of performing sensitivity analysis, and gives us local information about how to increase the response of a particular ST coefficient; an example of a work using the gradient in this manner for CNNs is by Simonyan \etal\cite{simonyanDeepConvolutionalNetworks2014}.


In \Cref{sec:synth-coord} we use a more direct method of calculating the \emph{pseudoinverse} than is found in either~\cite{cotterVisualizingImprovingScattering2017} or~\cite{anglesGenerativeNetworksInverse2018}, and use this to create signals which maximize a particular ST coefficient.
Previous work using this general framework in the context of CNNs includes~\cite{erhanVisualizingHigherLayerFeatures2009},~\cite{mahendranUnderstandingDeepImage2015}, and~\cite{simonyanDeepConvolutionalNetworks2014}.
Mahendran and Vedaldi~\cite{mahendranUnderstandingDeepImage2015} seek to understand what information is retained by a neural network by ``inverting'' them, while~\cite{simonyanDeepConvolutionalNetworks2014} seeks to understand class labels specifically by finding examples which maximize the output for a specific class.
In \Cref{sec:synth-coord} we seek to find inputs that correspond to particular coordinates or combinations of coordinates being active.

In \Cref{sec:theo-inter-scat-trans}, we tackle interpreting the ST coefficients using the interpretations of the wavelet coefficients themselves described in \Cref{sec:cont-wavel-transf} and \Cref{sec:dist-der-Lipschitz}.
Finally, in \Cref{sec:sign-shape-class} bringing all of this together, we apply the scattering transform to a classic signal classification problem discerning 3 classes, and interpret the LASSO coefficients used in the classification.

\begin{remark}[Normalization]
\label{sec:no-normal}
In \Cref{sec:normal} we describe our normalization process for classification.
Throughout this chapter however, we will be working with the unnormalized scattering transform, as the normalization can introduce some spurious dependence between coordinates using either the gradient-based or pseudo-inversion methods.
For the purposes of interpretation, the normalized and unnormalized coefficients are the same, since within a given layer the normalization is effectively multiplication by a constant.
If we were to do Pseudo-inversion fitting paths from multiple layers simultaneously, then it would be important to include normalization (this will come up in \Cref{sec:class-interp}).
As we restrict our investigation to fitting paths from the same layer (and in large part a particular location) in this chapter, we can consider normalized and unnormalized coefficients to contain much the same information.
Since this constant depends on the input signal however, both the gradient and the pseudoinverse introduce dependence.
For examples of how this causes issues, see either \Cref{sec:zeroth-layer}, or \Cref{sec:fit-wave-coef}.
\end{remark}


\clearpage
\section{Scattering Transform Coefficient Gradients}
\label{sec:gradient-based}
\input{chapters/interpret/gradient.tex}
\FloatBarrier

\section{Pseudo-inversion}\label{sec:synth-coord}
\input{chapters/interpret/pseudoinversion.tex}

\section{Theoretical Interpretation}\label{sec:theo-inter-scat-trans}
\input{chapters/interpret/theoretical.tex}

\section{Interpreting a Scattering Bell-Cylinder-Funnel Classifier}
\label{sec:sign-shape-class}
\input{chapters/interpret/bcf.tex}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Dissertation"
%%% End:
