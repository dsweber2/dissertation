One major advantage of real wavelets for the purpose of theoretical analysis is that they can be characterized as the $n$th gradient of some averaging function $\theta$, where $n$ gives the (maximum) number of vanishing moments, as we discussed in \Cref{sec:wavel-with-vanish}~\cite[Theorem 6.2]{mallatWaveletTourSignal2009}, which allows us to write $\mother[m] = (-\partial)^{n_{m}}\theta_{m}$, where $\theta_{m}$ is an averaging function (so $\int\theta_{m} \neq 0$); in the case of the DoG wavelets, $\theta_{m}$ is just the corresponding father wavelet, but in general we don't have a guarantee of this.
In this context, our frame index $\ind[m][i]$ gives the $i\textrm{th}$ scaling in layer $m$, so for convenience, define $\theta_{m}^{\ind[m][i]}(t) \define \theta_{m}\paren[\big]{\nf{t}{\ind[m][i]}}$, and the $k\textrm{th}$ member of this family as $\mother[m,k_{m}] \define (-\partial)^{k_{m}}\theta_{m}$.% $\frEl(t) = s^{n-\nf 1p}\paren[\big]{\partial^{n_{m}}\theta_{m}}(\nf ts)$.
To somewhat simplify our notation, we introduce the subsampling operator $R_{\sub}f(t) \define f(\sub t)$.
If $\pat[2] = (\ind[2][j], \ind[1][i])$, we have for the second layer internal state that
\begin{align}
  \layer[\pat[2]][f][2] &= \frEl[2][j][2]\conv R_{\sub[1]}{\nonlin*[]}\bigg(\frEl[1][i][2] \conv f\bigg)
  =(-\partial)^{n_{2}}\theta_{2}^{\ind[j][2]} \conv R_{\sub[1]}\nonlin*[][(-\partial)^{n_{1}}\theta_{1}^{\ind[1][i]}\conv f]\\
  &= \theta_{2}^{\ind[2][j]} \conv \sub[1]^{-n_2}R_{\sub[1]}(- \partial)^{n_{2}}\nonlin*[][(-\partial)^{n_{1}}\theta_{1}^{\ind[1][i]}\conv f]\\
  &= \theta_{2}^{\ind[2][j]} \conv \paren[\bigg]{\sub[1]^{-n_2}\s{k=1}{n_{2}} R_{\sub[1]} {\nonlin*[]}^{(k)}\paren[\Big]{(-\partial)^{n_{1}}\theta_{1}^{\ind[1][i]} \conv f} \cdot \\
  &\phantom{= \theta_{2}^{\ind[2][j]} \conv \bigg(\sub[1]^{-n_2}\s*{k=1}{n_{2}})} R_{\sub[1]}B_{n_{2},k}\paren[\big]{\theta_{1}^{\ind[1][i]}\conv (-\partial)^{n_{1}+1} f,\ldots, \theta_{1}^{\ind[1][i]}\conv (-\partial)^{n_{1} + n_{2}-k+1} f}}\\
  &= \theta_{2}^{\ind[2][j]} \conv \paren[\Bigg]{\sub[1]^{-n_2}\s{k=1}{n_{2}} R_{\sub[1]} {\nonlin*[]}^{(k)}\paren[\Big]{\frEl*[1,n_{1}][\ind[1][i]] \conv f} \cdot\\
  &\phantom{= \theta_{2}^{\ind[2][j]} \conv \bigg(\sub[1]^{-n_2}\s*{k=1}{n_{2}})} R_{\sub[1]}B_{n_{2},k}\paren[\Big]{\frEl*[1,n_{1}+1][\ind[1][i]]\conv f,\ldots, \frEl*[1,n_{1} +n_{2}-k+1][\ind[1][i]] \conv f}}
\end{align}
where $B_{n,k}(x_{1},\ldots,x_{n-k+1})$ are the partial (exponential) Bell polynomials, and we have used a variant of Fa\'a di Bruno's formula for the chain rule of the $n_{2}\textrm{th}$ derivative~\cite{johnsonCuriousHistoryFaa2002, comtetAdvancedCombinatoricsArt1974}.
The derivative here is a distributional one, especially in the relevant cases of $\abs{\cdot}$ or $\ReLU$; for a proof that the chain rule applies as normally for distributions, see~\cite[Section 6.1]{hormanderAnalysisLinearPartial1998}.
We have also made use of the ``only if'' part of the vanishing moment theorem to substitute the $n_{1}+n_{2}$ member of the wavelet family derived from $\theta_{1}$.
This means that $\layer[\pat[2]][f]$ is a smoothing of a higher derivative wavelet transform at the scale $\ind[1][i]$ of the first layer times a term depending on the properties of the derivative of $\rho$.

For $\nonlin*[]$ being either $\abs{\cdot}$ or $\ReLU$, higher order derivatives turn these $\calC^{1}$ functions into Heaviside functions and then into various derivatives of the delta function, which gives them a fairly natural interpretation as evaluating a blurred higher order derivative along the level sets of a lower order derivative.
The divergent case is when $n_{2}=1$, where we are only taking the first derivative of $\nonlin*[]$; this is the sign function for $\abs{\cdot}$ (derived in \Cref{sec:heav-deriv-eval}) or the Heaviside function for $\ReLU$.
Explicitly, this is
\begin{align}
  \label{eq:specificAbsoluteValue1}
  &\theta_{2}^{\ind[2][j]} \conv \sub[1]^{-1}R_{\sub[1]}\paren[\bigg]{ \sgn\paren[\Big]{\frEl*[1,n_{1}][\ind[1][i]] \conv f} \cdot \paren{\frEl*[1,n_{1}+1][\ind[1][i]]\conv f}}.
\end{align}
Explicitly pulling the derivative out of the second layer wavelet has had two effects.
The first is increasing the order of the wavelet from the first layer.
The second is that we are multiplying this pseudo-differential operator by the sign for the original order derivative.
All of this is subsampled and then blurred.
Returning to \Cref{fig:dog1Lay2compareScalogram}, each path is maximizing the result for a second order DoG wavelet, while maintaining a positive sign for the first derivative at the same point.

For $n_{2}=2$, our terms begin proliferating:
\begin{align}
  \label{eq:specificAbsoluteValue2}
  &\theta_{2}^{\ind[2][j]} \conv \sub[1]^{-2}R_{\sub[1]}\paren[\bigg]{ \sgn\paren[\big]{\frEl*[1,n_{1}][\ind[1][i]] \conv f} \cdot \paren[\Big]{\frEl*[1,n_{1}+2][\ind[1][i]]\conv f} \\
  &\phantom{\theta_{2}^{\ind[2][j]} \conv \sub[1]^{-2}R_{\sub[1]}\bigg( \sgn}+ \delta\paren[\Big]{\frEl*[1,n_{1}][\ind[1][i]] \conv f} \cdot \paren[\big]{\frEl*[1,n_{1}+1][\ind[1][i]]\conv f}^{2}},\\
  & \textrm{ where}\phantom{\delta\paren[\big]{g}}  \delta\paren[\big]{g} \define \s*{x_{i}\in\setbuild[\big]{x}{g(x)=0}}{} \frac{1}{\abs{g'(x_{i})}}\delta(x-x_{i}).
\end{align}
This composed form of the delta function is derived in~\cite[Section 6.1]{hormanderAnalysisLinearPartial1998} or~\cite[Section II.2.5]{gelfandGeneralizedFunctionsProperties1964}; thankfully $g$, being a convolution with a smooth wavelet, is sufficiently well-behaved for this formula to apply.
The first term is much the same as for $n_{2}=1$, but the order of the first layer wavelet has been increased by 1.
The second term is perhaps more interesting; it only returns a value in the neighborhood of the zero crossings of the $n_{1}$th derivative of $f$.
Instead of evaluating the derivative at that point, however, we get the value of the $n_{1}+1$st derivative squared.
Depending on the value of $n_{1}$, particularly if $n_{1}=2$, this closely resembles the more traditional singularity detection and characterization methods described in \eg~\cite[Section 6.2]{mallatWaveletTourSignal2009}.

For $n_{2}=3$ the coefficients from the Bell polynomials begin to increase in complexity, and we have the first $\delta'$ term:
\begin{align}
  \label{eq:specificAbsoluteValue3}
  &\theta_{2}^{\ind[2][j]} \conv \sub[1]^{-3}R_{\sub[1]}\paren[\bigg]{ \sgn\paren[\big]{\frEl*[1,n_{1}][\ind[1][i]] \conv f} \cdot \frEl*[1,n_{1}+3][\ind[1][i]]\conv f +\\
  &\phantom{\theta_{2}^{\ind[2][j]} \conv R_{\sub[1]}\bigg(\sub[1]\sgn}3 \delta\paren[\big]{\frEl*[1,n_{1}][\ind[1][i]] \conv f} \cdot \paren[\big]{\frEl*[1,n_{1}+1][\ind[1][i]]\conv f} \paren[\big]{\frEl*[1,n_{1}+2][\ind[1][i]]\conv f} +\\
  &\phantom{\theta_{2}^{\ind[2][j]} \conv R_{\sub[1]}\bigg(\sub[1]\sgn3}\delta'\paren[\big]{\frEl*[1,n_{1}][\ind[1][i]] \conv f} \cdot 3\paren[\big]{\frEl*[1,n_{1}+1][\ind[1][i]]\conv f}^{2}\paren[\big]{\frEl*[1,n_{1}+2][\ind[1][i]]\conv f}}
\end{align}
which after some simplification, specifically using $\delta'(f)\cdot\varphi = \delta(f)\cdot\nf{\varphi'}{f'}$ (see~\cite[Section II.2.5]{gelfandGeneralizedFunctionsProperties1964}), becomes
\begin{align}
  \label{eq:specificAbsoluteValue3Redux}
  &\theta_{2}^{\ind[2][j]} \conv \sub[1]^{-3}R_{\sub[1]}\paren[\bigg]{ \sgn\paren[\big]{\frEl*[1,n_{1}][\ind[1][i]] \conv f} \cdot \frEl*[1,n_{1}+3][\ind[1][i]]\conv f +\\
  &\phantom{\theta_{2}^{\ind[2][j]} \conv R_{\sub[1]}\bigg(\sub[1]\sgn}6 \delta\paren[\big]{\frEl*[1,n_{1}][\ind[1][i]] \conv f} \cdot \paren[\big]{\frEl*[1,n_{1}+1][\ind[1][i]]\conv f} \paren[\big]{\frEl*[1,n_{1}+2][\ind[1][i]]\conv f} }.
\end{align}
Through some lucky cancellation, we have reduced from three to two terms.
The first resembles the sign term from the previous two, but with the derivative order increased yet again.
The second term evaluates at the same points as the second term of \cref{eq:specificAbsoluteValue2}, but the value now comes from both higher order derivatives.

Finally for $n_{2}\geq 2$, using the generalization of the derivative of a composition with the delta function in~\cite[Section II.2.5]{gelfandGeneralizedFunctionsProperties1964}, we have
\begin{align}
  \label{eq:specificAbsoluteValueK}
  &\theta_{2}^{\ind[2][j]} \conv \sub[1]^{-n_2}R_{\sub[1]}\paren[\Bigg]{\sgn\paren[\big]{\frEl*[1,n_{1}][\ind[1][i]] \conv f}\cdot \paren[\big]{\frEl*[1,n_{1} +n_{2}][\ind[1][i]] \conv f} +\\
  &\phantom{\theta_{2}^{\ind[2][j]} \conv \sub[1]^{-n_2}R} \delta\paren[\big]{\frEl*[1,n_{1}][\ind[1][i]] \conv f}\s{k=2}{n_{2}} \cdot \paren[\Bigg]{\frac{1}{\paren{\frEl*[1,n_{1}][\ind[1][i]] \conv f}} \frac{\dd}{\dd x}}^{n_{2}-2}B_{n_{2},k}\paren[\big]{\frEl*[1,n_{1}+1][\ind[1][i]]\conv f,\ldots, \frEl*[1,n_{1} +n_{2}-k+1][\ind[1][i]] \conv f}}
\end{align}
Which is, as may have been expected, rather a complicated formula.
Away from the key points where the first layer $\frEl*[1,n_{1}][\ind[1][i]] \conv f$ is zero, however, the much simpler and more consistent first term predominates.
This is just a smoothed version of the $n_{1}+n_{2}$th derivative with sign adjusted by the $n_{1}$th derivative.
In the neighborhood of the zeros of $\frEl*[1,n_{1}][\ind[1][i]] \conv f$, the actual value has a complicated dependence on the rest of the derivatives up to $n_{1}+n_{2}-1$.
If any of these are particularly large, as in the case of a loss of regularity, that term will dominate.
So one possible interpretation of the second term is as an indicator of points where we have both that the $n_{1}-1$st derivative has an extrema, and that $f$ loses regularity at some point before $n_{1}+n_{2}-1$.


In the case of real wavelets with vanishing moments, having a larger number of vanishing moments in the second layer leads to a broader set of derivatives that are picked up in the second layer coefficients.
The second layer coefficients can be viewed as evaluating the derivatives between $n_{1}$ and $n_{1}+2n_{2}-2$ at the extrema of $f^{(n_{1})}$, along with the $n_{1}+n_{2}$th derivative, with sign determined by that of the $n_{1}$th derivative.

\clearpage
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Dissertation"
%%% End:
