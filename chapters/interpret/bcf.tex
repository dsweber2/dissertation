We will consider the simple signal shape separation problem, which consists of 3 prototype signals of varying lengths and shifts with added white noise.
The classes are cylinder, bell, and funnel, which consist of signals that all start at some time $t_{0}$ and end at some time $t_{1}$~\cite{saitoLocalDiscriminantBases1995}.
Between these two points, they are each characterized by different behaviors: $\phantom{aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa}$
\begin{wrapfigure}[11]{o}{.5\textwidth}
  \centering
  \includegraphics[width=.5\textwidth]{figures/bcf/cbf.pdf}
  \caption{A couple of examples; $a=6$ and the noise has $\sigma=1$}\label{fig:cbf}
\end{wrapfigure}
\begin{itemize}
  \item Cylinder is just a characteristic function with height $a$ of the interval $[t_{0},t_{1}]$, or $a\chi_{[t_{0},t_{1}]}$.
  \item Bell is a ramp starting at $t_{0}$ with a quick die off at $t_{1}$, ending with a height of $a$, or explicitly: $a(t-t_{0})\cdot (t_{1}-t_{0})^{-1}\chi_{[t_{0},t_{1}]}$.
  \item Funnel is the ``inverse'' of the bell class, starting with a jump of $a$ at $t_{0}$ and slowly decaying to noise at $t_{1}$, or\\ $a(t_{1} - t) \cdot (t_{1}-t_{0})^{-1}\chi_{[t_{0},t_{1}]}$.
\end{itemize}
A list of all classifiers on the problem can be found at \href{https://timeseriesclassification.com/description.php?Dataset=CBF}{the timeseriesclassification repository}.\footnote{\href{https://timeseriesclassification.com/description.php?Dataset=CBF}{https://timeseriesclassification.com/description.php?Dataset=CBF}}
The best recorded accuracy on this problem in the \href{http://www.timeseriesclassification.com/dataset.php}{time series classification website}\footnote{\href{http://www.timeseriesclassification.com/dataset.php}{http://www.timeseriesclassification.com/dataset.php}} with trained with 30 examples is 99.81\%.

The classic version of this problem generates the signals in a way that correlates the time $t_{0}$ and the signal duration $t_{1}-t_{0}$.
Specifically, $t_{0}$ is uniformly random between $16$ and $32$, while $t_{1}$ is chosen between $32$ and $96$.
So in addition to this classic dataset, we will consider a rotated version, which sets the starting time $t_{0}$ independently of the length $t_{1}-t_{0}$ by circularly shifting the example by a uniformly chosen index after choosing $t_{0}$ and $t_{1}$ as above.

\subsection{Classification Results}
\label{sec:class-results}
As a baseline reference, multinomial lasso regression on the raw coefficients of 300 examples yields a test accuracy of 93.6\%, which puts it in a comparable class of problem difficulty to the MNIST dataset~\cite{lecunGradientbasedLearningApplied1998}.
On the other hand, for the rotated version, a raw coefficient classifier yields a test accuracy of 51.8\%, which is appreciably greater than the random guessing of 33\%, but because it isn't translation invariant, is still far from the best possible accuracy. For more classification results, see \Cref{tab:variousBcfResults}.
We can use this rotated dataset to test for the role of translation invariance; unlike the raw signal classifier, the AVFT has roughly comparable accuracy on both the classic (59.5\%) and rotated (57.0\%) versions of the dataset.
\begin{table}
  \centering
  \begin{tabular}{c||cc|cc}
    Transform Method & Classic & Rotated & 10 ex Classic  & 10 ex Rotated\\
    \hline\hline
    Raw data & \rawAccuracy & \rawRotAccuracy &\rawSmallAccuracy & \rawSmallRotAccuracy\\
    AVFT     & \AVFTAccuracy & \AVFTRotAccuracy & \AVFTSmallAccuracy & \textbf{\AVFTSmallRotAccuracy}\\
    St 1st DoG & \stLaySecdogFirstAccuracy & \stLaySecdogFirstRotAccuracy & \textbf{\stLaySecdogFirstSmallAccuracy} & \stLaySecdogFirstRotSmallAccuracy \\
    St 1st then 2nd DoG  & \stLaySecdogSecdogFirstAccuracy & \stLaySecdogSecdogFirstRotAccuracy & \stLaySecdogSecdogFirstSmallAccuracy & \stLaySecdogSecdogFirstRotSmallAccuracy\\
    St 2nd DoG     & \textbf{\stLaySecdogSecAccuracy} & \textbf{\stLaySecdogSecRotAccuracy} & \stLaySecdogSecSmallAccuracy & \stLaySecdogSecRotSmallAccuracy\\
  \end{tabular}
  \caption{Percent accuracy on a test set of 1000 entries per type (so 3000 total). The first two columns use an input of 100 signals per class, while the last two use ten examples. The best classifier in each column is in \textbf{bold}.}
  \label{tab:variousBcfResults}
\end{table}
More interesting for our purpose, however, is understanding how a classifier separates these examples.
As a brief reminder (and establishing some notation), multinomial lasso regression for a set of classes enumerated by $i = 1,\ldots,L$ chooses a set of weights $\vect{\beta}^{i}$ and offsets $a_{i}$ for each class $i$ and generates a probability distribution over the classes for an input vector $\vect{v}$ according to the soft-max
\begin{align}
p_{i} = \frac{\e^{a^{i} + \vect{\beta}^{i}\cdot\vect{v}}}{\s{j=1}{L} \e^{a^{j} + \vect{\beta}^{j}\cdot\vect{v}}}.\label{eq:softmax}
\end{align}
These regression weights $\vect{\beta}^{i}$ and the biases $a^{i}$ are given in \Cref{fig:rawbcfPredicts}.
To get the prior probabilities (the classification of the null signal $\vect{v} \equiv 0$) we compute $\e^{a^{i}} \paren[\big]{\sum\e^{a^{j}}}^{-1}$ which for \Cref{fig:rawbcfPredicts} is 94\% a bell, 6\% a funnel, and only \num{4e-4}{\%} a cylinder.\footnote{Quite literally a rounding error.}
 Accordingly, the actual coefficients for the bell example tell us what it is \emph{not}, while the coefficients for the cylinder generally tell us what it is.
 A bell is a signal that is \emph{not} active near the front (specifically around 35), while a cylinder is a signal that is somewhat active throughout the middle section of the figure.

As expected, the coefficients for the rotated dataset in \Cref{fig:rawbcfPredictsRot} are much less spatially concentrated; the cylinder coefficients are primarily picking up on the overall higher average value, whereas there is little to distinguish the bell from the funnel.

\begin{figure}[h]
  \begin{subfigure}{.49\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/bcf/rawMultinomialbcf.pdf}
    \caption{Classic bcf}\label{fig:rawbcfPredicts}
  \end{subfigure}
  \begin{subfigure}{.49\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/bcf/rawMultinomialbcfRot.pdf}
    \caption{rotated bcf}\label{fig:rawbcfPredictsRot}
  \end{subfigure}
  \caption{Regression weights for the bcf problem on raw data}
\end{figure}

\subsection{Scattering Transform Setup}
\label{sec:scatt-transf-setup}
What we are looking for is to replicate a similar sort of analysis on the scattering coefficient weights and biases used to classify this data.
 The crux of the analysis is going from regression weights to an input example.
 Before that, let us examine the choice of parameters used to get \Cref{tab:variousBcfResults}.

\begin{figure}[h]
  \centering
  \includegraphics[width=.75\textwidth]{figures/bcf/chosingDog2Wavelets.pdf}
  \caption{Choosing the wavelet frequencies to maximize the coverage of the data's mean in \cref{eq:meanData} and variation in \cref{eq:stdData}. See most of \Cref{sec:scatt-transf-setup} for a discussion.}
  \label{fig:freqStd_dog2}
\end{figure}

As the signals can be characterized using their discontinuities, we are best off choosing some variety of real wavelet; the DoG2 performs the best, though only in the rotated 300 training examples did it stand out from the other scattering transform results in \Cref{tab:variousBcfResults}.
To choose the proper wavelets, we first look at the positive frequency information in \Cref{fig:freqStd_dog2}.
The second plot gives the mean and standard deviation for each frequency across the entire 300 example training set, so if we have examples $\vect{f}^{1},\ldots, \vect{f}^{300}$ from every class, then this is a plot of
\begin{align}
  \label{eq:meanData}
  b_{k} &\define \frac{1}{300} \abs[\Bigg]{\s{j=1}{300} \FT\paren[\big]{\vect{f} - \mean[\big]{\vect{f}^{i}}}_{k}}\text{ for }k \geq 0\\
  o_{k} &\define \sigma\paren[\bigg]{\FT\paren[\big]{\vect{f}}_{k}^{i}}_{i} \text{ for }k \geq 0 \label{eq:stdData}
\end{align}
where in this case by $\FT$ we mean the discrete Fourier transform.
The first is equivalent to the magnitude of the mean for every entry except $b_{0}$, which is zero, and the second is simply the standard deviation of $\fhat{\vect{f}}^{i}$ across the examples $i$ (as opposed to the frequencies).
We have subtracted out the zero frequency in order to see the variation in the other coordinates, as it is significantly larger than all other terms, and will be covered by the father wavelet no matter our other choices.
Even after this, both the mean and standard deviation are quite low frequency, so we have set the averaging length as low as possible, and set the subsampling rate discussed in \Cref{sec:EffQual} to be $\beta=1$, so there are exactly $Q=4$ wavelets per octave.
This maximizes the number of wavelets at frequencies where the data actually varies, while not choosing the hyper-parameters based on class information.


The fourth figure is the mean and standard deviation across all signals in the first layer $\layer[][][1]$ using the settings from the first figure; it calculates the same quantities as \cref{eq:meanData} and \cref{eq:stdData}, except across both the examples $i$ and the different first layer frequencies, so the collection
\begin{equation}
  \label{eq:allTheDiscs}
  \discInX[\ind[1][1]][][\vect{f}^{1}],\ldots, \discInX[\ind[1][N]][][\vect{f}^{1}],\ldots, \discInX[\ind[1][1]][][\vect{f}^{300}],\ldots, \discInX[\ind[1][N]][][\vect{f}^{300}].
\end{equation}
is treated in the same way as the collection $\vect{f}^{1},\ldots,\vect{f}^{300}$ was in the second subplot.
The first layer internal coefficients have a more uniform spread of standard deviation, but still concentrates on the low frequency, so we also set the second layer $\beta=1$, $Q=4$.
Explicitly, as used by \href{https://gitlab.com/Sonar-Scattering/ScatteringTransform-jl}{ScatteringTransform.jl}\footnote{\href{https://gitlab.com/Sonar-Scattering/ScatteringTransform-jl}{https://gitlab.com/Sonar-Scattering/ScatteringTransform-jl}} the parameters are
\jlinl{cw=dog2}, \jlinl{Q=4}, \jlinl{β =1}, \jlinl{extraOctaves=0}, \jlinl{averagingLength=[-1.5,-1.5,2]}, \jlinl{outputPool=8},\newline
\jlinl{convBoundary=Periodic()}, and \jlinl{boundary=PerBoundary()}.


\begin{table}
    \begin{subtable}{.49\textwidth}
        \centering
        \input{figures/tables/stLay2_dog2.tex}
        \caption{Classic}\label{tab:confMatSynthCla}
    \end{subtable}
    \begin{subtable}{.49\textwidth}
        \centering
        \input{figures/tables/stLay2_dog2Rot.tex}
        \caption{Rotated}\label{tab:confMatSynthRot}
    \end{subtable}
  \caption{Confusion matrix for classifying the bcf problem using the DoG2 scattering transform.
 The accuracies are \stLaySecdogSecAccuracy\ and \stLaySecdogSecRotAccuracy}
  \label{tab:confMatSynth}
\end{table}

With these settings, using just the second layer on a new test set of 1000 examples per class, the confusion matrix is given in \Cref{tab:confMatSynth}; on the classic problem it is nearly diagonal, while on the rotated version of the data in \Cref{tab:confMatSynthRot}, it effectively separates out the cylinder from the bell and funnel, but is much less effective at separating the bell and funnel from each other.
On separating bell and funnel from cylinder, the accuracy is 96.8\%, while for just signals classified as bell and funnel, the accuracy at distinguishing these is just 60.3\%.
We used a similar method to select the DoG1 and mixed scattering transform parameters.

\subsection{Scattering Second layer Weights}
\label{sec:scatt-second-weights}
Assured that the second layer of the scattering transform does in fact do well at this problem, what are the weight vectors responsible for this?
In \Cref{fig:wrappedbcf}, we have a representation of the regression weights $\vect{\beta}^{i}$ as they correspond to the scattering domain, while in \Cref{fig:wrappedbcfRot} we have the same plot, but for the rotated version.
In the second layer, the weights have a spatial component as well as a two index path, so for example, the most negative cylinder weight for \Cref{fig:wrappedbcf} is at path $(\ind[2],\ind[1]) = (17.4\si{Hz}, 20.7\si{Hz})$ towards the front.
Here the saturation of the color corresponds to the (signed) magnitude of the largest magnitude coefficient at that path.

\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{figures/bcf/St2dog2Multinomial.pdf}
  \caption[some text]{The weights used to classify the classic bcf problem using the second layer scattering transform, as wrapped in a set of ST coefficients.
    The second layer coefficients for each path consist of vectors of length 7, with the paths are along the $x$- and $y$- axis.
    The color represents the maximum value in that coordinate, with blue representing negative values in each plot, and each signal class having a unique positive value (green for the cylinder, red for the bell, and orange for the funnel).
    The colors in the bias correspond to the positive coefficient color.
 Only non-zero regions with at least one non-zero value have a plot of the vector.}
  \label{fig:wrappedbcf}
\end{figure}
As we might expect with lasso, many paths are zero (and thus not plotted), and of those that are, most coefficients are zero.
Somewhat unusually, the largest magnitude coefficients are all negative, indicating that each class is best thought of as neither of the other two.
This will pose difficulties for interpreting the fit later.

The bell coefficients have the distinction of having a presence across all frequencies, with most paths having a single negative coordinate towards the front of the signal, while most of the funnel coefficients occur towards the rear at any given path, many of which are also negative.
The one strongly positive path for the bell, $(98.2\si{Hz}, 69.4\si{Hz})$, has a negative value followed by a positive value, which fits the general space features, while the largest values for the funnel occur towards the front.
In the classic case, a bell then is mostly a signal that doesn't have a spike at the front, while a funnel is one that doesn't have a spike towards the back.\\

Such a characterization won't work for the rotated version of the problem, where the either peak could be located anywhere in the signal, so the classifier must use the difference in slopes.
As we might expect, \Cref{fig:wrappedbcfRot} shows less spatial consistency for the fit coefficients.
The weight vectors are also considerably sparser, and the biases are almost exactly $-\nf{1}{10}$th as large (flipped and smaller).
The paths and time locations of the larger negative coefficients for the cylinder are more consistent between the rotated and non-rotated problem, suggesting that these are responsible for the higher level of accuracy separating the cylinder in \Cref{tab:variousBcfResults}.

\begin{figure}[ht]
  \centering
  \includegraphics[width=\textwidth]{figures/bcf/St2dog2MultinomialRot.pdf}
  \caption[some text]{The weights used to classify the rotated bcf problem using the second layer scattering transform, as wrapped in a set of ST coefficients.
  See \Cref{fig:wrappedbcf} for a description of the format.}
  \label{fig:wrappedbcfRot}
\end{figure}

On their own, these weight vectors aren't terribly informative, so we would like to see what kinds of inputs correspond most strongly to these regression vectors.
Give that with the absolute value as the nonlinearity the second layer coefficients are purely non-negative, fitting these coefficients exactly would be impossible.
That doesn't mean we can't find the point actually in the range nearest to the weight vector $\vect{\beta}$, however, as \cref{eq:coordinateFit} finds the point $\vect{f}$ so that the output is close to $\vect{\beta}$ in a pointwise $\lp*[2]$ sense.
So explicitly, the problem we're optimizing is
\begin{align}
  \label{eq:objFitBcf}
  \min_{\vect{f}}\exp\paren[\Bigg]{\log(1.1)\paren[\bigg]{\norm{\vect{f}}_{2}^{2}-\s{q\in\PathSet[2]}{}\ip{\discOut[q][x]}{\vect{\beta}^{i}}}}
\end{align}
which is equivalent to \cref{eq:actualOptim} with $\vect{y}[q] = \vect{\beta}^{i}$ for all paths in the second layer, $q \in \PathSet[2]$.
\clearpage
\begin{wrapfigure}[10]{o}{.5\textwidth}
  \centering
  \includegraphics[width=.5\textwidth]{figures/bcf/interpret/dog2/fitSignals.pdf}
  \caption{Pseudo-inverses of the lasso weight coefficients $\vect{\beta}^i$ for each class using only the second layer of a scattering transform using the 2nd DoG wavelets}\label{fig:bcfDog2Fit}
\end{wrapfigure}

\subsection{Fit Results}
\label{sec:fit-results}

In \Cref{fig:bcfDog2Fit} we have the pseudo-inverses $\vect{f}^{i}$ of the $\vect{\beta}^{i}$s in \Cref{fig:wrappedbcf}, where $\vect{f}^{i}$ is the solution to \cref{eq:objFitBcf} using $\vect{\beta}^{i}$ as the target.
It is fairly clear that the cylinder fit roughly corresponds to indicating the edges on both left and right, while the bell ramps up from left to right.
As an alternate view, in \Cref{fig:bcfDog2FitScalogram} we have the scalograms of these signals.
Here the cylinder clearly has large stretches of low frequency response with relatively sharp edges, while the funnel has more response towards the front.
The bell has a more clearly visible edge at approximately the correct time.
Both the bell and funnel fits have a more uniform high frequency response than might be ideal; this is most likely because setting \jlinl{averagingLength=[-1.5,-1.5,2]} results in a very wide averaging function.
\begin{figure}[h]
  \centering
  \includegraphics[width=.7\textwidth]{figures/bcf/interpret/dog2/fitSignalsScalograms.png}
  \caption{The scalograms of the Pseudo-inverses shown in \Cref{fig:bcfDog2Fit}}\label{fig:bcfDog2FitScalogram}
\end{figure}


\begin{figure}[!h]
  \begin{subfigure}{.89\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/bcf/interpret/dog2/fitSignalsSecondLayer.pdf}
    \caption{Second layer transform of the fit coordinates, color is on a log scale}\label{fig:bcfDog2FitSecondLayer}
  \end{subfigure}
  \begin{subfigure}{.89\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/bcf/St2dog2Multinomial.pdf}
    \caption{Previously shown weights from \Cref{fig:wrappedbcf} for ease of reference}\label{fig:bcfDog2FitSecondLayerTarget}
  \end{subfigure}
  \caption{Comparing fit with target second layer coefficients for the classic bcf problem using DoG2 wavelets}\label{fig:compareBcfDog2Fit}
\end{figure}
\FloatBarrier

To see how well these fits match the target output, see \Cref{fig:compareBcfDog2Fit}, which has the second layer ST coefficients of the fit solutions $\discOutX[\pat[2]][][\vect{f}^{i}]$ on the left in \Cref{fig:bcfDog2FitSecondLayer} and the target $\vect{\beta}^i$s in \Cref{fig:bcfDog2FitSecondLayerTarget}.
The fit has roughly the right frequencies for the cylinder, although the time distribution is wider than at the target paths, and the negative values in $\vect{\beta}^i$ at $(17.4\si{Hz}, 20.7\si{Hz})$ caused the second layer frequencies in the resulting fit to skew higher than they otherwise had.

The bell coefficients only vaguely resemble the target.
In part this is likely because the vast majority of coefficients are negative; even the path with the largest positive coefficient $(98.2\si{Hz}, 69.4\si{Hz})$ has a negative value.
Consequently, while this path and its neighborhood are somewhat present, the fit skews towards the far lower frequency path $(17.4\si{Hz}, 20.7\si{Hz})$.
The funnel coefficients most closely resemble the target, though there is some displacement caused by the negative frequencies again.

\begin{wrapfigure}[10]{o}{.5\textwidth}
  \centering
  \includegraphics[width=.5\textwidth]{figures/bcf/interpret/dog2Rot/fitSignals.pdf}
  \caption{Rotated bcf problem fit solutions using a second layer DoG transform}\label{fig:bcfDog2RotFit}
\end{wrapfigure}
In \Cref{fig:bcfDog2RotFit} we have the pseudo-inverses of the $\vect{\beta}^i$s in \Cref{fig:wrappedbcfRot}, the rotate version of the problem, and similarly \Cref{fig:bcfDog2RotFitScalogram} gives the scalograms of these fit solutions.
None of them are particularly clear in the time domain; in the scalogram, the cylinder fit has similar low frequency ``bridges'' as in \Cref{fig:bcfDog2FitScalogram}, but like the bell and funnel has much higher frequency represented throughout.
When comparing the second layer transformation of these coordinates in \Cref{fig:bcfDog2FitSecondLayerRot} with the targets in \Cref{fig:bcfDog2FitSecondLayerTargetRot}, the fit is somewhat closer than it was in the previous layer, at least in part because the targets are much sparser.
Roughly speaking, the bell signal is low in the first frequency and high in the second, while the funnel is high in both.
That this only somewhat distinguishes the signals explains the confusion matrix in \Cref{tab:variousBcfResults}.

\begin{figure}[h]
  \centering
  \includegraphics[width=.9\textwidth]{figures/bcf/interpret/dog2Rot/fitSignalsScalograms.png}
  \caption{Scalograms of the signals in \Cref{fig:bcfDog2RotFit} (the rotated bcf problem)}\label{fig:bcfDog2RotFitScalogram}
\end{figure}

\begin{figure}
  \begin{subfigure}{.89\textwidth}
    \centering
  \includegraphics[width=\textwidth]{figures/bcf/interpret/dog2Rot/fitSignalsSecondLayer.pdf}
    \caption{Second layer transform of the fit coordinates, color is on a log scale}\label{fig:bcfDog2FitSecondLayerRot}
  \end{subfigure}
  \begin{subfigure}{.89\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/bcf/St2dog2MultinomialRot.pdf}
    \caption{Previously shown weights from \Cref{fig:wrappedbcfRot} for ease of reference}\label{fig:bcfDog2FitSecondLayerTargetRot}
  \end{subfigure}
  \caption{Comparing fit with target second layer coefficients for the rotated bcf problem using DoG2 wavelets}\label{fig:compareBcfDog2FitRot}
\end{figure}

\FloatBarrier
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Dissertation"
%%% End:
