We start with the simplest possible version of \cref{eq:coordinateFit}: fitting a single coordinate with output location $\outLoc[k]$ at path $p$, or $\otherOut[p][y] = \vect{e}_{k}$, and the value in every other path is ignored.\footnote{We have also tried minimizing every other path simultaneously.
  However, if we apply the same pseudo-inversion technique to a wavelet transform, maximizing one output $\WT[f][\scaleLoc][\outLoc]$ and minimizing the rest results in an extremely oscillatory solution not at all like the original wavelet.
  See \Cref{sec:penal-minMax} for a discussion of why.}
In the case of a wavelet transform, a pseudo-inversion targeting the wavelet output at time $i$ and scale $j$ results in the corresponding wavelet of scale $j$ translated to the time $i$.
The zeroth layer in \Cref{sec:zeroth-layer-coord} is the most straightforward, as it simply returns the averaging wavelet $\father$.
One might expect the first layer, discussed in \Cref{sec:first-layer-coord} to resemble the wavelet transform for the first layer, but the averaging by $\father$, absolute value, and subsampling cause some unexpected changes.
The second layer in \Cref{sec:second-layer-coord} is the most interesting.

\begin{figure}[h]
  \centering
\includegraphics[width=.8\linewidth]{figures/interpret/singleFits/zerothMultiBestPlot.pdf}
  \caption{Comparing the fit solutions for the zeroth layer with the corresponding father wavelet.}
  \label{fig:fatherWav}
\end{figure}
\subsection{Zeroth Layer Coordinate}
\label{sec:zeroth-layer-coord}
The zeroth layer result is simply a subsampling of the averaging wavelet, so results in exactly recreating the father wavelet, as can be seen in figure \Cref{fig:fatherWav}.
Here we are maximizing the output location $\nf{8}{16}$, which in the input space corresponds to $60\si{ms}$, rather than the exact center $64\si{ms}$, which isn't in the output space because the output grid has an even 16 coordinates.
\subsection{First Layer Coordinate}
\label{sec:first-layer-coord}

\begin{figure}
  \centering
\includegraphics[width=.8\linewidth]{figures/interpret/singleFits/morl2Pi/poolBy1.5_outPool8_pNorm2_aveLen-1-12_extraOct0/lay1/multiBestPlot.pdf}
  \caption{Comparing the fit solutions (target frequency along the $y$-axis) with the corresponding Morlet wavelet with mother wavelet mean frequency $2\pi$ translated to the input time matching $\discOut[\lambda^{1}][7]$. The original wavelet is in blue while the fit solution is in orange.}
  \label{fig:morlTwoPiLay1Compare}
\end{figure}
\begin{figure}
  \centering
\includegraphics[width=.8\linewidth]{figures/interpret/singleFits/morl/poolBy1.5_outPool8_pNorm2_aveLen-1-12_extraOct0/lay1/multiBestPlot.pdf}
  \caption{Similar figure to \Cref{fig:morlTwoPiLay1Compare} but with a Morlet wavelet of mean frequency $\pi$ instead.}
  \label{fig:morlLay1Compare}
\end{figure}
As was noted in \Cref{sec:first-layer-grad} for the gradient, the first layer differs from a continuous wavelet transform through the absolute value, averaging, and father wavelet.
The results in that section suggest that we should expect the low frequency wavelets to simply maximize the corresponding wavelet coefficients, while as the frequency increases, and it is possible to fit more copies of the target wavelet within the envelope of the father wavelet, we should expect multiple, partially overlapping copies of the wavelet at the correct frequency.

In each of \Cref{fig:morlTwoPiLay1Compare}, \Cref{fig:morlLay1Compare}, \Cref{fig:dog1lay1Compare}, and \Cref{fig:dog2lay1Compare}, we compare the (sign corrected) fit solution (orange) at a centrally located output time (specifically output location $\nf{7}{11}$ with varying paths) and at each scale with the original wavelet (blue).
The first major difference from simply maximizing the corresponding wavelet coefficient is that absolute value eliminates the phase of the wavelet.
The output is $\father* \conv \abs{\frEl* \conv f}=\father* \conv \abs{\frEl* \conv -f}$, the sign of the input is irrelevant to the error, so the signs on the actually fit solutions are arbitrary; we have multiplied by $-1$ whenever the peak of the solution doesn't match that of the target wavelet for easier comparison between the original wavelet and the fit solution.
\begin{figure}
  \centering
\includegraphics[width=.8\linewidth]{figures/interpret/singleFits/dog1/poolBy1.5_outPool8_pNorm2_aveLen-1-12_extraOct0/lay1/multiBestPlot.pdf}
  \caption{Similar figure to \Cref{fig:morlTwoPiLay1Compare} but with a first derivative of a Gaussian wavelet instead.}
  \label{fig:dog1lay1Compare}
\end{figure}
\begin{figure}
  \centering
\includegraphics[width=.8\linewidth]{figures/interpret/singleFits/dog2/poolBy1.5_outPool8_pNorm2_aveLen-1-12_extraOct0/lay1/multiBestPlot.pdf}
  \caption{Similar figure to \Cref{fig:morlTwoPiLay1Compare} but with a DoG2 wavelet instead.}
  \label{fig:dog2lay1Compare}
\end{figure}

In the case of \Cref{fig:morlTwoPiLay1Compare} and \Cref{fig:morlLay1Compare}, we are comparing the real part of the target wavelet with the fit result, which is a purely real input.
This explains the solutions which are phase shifts of the corresponding wavelet, like $82\si{Hz}$ or $176\si{Hz}$ in \Cref{fig:morlTwoPiLay1Compare}, or most of the frequencies in \Cref{fig:morlLay1Compare}.
The effects of the averaging wavelet are more pronounced for the derivative of Gaussian wavelets in \Cref{fig:dog1lay1Compare} and \Cref{fig:dog2lay1Compare}, where at high frequency the fit solution is effectively an enveloped sinusoid.
The difference in how much the high frequency fits spread away from the corresponding wavelet corresponds to the width of the averaging wavelets in \Cref{fig:fatherWav}, with appreciable deviation for both the First and Second DoG wavelets in \Cref{fig:dog1lay1Compare} and \Cref{fig:dog2lay1Compare}, while both of the Morlet wavelets have significantly less spread.

Another somewhat unusual feature for the 2nd DoG wavelets is the offset of the peaks from the original peak.
The solution at many of the frequencies in \Cref{fig:dog2lay1Compare} is zero or nearly zero at 64 (where the central peak of the original wavelet is).
As with the wider spatial support of the solution, this effect kicks in more dramatically as frequency increases.
This is a second effect of the averaging wavelet, and may be caused by the ratio between the support of the wavelet at that scale and the averaging function.
Depending on the ratio, more copies of the wavelet may be fit by adjusting how well they line up with the target frequency.

\subsection{Second Layer Coordinate}
\label{sec:second-layer-coord}
The second layer fits are the first that differ quite fundamentally from the original wavelets.
In \Cref{fig:morlLay2compare} we have a collection of fit solutions using the Morlet wavelet with mean frequency $\pi$.
The plots are laid out on a grid, where the plot's $y$-axis location corresponds to the frequency used in the first layer, while the $x$-axis location corresponds to the frequency in the second layer.
To simplify interpretation, the output location is fixed to $\nf 37$ for all paths.

\begin{figure}[!h]
  \centering
\includegraphics[width=\linewidth]{figures/interpret/singleFits/morl/poolBy1.5_outPool8_pNorm2_aveLen-1-12_extraOct0/lay2/multiBestPlot.pdf}
\caption{Fitting all paths using Morlet wavelets of mean frequency $\pi$. Each subplot is a separate fit, which is 1 at exactly the output location $\nf{3}{7}$ and zero everywhere else.}
\label{fig:morlLay2compare}
\end{figure}
It is somewhat hard to discern what is going on in the spatial domain in \Cref{fig:morlLay2compare}, so instead we will look at scalograms with the same layout.
The scalograms in \Cref{fig:morlLay2compareScalogram} correspond to the plots in \Cref{fig:morlLay2compare}.
We use the same wavelet transform used in the scattering transform we are examining in that figure, and square the absolute value of the coefficients as in a standard scalogram.
For more examples of space domain plots, see \Cref{cha:AppendixFits}.


\begin{figure}[!h]
  \centering
\includegraphics[width=\linewidth]{figures/interpret/singleFits/morl/poolBy1.5_outPool8_pNorm2_aveLen-1-12_extraOct0/lay2/multiBestScalogramPlot.pdf}
\caption{The scalograms corresponding to the signals in \Cref{fig:morlLay2compare}.}
\label{fig:morlLay2compareScalogram}
\end{figure}
In \Cref{fig:morlLay2compareScalogram} it is clearer to see what is happening.
Consider the path with frequency 171Hz in the first layer and 88.4Hz in the second (on the middle right of the figure, it may help to zoom in quite a bit).
At the particular target frequency and neighboring frequencies, the response is oscillatory.
To more clearly see the effects of adjusting the frequency in either layer, compare the several figures in the upper left of the figure, with first layer frequencies between 203Hz and 238Hz, and second layer frequencies between 24.3 and 51.3.
Each of these figures roughly consists of 3 peaks.
As the first layer frequency increases (comparing plots vertically), the average frequency of the peaks increases (moves up \emph{within} a given plot).
As the second layer frequency increases (comparing plots horizontally), the distance between the peaks decreases.
So the second layer frequency corresponds to oscillations in the envelope for a particular frequency in the first layer.
The total number of oscillations increases as the second layer frequency does, \eg the two plots at second layer frequency 118Hz.


An inconsistent feature of these scalograms is the amount of response at frequencies above and below the first layer target frequency.
For \Cref{fig:morlLay2compare}, this happens consistently for first layer frequency above 24.2Hz and second layer frequency above 69.8Hz.

\begin{figure}
  \centering
\includegraphics[width=\linewidth]{figures/interpret/singleFits/morl2Pi/poolBy1.5_outPool8_pNorm2_aveLen-1-12_extraOct0/lay2/multiBestScalogramPlot.pdf}
\caption{Scalograms for fit signals with a Morlet mean frequency of $2\pi$.}
\label{fig:morl2PiLay2compareScalogram}
\end{figure}
Moving on to the other wavelets, the higher frequency Morlet wavelet in \Cref{fig:morl2PiLay2compareScalogram} follows similar patterns, although as expected the envelope is higher frequency.
This is particularly visible for the highest frequency in the first layer 323Hz and the lowest in the second layer.
On the other hand, the real wavelets in \Cref{fig:dog1Lay2compareScalogram} and \Cref{fig:dog2Lay2compareScalogram} generally don't follow this pattern.

\begin{figure}[h]
  \centering
\includegraphics[width=\linewidth]{figures/interpret/singleFits/dog1/poolBy1.5_outPool8_pNorm2_aveLen-1-12_extraOct0/lay2/multiBestScalogramPlot.pdf}
\caption{Scalograms for the first Derivative of Gaussian wavelets.}
\label{fig:dog1Lay2compareScalogram}
\end{figure}

For the case of the first DoG in \Cref{fig:dog1Lay2compareScalogram}, this is not surprising, as it is principally a first derivative detector.
We should expect to see a large response at the target frequency to the left of the center, and then a region that has zero response to the right, since after taking an absolute value, there are no negative values left.
This pattern is fairly clear in the first layer 24.7Hz second layer 23.9Hz figure, where there is even a secondary tail on the far right.
As the first layer frequency increases, more edges are included to increase the overall response within the positive window provided by the left half of the second layer wavelet.
Adjusting the second layer scale determines the width of response.
The lack of negative valued inputs to the second layer gives these plots a particularly lopsided response, with most of the response happening on the left half of the plot.

\begin{figure}[h]
  \centering
\includegraphics[width=\linewidth]{figures/interpret/singleFits/dog2/poolBy1.5_outPool8_pNorm2_aveLen-1-12_extraOct0/lay2/multiBestScalogramPlot.pdf}
\caption{Scalograms for the DoG2 wavelets.}
\label{fig:dog2Lay2compareScalogram}
\end{figure}
The DoG2 in \Cref{fig:dog2Lay2compareScalogram} is closer to the Morlet wavelet with mean frequency $\pi$ in \Cref{fig:morlLay2compareScalogram}.
The side lobes in the plots for the paths with first layer frequencies between 127--149Hz are significantly fainter than in the case for the morlet wavelets, but they are still clearly present, and for second layer frequency 75.5Hz they are much more clearly visible.
Unlike the case of the Morlet wavelets, there are no regions of high response surrounding the target wavelet; instead, the lines of wavelet maxima that are typical of wavelets with vanishing moments are much more clearly visible.
Luckily, wavelets with $n$ vanishing moments are more amenable to direct analysis, as will be discussed in the next section.

\subsection{Summary}
\label{sec:coord-fit-summary}
Much like in the case of the gradients, each of the layers has drastically different interpretation.
The zeroth layer coordinates are maximized simply by the original father wavelet, shifted appropriately.
The first layer coordinates overall maximizers most closely resemble the first layer gradient evaluated at the delta function; at low frequencies, they are simply the original wavelet, while as the scale decreases below the width of the father wavelet, they begin to produce many copies of the original wavelet, potentially with the sign flipped.
The second layer coordinates maximizers depend more on which kind of wavelet is used.
For the quasi-analytic Morlet wavelets, the maximizers are similar to the gradients; the overall envelope of the signal at the frequency of the first layer wavelet is the wavelet used in the second layer.
For real wavelets, the sign of the wavelets used in the second layer becomes more important, with the negative portions of the second layer wavelet corresponding to zeros of the fit solution.
\FloatBarrier
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Dissertation"
%%% End:
