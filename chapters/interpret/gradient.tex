\begin{wrapfigure}[9]{o}{.5\textwidth}
  \includegraphics[width=.5\textwidth]{figures/interpret/gradientAlone/gradientInputs.pdf}
  \caption{Examples}\label{fig:examples}
\end{wrapfigure}
One potential meaning of interpreting a transform is demonstrate how sensitive the output is to a given change in the input; this is a principal concern behind the subject of \emph{sensitivity analysis}~\cite{saltelliSensitivityAnalysisPractice2004}.
A simple way of doing this analysis is using the gradient information, and was one of the reasons behind the development of \emph{automatic differentiation}, a set of computational methods to compile a function and return its gradient~\cite{griewankEvaluatingDerivativesPrinciples2008}.
We can write a linear regression as $g(\vect{x}) = a + \vect{\beta} \cdot \vect{x}$, and in this case the sensitivity of $g(\vect{x})$ with respect to coordinate $x_{j}$ is simply $\beta_{j}$.
The sign of the $\beta_{j}$ tells us whether there is a positive or negative correlation with the output, while the relative magnitudes of the weights $\beta_{j}$ tells us the relative importance of the $x_{j}$.

Similarly, the gradient of the discrete wavelet transform at a particular output time and scale is just a translated and scaled copy of the mother wavelet.
Using notation from \Cref{sec:discrete-output}, the discrete equivalent of \cref{eq:WTdefinition} with $p=1$ for mother wavelet $\mother*$ at scales $\scaleLoc$ and output locations $\tau_{i}$ is given by
\begin{align}
  \label{eq:dwt}
  \WT[\vect{f}][\scaleLoc][\tau_{i}] = \scaleLoc^{-1}\s{l=0}{N-1}\inLoc[l] \mother*\paren[\bigg]{\frac{t_{l}-\tau_{i}}{\scaleLoc}}
\end{align}
So the derivative with respect to $\inLoc$ at scale $\scaleLoc$ and location $\tau_{i}$, evaluated at the input function $\vect{g}$ is given by
\begin{align}
  \label{eq:dwtGrad}
  \frac{\partial}{\partial \inLoc}\WT[\vect{f}][\scaleLoc][\tau_{i}]\Bigg|_{\vect{f}=\vect{g}} = \frac{\partial}{\partial \inLoc}\scaleLoc^{-1}\s{l=0}{N-1}\inLoc[l] \mother*\paren[\bigg]{\frac{t_{l}-\tau_{i}}{\scaleLoc}}\Bigg|_{\vect{f}=\vect{g}} = \scaleLoc^{-1}\mother*\paren[\bigg]{\frac{t_{j}-\tau_{i}}{\scaleLoc}} = \trans[\tau_{i}]\mother*_{\scaleLoc}(t_{j}),
\end{align}
which is simply a translated copy of the corresponding wavelet.
This tells us that, given that we can add a vector $\vect{x}$ of fixed magnitude to the initial signal $\vect{f}$, the most efficient way of increasing the value of the wavelet coefficient $\WT[\vect{f}][\scaleLoc][\tau_{i}]$ is to set $x_{j} = \trans[\tau_{i}]\mother*_{\scaleLoc}(t_{j})$, regardless of the signal $\vect{f}$.

Moving on to the ST coefficients, as the ST is nonlinear, the gradient is no longer independent of the input signal.
Instead of having output indexed just by scale and time, it is indexed by the layer $m$, then the path $\pat$, and finally the output location $\outLoc$ (see \Cref{sec:discrete-output} for a discussion of this notation).
The derivative with respect to changing $f_{j}$ at output location $i$ and path $\pat$, when evaluated at input $g$ is the Jacobian
\begin{align}
  \label{eq:jacob}
  \pdd{}{\inLoc} \discOut[\pat][i]\Bigg|_{\vect{f}=\vect{g}},
\end{align}
though we will for the most part consider a fixed output coordinate $\outLoc$ and a path $\pat$ to get the gradient $\grad_{\vect{f}} \discOut[\pat][i]\Big|_{\vect{f}=\vect{g}}$.
As noted above, the choice of input signal $\vect{f}$ matters for the case of the scattering transform, so we will look at a couple of different examples (see \Cref{fig:examples}).
Arguably the simplest example would be a constant function, but every path except the initial father wavelet is not dependent on the input signal's mean, so the gradient at a constant function for all other paths is simply zero.
Hence, instead of the constant function, we will use: a delta spike, arguably the next simplest spatial signal; the ``cylinder signal''  function, which is just a characteristic function and will return in \Cref{sec:sign-shape-class}; the ``bumps'' function described in \Cref{sec:WaveletExamples} and \Cref{sec:example-transforms}; a pure tone of 31.25\si{Hz}, which is a frequency delta spike; and finally white noise.

We find that as one may expect, as the depth increases, the degree of abstraction away from the used wavelets increases.
The zeroth layer in \Cref{sec:zeroth-layer} is simply the father wavelet.
The first layer in \Cref{sec:first-layer-grad} is still responsive to the wavelet at the output location and scale as in \cref{eq:dwtGrad}, but is also increasingly responsive to somewhat displaced copies of the wavelet.



% figuring out which order the examples are in
% c,d,b,s
% zero: d
% first: output: d,c,n,b,s
%      gradient: d, b,s
%      scalogram: d, b
% second: d, c, n, b, s
%         d, c, b, s, n

\subsection{Zeroth Layer}
\label{sec:zeroth-layer}
\begin{figure}[h!]
  \begin{subfigure}{.48\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/interpret/gradientAlone/zerothLayerDelta128unnormalized.pdf}
    \caption{Unnormalized}\label{fig:zeroDelta}
  \end{subfigure}
  \begin{subfigure}{.48\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/interpret/gradientAlone/zerothLayerDelta128normalized.pdf}
    \caption{Normalized}\label{fig:zeroDeltaNormalized}
  \end{subfigure}
  \caption{Comparing the zeroth layer derivative corresponding to the delta function in the normalized and unnormalized case. The horizontal axis varies with the gradient $\inLoc$, while the vertical axis in the heatmap corresponds to the output location $\outLoc$. The bottom line plot gives the derivative when $\outLoc = \nf{128}{256}$.}
\end{figure}
As the zeroth layer is a subsampled version of $\father[0]\conv f(x)$, one would expect (and get) just the father wavelet as a derivative $\grad_{\vect{f}}\discOut[\emptyset][i]\Big|_{\vect{f}=\vect{g}} = \trans[\subTime[i][0]']\father[0](t_{j})$, as in \Cref{fig:zeroDelta}, regardless of the target function $f$.
$\subTime[i][0]'$, as defined in \cref{sec:discrete-output} is the $i$th sample at the subsampled rate of the zeroth layer output.
For the DoG2 case shown here, $\trans[\outLoc]\father[0](t_{j})$ is just a shifted Gaussian.
Normalization as discussed in \Cref{sec:normal}, while useful for classification, can cause some counter-intuitive results, as in \Cref{fig:zeroDeltaNormalized}, where the derivative $\grad_{\vect{f}}\discOut[\emptyset][i]\big|_{\vect{f}=\vect{e}_{128}}$ is not just a translation as we change $j$.
The value of the output at the location of the delta spike (in \Cref{fig:zeroDeltaNormalized} this is at input index $j=1024$ and output index $i=128$) will actually decrease if the signal increases at any other point, as the fixed total mass is distributed over a larger area.
This results in the derivative at the center shown in the lower line graph of \Cref{fig:zeroDeltaNormalized} being strictly non-positive.
Any increase at a point other than $j=1024$ results in some other output coordinate increasing in value, which pulls some of the fixed mass away from $\discOut[\emptyset][128]$.
For the remainder of this section we will not discuss normalized examples, as they primarily serve to complicate the narrative and introduce spurious dependence between the coordinates.

\subsection{First Layer}
\label{sec:first-layer-grad}
For the first layer, we can directly compute the gradients, as the discrete output is $\discOut[\lambda][i] = \frac{1}{2K+1}\s*{k=-K}{K}\father[1]\conv\abs{\psi_{\lambda}\conv f}[i-k]$, where our averaging subsampling has window width of $2K+1$.
Thus, the derivative\footnote{Points where $\psi_{\lambda}\conv f = 0$ pose somewhat of a problem; for the numerical examples, we will choose the $\sgn$ function with $\sgn(0)=0$.} is
\begin{align}
  \label{eq:firstDerivative}
  \pdd{}{\inLoc} \discOut[\lambda][i]
  &= \pdd{}{\inLoc}\frac{1}{2K+1}\s*{k=-K}{K}\father[1]\conv\abs{\psi_{\lambda}\conv f}[i+k]\\
  &= \pdd{}{\inLoc}\frac{1}{2K+1}\s*{k=-K}{K}\s*{l_{1}=0}{N-1}\father[1][i+k-l_{1}]\abs[\bigg]{\s{l_{2}=0}{N-1}\psi_{\lambda}[l_{1}-l_{2}]f[l_{2}]}\\
  &= \frac{1}{2K+1}\s*{k=-K}{K}\s*{l_{1}=0}{N-1}\father[1][i+k-l_{1}]\sgn\paren[\big]{\psi_{\lambda}\conv f}[l_{1}] \psi_{\lambda}[l_{1}-j]\\
  &= \s{l_{1}=0}{N-1}\sgn\paren[\big]{\psi_{\lambda}\conv f}[l_{1}] \psi_{\lambda}[l_{1}-j]\frac{1}{2K+1}\s*{k=-K}{K}\father[1][i+k-l_{1}]\\
  &= \s{l_{1}=0}{N-1}\sgn\paren[\big]{\psi_{\lambda}\conv f}[l_{1}] \psi_{\lambda}[l_{1}-j]\widetilde{\father[1]}[i-l_{1}]\\
  &= \paren[\bigg]{\sgn\paren[\big]{\psi_{\lambda}\conv f}\psi_{\lambda}[ \cdot-j]} \conv \widetilde{\father[1]}[i]\label{eq:eval-at-output}\\
  &= \paren[\bigg]{\sgn\paren[\big]{\psi_{\lambda}\conv f}[-\cdot] \psi_{\lambda}[-\cdot] \conv \invol[\widetilde{\father[1]}][i-\cdot]}[j]\label{eq:eval-at-derivative}
\end{align}
where $\widetilde{\father[1]}[l] =\frac{1}{2K+1}\s*{k=-K}{K}\father[1][l+k]$ is the local average of $\father[1]$ around index $l$.
Either \cref{eq:eval-at-output} or \cref{eq:eval-at-derivative} could be considered as a simplified form.
\cref{eq:eval-at-derivative} has the advantage of being evaluated at the index $j$ of the gradient, while \cref{eq:eval-at-output} is a simpler expression.

So the gradient is \emph{a convolution of an averaged version of the involuted father wavelet centered at the output location with a possibly signed flipped version of the involuted target wavelet}.
Relative to the sign function, the output index $i$ determines the displacement of the averaging function, while the gradient index $j$ determines the displacement of the wavelet itself.
In the case of the input being a delta function at $l_{0}$, this is explicitly
\begin{align}
  \label{eq:1}
  \pdd{}{\inLoc} \discOut[\lambda][i] = \paren[\big]{\sgn(\psi_{\lambda})[\cdot-l_{0}]\psi_{\lambda}[\cdot-j]}\conv\widetilde{\father[1]}[i]
  = \paren[\bigg]{\sgn(\psi_{\lambda})[l_{0}-\cdot]\psi_{\lambda}\conv\widetilde{\father[1]}[i - \cdot]}[j],
\end{align}
 or the sign of the wavelet at one point, the value at another, and then smoothed.

\begin{figure}
  \includegraphics[width=\textwidth]{figures/interpret/gradientAlone/firstLayerSinusoidunnormalized32WaveletTransformed.pdf}
  \caption{The scalogram using the first layer wavelets of the gradient $\grad_{\vect{f}} \discOut[\lambda][32]\big|_{f=\sin}$ of a sinusoid of frequency 31.25Hz, centered at $\outLoc=\nf{32}{128}$.}\label{fig:sinusoidFirst}
\end{figure}
While the gradient evaluated at the delta spike has the simplest explicit form, the simplest example to understand is a pure-tone sinusoid of frequency 31.25Hz, whose scalogram is in \Cref{fig:sinusoidFirst}.
For wavelets that don't overlap the relevant frequency, the gradient is (nearly) zero, while for those that do, the response is at the frequency of the input, with an envelope dictated by the averaging function centered around the output location.

\begin{figure}[h!]
    \centering
    \includegraphics[width=\textwidth]{figures/interpret/gradientAlone/firstLayerDeltaunnormalizedLoc64.pdf}
  \caption{The gradient of the first layer for an input signal of a delta function as in \Cref{fig:examples} (located at $1024\si{ms}$, halfway through the signal). The output location whose gradient we are taking is also halfway through the signal, at output location $\nf{64}{128}$. Each separate sub-plot corresponds to a different path; in the first layer each path is uniquely identified by the corresponding frequency.} \label{fig:oneDelta}
\end{figure}
The gradient evaluated at the delta function in \Cref{fig:oneDelta} introduces more complexity.
At the low frequencies, up until $\slimsim 3.114\si{Hz}$, the gradient is simply the original wavelet.
For higher frequencies however, additional oscillatory features begin to appear.
The effect is most clear visually at $5.237 \si{Hz}$ and $7.406\si{Hz}$, where in addition to the central DoG2 wavelet, there are smaller copies surrounding it.
Beyond this point, the frequency becomes too high for the eye to discern the pattern.

\begin{figure}[h!]
  \begin{subfigure}{0.88\textwidth}
    \centering
  \includegraphics[width=\textwidth]{figures/interpret/gradientAlone/firstLayerDeltaunnormalized64WaveletTransformed.pdf}
    \caption{Scalogram of the gradient of the first layer evaluated at the delta spike.}\label{fig:waveletTransformedDeltaFirstLayer}
  \vspace{.4em}
  \end{subfigure}
  \begin{subfigure}{0.88\textwidth}
    \centering
    \includegraphics[width=\textwidth]{figures/interpret/gradientAlone/dog2WaveletsWaveletTransformed.pdf}
    \caption{Scalogram of the DoG2 wavelets for comparison.}\label{fig:waveletWaveletTransform}
  \end{subfigure}
  \caption{Comparing Scalograms. The subfigures correspond to paths in the same manner as in \Cref{fig:oneDelta}.}\label{fig:waveletTransforms}
\end{figure}
\FloatBarrier

To get a clearer look at this effect at high frequencies, it is most effective to compare the scalogram of the resulting signals as in \Cref{fig:waveletTransformedDeltaFirstLayer} with the scalogram of the original wavelets in \Cref{fig:waveletWaveletTransform}.
In addition to the central region matching the original wavelet, there are multiple fainter copies of this central wavelet in the immediate neighborhood of the main wavelet.
This arises because of the averaging wavelet, since the output $\discOut[\lambda][64]$ will increase if a copy of the wavelet is present anywhere within the support of $\father*[1]$, in proportion to the distance from the maximum.
The interval where $\father*[1]$ is greater than half maximum is $j=703$ to $j=1345$, out of 2048 entries, or approximately the center third of the signal.


\begin{figure}[h]
  \begin{subfigure}{\linewidth}
    \includegraphics[width=\textwidth]{figures/interpret/gradientAlone/firstLayerBumpsunnormalizedLoc64.pdf}
    \caption{unnormalized}\label{fig:bumpsUnnormal}
  \end{subfigure}
  \caption{The gradient evaluated at the bumps function in \Cref{fig:examples}}\label{fig:bumpsGradients}
\end{figure}
The gradient of the bumps function in \Cref{fig:bumpsGradients} is generally much sparser than \Cref{fig:oneDelta}, especially at high frequency, suggesting that the response at the delta function is much more sensitive to any change at all, whereas the response at the bumps function is only dependent on the behavior in the neighborhood of the already existing peaks.
As the frequency increases past $\slimsim 10.47\si{Hz}$, each peak contains a small copy of the corresponding wavelet, while as it decreases, the peaks continue with the expected joining of peaks as the frequency passes below the distance between the bumps, until at 1.31Hz they are all covered by the same wavelet~\cite{yuilleScalingTheoremsZero1986}.
A feature that does carry over is that the magnitude depends on the distance from the output location $\nf{64}{128}$.
This feature is somewhat obscured by the sparsity of the figure, but the magnitude of the wavelets at $24.91\si{Hz}$, for example, are determined by the distance from the central line, rather than the magnitude of the original signal or wavelet transform (compare with \Cref{fig:bumpsDog2}).

Finally, the gradient evaluated at the cylinder signal in \Cref{fig:cylinderGradients} shares many similarities with the gradient at the delta function in \Cref{fig:waveletTransformedDeltaFirstLayer}, in that there is a central spike with some activity around the edges.
The difference is most prominent beyond 14.81\si{Hz}; for the cylinder signal, the strength of the support remains stronger throughout the supported center third of the signal than it is for the delta function.
Increasing the response of a wavelet at either the beginning or the end of the interval is easier than for the delta function, which is 0 in the neighborhood of the corresponding locations.
\begin{figure}[h]
    \includegraphics[width=\textwidth]{figures/interpret/gradientAlone/firstLayercylinderunnormalized64WaveletTransformed.pdf}
  \caption{The scalogram of the gradient evaluated at the cylinder signal in \Cref{fig:examples}}\label{fig:cylinderGradients}
\end{figure}

\FloatBarrier
\subsection{Second Layer}
\label{sec:second-layer-grad}
Following a similar calculation as the first layer, if we have a path $p = (\ind[2],\ind[1])$, then the derivative of the second layer is
\begin{align}
  \label{eq:secondDerivative}
  \pdd{}{\inLoc} \discOut[p][i]
  &= \pdd{}{\inLoc}\sum_{k_{0}=-K_{0}}^{K_{0}}\father[2]\conv\abs[\Bigg]{\frEl*[2][\ind[2]] \conv \s{k_{1}=-K_{1}}{K_{1}}\abs[\Big]{\frEl*[1][\ind[1]]\conv f}\brac[\big]{\cdot+k_{1}}}[i+k_{0}]\\
  &= \widetilde{\father[2]} \conv \paren[\bigg]{\sgn\paren[\bigg]{\frEl*[2][\ind[2]] \conv \abs[\bigg]{\s*{k_{1}=-K_{1}}{K_{1}}\frEl*[1][\ind[1]] \conv f\brac[\big]{\cdot+k_{1}}}}\cdot\\
  &\hspace{45pt}\frEl*[2][\ind[2][2]]\conv \s{k_{1}=-K_{1}}{K_{1}}\sgn\paren[\big]{\frEl*[1][\ind[1]]\conv f}\brac[\big]{\cdot+k_{1}}\frEl*[1][\ind[1]]\big[\cdot+k_{1}-j\big]}[i]
\end{align}
which is not terribly enlightening, though it tells us that the input function $\vect{f}$ comes through in two sign functions: the first is the sign of the internal second layer term, while the second is similar to that in the first layer \cref{eq:firstDerivative}.

\begin{figure}[!h]
  \includegraphics[width=.75\textwidth]{figures/interpret/gradientAlone/dog2BumpsInputGradient.png}
  \caption{The gradients for each path, with the first layer varying along the vertical axis and the second layer varying along the horizontal axis. The output location is $\nf{16}{32}$ for every path. This is specifically the Bumps signal}\label{fig:BumpsInputGradient}
\end{figure}

For the second layer gradients, there are both common features and some major differences between the fit results.
The gradients of the bumps function in \Cref{fig:BumpsInputGradient} are the most different from the remaining figures, with a similar set of copies of the wavelet at each spike as in \Cref{fig:bumpsUnnormal}, however instead of uniformly decreasing in magnitude away from the center, the magnitude at each peak depends on the path, with the high/low frequency paths (such as $(1.102\si{Hz}, 140.9\si{Hz})$\footnote{following the convention of (second layer frequency, first layer frequency) established in \Cref{sec:example-transforms}} in the upper left, where this is most pronounced) having an oscillation in the magnitude of the response.

All of \Cref{fig:SinusoidInputGradient}, \Cref{fig:DeltaInputGradient}, \Cref{fig:CylinderInputGradient}, and \Cref{fig:NoiseInputGradient} on path $(1.102\si{Hz}, 99.64\si{Hz})$ have envelopes with a similar scale of variation and high internal frequency, though the exact shape of the envelope differs between them.
The envelope consists of 3 lobes, with the center lobe larger than either of the side lobes.
This is a reasonable description of the magnitude of the DoG2 wavelet itself.
Also worth noting is that these envelopes all decrease in scale as the second layer frequency increases (so along the top row), as we would expect if they correspond to the second layer wavelet.
A similar effect is happening in \Cref{fig:BumpsInputGradient}, but is somewhat obscured by the sparsity of the signal, so in effect we are only sampling from this envelope at the actual peaks in the signal.

\begin{figure}[!h]
  \includegraphics[width=.75\textwidth]{figures/interpret/gradientAlone/dog2SineInputGradient.png}
  \caption{This is specifically for the $31.25\si{Hz}$ Sinusoid, same format as \Cref{fig:BumpsInputGradient}}\label{fig:SinusoidInputGradient}
\end{figure}
\begin{figure}
  \includegraphics[width=.75\textwidth]{figures/interpret/gradientAlone/dog2DeltaInputGradient.png}
  \caption{Delta function, same format as \Cref{fig:BumpsInputGradient}}\label{fig:DeltaInputGradient}
\end{figure}
\begin{figure}
  \includegraphics[width=.75\textwidth]{figures/interpret/gradientAlone/dog2CylinderInputGradient.png}
  \caption{Cylinder Signal, same format as \Cref{fig:BumpsInputGradient}}\label{fig:CylinderInputGradient}
\end{figure}
\begin{figure}
  \includegraphics[width=.75\textwidth]{figures/interpret/gradientAlone/dog2whiteNoiseInputGradient.png}
  \caption{White noise, same format as \Cref{fig:BumpsInputGradient}}\label{fig:NoiseInputGradient}
\end{figure}



In the cylinder signal the two edges show up as spikes along the paths in the $4$th row from the bottom, with first layer frequency $3.114\si{Hz}$.
Somewhat more puzzling is the sign switch from the lowest frequency path $(1.102\si{Hz},1.102\si{Hz})$ to any of the other paths for the cylinder signal; at $(1.102\si{Hz},1.102\si{Hz})$, the central peak is positive, while for other low frequency paths where there is a central peak, it is negative.


The paths in the first row from the bottom, such as $(70.45\si{Hz}, 1.102\si{Hz})$, have almost identical responses to the first layer wavelets (compare \Cref{fig:BumpsInputGradient} and the low frequencies of \Cref{fig:bumpsUnnormal}, or \Cref{fig:DeltaInputGradient} with \Cref{fig:oneDelta}).

\subsection{Summary}
\label{sec:grad-summary}
So having examined the gradients from each layer, the kinds of signals that most increase the value of the ST coefficients in different layers differ dramatically.
For the zeroth layer coefficients, the most effective way to increase those coefficients is to simply add the original averaging function translated to that output location.
For the first layer coefficients, the most effective increase depends on the original signal.
It consists of adding copies of the original wavelets, located at or near the discontinuities already present in the signal, with the importance of different discontinuities modulated by the father wavelet located at the output location.
For the second layer coefficients, in the case of paths with decreasing frequency, the most effective way to increase the coefficients is to repeat copies of the first layer wavelet, with magnitude distributed according to the second layer wavelet.
It is difficult to interpret the paths with increasing frequency using the methods in this section.
We now turn to pseudo-inversion, where instead of seeking local methods of increasing the output for a particular coordinate, we seek signals which maximize a particular coordinate.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Dissertation"
%%% End:
