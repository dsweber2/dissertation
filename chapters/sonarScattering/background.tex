In this chapter, we begin the second half of the study promised in the title: sonar signal classification.
The bulk of the work was published previously in~\cite{saitoUnderwaterObjectClassification2017}.
In the wake of various conflicts, notably WWII, large quantities of unexploded ordinance have made their way to the seabed~\cite{kolbelDealingUXOUnexploded2015}.
In shallow waters, these explosives pose a risk to any ships traveling through the region.
However, the area to be surveyed is vast, so a relatively novel strategy involves deploying unmanned underwater vehicles (UUV) equipped with synthetic aperture sonar (SAS) to detect unexploded ordinance, such as the top left 3 items in \Cref{fig:stuff}.

\begin{figure}[ht]
  {\centering
  \includegraphics[width=0.65\textwidth]{images/TargetsInEnvironment.pdf}
  \caption{Example targets on shore and in the target environment\cite{karglAcousticResponseUnderwater2015}}\label{fig:stuff}}
\end{figure}
The standard approach to analyzing such SAS data is to use beamforming techniques to create an image that can be interpreted by the human eye\cite[Section 6.4]{ziomekIntroductionSonarSystems2016}.
However, in the case of UXO detection, the size of the objects is both too small and noisy (as these objects are often embedded in mud/sand/\etc) for the beamformed images to be interpretable.
Even if that were solved, there is also a problem of scale, as training is required to interpret beamformed images.

Thus begins the search to find an automated method of detecting UXOs directly from the raw signals.
This dissertation is not the first effort on the part of Professor Saito and his group towards detecting UXOs directly from sonar data~\cite{marchandClassificationObjectsSynthetic2007, lieuSignalEnsembleClassification2011}.
However, sonar data is naturally well suited to the benefits of the scattering transform.
When moving directly towards or away from an object, sonar data is translation invariant, and because of the underlying generation mechanism (see \Cref{sec:PDE-sig-gen} and \Cref{sec:geom}), we expect that as we move around an object the variations that occur can be characterized as the space/frequency deformation of \Cref{thm:shifts}.
So long as morphing via $f\paren[\big]{t-\tau(t)}$ from one class to another requires a $\tau$ with large derivative, then the classes will be well separated.
Accordingly, we use a linear classifier on the output of the scattering transform.
Additionally, because the scattering transform concentrates energy at coarser scales and wavelets in general encourage sparsity for smooth signals with singularities, we use a sparse version of logistic regression as our linear classifier, using LASSO~\cite{hastieStatisticalLearningSparsity2015}.
Specifically, we use a Julia wrapper around \glmnet\cite{friedmanRegularizationPathsGeneralized2010}.

As a baseline classifier, we use LASSO on the absolute value of the Fourier transform (AVFT) of the signal.
One strong advantage of the absolute value of the Fourier transform is its complete invariance to translations, and is the prototype of every linear filter which is invariant to translations, as discussed back in \Cref{sec:Four-Trans}.
In addition to this invariant, the close ties between frequency and the speed of sound suggest that it should be sensitive to changes in the material.
This will be examined in more depth in \Cref{sec:geom}.

For this problem, we have both real and synthetic examples.
The real examples, collected in the BAYEX14 dataset~\cite{karglAcousticResponseUnderwater2015}, consist of 14 partially buried objects at various distances and rotations in a shallow mud layer on top of a sand ocean bed at a depth of $\sim8$m.
To model a UUV, they then set up a sensor/emitter on a rail like the leftmost figure of \Cref{fig:geom}, and pinged the field of objects at various rotations.
So for each rotation of the object relative to the rail, there is a 2D wavefield.

\begin{figure}[ht]
\begin{subfigure}[t]{.33\textwidth}
    \includegraphics[width=\textwidth]{images/geometryBaseRect.pdf}
\end{subfigure}%
\begin{subfigure}[t]{.33\textwidth}
    \includegraphics[width=\textwidth]{images/triAlone.pdf}
\end{subfigure}
\begin{subfigure}[t]{.33\textwidth}
    \includegraphics[width=\textwidth]{images/sfAlone.pdf}
\end{subfigure}
    \caption{The three shapes of $\Omega$ used in the synthetic setting. The rectangle on the left includes the observation rail without rotation. The range is approximately $10\textrm{m}$, while the observation rail itself is  $12\textrm{m}$ in total. The triangle has side lengths of $1\textrm{m}$, while the shark fin is deformed from the same triangle. The rectangle has side lengths of $1\textrm{m}$ and $1.5\textrm{m}$.}
    \label{fig:geom}
\end{figure}

Additionally, we can generate synthetic waveforms using a fast solver for the Helmholtz equation in two regions with differing speed of sound, provided by Ian Sammis and James Bremer\cite{bremerFastDirectSolver2012, bremerNumericalEvaluationSingular2013}.
We use this to examine more closely the dependence of both classification and the output of the scattering transform on both material properties and shape variations.
The setup for the synthetic case is made to replicate the real case, and is in \Cref{fig:geom}. Further discussion of this process is in \Cref{sec:PDE-sig-gen}.

\Cref{sec:synth-class} gives the results of applying the ST and AVFT to the dataset to performing binary classification on shape and material in the synthetic case, while \Cref{sec:real-class} gives the results on a similar binary UXO/non-UXO classification.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Dissertation"
%%% End:
