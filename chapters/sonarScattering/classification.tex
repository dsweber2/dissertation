\begin{wrapfigure}[12]{o}{.5\textwidth}
	\begin{center}
      \includegraphics[width=.5\textwidth]{figures/tri2000simpleEx.png}
	\end{center}
	\begin{flushleft}
	  \caption{The ST coefficients for the synthetic triangle with speed of sound is $c_1=2000\si{m\per s}$, while the ambient speed of sound is $c_2=1503\si{m\per s}$. We use morlet wavelets, the output subsampling is only $10$ instead of the $80$ actually used in the classifier.}\label{fig:st}
	\end{flushleft}
\end{wrapfigure}
As a baseline to compare against, we use the same \glmnet\ logistic classifier on the absolute value of the Fourier transform (AVFT), which is a simple classification technique that is translation invariant and sensitive to frequency shifts.
To understand the generalization ability of the techniques, we split the data 10 times into two halves, one half training set and one half test set, uniformly at random, \ie 10-fold cross validation.
For the synthetic dataset, we also normalized the signals and added uniform Gaussian white noise so the SNR is 5dB.

For the synthetic data, there are two primary problems of interest.
The first is determining the effects of varying shape on the scattering transform.
An example of the 1D scattering transform for a triangle is in \Cref{fig:st}.
Since the energy at each layer decays exponentially with layer index, layers 1-3 have been scaled to match the intensity of the first layer.
Note that only the zeroth layer has negative values; this is because the nonlinearity used by the scattering transform is an absolute value.
In the figure, one can clearly see a time concentrated portion of the signal in layers 0, 1, and 2.

For the real dataset, the problem of interest is somewhat more ambiguous.
In addition to a set of UXO's and a set of arbitrary objects, there are some UXO replicas, not all of which are made of the same material.
As we will see in the synthetic case, the difference in the speed of sound has a much clearer effect on classification accuracy than shape, so it is somewhat ambiguous how to treat these.
We should expect that correctly classifying non-UXO's to be more difficult, since as a class they do not have much in common-- a SCUBA tank is much more similar, in both material and shape, to a UXO than to a rock.

\subsection{Synthetic Classification}
\label{sec:synth-class}

For the synthetic dataset, we compare three transforms.
The first is the absolute value of the Fourier transform (AVFT).
The second, which we will call the coarser ST, is a two layer scattering transform using Morlet Wavelets, with different quality factors rates in each layer: $Q_1 = Q_2 = Q_3 = 1$.
Increasing quality factor corresponds to decreasing the rate of scaling of the mother wavelet, and thus gives more coefficients for the same frequency regime.
The third, which we will call the finer ST, is another three layer scattering transform with increased quality in all 3 layers: $Q_1 = 8, Q_2 = 4, Q_3 = 4$.
For each of these, we use 10-fold cross validation to check the generalization of our results.


We investigate two of the problems from \Cref{sec:geom}: shape and material discrimination.
For the shape discrimination, we compare the triangle and the shark fin, with the material speed of sound fixed at $c_1=2000\textrm{m}/\textrm{s}$, while for the material discrimination, we fix $\Omega$ to be a triangle, and compare $c_1=2000\textrm{m}/\textrm{s}$ with $c_1'=2500\textrm{m}/\textrm{s}$.
To do this comparison, we use the received operator characteristic (ROC) curve, which compares the trade off between false positives and true positives as we change the classification threshold; since it strictly concerns one class, it is insensitive to skewed class sizes\cite{fawcettIntroductionROCAnalysis2006}.
A way of summarizing the ROC is the area under the curve (AUC), which simply integrates the total area underneath the curve; we use the trapezoidal approximation.
This varies from .5 for random guessing\footnote{Strictly speaking, values below .5 are possible for particularly terrible classifiers.} to 1 for the ideal classifier, which doesn't misclassify.

\begin{figure}[ht]
 {\center
 \includegraphics[width=.5\textwidth]{images/ROCmaterialDS.pdf}
 \caption{The ROC curve for detecting the material difference in a triangle, for speeds of sound $c_1=2000\textrm{m}/\textrm{s}$ and $c_1=2500\textrm{m}/\textrm{s}$. Note that the finer ST curve is an ideal classifier, completely in the upper left. The diagonal line is equivalent to random guessing.}
 \label{fig:ROCmat}}
\end{figure}
The results for material discrimination are in \Cref{fig:ROCmat}; the corresponding AUCs are $.99284$ for the AVFT, $.97778$ for the coarser ST, and $.99994$ for the finer ST.
Fitting with the basic derivation in the geometry \Cref{sec:geom}, even the AVFT is capable of discriminating material effectively.
Somewhat surprisingly, the coarser ST performs worse than the AVFT.
This is likely because of insufficient frequency resolution, which the finer ST is able to achieve.

\begin{figure}[ht]
	{\center
	\includegraphics[width=.5\textwidth]{images/ROCshapeDS.pdf}
	\caption{The ROC curve for discriminating a shark fin from a triangle where both have a speed of sound fixed at $2000\textrm{m}/\textrm{s}$.}
	\label{fig:ROCshape}}
\end{figure}

The results for shape discrimination are in \Cref{fig:ROCshape}, and are more definitive in demonstrating the effectiveness of the scattering transform.
The coarser ST, with an AUC of $.886$, outperforms the AVFT with an AUC of $.775$.
But the finer ST clearly outperforms both of these, with an AUC of $.998$, on par with the classification rates for the speed of sound problem.

\subsection{Real Classification}
\label{sec:real-class}

\begin{table}
	\begin{center}
    \caption{The objects in each class for the real dataset\label{tab:objects}}
    \begin{tabular}{|c|c|}\hline
        UXO-like & Other Objects\\ \hline
        155mm Howitzer with collar & 55-gallon drum, filled with water\\
        152mm TP-T & rock\\
        155mm Howitzer w/o collar & 2ft aluminum pipe\\
        aluminum UXO replica & Scuba tank, water filled\\
        steel UXO replica & \\
        small Bullet&\\
        DEU trainer (mine-like object)&\\\hline
    \end{tabular}
   \end{center}
\end{table}

\begin{figure}[ht]
	{\center
	\includegraphics[width=.5\textwidth]{images/ROCUXOSTAVFT.pdf}
	\caption{The ROC curve for detecting UXOs.}
	\label{fig:ROCreal}}
\end{figure}
For the real dataset, we compare two transforms, the AVFT and a two layer scattering transform with $Q_1=8,Q_2=1$.
We have split the data into a set of objects that are either UXOs or replicas, and a set of the other objects in the dataset, as listed in \Cref{tab:objects}.
In both classes, there are a variety of materials and shapes.
Between classes, there are no similar shapes (as the shape is what determines if it is a replica rather than a UXO), but there are two with the same material (aluminum UXO replica vs aluminum pipe).
The ROC curves are in \Cref{fig:ROCreal}.
The ST has an AUC of .9487, while the AVFT has an AUC of .8186.
The AVFT actually did better on this problem than it had on the shape detection problem, suggesting that it is primarily the material properties of the UXOs that distinguish them.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Dissertation"
%%% End:
