Ideally the invariants discussed above would apply to transformations in the \textit{object} domain rather than to signals.
But translations of the object (or equivalently, the observation rail), have a more complicated effect on the signal than simply translating the observation; even translating away from the object will cause a decay in signal amplitude in addition to delaying the response.
The changes in the object domain we seek to understand are changes in object material, translations and rotations of the object/rail, and changes in geometry.
A classifier for this problem should be invariant to translation and rotation, but sensitive to the geometry of the object and the material.
In this section we examine what can be said about the correspondence between object variations and scattering coefficient variations.
We begin with the what happens as we vary the speed of sound.

\subsection{Effects of the speed of sound}
\label{sec:eff-speed-sound}
To determine the behavior of a fixed location $\vect{x}_\theta(r)$ as we change the speed of sound, we use common acoustic properties such as reflection coefficients and Snell's law~\cite[Section 3.3]{saitoUnderwaterObjectClassification2017}.
The crudest possible assumption that still gives meaningful results is that internal angles are irrelevant, and only refraction, reflection, and average internal distance matters.
Going from $\Omega^c$ with speed of sound $c_2$ to $\Omega$ with speed of sound $c_1$, the reflection coefficient is given by $V_{2,1} = \frac{Z_2-Z_1}{Z_1+Z_2}$, while the refraction coefficient is $W_{2,1} = 1-V_{2,1} = \frac{2Z_1}{Z_1+Z_2}$, where the impedance is $Z_i =\rho_i c_i$, with $\rho_i$ the density of the material.
The distance from the center to a given point $x_\theta(r)$ on the line is just given by the Pythagorean theorem, $\sqrt{x^2+r^2}$.
This means the initial peak occurs at $\frac{1}{c_2}\sqrt{x^2+r^2}$.
\begin{figure}[ht]
  \centering
  \includegraphics[width=.75\textwidth]{images/speedDrawring.pdf}
  \caption[short?]{A motivating diagram for determining the prototypical response as a function of speed. $t_{1}$ and $t_{2}$ give the time until the first and second peaks arrive back at the receiver. Note that the direction of time is flipped between the signal going towards the object and towards the receiver.}
  \label{fig:proto}
\end{figure}
If the input peak has magnitude $A_0$, then the first return peak should be approximately $A_1 = V_{2,1}\frac{A_0}{x^2+r^2} = \frac{(Z_2-Z_1)}{(x^2+r^2)(Z_2+Z_1)}A_0$; since $Z_2 > Z_1$ for most relevant examples, this is positive.
Setting $\textrm{diam}(\Omega)=D$, the next peak can be approximated as $A_2 = W_{2,1}V_{1,2}W_{1,2}\frac{A_0}{(x^2+r^2)d} = \frac{4Z_2Z_1(Z_1-Z_2)} {(x^2+r^2)D(Z_1+Z_2)^3}A_0$, and the sign of this second peak will flip.
The third peak is $A_3 = W_{2,1}V_{1,2}^2W_{1,2}\frac{A_0}{(x^2+r^2)D^2}$ and will flip sign again.
Similarly $A_n= W_{2,1}V_{1,2}^n W_{1,2}\frac{A_0}{(x^2+r^2)D^n}$.

\begin{figure}[ht]
		{\centering
		\includegraphics[width=.75\textwidth]{images/comparingPeakOscilationsRect90.pdf}
	 \caption{Non-zero portion of the rectangle signal for varying speed of sound, when facing the long edge. \label{fig:rectLong}}
  }
\end{figure}

From this, we should expect that the decay from the first peak to the second peak, $\frac{4Z_1Z_2}{D(Z_1+Z_2)^2}$, is larger than that between any further consecutive peaks, $\frac{2|Z_1-Z_2|}{(Z_1+Z_2)D}$.
One can see the sign flip clearly in \Cref{fig:rectLong} (take care that the input signal (\Cref{fig:input}) is a positive spike followed by a negative spike, so the second peak begins at $\sim .75 \si{ms}$ for speed of sound $4000 \si{m \per s}$).

For classification purposes, for a fixed angle and position on the rail, the reflection of every peak but the first depends on both $D$ and the speed of sound $c_1$. Further, the time between peaks should be $2D/c_1$, so the scale of the solutions should provide a strong indicator of the speed of sound in the material.
We will indeed see that the absolute value of a Fourier transform (AVFT), which has access to scale information (as its inverse, frequency), is reasonably effective at separating shapes based the material speed of sound.



\subsection{Effects of rotation}
The previous section almost completely ignored the internal geometry of $\Omega$.
For a rectangle, when facing the longer side, this approximation is reasonable.
For other shapes or angles, however, Snell's law will both change the magnitude and the angle of the signal, and the problem of ray tracing in a region $\Omega$ becomes non-trivial in its own right, requiring averaging over all paths.


To avoid this, instead of trying to directly construct properties of the observations, we can derive how they will change under rotation and translation.
We can use the far field approximation to do so~\cite[Chapter 4]{etterUnderwaterAcousticModeling2018}.
Since $\textrm{diam}(\Omega)\approx 1$ and the center frequency of $s$ is $\omega_0 = 2500\textrm{Hz}$, we have that the observation rail at a range of $10\textrm{m}$ is much farther than $\frac{c_1}{\omega_0} \approx .6\textrm{m}$, the condition for far-field.
Here we take the solution as approximately separable, so $v(\omega,\vect{x})\e^{\im \omega P(\omega,\vect{x})}\approx R(\omega,r)\Theta(\omega,\theta)$.
The solution to the radial Helmholtz equation is a Bessel function of the first kind, and so to zeroth order is approximately $R(\omega,r)=J_0(k_2 r) \approx \frac{1}{\sqrt{k_2r}}\e^{\im k_2 r- \pi/ 4}$.

 % We will assume that the angular component $\Theta(\theta)$ has some Lipschitz constant $\gamma_\theta$.

% Along these lines, looking directly at the signals, when we change the material speed, the peaks are concentrated closer to the source as the speed increases. Additionally amplitudes are more positive (but not by the same amount at all points in time) as the speed increases. The speed of water is $1503\textrm{m}/\textrm{s}$. As one might expect, the smallest total energy occurs for $1500\textrm{m}/\textrm{s}$. $1000 \textrm{m}/\textrm{s}$ is broken, as are 500 and 1. Some angles of the shark fin, specifically $0$ (which is dead on a $\approx 45\degree$ ``angle'' of the shark fin) more closely resemble a triangle than they do other shark fins (somewhat more negative after the initial peak, and more sustained 2nd positive peak).

Using this, we can examine the effect of rotation of the object (or the rail about the object).
Suppose we know the solution at angles $\theta_{-1}$ and $\theta_{1}$, and we want to determine the solution at an angle $\theta_0$ between these.
Every point in $\vect{x}_0(q)$ will have the same angle as a point on either $\vect{x}_1(r)$ or $\vect{x}_{-1}(p)$ (or possibly both) if $\theta_{-1}$ and $\theta_{1}$ are close enough that the paths cross, such as the case in \Cref{fig:Rotation}.
This happens when $\tan\paren[\big]{\frac 12 (\theta_1-\theta_{-1})}< y/x$, or in the synthetic dataset, $\theta_1-\theta_{-1}\leq  \pi/ 6$).
Some geometry gives that the point $\vect{x}_0(q)$ has the same angle as $\vect{x}_1(r)$ if
\begin{figure}[ht]
	{\centering
  \includegraphics[width=.55\textwidth]{images/angleCoverage.pdf}
  \caption{Composing a rail observation along $\vect{x}_0(q)$ from that of $\vect{x}_{-1}(p)$ and $\vect{x}_1(r)$.\label{fig:Rotation}}
  }
\end{figure}


\begin{equation}
  T(q) = -x\tan\paren[\big]{\theta_1-\theta_0-\arctan( q/x)}\label{eq:rotRad}
\end{equation}
for $q>0$; similar reasoning works for $q<0$ and $\vect{x}_{-1}$ with flipped signs.
The distance changes from $\sqrt{x^2+q^2}$ to $\sqrt{x^2+T(q)^2}$, while the angle remains fixed, so plugging the zeroth order approximation to the Bessel function $J_0(k_2r)$ above into \cref{eq:recon},
\begin{align}
  f(t,\vect{x}_0(q)) &\approx \integral{-\infty}{\infty}{\fhat{s}(\omega) \frac{J_0\paren[\bigg]{k_2\sqrt{q^2+x^2}}}{J_0
		\paren[\bigg]{k_2\sqrt{T(q)^2+x^2}}} v\paren[\big]{\omega,\vect{x}_1\paren[\big]{T(q)}}\e^{-\im\omega \paren{t- P(\omega,\vect{x}_1(T(q)))}}}{\omega}\label{eq:full}\\
  f(t,\vect{x}_0(q)) &\approx \frac{\sqrt[4]{T(q)^2+x^2}}{\sqrt[4]{q^2+x^2}}
  \integral{-\infty}{\infty}{\fhat{s}(\omega) v\paren[\big]{\omega,\vect{x}_1\paren[\big]{T(q)}}\e^{-\im\omega \paren[\big]{t + h(q)- P(\omega,\vect{x}_1(T(q)))}}}{\omega}
\end{align}
where $h(q) =-\frac{1}{c_2}(\sqrt{q^2+x^2}-\sqrt{T(q)^2+x^2})$.
So there are two effects on $f$ in the zeroth order approximation.
The first is a phase shift by $h(q)$, under which both the AVFT and the ST are invariant.
The second is a small amplitude modulation (for $|\theta_1-\theta_0| = \pi/ 6$, this ranges from .94 to 1.077). However, since the error in this approximation is $\paren[\big]{\frac{1}{k x}}^{ 1/2}\approx .24$, we should only roughly expect this to hold, since most of the signals have amplitudes on the order of $.001$.
In the full case of \cref{eq:full}, we have a ratio of Bessel functions $A(q,\omega)\e^{\im \kappa(q,\omega)} \define \frac{J_0\paren[\big]{k_2\sqrt{q^2+x^2}}}{J_0\paren[\big]{k_2\sqrt{T(q)^2+x^2}}}$ whose argument depends linearly on the frequency $\omega$. If $A(q,\omega)\leq 1$, then this can definitely be written in the form of a non-constant frequency shift $\omega(t)$ as in \Cref{thm:shifts}.

\subsection{Effects of translation}

Increasing the distance $x$ to the object from $x_1$ to $x_2$ is a similar transformation to rotation, since every point on the new rail corresponds to a point on the old rail, but with increased radius.
If $r$ is the location on the new rail then the point with the same angle is just $T(r)=\frac{x_1}{x_2}r$; since $x_2>x_1$, this is smaller than $y$.
Then we have the same sort of derivation as above, with a strictly linear function instead of \cref{eq:rotRad}.

For translation along a given rail, the dependence on $\Theta(\theta)$ is unavoidable.
For a given point $x_\theta(r)$, the radius is simply $\sqrt{r^2+x^2}$, while the angle is $\varphi(r) = \theta + \arctan( r/x)$.
% \subsection{Within a rail}
% Using the far field approximation, we can also consider how signals change along a rail for a fixed rotation. This is much more sensitive to the particular details of the shape, since it will necessarily involve changing both the observed angle and radius. The angle for a given point $x_\theta(r)$ is $ P(r) = \arctan(\nf r x)$, while the radius changes to $\sqrt{r^2 + x^2}$, so with a slight abuse of notation,
%  writing $v$ in polar coordinates as $v(\omega, x_\theta(r))\e^{\im P(\omega,\vect{x})}=v(\omega,  P(r), \sqrt{r^2 + x^2})\e^{\im P(\omega,  P(r), \sqrt{r^2 + x^2})}\approx R(\omega,\sqrt{r^2 + x^2})\Theta(\omega, P(r))$, we have that
% \begin{align}
%  v\bigg(\omega,  P(r_1), \sqrt{r_1^2 + x^2}\bigg)-v\bigg(\omega,  P(r_2), \sqrt{r_2^2 + x^2}\bigg)
%     \abs{f(t,x_\theta(r_1))-f(t,x_\theta(r_2))} \leq \abs[\bigg]{\integral{-\infty}{\infty}{\fhat{s}(\omega)}{\omega}}
% \end{align}
% where we have used the Lipschitz property of $\Theta(\theta)$, and the

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Dissertation"
%%% End:
