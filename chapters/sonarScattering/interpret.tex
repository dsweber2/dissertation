In this section we compute the pseudo-inversion of the discrimination weights $\vect{\beta}$ used in a LASSO logistic regression for two layer classifiers for both speed and shape.
Because we have two class classifiers, there is only one vector $\vect{\beta}$, so unlike in \Cref{sec:fit-results}, maximizing the inner product corresponds to one class, while minimizing corresponds to the other.
For the speed classifier, for example, maximizing $\s{q}{}\ipll{\vect{\beta}}{\discOutX[q][][\vect{x}]}$ emphasizes features that signals coming from the $2000\si{m\per s}$ class have that those coming from the $2500\si{m\per s}$ don't have, while minimizing it emphasizes features corresponding to $2500\si{m\per s}$.

\subsection{Fitting the Speed Classifier}\label{sec:fit-speed-classifier}

In \Cref{fig:joint-plot-speed} we have the weights used to distinguish $2000\si{m\per s}$ signals from $2500\si{m\per s}$ ones.
The second layer weights are larger than either the first or zeroth layer weights, and somewhat surprisingly, the increasing paths such as $(31.0\si{Hz},12.3\si{Hz})$ and $(31.0\si{Hz},9.78\si{Hz})$ are more important for classification than the decreasing paths.

\begin{figure}[h]
  \centering
    \includegraphics[width=\textwidth]{figures/sonar/interpret/speedTri/jointPlotWeights.pdf}
    \caption{The Lasso logistic regression weights being fit for speed discrimination.
      The bias is $-2.05$, so the prior is $\slimsim 88.1\%$ that the signal has speed $2500\si{m\per s}$.
    }
  \label{fig:joint-plot-speed}
\end{figure}
\begin{wrapfigure}[18]{O}{.5\textwidth}
  \begin{subfigure}[h]{1.0\linewidth}
    \includegraphics[width=\textwidth]{figures/sonar/interpret/speedTri/bestExample6.pdf}
    \caption{Pseudo-inversion of $\vect{\beta}$}\label{fig:speed-best-positive}
  \end{subfigure}
  \begin{subfigure}[h]{1.0\linewidth}
    \includegraphics[width=\textwidth]{figures/sonar/interpret/speedTri/bestExampleFit25_1.pdf}
    \caption{Pseudo-inversion of $-\vect{\beta}$}\label{fig:speed-best-negative}
  \end{subfigure}
  \caption{Time domain pseudo-inverses of the speed classifier weights}\label{fig:speed-best}
\end{wrapfigure}
The proximity of the paths, frequencies, and times used to distinguish the two will prove somewhat difficult for creating a pseudo-inverse of the weight vector.
For example, in the first layer, the most important weight for the speed $2000\si{m\per s}$ is at $\slimsim 220\si{ms}$ and $15.5\si{Hz}$ while for the speed $2500\si{m\per s}$, the most important weight is at $\slimsim 190\si{ms}$ and $16.4\si{Hz}$, just 1 index in both space and frequency away.
Emphasizing the later and lower frequency response is exactly what we should expect to distinguish the signals generated from the $2000\si{m\per s}$ class from the $2500\si{m\per s}$ one based on our discussion in \Cref{sec:eff-speed-sound}.

The direct time display of both the pseudo-inversion of $\vect{\beta}$ and that of $-\vect{\beta}$ are in \Cref{fig:speed-best-positive} and \Cref{fig:speed-best-negative}.
Fitting $\vect{\beta}$ in \Cref{fig:speed-best-positive} corresponds to the coefficients with best discriminative power in favor of $2000\si{m\per s}$, while \Cref{fig:speed-best-negative} corresponds to the coefficients in favor of $2500\si{m \per s}$.
As might be expected, \Cref{fig:speed-best-positive} has much more prominent lower frequency, and even roughly has the expected shape of a typical example with either speed; initially noise, with a pulse, followed by a gap, and then trailing oscillations.
Unlike actual examples signals in, \eg, \Cref{fig:speed-compare}, the oscillations continue until the end of the signal.
This is most likely due to the high variation in where the actual signal occurs.
The pseudo-inversion of $-\vect{\beta}$ contains too much high frequency energy for the time domain version to be particularly informative.

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{figures/sonar/interpret/speedTri/compare2025EmphWithRandomExamples.pdf}
  \caption{Comparing randomly selected signals from each speed with the pseudo-inverses emphasizing each respective speed.
    The first 13 blue signals are random examples at speed $2500\si{m\per s}$, while the last 13 red signals are random examples at speed $2000\si{m\per s}$.
    The first red signal is the pseudo-inverse of $\vect{\beta}$ in \Cref{fig:joint-plot-speed}, which corresponds to maximizing the $2000\si{m\per s}$ class, while the last blue signal is the pseudo-inverse for $-\vect{\beta}$, which corresponds to maximizing the $2500\si{m\per s}$ class.}
  \label{fig:speed-compare}
\end{figure}

In \Cref{fig:scalogram-beta} and \Cref{fig:scalogram-min-beta}, we have the scalograms using the same wavelets of both fits.
Here, the represented frequencies of the pseudo-inverse of $-\vect{\beta}$ are more clearly visible, along with some spatial variation.
Particularly, there is a gap around $190\si{Hz}$ of \Cref{fig:scalogram-min-beta}.
The oscillations in the scalogram that characterize the single coordinate fits in \Cref{sec:second-layer-coord} are clearly visible in \Cref{fig:scalogram-min-beta}.

\begin{figure}[h]
  \begin{subfigure}{1.0\linewidth}
    \centering
    \includegraphics[width=.8\textwidth]{figures/sonar/interpret/speedTri/Scalogram6.pdf}
    \caption{Scalogram of the pseudo-inverse of $\vect{\beta}$ (maximizing $2000\si{m\per s}$)}
  \label{fig:scalogram-beta}
  \end{subfigure}
  \begin{subfigure}{1.0\linewidth}
    \centering
    \includegraphics[width=.8\textwidth]{figures/sonar/interpret/speedTri/ScalogramFit25_1.pdf}
    \caption{Scalogram of the pseudo-inverse of $-\vect{\beta}$ (maximizing $2500\si{m\per s}$)}
  \label{fig:scalogram-min-beta}
  \end{subfigure}
  \caption{Scalograms of the pseudo-inverses for the speed problem.}\label{fig:scalograms-speed}
\end{figure}

The scattering transform of the pseudo-inverses can be found in \Cref{fig:comp-fit-speed} and \Cref{fig:comp-fit-speed-neg}, where the color corresponds to the power on a log scale.
As expected, the first layer coefficients roughly correspond to the scalograms above, but with a lower time resolution.
Comparing the zeroth layer in \Cref{fig:comp-fit-speed} and \Cref{fig:comp-fit-speed-neg} shows that the pseudo-inverse of $\vect{\beta}$ has significantly more variation in the average value than the pseudo-inverse of $-\vect{\beta}$.
Comparing either with the target weights in \Cref{fig:joint-plot-speed} demonstrates the difficulty in maximizing coefficients for some paths while minimizing coefficients from adjacent paths, particularly when those paths are frequently quite small to begin with.
Doing so frequently means maximizing the value at a distant path.
To maximize the result at $(31.0\si{Hz},12.3\si{Hz})$ and $(31.0\si{Hz},13.8\si{Hz})$ in \Cref{fig:comp-fit-speed}, the paths with the same first layer frequency and lowest second layer frequency, that is $(9.74\si{Hz},12.3\si{Hz})$ and $(9.74\si{Hz},13.8\si{Hz})$ have the actually largest values, even though the weights in \Cref{fig:joint-plot-speed} at these paths is zero.

\begin{figure}[h]
  \begin{subfigure}[h]{1.0\linewidth}
    \centering
    \includegraphics[width=.922\textwidth]{figures/sonar/interpret/speedTri/jointPlot6.pdf}
    \caption{ST coefficients of the pseudo-inverse of $\vect{\beta}$ (so maximizing the red paths and minimizing the blue paths in \Cref{fig:joint-plot-speed}).\vspace{10pt}}
    \label{fig:comp-fit-speed}
  \end{subfigure}
  \begin{subfigure}[h]{1.0\linewidth}
    \centering
    \includegraphics[width=.922\textwidth]{figures/sonar/interpret/speedTri/jointPlotFit25_1.pdf}
    \caption{ST coefficients of the pseudo-inverse of $-\vect{\beta}$ (so maximizing the blue paths and minimizing the red paths in \Cref{fig:joint-plot-speed})\vspace{-8pt}}
    \label{fig:comp-fit-speed-neg}
  \end{subfigure}
  \caption{ST coefficients of the pseudo-inversion of the speed classifier weights}\label{fig:joint-plot-speed-both}
\end{figure}

For the speed classifier, the primary difference is one of frequency, along with more variation in the later signal for the speed $2500\si{m\per s}$ signals.
\FloatBarrier
\subsection{Fitting the Shape Classifier}
\label{sec:fit-shape-classifier}
The coefficients used for the shape classifier are in \Cref{fig:joint-plot-shape}.
Compared to the speed case, there is more emphasis on the second layer coefficients.
As in the speed case, the weights are skewed towards increasing paths.
Perhaps the strongest difference is that the paths $(13.8\si{Hz}, 12.3\si{Hz})$ and $(15.5\si{Hz}, 12.3\si{Hz})$ are both quite large in magnitude, which may cause some difficulties in finding the pseudo-inverse of this classifier.



\begin{figure}[h]
  \centering
    \includegraphics[width=\textwidth]{figures/sonar/interpret/triSf/jointPlotWeights.pdf}
    \caption{The Lasso weights being fit for shape discrimination.
      The bias is $0.67$, so the prior is $\slimsim 66.1\%$ that the signal is a triangle.
    }
  \label{fig:joint-plot-shape}
\end{figure}

\begin{figure}[h]
  \begin{subfigure}[h]{0.48\linewidth}
    \includegraphics[width=\textwidth]{figures/sonar/interpret/triSf/bestExample1.pdf}
    \caption{Pseudo-inversion of $\vect{\beta}$}\label{fig:shape-best-positive}
  \end{subfigure}
  \begin{subfigure}[h]{0.48\linewidth}
    \includegraphics[width=\textwidth]{figures/sonar/interpret/triSf/bestExampleFit25_1.pdf}
    \caption{Pseudo-inversion of $-\vect{\beta}$}\label{fig:shape-best-negative}
  \end{subfigure}
    \caption{Single space domain example of the best Pseudo-inversion of $\pm\vect{\beta}$}\label{fig:shape-best}
\end{figure}
The resulting pseudo-inverses are in \Cref{fig:shape-best-positive} and \Cref{fig:shape-best-negative}.
A couple of features to note: the sharkfin pseudo-inverse is strictly positive, with a depressed period roughly in the region where the signals tend to be active, between $\slimsim 190-500\si{ms}$.
While it is consistently oscillatory, the triangle has a lower frequency response in a subset of that range, $\slimsim 190-350\si{ms}$.
The most striking feature of the sharkfin pseudo-inverse is the peak at $\slimsim 500\si{ms}$.
We can take this as some indication that the sharkfin response tends to both be more positive on average, and have sharper transitions.
We can somewhat see this in \Cref{fig:shape-compare}, where the period where the Sharkfin is negative is generally of shorter duration and sharper.

% A feature that is visible in the sharkfin examples in \Cref{fig:shape-compare} and the pseudo-inverse of $-\vect{\beta}$ (in either that figure or \Cref{fig:shape-best-negative}) is the pair of.
% This is actually the response that is predicted by examining the rectangle in \Cref{sec:eff-speed-sound}, but is not present in the triangle examples in \Cref{fig:shape-compare} (or the similar figure \Cref{fig:speed-compare}).
% The edges of the triangle, because they are straight, are never parallel, so the sound never takes a direct path into and out of the object.
% However, in the case of the sharkfin, because the edges are curved, a wider range of angles are possible.
\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{figures/sonar/interpret/triSf/comparetriSfEmphWithRandomExamples.pdf}
  \caption{Comparing randomly selected signals from each shape with the pseudo-inverses emphasizing each respective shape.
    The first 13 blue signals are random sharkfin examples, while the last 13 red signals are random triangle examples.
    The first red signal is the pseudo-inverse of $\vect{\beta}$ in \Cref{fig:joint-plot-shape}, which corresponds to maximizing the triangle class, while the last blue signal is the pseudo-inverse for $-\vect{\beta}$, which corresponds to maximizing the sharkfin class.}
  \label{fig:shape-compare}
\end{figure}
\clearpage
In \Cref{fig:scalogram-shape-beta} and \Cref{fig:scalogram-shape-min-beta} we have scalograms of the pseudo-inverses.
The triangle fit in \Cref{fig:scalogram-shape-beta} is concentrated around frequency $13.8\si{ms}$, with a spatial gap at around $\slimsim 250\si{ms}$.
There is more activity at all frequencies for the sharkfin in \Cref{fig:scalogram-shape-min-beta}.
The oscillations in the scalogram that are characteristic of high (first layer) frequency second layer coefficients is visible at $34.8\si{Hz}$ throughout, though with a distinct gap again at $\slimsim 200\si{ms}$.
The jump at $\slimsim 500\si{ms}$ is part of a low frequency response that continues in that region.
One conclusion that we can draw from this is that the sharkfin has more variation at both high and low frequency across different examples.
\begin{figure}[h]
  \begin{subfigure}{0.65\linewidth}
    \centering
    \includegraphics[width=1.0\textwidth]{figures/sonar/interpret/triSf/Scalogram1.pdf}
    \caption{Scalogram of the pseudo-inverse of $\vect{\beta}$ (maximizing the triangle).}
  \label{fig:scalogram-shape-beta}
  \end{subfigure}
  \begin{subfigure}{0.65\linewidth}
    \centering
    \includegraphics[width=1.0\textwidth]{figures/sonar/interpret/triSf/ScalogramFit25_1.pdf}
    \caption{Scalogram of the pseudo-inverse of $-\vect{\beta}$ (maximizing the sharkfin).}
  \label{fig:scalogram-shape-min-beta}
  \end{subfigure}
  \caption{Scalograms of the pseudo-inverses for the shape problem.}\label{fig:scalograms-shape}
\end{figure}


The scattering transform of the pseudo-inverses can be found in \Cref{fig:comp-fit-shape-pos} and \Cref{fig:comp-fit-shape-neg}.
As may have been expected by the lower importance of the first layer coefficients for the classifier, the resulting ST coefficients for both directions have a stronger response in the second layer, with the sharkfin having a significantly stronger response in the second layer, to the point that it is difficult to actually see the difference in magnitude.
There is significant variation between different paths in the second layer for the sharkfin.
In contrast, the triangle has an almost uniform second layer response when the first layer frequency is $15.5\si{Hz}$.
Comparing the actual smallest and largest locations in either \Cref{fig:comp-fit-shape-pos} and \Cref{fig:comp-fit-shape-neg} with the target \Cref{fig:joint-plot-shape} shows that in order to maximize these particular locations resulted in completely different coordinates also increasing.
For \Cref{fig:comp-fit-shape-pos}, maximizing both $(13.8\si{Hz}, 12.3\si{Hz})$ and $(12.3\si{Hz}, 11.0\si{Hz})$ while minimizing $(15.5\si{Hz}, 12.3\si{Hz})$ results in a large portion of the bottom left corner having a relatively large value.

\begin{figure}[h]
  \begin{subfigure}[h]{0.8\linewidth}
    \centering
    \includegraphics[width=\textwidth]{figures/sonar/interpret/triSf/jointPlot1.pdf}
    \caption{ST coefficients of the pseudo-inverse of $\vect{\beta}$ (so maximizing the red paths and minimizing the blue paths in \Cref{fig:joint-plot-shape}).}
    \label{fig:comp-fit-shape-pos}
  \end{subfigure}
  \begin{subfigure}[h]{0.8\linewidth}
    \centering
    \includegraphics[width=\textwidth]{figures/sonar/interpret/triSf/jointPlotFit25_1.pdf}
    \caption{ST coefficients of the pseudo-inverse of $-\vect{\beta}$ (so maximizing the blue paths and minimizing the red paths in \Cref{fig:joint-plot-shape})}
    \label{fig:comp-fit-shape-neg}
  \end{subfigure}
  \caption{St coefficients of the pseudo-inversion of the shape discrimination weights.}\label{fig:comp-fit-shape}
\end{figure}

\subsection{Summary}
\label{sec:sonar-fitting-summary}
As might be expected based on the target coefficients being distributed across all layers, the pseudo-inverses of both the speed and shape classifiers have features present in fitting the single coordinates in the zeroth, first and second layers, such as the large scale variation of the zeroth layer coefficients, or the oscillations in the scalogram characteristic of the second layer coefficients.
For the pseudo-inverse of the $2000\si{m\per s}$ speed classifier, this results in a signal that bears some resemblance to the target class signals.
More often, the resulting pseudo-inverse has exaggerated versions of the distinguishing features for each class, such as the relative frequency difference that discriminates the speed of sound amplified in \Cref{fig:scalograms-shape}.
This is a natural consequence of using logistic regression, which focuses on coefficients which best and only characterize the difference between the two classes.
A reasonable next step would be finding the pseudo-inverses for linear discriminant analysis weights.
In the speed case, the distinguishing feature is principally the separation between the initial peak and the following peaks, and is characterized by which of the low frequency second layer paths are largest.
Additionally, the tail of second, third, \etc, signals have significantly higher frequency for $2500\si{m\per s}$ signals.
The pseudo-inverse corresponding to the sharkfin is more positive overall, but the troughs are sharper.
\FloatBarrier
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Dissertation"
%%% End:
