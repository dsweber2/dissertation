# chapter 2
include("scriptsByChapter/chapter2.jl")
# chapter 3
include("scriptsByChapter/chapter3.jl")
# chapter 4
# plots 4.1-4.12
include("scriptsByChapter/chapter4/section1.jl")
# purely generating data
include("scriptsByChapter/chapter4/section2Data.jl")
# plots 4.13-4.22, using the data from the previous file
include("scriptsByChapter/chapter4/section2Plots.jl")
# creates classifiers and generates
# plots 4.24, 4.26, and 4.27, and all of the classifiers in tables 4.1 and 4.2
include("scriptsByChapter/chapter4/classifierGeneration.jl")
# section 4's equivalent of section2Data
include("scriptsByChapter/chapter4/section4Fitting.jl")
# plots 4.23, 4.25, 4.28, 29, 30(a), 31, 32, and 33(a)
include("scriptsByChapter/chapter4/section4.jl")
