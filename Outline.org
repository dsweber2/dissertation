Should generally have 3 parts. I'm thinking that Sonar Scattering should be the
last section, or partially integrated with the others.
* Introduction
The order is a bit hard to pin down. Do I want to talk about the scattering transform, then dive into the theory for each constituent part?
Or describe Wavelets, Shearlets, etc first, and then refer back to those sections?
This problem also comes up for interpretation, and whether I want to have a separate background section about it at all.
Given the length of previous discussion, probably best not

Maybe have a running transform of e.g. the sinc/spikes and
** The Fourier Transform and Linear Feature Extractors
- [X] FT definition
- [X] Distributional derivatives and extending the FT to all functions (moved to a later section) cite:follandFourierAnalysisIts1992
- [X] Tempered distributions (treat all functions in sight as tempered distributions unless explicitly mentioned)
- [X] translation invariance
- [X] convolution theorem
- [ ] fft
- [ ] plancherel theorem (?)
- [X] time derivative -> frequency monomial
- [X] relation between decay and continuity via derivatives \cite[theorem 7.5(c)]{follandFourierAnalysisIts1992}
- [ ] Fourier Series definition, similarly \cite[theorem 2.6]{follandFourierAnalysisIts1992} for the Fourier series
** Continuous Wavelet transforms
\cite[1.3.1]{daubechiesTenLecturesWavelets1992a}
\cite[chapter 2]{daubechiesTenLecturesWavelets1992a}
\cite[chapter 3-6]{kaiserFriendlyGuideWavelets1994}
\cite[4.3-4, ]{mallatWaveletTourSignal2009}
\cite[chapter 11]{christensenIntroductionFramesRiesz2016}
- [X] Calderon condition, zero mean mother wavelet \cite[prop 2.4.1]{daubechiesTenLecturesWavelets1992a}
- [X] nested wavelet families (multiresolution analysis) and the father wavelet
- [ ] creating continuous wavelets from orthogonal wavelets
- [ ] practical inversion and frames (not actually used, though it is in Wavelets.jl) so maybe not necessary
- [X] resolution of the identity (inversion)
- [X] uniform coverage in frequency and practical concerns
- [X] translation covariance (reference discrete wavelets)
- [ ] relation to the short-time/windowed fourier transform (and a citation to the stft scattering transform)
- [ ] redundant discrete system/frame
  + [ ] definition
  + [ ] comparison with bases
  + [ ] $A$ and $B$ as the min/max of the singular values of the frame operator $\Phi^*\Phi$
  + [ ] inversion and dual frames, this being nearly the same for wavelets (specifically the dual weights) 5.3 infinite left inverses, 5.4 canonical one pseudo inverse/dual frame
  + [ ] 12.1 the inverse also has the wavelet structure
  + [ ] 5.3.1 frame stays a frame under BLOs
  + [ ] translation invariant frames and equivalent fourier condition
- [X] scalograms
** Distributional Derivatives and vanishing moments
\cite[6]{mallatWaveletTourSignal2009}
cite:follandFourierAnalysisIts1992,gelfandGeneralizedFunctionsProperties1964
- [X] relation between decay and continuity \cite[chapter 6]{mallatWaveletTourSignal2009}
- [ ]  (mention shearlets and shattering)
** Wavelets
   Mallat, others on [[https://www.math.ucdavis.edu/~saito/courses/ACHA.w18/refs.html][Saito's course webpage]]
   Ten lectures on Wavelets (Daubichies)
*** Morlet Wavelets and (pseudo)-analytic wavelet
cite:lillyHigherOrderPropertiesAnalytic2009 and cite:lillyAnalyticWaveletTransform2010, also see [[file:~/allHail/projects/ScatteringTransformExperiments/notes/analyticSignals.org::*What do analytic wavelets do in general?][What do analytic wavelets do in general?]]
*** Vanishing moments
See chapter 6 of cite:mallatWaveletTourSignal2009
*** Discussion of 2D version and the difficulty of the type of singularities
** Scattering Transform
*** Generalized (mostly done)
*** Mallat's (could use more fleshing out of the extra results)
*** Discretization(?) (not at all)
*** Our specific implementation details (very minimal)
*** Waldspruger's results on inversion (no time, ideal)
*** Demonstration plot examining the scattering transform of a choice signal
I think I have the transformation and discussion of the example signals from Wavelets.jl buried somewhere

** Interpolation/Subsampling (nope)
** Shearlets (embedded in the relevant chapter)
*** Core description
*** Old Sparsity results
* Interpretation methods
The research here is active enough that I'm not sure writing an outline is even a good idea presently.
- interpretation of Wavelets
  + COI is probably relevant in here
  + there's a discussion Saito has on his web page worth going over
  + at some point also discuss the role of abs and relu
- black box methods: description and application to the toy problem of
  cylinder/funnel/bell
  + image generation 
    constrained l2 minimization
    - related problem: inversion cite:anglesGenerativeNetworksInverse2018
    - does the l₂ loss function approach cite:mahendranUnderstandingDeepImage2015
    - +something to be said about the guarantee of convergence of BFGS+
  + Gradient (in light of what's happened with BlackBoxOptim vs BFGS, this is maybe not a very informative picture)
  + Integrated gradients (this is work)
* Shattering Transform
I'm not sure this section is really necessary/doable, as the results were pretty much the same as using normal wavelets on MNIST and FashionMNIST. I could try to do something with this on the cluster.

** description
** sparsity result
** TODO inversion/interpretation of coefficients
** TODO Classification Performance across datasets
I've tried this once on Fashion MNIST. I should really try it on a set of much
larger images, as there we would actually see the benefits of the anisotropy. I
think imagenet might actually be a viable candidate, though obviously just a
subset of the data. Images there are recommended to subsample to size 256²
which is large enough to start seeing anisotropy.
* Collating Transform
So I've got some preliminary theoretical discussions that I could include in this section, but the actual experiments are generally pretty lacking.
I'm not sure if just the theorems about the reduction in the number of wavelets would really be worth writing up on their own; this section would feel incomplete, and any reasonable reviewer would probably ask for some experiments.
Which I probably don't have time for.

A experiment w.r.t compressing scattering coefficients is to compose signals that are generated by selecting a couple of paths with locations which don't overlap, and then generate examples matching various signals at those locations, and seeing if the collating transform can pick up on those signals.

Write up a week or two (significant effort required for generalization). Path
Equivalence .5-2 direct. Curvature properties 2-4 weeks. 3.5 to 8

Months David. This would be literal months of work. No idea where you were getting earlier numbers from. No way could I include this.
** Description (significantly longer)
*** Major difference between the scattering transform and conv nets
(demo with tutorial images on conv nets)
*** Computational advantages of combining
*** theoretical advantages of combining
** Generalization of old results (partially written up)
** Path Equivalence (can probably get more out of this)
** TODO Curvature Properties
I have done some initial experiments on this, but I think it was before the
gradients were written correctly, so I could get a lot more out of this.
*** Experimental demonstration
*** Theoretical results
** TODO Performance on Fashion_MNIST or some such
Alternatively, since I'd rather not depend on Shearlets, [[https://www.kaggle.com/mmoreaux/environmental-sound-classification-50][here's an audio classification problem]].
Or I could see how well it does on the toy problem I've been using.
* Sonar Scattering
the most finished of these sections.
** Classification
Probably the longer of the two sections. Mostly done already, just requires
collating older results and writing them up neatly.
*** Synthetic Data
**** 1D Classification
**** 2D Classification (still work to do)
*** Real Data (check if that's ok)
*** Possibly Classifier for SSAM data built from the BAYEX14 data
** Interpretation of coefficients
*** Simple non-sonar example
*** Specifically in the sonar context
** Analysis
I have what I would call preliminary results in the conference proceeding, but there's definitely more to be said for what happens with various deformations.
I don't think I'm going to have time to do anything beyond the initial results...
*** The stuff from the conference proceeding
*** Small Variations
*** check notes
* Notes on other theses
The lengths of the dedications are wildly different, as well as the acknowledgements, from Carter who has but a nice quote about C. Elegans and a drawing, to Sam, who uses a page and a half to thank a bunch of people.

As far as the organization, in the six I grabbed there is one that puts a background in each section; the rest seem to leave the background primarily to the front.
General lengths vary from 58 to 129.
Only Sam wrote in the first person singular, others use we.
Background is around 10-20 pages.

* Time Estimation
DEADLINE:
The winter deadline of <2021-02-26 Fri> is way too close, as I'm supposed to give the committee at least 2 weeks to read the damn thing, and that would mean .
The spring deadline is <2021-03-25 Thu>.
#+begin_quote
They recommend completing your dissertation in time to give it to your committee for review at the beginning of the quarter in which you intend to graduate. In most cases, your committee probably won't need that much time, but you should check in with your committee members well in advance to make sure they'll be around when you need them.
#+end_quote
Weeell shit, I'm way behind.
In that case, I should be aiming to be finished with the *second* draft by the start of Spring quarter, or <2021-03-25 Thu>, which is 53 days from now.
** introspection
Thing is, I've mostly been letting Naoki's feedback at group meetings dictate what I'm working on.
This ended poorly, as he seems to be thinking primarily about what will further the projects that he has an interest in (e.g. the most egregious case of not looking out for my interests is insisting so thoroughly that I remember to pass on what I'm working on *despite me having clearly indicated multiple times, and demonstrated through work, that I intend to do so*).
I'm going to need to look at weeks where all I say in group meeting is "I wrote" as a success.

** dear lord were these bad
Spring dates: 13, 19, 22
Target week (Fall): 47/51
Target week (Winter): 6/11 (53 weeks in a year, so effectively 59/64)
File with committee week 38/42 (that's september or early october)
Current week: 28
10/14 weeks to Fall
31/36 weeks to Winter
my very rough estimates just now give me...
1+3.5+3.5+5.5=13.5 weeks on the low end (probably in the 10% range)
3+7+8+10=28 weeks on the medium high end (probably in the 70% range, with some
long-ass tails) Congrats we're in the long-ass tail
13.5-28 weeks needed

Does it make sense to actually plan out which bit I do after the one I'm currently working on?
Probably makes dovetailing a little bit easier.
It's also worth paying attention to which parts if any can be done in parallel.
Which order to work on things in seems pretty difficult
