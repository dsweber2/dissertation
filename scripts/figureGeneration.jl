using Pkg
using Revise
using Plots, LaTeXStrings
using Wavelets, ContinuousWavelets, FFTW
using ScatteringTransform

n = 1024
waveType = Morlet(π)
Ψ = wavelet(waveType, s=8)
daughters8, ξ = ContinuousWavelets.computeWavelets(n,Ψ)

Ψ0 = wavelet(waveType,decreasing=1)
d0, ξ = ContinuousWavelets.computeWavelets(n,Ψ0)

dRate = 4
Ψ1 = wavelet(waveType, s=8, decreasing =dRate)
d1, ξ = ContinuousWavelets.computeWavelets(n,Ψ1)
ψ1 = ifftshift(irfft(d1, 2n, 1),1)

# sketch of how the frequencies are chosen
pyplot()
locs = ContinuousWavelets.polySpacing(10,Ψ1)
#Figure 3.1
scatter(1:length(locs), locs, legend=:bottomright, label="mean log frequency", xlabel="Wavelet Index (k)", ylabel= "log-Frequency (y)", color=:black)
scatter!(length(locs):length(locs), locs[end:end], markersize=10, markershape=:x, color=:black, label=:none)
firstW, lastW, stepS = ContinuousWavelets.genSamplePoints(8,Ψ1)
fieldnames(typeof(Ψ1))
b = (dRate/Ψ1.Q)^(1 ./dRate)*(8+Ψ1.averagingLength)^((dRate-1)/dRate)
t= range(1,stop=length(locs),step=.1)
curve = b .*(range(firstW,stop=(locs[end]/b)^dRate,length=length(t))).^(1 / dRate)
plot!(t, curve, color=:blue, line=:dash, label=L"y=a(mk+k_0)^{^1/_\beta}", legend=:bottomright, legendfontsize=12, xrange=(0,length(locs)+3), xticks= [1; 5:5:1+length(locs)...], yrange=(minimum(locs)-1, maximum(locs)+1), yticks=(2:2:10,[L"\alpha", (4:2:8)..., "N.Octaves"]))
x = range(15, stop=28, step=.5)
ycord(x)= curve[end] .+ b/dRate*length(locs) .^(1/dRate-1).*(x .-length(locs))
plot!(x, ycord(x), c=:black,line=2,label=:none)
annotate!(length(locs)-1/8, locs[end]+7.5/16, Plots.text(L"\frac{dy}{dk}=^{1}/_{Q}", 11, :black, :center))
savefig("../figures/plotOfLogCentralFrequencies.pdf")



count(sum(d0 .* ξ, dims=1).<50) # how many filters are below freq 50?
count(sum(d1 .* ξ, dims=1).<50) # how many filters are below freq 50?
# Figure 3.2 comparing the number of filters and their rate for different β
xticksFewer = [1; 4; 7; 9; 12; 15; 18]
plot(heatmap(1:size(d0,2), ξ, d0, color=:Greys,
             yaxis = (L"\omega", font(20,"sans-serif")),
             xaxis = ("wavelet index", font(12,"sans-serif")),
             title=L"Q=8, \beta=1",colorbar=false, tickfontsize=10,
             clims=(minimum([d0 d1]), maximum([d0 d1]))),
     heatmap(1:size(d1,2), ξ, d1,color=:Greys, yticks=[],
             xaxis = ("wavelet index", font(12,"sans-serif")), tickfontsize=12, xticks=xticksFewer,
             title=L"Q=8, \beta=4"),layout=(1,2),
     clims=(minimum([d0 d1]), maximum([d0 d1])),
     colorbar_title=L"\widehat{\psi_i}")
savefig("../figures/QDecreasingBeta0vs1.pdf")

using DSP

# plotting the actual sonar data
using sonarProject, CUDA, Flux,ContinuousWavelets, PerceptualColourMaps
plotlyjs()
dat = cpu(loadData(shape="triangle"))
heatmap(dat[:,:,1, 4]', c=:viridis, title="Synthetic example", xlabel="range", ylabel="crossrange")
hline!([241], c=:red, legend=false, linewidth=.5)
savefig("figures/tri2000simpleEx.pdf")
StTri = scatteringTransform((641, 1, 481), 2; poolBy=2, outputPool=8, β=1.5, cw=Morlet(π), averagingLength=0); size(StTri.mainChain[1].weight)
plot(abs.(StTri.mainChain[1].weight))
specificEx = reshape(dat[:,:,1,4],(641,1,481))
plot(abs.(rfft(specificEx[:,1,1],1)))
@time res = StTri(specificEx)
res
gr(size=2.5 .* (480, 180))
plot(specificEx[:, 1, 241])
plot(res[0][:,1,241],legend=false,title="Layer zero")
savefig("figures/tri2000simpleEx_lay0.pdf")
gr(size=2 .* (340, 150))
heatmap(res[1][:,:,241,1]', title="Layer one", xlabel="time", ylabel="frequency", c =:viridis)
savefig("figures/tri2000simpleEx_lay1.pdf")
plotSecondLayer(res[:,241])
savefig("figures/tri2000simpleEx_lay2.pdf")
gr()
jointPlot(res, "", cmap("L20"), StTri, sharedColorScaling=:log, allPositive=true, logPower=true, frameTypes=:none, xlabel="", firstFreqSpacing=1:2:size(res[2],3), secondFreqSpacing=1:2:size(res[2],2))
savefig("../figures/tri2000simpleEx.pdf")
savefig("../figures/tri2000simpleEx.png")
StTri = scatteringTransform((641, 1, 481), 2; poolBy=2, outputPool=8, β=3, cw=Morlet(π), averagingLength=4); size(StTri.mainChain[1].weight)
@time res = StTri(specificEx)
output = ScatteringTransform.flatten(res)
cvp = glmnetcv(d',y,Multinomial(),lambda=λ,rng=rng)
