using Revise
using LinearAlgebra, Plots, FFTW
using sonarProject, CUDA, Flux,ContinuousWavelets
using ScatteringTransform
using Random, GLMNet, LaTeXStrings
using JLD2, FileIO

dat1 = reshape(loadData(shape="triangle", normalize=true, onGpu=false, noise=true), (641, 1, 481, 46))
dat2 = reshape(loadData(shape="triangle", normalize=true, onGpu=false, noise=true, speed=2500), (641, 1, 481, 46))
joinedData = reshape(cat(dat1, dat2, dims=4), (641, 1, :))
joinedDataFreq = reshape(cat(abs.(rfft(cat(dat1, reverse(dat1, dims=1), dims=1), 1)), abs.(rfft(cat(dat2, reverse(dat2, dims=1), dims=1), 1)), dims=4), (642, 1, :))
variationInDataset = std(joinedDataFreq, dims=(2, 3, 4))[:,1,1]
chunkSize = 10
poolBy = 2; Q = (8, 6.0, 1.0); extraOctaves = (-2.5, -1, 0); outputPool = 10; β = (1.5, 1, 1.0); cw = Morlet(π); aveLen = (-1, -2, 2)
St = scatteringTransform((641, 1, chunkSize), 2; poolBy=poolBy, Q=Q, extraOctaves=extraOctaves, outputPool=outputPool, β=β, cw=cw, averagingLength=aveLen)
a, b, c = getWavelets(St)
firstLayRes = St.mainChain[3](St.mainChain[2](St.mainChain[1](joinedData[:,:,rand(1:size(joinedData, 3), 10)])))
firstLayRes = [firstLayRes; reverse(firstLayRes, dims=1)]
plot(plot(abs.(a), legend=false, title="first layer wavelets"),
     plot(variationInDataset, title="standard deviation of the positive frequencies for triangles", legend=false),
     plot(abs.(b), legend=false, title="second layer wavelets"),
     plot([abs.(mean(rfft(firstLayRes, 1), dims=(2, 3))[:,1,1]) abs.(std(rfft(firstLayRes, 1), dims=(2, 3))[:,1,1])], labels=["mean" "std"], title="internal first layer data frequency mean and standard deviation"),
     layout=(4, 1))
plot(plot(abs.(b), legend=false, title="second layer wavelets"), plot([abs.(mean(rfft(firstLayRes, 1), dims=(2, 3))[:,1,1]) abs.(std(rfft(firstLayRes, 1), dims=(2, 3))[:,1,1])], labels=["mean" "std"], title="internal first layer data frequency mean and standard deviation"),layout=(2, 1))
size(firstLayRes)
plot(plot(abs.(a)), plot(abs.(b)), plot(abs.(c)))
# comparing the filters and some data
plot(plot(abs.(a)), plot(abs.(b)), plot(abs.(c)), plot(abs.(rfft(dat1[:,1,341,11]))), legend=false)
plot(real.(fftshift(originalDomain(St.mainChain[1]), 1))[:,1])
plot(real.(fftshift(originalDomain(St.mainChain[4]), 1))[:,1])
plot(real.(fftshift(originalDomain(St.mainChain[1]), 1))[floor(Int, 641 / 2) .+ (1:641),end])
length(St.mainChain)
plot(real.(fftshift(originalDomain(St.mainChain[7]), 1)))
