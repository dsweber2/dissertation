using Revise
using PerceptualColourMaps
using Pkg
# Pkg.activate("../../")
using LinearAlgebra, Plots, FFTW
using CUDA, Flux,ContinuousWavelets
using ScatteringTransform
using Random, GLMNet, LaTeXStrings
using Lasso
using JLD2, FileIO
loadInsteadOfFit = true
resST = load("../results/2k2500chooseAngles/triangleSharkfin/lassojl/Scale1.0pool2.0__2.0__2.0__outputPool10__10__10__decrease1.0_OTHERCHANGES_cwMorletmean3.141592653589793_extraOctaves0_aveLen2.jld2")["results"]
resST.aucScore
logitST = load("../results/2k2500chooseAngles/triangleSharkfin/lassojl/Scale1.0pool2.0__2.0__2.0__outputPool10__10__10__decrease1.0_OTHERCHANGES_cwMorletmean3.141592653589793_extraOctaves0_aveLen2_logit.jld2")["logit"][2]
target = Lasso.coef(logitST)[2:end]
target[1]
size(target)
plot(target)
using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, LinearAlgebra
using FFTW, FourierFilterFlux
using Optim, LineSearches, GLMNet
using Plots
using CUDA, Zygote, Flux
using LinearAlgebra
using BlackBoxOptim
using BlackBoxOptim: num_func_evals
using ScatteringInterpretation
using LaTeXStrings
function moveToFront!(respp, extraDims = 3, percent = 0.15, padBy = 150)
    ax = axes(respp)
    sorted = sort(reshape(abs.(respp), (length(respp), :)), dims = 1)
    keepThresh = sorted[floor(Int, (1 - percent) * length(sorted))]

    for (iθ, respθ) in enumerate(eachslice(respp, dims = extraDims))
        viableLocs = cat([abs.(respθ[:, cross]) .> keepThresh for cross =
                1:size(respθ, 2)]..., dims = 2)
        firstSignificantVal = findfirst.(x for x in eachcol(viableLocs)) # find
        # the first in each cross-range location
        firstSignificantVal[firstSignificantVal.==nothing] .= 0 # findlast
        # returns nothing rather than zero, so convert
        moveBy = minimum(firstSignificantVal) .- padBy
        accessLoc = (ax[1:extraDims-1]..., iθ, ax[(extraDims+1):end]...)
        respp[accessLoc...] = circshift(respθ, (-moveBy, 0))
    end
end
DATA_DIR = "/Elysium/dsweber/data/helmholtzSolver/"
OLD_DATA_DIR = "/Elysium/dsweber/data/sonarProject/bodin_synthetic/"
function newData(shape, speed::Real, dist::Real, useJLD2)
    jld2Version = joinpath(DATA_DIR, shape, "$(speed)", "$(dist)",
        "resp$(Float64(speed))_$(dist).jld2")
    println(jld2Version)
    if isfile(jld2Version) && useJLD2
        JLD2.jldopen(jld2Version, "r", mmaparrays = true) do file
            resp = file["resp"]
        end
    else
        println("loading $(jld2Version[1:end - 1])")
        JLD2.jldopen(jld2Version[1:end-1], "r") do file
            resp = file["resp"]
        end
    end
    resp = permutedims(resp, (1, 3, 2))
    return reshape(resp, (size(resp)[1:2]..., 1,
        size(resp, 3)))
end
function loadData(; shape = "sharkfin", speed = 2000, dist = 10.2, onGpu = true,
    useJLD2 = true, oldData = false, normalize = false, noise = false,
    SNRdB = 5, moveForward = true)
    if oldData
        if shape != "sharkfin"
            result = oldLoad(shape = shape, speed = speed, dist = dist)
        else
            result = oldLoad(speed = speed, dist = dist)
        end
    else
        result = newData(shape, speed, dist, useJLD2)
    end
    if moveForward
        moveToFront!(result, 4)
    end
    if normalize
        result = normalizePart(result)
    end
    if noise
        η = randn(eltype(result), size(result))
        η = normalizePart(η)
        result .+= 10^(-SNRdB / 20) * η
    end
    if onGpu
        return CuArray(result)
    else
        return result
    end
end
function wiggle!(args...; kwargs...)
    @nospecialize
    local plt
    try
        plt = current()
    catch
        return wiggle(args..., kwargs...)
    end
    wiggle!(current(), args...; kwargs...)
end

function wiggle!(plt::Plots.Plot,
    wav::AbstractArray{T,2};
    taxis::AbstractVector = 1:size(wav, 1),
    zaxis::AbstractVector = 1:size(wav, 2),
    sc::Real = 1,
    lineColor = :black,
    fillColor = :black,
    Overlap::Bool = true,
    Orient::Symbol = :across,
    ZDir::Symbol = :normal, kwargs...) where {T<:Number}
    # Set axes
    (n, m) = size(wav)

    # Sanity check
    @assert Orient ∈ [:across, :down]
    @assert ZDir ∈ [:normal, :reverse]
    if length(taxis) != n
        error("Inconsistent taxis dimension!")
    end
    if length(zaxis) != m
        error("Inconsistent zaxis dimension!")
    end

    if !(lineColor isa Array || lineColor isa Tuple)
        lineColor = [lineColor for i = 1:n]
    end
    if !(fillColor isa Array || fillColor isa Tuple)
        fillColor = [fillColor for i = 1:n]
    end

    # For calculation purposes
    maxrow = zeros(m)
    minrow = zeros(m)
    for (i, wavᵢ) in enumerate(eachcol(wav))
        maxrow[i] = maximum(wavᵢ)
        minrow[i] = minimum(wavᵢ)
    end

    # Scale the data for plotting
    dz = mean(diff(zaxis))
    if Overlap
        wamp = 2 * dz * (sc / maximum(maxrow - minrow)) * wav
    else
        wmax = maximum(maxrow) <= 0 ? 0 : maximum(maxrow)
        wmin = minimum(minrow) >= 0 ? 0 : minimum(minrow)
        wamp = sc * wav / (wmax - wmin)
    end

    # Set initial plot
    t0 = minimum(taxis)
    t1 = maximum(taxis)
    z0 = minimum(zaxis)
    z1 = maximum(zaxis)
    if Orient == :down
        plot!(plt; xlims = (z0 - dz, z1 + dz), ylims = (t0, t1), yflip = true, legend = :none, kwargs...)
    else
        plot!(plt; xlims = (t0, t1), ylims = (z0 - dz, z1 + dz), legend = :none, kwargs...)
    end
    if ZDir == :reverse
        wamp = reverse(wamp, dims = 2)
    end

    # Plot each wavelet
    for (i, wampᵢ) in enumerate(eachcol(wamp))
        t = deepcopy(taxis)
        w_sign = sign.(wampᵢ)
        for j = 1:(n-1)
            if (w_sign[j] != w_sign[j+1] && w_sign[j] != 0 && w_sign[j+1] != 0)
                wampᵢ = [wampᵢ; 0]
                t = [t; t[j] - wampᵢ[j] * (t[j+1] - t[j]) / (wampᵢ[j+1] - wampᵢ[j])]
            end
        end
        ix = sortperm(t)
        t = t[ix]
        wampᵢ = wampᵢ[ix]
        len = length(t)
        indperm = [1:len; len:-1:1]
        inputx = t[indperm]
        inputy = zaxis[i] .+ [wampᵢ; min.(wampᵢ[len:-1:1], 0)]
        # In the plot! functions below, theoretically speaking, either fillrange = zaxis[k]
        # or fillrange=[zaxis[k], zaxis[k]+dz] should be used. However, those do not
        # generate the desired plots as of O3/19/2018. Somehow, the relative value of 0,
        # i.e., fillrange=0, works well, which is used temporarily.
        if Orient == :down
            plot!(plt, inputy, inputx; fillrange = 0, fillalpha = 0.75,
                fillcolor = fillColor[i], linecolor = lineColor[i], orientation = :v, kwargs...)
        else
            plot!(plt, inputx, inputy; fillrange = 0, fillalpha = 0.75,
                fillcolor = fillColor[i], linecolor = lineColor[i], kwargs...)
        end
    end
    return plt
end
wiggle(args...; kwargs...) = wiggle!(Plots.Plot(), args...; kwargs...)
wiggle(plt::Plots.Plot, args...; kwargs...) = wiggle!(deepcopy(plt), args...; kwargs...)
NDims = 641
N = 100
popSize = 5000
nMinsBBO = 3
perturbRate = 1 / 5
totalMins = 8 * 60
homeDir = "../../results/fitSonarClassifier/"
figDir = "../../figures/sonar/interpret/triSf"
St = scatteringTransform((NDims, 1, 1), 2; poolBy=2, Q=(6.0, 6.0, 1.0), extraOctaves=(-2.5, -2, 0), outputPool=10, β=(1, 1, 1.0), cw=Morlet(π), aveLen=(-2, -1.5, 2), useGpu=false)
targetSt = roll(target, St)
freqs = getMeanFreq(St, 1000)
x = randn(641, 1, 1)
saveDir = joinpath(homeDir, "triSf")
if !isdir(saveDir)
    mkpath(saveDir)
end
if !isdir(figDir)
    mkpath(figDir)
end
locDir = saveDir

# maximizing 2000
function objDCT20(x̂)
    x = idct(x̂, 1)
    t = St(reshape(x, (NDims, 1, 1)))
    1.1f0^(-sum([sum(targetSt[ii] .* t[ii]) for ii = 0:2]) + 1f-5 * norm(x)^2)
end
# maximizing 2500
function objDCT25(x̂)
    x = idct(x̂, 1)
    t = St(reshape(x, (NDims, 1, 1)))
    1.1f0^(sum([sum(targetSt[ii] .* t[ii]) for ii = 0:2]) + 1f-5 * norm(x)^2)
end

extraName = ""
nRepeat = 1
# if it already exists, append the appropriate count so we get multiple copies
if isfile(locDir * "data.jld2") && !loadInsteadOfFit
    nRepeat = 1
    while isfile(locDir * "data$(nRepeat).jld2")
        global nRepeat
        nRepeat += 1
    end
    extraName = "$(nRepeat)"
    if isfile(locDir * "dataFit25_.jld2")
        nRepeat25 = 1
        while isfile(locDir * "dataFit25_$(nRepeat25).jld2")
            global nRepeat25
            nRepeat25 += 1
        end
        extraName25 = "$(nRepeat25)"
    else
        extraName25 = ""
    end
elseif loadInsteadOfFit
    compareScores = Float64[]
    saveDict = load(locDir * "data.jld2")
    popul = saveDict["popul"]
    scores = saveDict["scores"]
    scores = [objDCT20(popul[:, ii]) for ii = 1:size(popul, 2)]
    iScores = sortperm(scores)
    append!(compareScores, scores[iScores[1]])
    nRepeat = 1
    while nRepeat <= 10
        global nRepeat
        extraName = "$(nRepeat)"
        if isfile(locDir * "data$(nRepeat).jld2")
            saveDict = load(locDir * "data$(extraName).jld2")
            popul = saveDict["popul"]
            scores = saveDict["scores"]
            scores = [objDCT20(popul[:, ii]) for ii = 1:size(popul, 2)]
            iScores = sortperm(scores)
            append!(compareScores, scores[iScores[1]])
        end
        nRepeat += 1
    end
    println(compareScores)
    bestInd = argmin(compareScores) - 1
    extraName = bestInd == 0 ? "" : "$(bestInd)"
    compareScores25 = Float64[]
    saveDict25 = load(locDir * "dataFit25_.jld2")
    popul = saveDict25["popul25"]
    scores25 = saveDict25["scores25"]
    scores25 = [objDCT25(popul[:, ii]) for ii = 1:size(popul, 2)]
    iScores25 = sortperm(scores25)
    append!(compareScores25, scores25[iScores25[1]])
    nRepeat = 1
    while nRepeat <= 10
        global nRepeat
        extraName25 = "$(nRepeat)"
        if isfile(locDir * "dataFit25_$(nRepeat).jld2")
            saveDict25 = load(locDir * "dataFit25_$(extraName).jld2")
            popul = saveDict25["popul25"]
            scores25 = saveDict25["scores25"]
            scores25 = [objDCT25(popul[:, ii]) for ii = 1:size(popul, 2)]
            iScores25 = sortperm(scores25)
            append!(compareScores25, scores25[iScores25[1]])
        end
        nRepeat += 1
    end
    println(compareScores25)
    bestInd25 = argmin(compareScores25) - 1
    extraName25 = bestInd25 == 0 ? "" : "$(bestInd25)"
end
extraName, extraName25
# extraName = ""
saveName = locDir * "data$(extraName).jld2"
saveNameDict = locDir * "dictionary"
saveSerialName = locDir * "data$(extraName)"
saveName25 = locDir * "dataFit25_$(extraName25).jld2"
saveNameDict25 = locDir * "dictionary25"
saveSerialName25 = locDir * "dataFit25_$(extraName25)"

if !isdir(locDir)
    mkpath(locDir)
end
# second layer setup end

if !(loadInsteadOfFit)
    setProbW = bbsetup(objDCT20; SearchRange=(-10000., 10000.), NumDimensions=NDims, PopulationSize=popSize, MaxTime=10.0, TraceInterval=5, TraceMode=:silent)
    println("pretraining")
    bboptimize(setProbW)

    setProbW25 = bbsetup(objDCT25; SearchRange=(-10000., 10000.), NumDimensions=NDims, PopulationSize=popSize, MaxTime=10.0, TraceInterval=5, TraceMode=:silent)
    println("pretraining")
    bboptimize(setProbW25)

    popul = dct(pinkStart(NDims, -1, popSize), 1)
    adjustPopulation!(setProbW, popul)

    popul25 = dct(pinkStart(NDims, -1, popSize), 1)
    adjustPopulation!(setProbW25, popul25)

    nIters = ceil(Int, totalMins / nMinsBBO)
    for ii in 1:nIters
        println("----------------------------------------------------------")
        println("round $ii: GO")
        println("----------------------------------------------------------")
        bboptimize(setProbW, MaxTime=nMinsBBO * 60.0) # take some black box steps for the whole pop
        popul = setProbW.runcontrollers[end].optimizer.population.individuals
        scores = [objDCT20(popul[:,ii]) for ii in 1:size(popul, 2)]
        iScores = sortperm(scores)

        relScoreDiff = abs(log(scores[iScores[1]]) - log(scores[iScores[round(Int, 4 * popSize / 5 - 1)]])) / abs(log(scores[iScores[1]]))
        if ii > 3 && relScoreDiff .< 0.01 # the relative difference between the best and the worst signals is less than 1%
            println("finished after $(ii) rounds")
            break
        end

        # perturb a fifth to sample the full space if it hasn't converged yet
        perturbWorst(setProbW, scores, perturbRate)
    end
    # NDescents = 30
    popul = setProbW.runcontrollers[end].optimizer.population.individuals
    scores = [objDCT20(popul[:,ii]) for ii in 1:size(popul, 2)]

    saveSerial(saveSerialName, setProbW)
    save(saveName, "popul", popul, "scores", scores)
else
    println("reloading")
    saveDict = load(saveName)
    popul = saveDict["popul"]
    scores = saveDict["scores"]
    scores = [objDCT20(popul[:, ii]) for ii = 1:size(popul, 2)]
    iScores = sortperm(scores)
end

histogram(log.(scores) / log(1.1), legend=false, xlabel="inner product with weights + norm of input", title="Histogram of log scores\nFitting Shape")
savefig(joinpath(figDir, "histogramPop$(extraName).pdf"))

# examples
iScores = sortperm(scores)
plotFirstXEx(idct(popul[:,iScores], 1), scores[iScores], "Fit solutions: Shape, Triangle positive")
savefig(joinpath(figDir, "exampleResults$(extraName).pdf"))
plot(idct(popul[:,iScores[1]]), title="Best Example log-score: $(round(log(objDCT20(popul[:,iScores[1]])), sigdigits=3))\nShape, Triangle positive", c=:black, legend=false)
savefig(joinpath(figDir, "bestExample$(extraName).pdf"))
plot(abs.(rfft(idct(popul[:,iScores[1]]))), title="Fourier Domain,\n Shape, Triangle positive", c=:black, legend=false)
savefig(joinpath(figDir, "rfftBestExample$(extraName).pdf"))
plot(popul[:,iScores[1]], title="dct best example,\n Shape, Triangle positive", c=:black, legend=false)
savefig(joinpath(figDir, "dctBestExample$(extraName).pdf"))


# plots of the first layer
f1, f2, f3 = getMeanFreq(St)
firstLayerMats = cat([St(reshape(idct(popul[:,iScores][:,ii], 1), (NDims, 1, 1)))[1][:,:,1]' for ii = 1:20]..., dims=3)
colorMax = (minimum(firstLayerMats), maximum(firstLayerMats))
p = plotOverTitle([heatmap(x, clims=colorMax, colorbar=false, c=cgrad(:viridis, scale=:log10), yticks=(1:3:size(x, 1), round.(f1[1:3:end - 1], sigdigits=3)), xticks=false) for x in eachslice(firstLayerMats, dims=3)], "First Layer results for the best example\nShape, Triangle positive", (4, 5))
savefig(joinpath(figDir, "ArrayFirstLayerOutput$(extraName).pdf"))
heatmap(firstLayerMats[:,:,1], clims=colorMax, colorbar=false, c=cgrad(:viridis, scale=:log10), xlabel="time", ylabel="frequency", title="First Layer results\nShape, Triangle positive", yticks=(1:1:size(firstLayerMats, 1), round.(f1[1:1:end - 1], sigdigits=3)))
savefig(joinpath(figDir, "bestFirstLayerOutput$(extraName).pdf"))
heatmap(abs.(St.mainChain[1](idct(popul[:,iScores[1]]))[:,1:end - 1,1,1]'), title="internal first layer\nShape, Triangle positive", yticks=(1:1:size(firstLayerMats, 1), round.(f1[1:1:end - 1], sigdigits=3)),c=cgrad(:viridis),xlabel="time", ylabel="frequency")
savefig(joinpath(figDir, "ArrayInternalFirst$(extraName).pdf"))
# second layer plot
plotSecondLayer(St(reshape(idct(popul[:, iScores[1]]), (NDims, 1, 1))), St; title="Second Layer fit: Shape, Triangle positive",logPower=false,c=cgrad(:viridis))
savefig(joinpath(figDir, "SecondLayerOutput$(extraName).pdf"))
jointPlot(St(reshape(idct(popul[:, iScores[1]]), (NDims, 1, 1))), "ST coefficients of fitting Shape, Triangle positive", cmap("L20"), St; sharedColorScaling = :log10, allPositive = true, logPower = true, frameTypes = :none)
savefig(joinpath(figDir, "jointPlot$(extraName).pdf"))
# scalograms best example
freqs = getMeanFreq(St, 1000)
freqs[1]
heatmap(abs.(circshift(St.mainChain[1](idct(popul[:, iScores[1]]))[:, 1:(end-1), 1, 1]', (0, 0))) .^ 2; yticks = (1:size(freqs[1], 1)-1, round.(freqs[1][1:end-1], sigdigits = 3)), c = cgrad(:viridis, scale = :exp), framestyle = :box, title = "Scalogram fitting Triangle positive", titlefontsize = 13, guidefontsize = 9, tickfontsize = 7, bottom_margin = -3Plots.px, ylabel = "Frequency (Hz)", xlabel = "Time (ms)")
savefig(joinpath(figDir, "Scalogram$(extraName).pdf"))
savefig(joinpath(figDir, "Scalogram$(extraName).png"))



println("___----------------------------------------------------------___")
println("starting the inverted problem")
println("___----------------------------------------------------------___")
if !(loadInsteadOfFit)
    for ii in 1:nIters
        println("----------------------------------------------------------")
        println("round $ii: GO")
        println("----------------------------------------------------------")
        bboptimize(setProbW25, MaxTime=nMinsBBO * 60.0) # take some black box steps for the whole pop
        popul25 = setProbW25.runcontrollers[end].optimizer.population.individuals
        scores25 = [objDCT25(popul25[:,ii]) for ii in 1:size(popul25, 2)]
        iScores25 = sortperm(scores25)

        relScoreDiff = abs(log(scores25[iScores25[1]]) - log(scores25[iScores25[round(Int, 4 * popSize / 5 - 1)]])) / abs(log(scores25[iScores25[1]]))
        if ii > 3 && relScoreDiff .< 0.01 # the relative difference between the best and the worst signals is less than 1%
            println("finished after $(ii) rounds")
            break
        end

        # perturb a fifth to sample the full space if it hasn't converged yet
        perturbWorst(setProbW25, scores25, perturbRate)
    end
    # NDescents = 30
    popul25 = setProbW25.runcontrollers[end].optimizer.population.individuals
    scores25 = [objDCT25(popul25[:,ii]) for ii in 1:size(popul25, 2)]

    saveSerial(saveSerialName25, setProbW25)
    save(saveName25, "popul25", popul25, "scores25", scores25)
else
    println("reloading 25")
    saveDict25 = load(saveName25)
    popul25 = saveDict25["popul25"]
    scores25 = saveDict25["scores25"]
    scores25 = [objDCT25(popul25[:, ii]) for ii = 1:size(popul25, 2)]
end

histogram(log.(scores25) / log(1.1), legend=false, xlabel="inner product with weights + norm of input", title="Histogram of log scores\nFitting Shape Sharkfin positive")
savefig(joinpath(figDir, "histogramPopFit25_$(extraName).pdf"))

# examples
iScores25 = sortperm(scores25)
plotFirstXEx(idct(popul25[:,iScores25], 1), scores25[iScores25], "Fit solutions: Shape")
savefig(joinpath(figDir, "exampleResultsFit25_$(extraName).pdf"))
plot(idct(popul25[:,iScores25[1]]), title="Best Example $(round(log(objDCT25(popul25[:,iScores25[1]])), sigdigits=3))\nShape, Sharkfin", c=:black, legend=false)
savefig(joinpath(figDir, "bestExampleFit25_$(extraName).pdf"))
plot(abs.(rfft(idct(popul25[:,iScores25[1]]))), title="Fourier Domain,\n Shape, Sharkfin", c=:black, legend=false)
savefig(joinpath(figDir, "rfftBestExampleFit25_$(extraName).pdf"))
plot(popul25[:,iScores25[1]], title="dct best example,\n Shape, Sharkfin", c=:black, legend=false)
savefig(joinpath(figDir, "dctBestExampleFit25_$(extraName).pdf"))


# plots of the first layer
f1, f2, f3 = getMeanFreq(St)
firstLayerMats25 = cat([St(reshape(idct(popul25[:,iScores25][:,ii], 1), (NDims, 1, 1)))[1][:,:,1]' for ii = 1:20]..., dims=3)
colorMax = (minimum(firstLayerMats25), maximum(firstLayerMats25))
p = plotOverTitle([heatmap(x, clims=colorMax, colorbar=false, c=cgrad(:viridis, scale=:log10), yticks=(1:3:size(x, 1), round.(f1[1:3:end - 1], sigdigits=3)), xticks=false) for x in eachslice(firstLayerMats, dims=3)], "First Layer results for the best example\nShape, Sharkfin positive", (4, 5))
savefig(joinpath(figDir, "ArrayFirstLayerOutputFit25_$(extraName25).pdf"))
heatmap(firstLayerMats25[:,:,1], clims=colorMax, colorbar=false, c=cgrad(:viridis, scale=:log10), xlabel="space", ylabel="frequency", title="First Layer results\nShape, Sharkfin positive", yticks=(1:1:size(firstLayerMats25, 1), round.(f1[1:1:end - 1], sigdigits=3)))
savefig(joinpath(figDir, "bestFirstLayerOutputFit25_$(extraName25).pdf"))
heatmap(abs.(St.mainChain[1](idct(popul25[:,iScores25[1]]))[:,1:end - 1,1,1]'), title="internal first layer\nShape, Sharkfin positive", yticks=(1:1:size(firstLayerMats25, 1), round.(f1[1:1:end - 1], sigdigits=3)),c=cgrad(:viridis))
savefig(joinpath(figDir, "ArrayInternalFirstFit25_$(extraName25).pdf"))
# second layer plot
plotSecondLayer(St(reshape(idct(popul25[:, iScores25[1]]), (NDims, 1, 1))), St; title="SecondLayer: Shape, Sharkfin positive",logPower=false,c=cgrad(:viridis, [0 .9]))
savefig(joinpath(figDir, "SecondLayerOutputFit25_$(extraName25).pdf"))

# joint plot
jointPlot(St(reshape(idct(popul25[:, iScores25[1]]), (NDims, 1, 1))), "ST coefficients of fitting Shape, Sharkfin positive", cmap("L20"), St; sharedColorScaling=:linear)
savefig(joinpath(figDir, "jointPlotFit25_$(extraName25).pdf"))

# scalogram
heatmap(abs.(circshift(St.mainChain[1](idct(popul25[:, iScores25[1]]))[:,1:(end - 1),1,1]', (0, 0))).^2; yticks = (1:size(freqs[1], 1)-1, round.(freqs[1][1:end-1], sigdigits=3)), c=cgrad(:viridis, scale=:exp), framestyle=:box, title="Scalogram fitting Shape Sharkfin positive",  titlefontsize=13, guidefontsize=9, tickfontsize=7, bottom_margin=-3Plots.px, ylabel="Frequency (Hz)", xlabel="Time (ms)")
savefig(joinpath(figDir, "ScalogramFit25_$(extraName25).pdf"))
savefig(joinpath(figDir, "ScalogramFit25_$(extraName25).png"))




bestEx = idct(popul[:, iScores[1]], 1)
bestEx25 = idct(popul25[:, iScores25[1]], 1)

# compare with examples
dat20 = reshape(loadData(shape = "triangle", speed = 2000, onGpu = false), (:, 1, 481, 46))
dat25 = reshape(loadData(shape = "sharkfin", speed = 2000, onGpu = false), (:, 1, 481, 46))
rng = MersenneTwister(34)
nExs = 13
inds20 = zip(rand(rng, 1:481, nExs), rand(rng, 1:46, nExs))
inds25 = zip(rand(rng, 1:481, nExs), rand(rng, 1:46, nExs))
Exs20 = cat([dat20[:, 1, ii, jj] for (ii, jj) in inds20]..., dims = 2)
Exs25 = cat([dat25[:, 1, ii, jj] for (ii, jj) in inds25]..., dims = 2)
divider = cat(["" for ii = 1:floor(Int, nExs / 2)]..., dims = 1)
yTicks = cat(divider..., "Triangle", divider..., L"St^{-1}(\beta)", divider..., "Sharkfin", divider..., dims = 1)
lineColors = cat([cmap("D01")[end] for ii = 1:nExs]..., :black, [cmap("D01")[1] for ii = 1:nExs]..., dims = 1)
overallMax = maximum(Exs20)
Exs20 ./ maximum(Exs20,dims=1)#/overallMax
normedExs20 = Exs20 ./ maximum(Exs20, dims=1)
normedExs25 = Exs25 ./ maximum(Exs25, dims=1)
p = wiggle([normedExs20 1 * bestEx / norm(bestEx, Inf) normedExs25]; Overlap = true, rotation = 90,
    lineColor = lineColors,
    yticks = (1:(nExs*2+1), cat(divider..., "Triangle", divider..., L"St^{-1}(\beta)", divider..., "Sharkfin", divider..., dims = 1)),
    title = "Randomly selected training waveforms and the\n pseudo-inverse of the shape weight vector")
savefig(joinpath(figDir, "compareTriEmphWithRandomExamples.pdf"))
savefig(joinpath(figDir, "compareTriEmphWithRandomExamples.png"))
p = wiggle([normedExs20 -1 * bestEx / norm(bestEx, Inf) normedExs25]; Overlap = true, rotation = 90,
    lineColor = lineColors,
    yticks = (1:(nExs*2+1), cat(divider..., "Triangle", divider..., L"St^{-1}(\beta)", divider..., "Sharkfin", divider..., dims = 1)),
    title = "Randomly selected training waveforms and the\n pseudo-inverse of the shape weight vector")
savefig(joinpath(figDir, "compareTriEmphWithRandomExamplesNeg.pdf"))
savefig(joinpath(figDir, "compareTriEmphWithRandomExamplesNeg.png"))

yInvTicks = cat(divider..., "Triangle", divider..., L"St^{-1}(-\beta)", divider..., "Sharkfin", divider..., dims = 1)
p = wiggle([normedExs20 bestEx25 / norm(bestEx25, Inf) normedExs25]; Overlap = true, rotation = 90,
    lineColor = lineColors,
    yticks = (1:(nExs*2+1), yInvTicks),
    title = "Randomly selected training waveforms and the\n pseudo-inverse of the shape negative weight vector")
savefig(joinpath(figDir, "compareSfEmphWithRandomExamples.pdf"))
savefig(joinpath(figDir, "compareSfEmphWithRandomExamples.png"))
p = wiggle([normedExs20 -bestEx25 / norm(bestEx25) normedExs25]; Overlap = true, rotation = 90,
    lineColor = lineColors,
    yticks = (1:(nExs*2+1), yTicks),
    title = "Randomly selected training waveforms\n and the pseudo-inverse of the shape negative weight vector")
savefig(joinpath(figDir, "compareSfEmphWithRandomExamplesNeg.pdf"))
savefig(joinpath(figDir, "compareSfEmphWithRandomExamplesNeg.png"))
nExs = 13
inds20 = zip(rand(rng, 1:481, nExs), rand(rng, 1:46, nExs))
inds25 = zip(rand(rng, 1:481, nExs), rand(rng, 1:46, nExs))
Exs20 = cat([dat20[:, 1, ii, jj] for (ii, jj) in inds20]..., dims = 2)
Exs25 = cat([dat25[:, 1, ii, jj] for (ii, jj) in inds25]..., dims = 2)
normedExs20 = Exs20 ./ maximum(Exs20, dims=1)
normedExs25 = Exs25 ./ maximum(Exs25, dims=1)
divider = cat(["" for ii = 1:floor(Int, nExs / 2)]..., dims = 1)
p = wiggle([normedExs20  bestEx / norm(bestEx, Inf)  bestEx25 / norm(bestEx25, Inf) normedExs25]; Overlap = true, rotation = 90,
    lineColor = cat([cmap("D01")[end] for ii = 1:nExs+1]..., [cmap("D01")[1] for ii = 1:nExs+1]..., dims = 1),
        yticks = (1:(nExs*2+2), cat(divider..., "Triangle", divider..., L"St^{-1}(\pm\beta)", divider..., "Sharkfin", divider..., dims = 1)),
           titlefontsize=12,
    title = "Randomly selected training waveforms and the pseudo-inverse\n of the regression vector and its negative", xlabel="Time (ms)")
savefig(joinpath(figDir, "compareTriSfEmphWithRandomExamples.pdf"))
savefig(joinpath(figDir, "compareTriSfEmphWithRandomExamples.png"))


# just a plot of a particular example
inds20 = zip(rand(rng, 1:481, nExs), rand(rng, 1:46, nExs))
Exs20 = cat([dat20[:, 1, ii, jj] for (ii, jj) in inds20]..., dims = 2)
randEx = St(reshape(Exs20[:, 4], (:, 1, 1)))
jointPlot(randEx, "ST coefficients of fitting shape, 2000 positive", cmap("L20"), St; sharedColorScaling = :log10, allPositive = true, logPower = true, frameTypes = :none)
savefig("../../figures/sonar/basicExample.pdf")
savefig("../../figures/sonar/basicExample.png")





getMostExtremeValue(X) = X[argmax(abs.(X), dims=1)][1,:,:]
jointPlot(targetSt, "Shape Coefficients, Triangle is positive, Sharkfin negative", cmap("D01"), St; sharedColorScaling=:linear, toHeat=getMostExtremeValue(targetSt[2]),xlabel=" ")
savefig(joinpath(figDir, "jointPlotWeights.pdf"))
