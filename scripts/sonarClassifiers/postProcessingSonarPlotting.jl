using Revise
using PerceptualColourMaps
using Pkg
# Pkg.activate("../../")
using LinearAlgebra, Plots, FFTW
using sonarProject, CUDA, Flux,ContinuousWavelets
using ScatteringTransform
using Random, GLMNet, LaTeXStrings
using Lasso
using JLD2, FileIO
resST = load("../../results/2k2500chooseAngles/triangle1Dist/lassojlDiffAveLen/Scale1.0pool2.0__2.0__2.0__outputPool10__10__10__decrease1.0.jld2")["results"]
logitST = load("../../results/2k2500chooseAngles/triangle1Dist/lassojlDiffAveLen/Scale1.0pool2.0__2.0__2.0__outputPool10__10__10__decrease1.0_logit.jld2")["logit"]
target = Lasso.coef(logitST)[2:end]
size(target)
plot(target)
using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, LinearAlgebra
using FFTW, FourierFilterFlux
using Optim, LineSearches, GLMNet
using Plots
using CUDA, Zygote, Flux
using LinearAlgebra
using BlackBoxOptim
using BlackBoxOptim:num_func_evals
using ScatteringInterpretation


# comparing plots for 2000 and 2500



# shape specific
using Revise
using PerceptualColourMaps
using Pkg
# Pkg.activate("../../")
using LinearAlgebra, Plots, FFTW
using sonarProject
using CUDA, Flux,ContinuousWavelets
using ScatteringTransform
using Random, GLMNet, LaTeXStrings
using Lasso
using JLD2, FileIO
NDims = 641
N = 100
popSize = 500
nMinsBBO = 3
perturbRate = 1 / 5
totalMins = 1 * 60
resST = load("../results/2k2500chooseAngles/triangleSharkfin/lassojl/Scale1.0pool2.0__2.0__2.0__outputPool10__10__10__decrease1.0_OTHERCHANGES_cwMorletmean3.141592653589793_extraOctaves0_aveLen2.jld2")["results"]
logitST = load("../results/2k2500chooseAngles/triangleSharkfin/lassojl/Scale1.0pool2.0__2.0__2.0__outputPool10__10__10__decrease1.0_OTHERCHANGES_cwMorletmean3.141592653589793_extraOctaves0_aveLen2_logit.jld2")["logit"][2]
target = Lasso.coef(logitST)[2:end]
resST

figDir = "../../figures/sonar/interpret/triSf"
homeDir = "../../results/fitSonarClassifier/"
saveDir = joinpath(homeDir, "triSf")
extraName = ""
locDir = saveDir
saveName = locDir * "data$(extraName).jld2"
saveDict = load(saveName)
popul = saveDict["popul"]
saveNameInv = locDir * "dataFit25_1.jld2"
saveDictInv = load(saveNameInv)
populInv = saveDictInv["popul25"]
St = scatteringTransform((NDims, 1, 1), 2; poolBy=2, Q=(6.0, 6.0, 1.0), extraOctaves=(-2.5, -2, 0), outputPool=10, β=(1, 1, 1.0), cw=Morlet(π), aveLen=(-2, -1.5, 2), useGpu=false)
targetSt = roll(target, St)
function objDCT20(x̂)
    x = idct(x̂, 1)
    t = St(reshape(x, (NDims, 1, 1)))
    1.1f0^(-sum([sum(targetSt[ii] .* t[ii]) for ii = 0:2]) + 1f-5 * norm(x)^2)
end
function objDCT25(x̂)
    x = idct(x̂, 1)
    t = St(reshape(x, (NDims, 1, 1)))
    1.1f0^(sum([sum(targetSt[ii] .* t[ii]) for ii = 0:2]) + 1f-5 * norm(x)^2)
end
scores = [objDCT20(popul[:,ii]) for ii in 1:size(popul, 2)]
scoresInv = [objDCT25(populInv[:,ii]) for ii in 1:size(popul, 2)]
iScores = sortperm(scores)
iScoresInv = sortperm(scoresInv)
scoresInv[iScoresInv]
bestEx = idct(popul[:,iScores[1]], 1)
bestExInv = idct(populInv[:,iScores[1]], 1)
plot(bestExInv)
using LaTeXStrings
function moveToFront!(respp, extraDims=3, percent=.15, padBy=150)
    ax = axes(respp)
    sorted = sort(reshape(abs.(respp), (length(respp), :)), dims=1);
    keepThresh = sorted[floor(Int, (1 - percent) * length(sorted))]

    for (iθ, respθ) in enumerate(eachslice(respp, dims=extraDims))
        viableLocs = cat([abs.(respθ[:,cross]) .> keepThresh for cross in
                          1:size(respθ, 2)]..., dims=2)
        firstSignificantVal = findfirst.(x for x in eachcol(viableLocs)) # find
        # the first in each cross-range location
        firstSignificantVal[firstSignificantVal .== nothing] .= 0 # findlast
        # returns nothing rather than zero, so convert
        moveBy = minimum(firstSignificantVal) .- padBy
        accessLoc = (ax[1:extraDims - 1]..., iθ, ax[(extraDims + 1):end]...)
        respp[accessLoc...] = circshift(respθ, (-moveBy, 0))
    end
end
DATA_DIR = "/Elysium/dsweber/data/helmholtzSolver/"
OLD_DATA_DIR = "/Elysium/dsweber/data/sonarProject/bodin_synthetic/"
function newData(shape, speed::Real, dist::Real, useJLD2)
    jld2Version = joinpath(DATA_DIR, shape, "$(speed)", "$(dist)",
                           "resp$(Float64(speed))_$(dist).jld2")
    println(jld2Version)
    if isfile(jld2Version) && useJLD2
        JLD2.jldopen(jld2Version, "r", mmaparrays=true) do file
            resp = file["resp"]
        end
    else
        println("loading $(jld2Version[1:end - 1])")
        JLD2.jldopen(jld2Version[1:end - 1], "r") do file
            resp = file["resp"]
        end
    end
    resp = permutedims(resp, (1, 3, 2))
    return reshape(resp, (size(resp)[1:2]..., 1,
                          size(resp, 3)))
end
function loadData(; shape="sharkfin", speed=2000, dist=10.2, onGpu=true,
                  useJLD2=true, oldData=false, normalize=false, noise=false,
                  SNRdB=5, moveForward=true)
    if oldData
        if shape != "sharkfin"
            result = oldLoad(shape=shape, speed=speed, dist=dist)
        else
            result = oldLoad(speed=speed, dist=dist)
        end
    else
        result = newData(shape, speed, dist, useJLD2)
    end
    if moveForward
        moveToFront!(result, 4)
    end
    if normalize
        result = normalizePart(result)
    end
    if noise
        η = randn(eltype(result), size(result))
        η = normalizePart(η)
        result .+= 10^(-SNRdB / 20) * η
    end
    if onGpu
        return CuArray(result)
    else
        return result
    end
end
function wiggle!(args...; kwargs...)
    @nospecialize
    local plt
    try
        plt = current()
    catch
        return wiggle(args..., kwargs...)
    end
    wiggle!(current(), args...; kwargs...)
end

function wiggle!(plt::Plots.Plot,
                 wav::AbstractArray{T,2};
                 taxis::AbstractVector=1:size(wav, 1),
                 zaxis::AbstractVector=1:size(wav, 2),
                 sc::Real=1,
                 lineColor=:black,
                 fillColor=:black,
                 Overlap::Bool=true,
                 Orient::Symbol=:across,
                 ZDir::Symbol=:normal,kwargs...) where T <: Number
    # Set axes
    (n, m) = size(wav)

    # Sanity check
    @assert Orient ∈ [:across, :down]
    @assert ZDir ∈ [:normal, :reverse]
    if length(taxis) != n
        error("Inconsistent taxis dimension!")
    end
    if length(zaxis) != m
        error("Inconsistent zaxis dimension!")
    end

    if !(lineColor isa Array || lineColor isa Tuple)
        lineColor = [lineColor for i = 1:n]
    end
    if !(fillColor isa Array || fillColor isa Tuple)
        fillColor = [fillColor for i = 1:n]
    end

    # For calculation purposes
    maxrow = zeros(m)
    minrow = zeros(m)
    for (i, wavᵢ) in enumerate(eachcol(wav))
        maxrow[i] = maximum(wavᵢ)
        minrow[i] = minimum(wavᵢ)
    end

    # Scale the data for plotting
    dz = mean(diff(zaxis))
    if Overlap
        wamp = 2 * dz * (sc / maximum(maxrow - minrow)) * wav
    else
        wmax = maximum(maxrow) <= 0 ? 0 : maximum(maxrow)
        wmin = minimum(minrow) >= 0 ? 0 : minimum(minrow)
        wamp = sc * wav / (wmax - wmin)
    end

    # Set initial plot
    t0 = minimum(taxis)
    t1 = maximum(taxis)
    z0 = minimum(zaxis)
    z1 = maximum(zaxis)
    if Orient == :down
        plot!(plt; xlims=(z0 - dz, z1 + dz), ylims=(t0, t1), yflip=true, legend=:none, kwargs...)
    else
        plot!(plt; xlims=(t0, t1), ylims=(z0 - dz, z1 + dz), legend=:none, kwargs...)
    end
    if ZDir == :reverse
        wamp = reverse(wamp, dims=2)
    end

    # Plot each wavelet
    for (i, wampᵢ) in enumerate(eachcol(wamp))
        t = deepcopy(taxis)
        w_sign = sign.(wampᵢ)
        for j in 1:(n - 1)
            if (w_sign[j] != w_sign[j + 1] && w_sign[j] != 0 && w_sign[j + 1] != 0)
                wampᵢ = [wampᵢ; 0]
                t = [t; t[j] - wampᵢ[j] * (t[j + 1] - t[j]) / (wampᵢ[j + 1] - wampᵢ[j])]
            end
        end
        ix = sortperm(t)
        t = t[ix]
        wampᵢ = wampᵢ[ix]
        len = length(t)
        indperm = [1:len; len:-1:1]
        inputx = t[indperm]
        inputy = zaxis[i] .+ [wampᵢ; min.(wampᵢ[len:-1:1], 0)]
        # In the plot! functions below, theoretically speaking, either fillrange = zaxis[k]
        # or fillrange=[zaxis[k], zaxis[k]+dz] should be used. However, those do not
        # generate the desired plots as of O3/19/2018. Somehow, the relative value of 0,
        # i.e., fillrange=0, works well, which is used temporarily.
        if Orient == :down
            plot!(plt, inputy, inputx; fillrange=0, fillalpha=0.75,
                  fillcolor=fillColor[i], linecolor=lineColor[i], orientation=:v, kwargs...)
        else
            plot!(plt, inputx, inputy; fillrange=0, fillalpha=0.75,
                  fillcolor=fillColor[i], linecolor=lineColor[i], kwargs...)
        end
    end
    return plt
end
wiggle(args...; kwargs...) = wiggle!(Plots.Plot(), args...; kwargs...)
wiggle(plt::Plots.Plot, args...; kwargs...) = wiggle!(deepcopy(plt), args...; kwargs...)
triDat = reshape(loadData(shape="triangle", onGpu=false), (:, 1, 481, 46))
sfDat = reshape(loadData(shape="sharkfin", onGpu=false), (:, 1, 481, 46))
rng = MersenneTwister(34)
nExs = 19
indsTri = zip(rand(rng, 1:481, nExs), rand(rng, 1:46, nExs))
indsSf = zip(rand(rng, 1:481, nExs), rand(rng, 1:46, nExs))
triExs = cat([triDat[:,1,ii,jj] for (ii, jj) in indsTri]..., dims=2)
sfExs = cat([sfDat[:,1,ii,jj] for (ii, jj) in indsSf]..., dims=2)
divider = cat(["" for ii = 1:floor(Int, nExs / 2)]..., dims=1)
yTicks = cat(divider..., "Triangles", divider..., L"St^{-1}(\beta)", divider..., "Sharkfins", divider..., dims=1)
lineColors = cat([:blue for ii = 1:nExs]..., :black, [:green for ii = 1:nExs]..., dims=1)
p = wiggle([triExs 1 * bestEx * norm(triExs) / norm(bestEx) sfExs]; Overlap=true, rotation=90,
           lineColor=lineColors,
           xticks=(100:100:600, ["", "",""]),
           yticks=(1:(nExs * 2 + 1), cat(divider..., "Triangles", divider..., L"St^{-1}(\beta)", divider..., "Sharkfins", divider..., dims=1)),
           title="Randomly selected training waveforms\n and the pseudo-inverse of the regression vector")
savefig(joinpath(figDir, "compareTriEmphWithRandomExamples.pdf"))
savefig(joinpath(figDir, "compareTriEmphWithRandomExamples.png"))
p = wiggle([triExs -1 * bestEx * norm(triExs) / norm(bestEx) sfExs]; Overlap=true, rotation=90,
           lineColor=lineColors,
           xticks=(100:100:600, ["", "",""]),
           yticks=(1:(nExs * 2 + 1), cat(divider..., "Triangles", divider..., L"St^{-1}(\beta)", divider..., "Sharkfins", divider..., dims=1)),
           title="Randomly selected training waveforms\n and the pseudo-inverse of the regression vector")
savefig(joinpath(figDir, "compareTriEmphWithRandomExamplesNeg.pdf"))
savefig(joinpath(figDir, "compareTriEmphWithRandomExamplesNeg.png"))


p = wiggle([triExs bestExInv * 1 * norm(triExs) / norm(bestExInv) sfExs]; Overlap=true, rotation=90,
           lineColor=lineColors,
           xticks=(100:100:600, ["", "",""]),
           yticks=(1:(nExs * 2 + 1), yTicks),
           title="Randomly selected training waveforms\n and the pseudo-inverse of the negative regression vector")
savefig(joinpath(figDir, "compareSfEmphWithRandomExamples.pdf"))
savefig(joinpath(figDir, "compareSfEmphWithRandomExamples.png"))
p = wiggle([triExs -bestExInv * 1 * norm(triExs) / norm(bestExInv) sfExs]; Overlap=true, rotation=90,
           lineColor=lineColors,
           xticks=(100:100:600, ["", "",""]),
           yticks=(1:(nExs * 2 + 1), yTicks),
           title="Randomly selected training waveforms\n and the pseudo-inverse of the negative regression vector")
savefig(joinpath(figDir, "compareSfEmphWithRandomExamplesNeg.pdf"))
savefig(joinpath(figDir, "compareSfEmphWithRandomExamplesNeg.png"))
nExs = 13
indsTri = zip(rand(rng, 1:481, nExs), rand(rng, 1:46, nExs))
indsSf = zip(rand(rng, 1:481, nExs), rand(rng, 1:46, nExs))
triExs = cat([triDat[:,1,ii,jj] for (ii, jj) in indsTri]..., dims=2)
sfExs = cat([sfDat[:,1,ii,jj] for (ii, jj) in indsSf]..., dims=2)
divider = cat(["" for ii = 1:floor(Int, nExs / 2)]..., dims=1)
p = wiggle([triExs 1 * bestEx * norm(triExs) / norm(bestEx) bestExInv * 1 * norm(triExs) / norm(bestExInv) sfExs]; Overlap=true, rotation=90,
           lineColor=cat([:blue for ii = 1:nExs]..., :blue, :green, [:green for ii = 1:nExs]..., dims=1),
           xticks=(100:100:600, ["", "",""]),
           yticks=(1:(nExs * 2 + 2), cat(divider..., "Triangles", divider..., L"St^{-1}(\pm\beta)", divider..., "Sharkfins", divider..., dims=1)),
           title="Randomly selected training waveforms and the pseudo-inverse\n of the regression vector and its negative")
savefig(joinpath(figDir, "compareTriSfEmphWithRandomExamples.pdf"))
savefig(joinpath(figDir, "compareTriSfEmphWithRandomExamples.png"))
Plots.rotation!(90)
tickrotation!(90)
title!("")
