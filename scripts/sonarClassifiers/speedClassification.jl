println("the very beginning")
# plotting the actual sonar data
#using Revise
using Pkg
Pkg.activate("../../")
println("using LinearAlgebra, Plots, FFTW")
using LinearAlgebra, Plots, FFTW
println("sonarProject, CUDA, Flux,ContinuousWavelets")
using sonarProject, CUDA, Flux,ContinuousWavelets
println("ScatteringTransform")
using ScatteringTransform
println("Random, GLMNet, LaTeXStrings")
using Random, GLMNet, LaTeXStrings
println("JLD2, FileIO")
using JLD2, FileIO
println("finished with the usings")
# cd("/home/dsweber/sonarProject/results/synthetic/1D")
# cd("/home/dsweber/allHail/Fundamentals/Dissertation/scripts/sonarClassifiers")
# w = @load "beforeFeb2020/2kvs2500/triangleacc/scale[8_1_1]pool[2_2_2]_1DScattering.jld2"
# resAVFT = load("/home/dsweber/allHail/Fundamentals/Dissertation/results/2k2500chooseAngles/triangle1Dist/glmnet/AVFT1D.jld2")["results"]
# logitAVFT = load("/home/dsweber/allHail/Fundamentals/Dissertation/results/2k2500chooseAngles/triangle1Dist/glmnet/AVFT1D_logit.jld2")["logit"]
# plot(logitAVFT[1].λ, logitAVFT[1].pct_dev, xscale=:log10)
# resAVFT.l
# fieldnames(typeof(logitAVFT.lpm.rr))
# resST = load("/home/dsweber/allHail/Fundamentals/Dissertation/results/2k2500chooseAngles/triangle1Dist/glmnetDiffAveLen/Scale1.0pool2.0__2.0__2.0__outputPool10__10__10__decrease1.0.jld2")
# res = (resAVFT["results"], resST["results"])
# cd("/home/dsweber/allHail/Fundamentals/Dissertation/")
# typeof(transforms)
# typeof((resAVFT["results"], resST))
# plotROC(res, transforms, "this name")
# res[1]
transforms = [AVFT{1}(),
              ScatteringJL(nChunks=10, poolBy = 2, Q = (6.0, 6.0, 1.0), extraOctaves = (-2.5,-2,0), outputPool=10, β=(1, 1, 1.0), cw=Morlet(π), aveLen = (-2, -1.5, 2), useGpu=true), #morl but better
              # ScatteringJL(nChunks=10, poolBy = 2, Q = (8, 6.0, 1.0), extraOctaves = (-1,0.5,0), outputPool=10, β=(1.5, 1, 1.0), cw=[dog1, dog2], aveLen = (-1, 0, 2),useGpu=true), #dog1 and dog2
              # ScatteringJL(nChunks=10, poolBy = 2, Q = (8, 6.0, 1.0), extraOctaves = (-1,0.5,0), outputPool=100, β=(1.5, 1, 1.0), cw=[dog1, dog2], aveLen = (-1, 0, 2),useGpu=true), # output pooling
              # ScatteringJL(nChunks=10, poolBy = 2, Q = (8, 6.0, 1.0), extraOctaves = (-2,0.5,0), outputPool=10, β=(1.5, 1, 1.0), cw=dog2, aveLen = (-1, 0, 2), useGpu=true), # just dog2
              # ScatteringJL(nChunks=10, poolBy = 2, Q = (8, 6.0, 1.0), extraOctaves = (-2.5, -1, 0), outputPool = 10, β = (1.5, 1, 1.0), cw = Morlet(π), aveLen = (-1, -2, 2), useGpu=true), # morlet
              ]
classi = glmnetModel
dists = [10.2]
rname = "../../results/2k2500chooseAngles/triangle1Dist/lassojl/"
rP = runParameters(rname, shapes = ("tri", "tri"), dists=dists, backend=classi, speeds=(2000, 2500), transforms=transforms, saveName = rname, check_prefit=false, prefit=false, maxit=1e9)
res = rP()
exit()

using MLBase
l = rP.backend; tra = transform
logit.accuracy
# 8 runs
for (rname,rP) in rPdict
    dictOfResults[rname] = rP()
end
save("newTransform" * Dates.format(today(), "d-m-Y") * ".jld2", "results", dictOfResults,
     "cleanNaming", cleanNaming, "transforms", formatJLD(transforms))

results = dictOfResults
longNames = collect(keys(results))
plotROC(results[longNames[1]], transforms, cleanNaming[longNames[1]])

nn = 1; wef =
    plotROC(results[longNames[nn]],transforms,cleanNaming[longNames[nn]],
            legend = :bottomright,guidefontsize=8)
plts = Array{typeof(wef)}(undef, length(longNames)*2)
legendSize = 6; titleSize=10; labelsize = 8
plottingInOrder = 1:5#[1,2,5,4]
for (pltOrder,dictOrder) in enumerate(plottingInOrder)
    longName = longNames[dictOrder]
    plts[pltOrder] = plotROC(results[longName], transforms,
                             cleanNaming[longName], legendfontsize=legendSize,
                             titlefontsize=titleSize, guidefontsize=labelsize,legend=:bottomright)
end

longGLMNames = collect(keys(dictOfResults))

plottingInOrderUnmoved = 1:5
for (pltOrder,dictOrder) in enumerate(plottingInOrderUnmoved)
    longName = longNames[dictOrder]
    plts[length(plottingInOrder)+pltOrder] = plotROC(dictOfResults[longName], transforms, cleanNaming[longName],legendfontsize=legendSize,titlefontsize=titleSize)
end





# deciding on the wavelet parameters
saveDir = "../figures/sonar/"
plotlyjs()
dat1 = reshape(loadData(shape="triangle", speed=[2000,2500], normalize=true, onGpu=false, noise=true), (641, 1, 481, :))
dat2 = reshape(loadData(shape="sharkfin", normalize=true, onGpu=false, noise=true), (641, 1, 481, 46))
using NearestNeighbors, MLDataUtils, MultivariateStats, Statistics
triRfft = rfft(reshape(dat1, (641,:)), 1)
St = scatteringTransform((641, 1, 100), 2; poolBy=2, Q=(6,6,1), extraOctaves = (-2.5, -2, 0), outputPool=10, cw = Morlet(π), β=(1,1,1), averagingLength=(-2,-1.5,2))
a, b, c = getWavelets(St)
(size(a,2), size(b,2), size(c,2))
plFirstWavLog = plot(abs.(a), legend=false, title="First Layer Wavelets",xscale=:log10)
plTargetLog = plot([abs.(mean(triRfft,dims=2)) abs.(std(triRfft,dims=2))], xscale=:log10, xlabel="log Frequency (Hz)", labels=["mean" "std"])
plot(plTargetLog, plFirstWavLog,layout=(2,1))

plFirstWav = plot(abs.(a), legend=false, title="First Layer Wavelets",)
plTarget = plot([abs.(mean(triRfft,dims=2)) abs.(std(triRfft,dims=2))], xlabel="Frequency (Hz)", labels=["mean" "std"])
plot(plTarget, plFirstWav,layout=(2,1))

gSt = gpu(St)
size(gpu(reshape(dat1, (641, 1, :))))
sampInds = rand(1:size(reshape(dat1, (641, 1, :)),3),100)
firstLayRes = gSt.mainChain[3](gSt.mainChain[2](gSt.mainChain[1](gpu(reshape(dat1, (641, 1, :))[:,:,sampInds]))))
triFirstRfft = rfft(reshape(firstLayRes, (320,:)),1)
triFirstMean = abs.(mean(cpu(triFirstRfft),dims=2))
triFirstStd = abs.(std(cpu(triFirstRfft),dims=2))

plFirstTargetLog = plot([triFirstMean triFirstStd],xscale=:log10, labels=["mean" "std"])
plSecondWavLog = plot(abs.(b), legend=false, title="Second Layer Wavelets", xscale=:log10)
plot(plFirstTargetLog, plSecondWavLog,layout=(2,1))

plFirstTarget= plot([triFirstMean triFirstStd], labels=["mean" "std"])
plSecondWav= plot(abs.(b), legend=false, title="Second Layer Wavelets",)
plot(plFirstTarget, plSecondWav,layout=(2,1))

w1, w2, w3 = ScatteringTransform.getWavelets(St;spaceDomain=true)
size(w1)
plot(real.(ifft([w1;zeros(size(w1).-(1,0))],1))[:,8])

ScatteringTransform.originalDomain(w1,)
p1 = plot(plot(abs.(a), legend=false, title="First Layer Wavelets"), plot([abs.(mean(rfft(d .- mean(d, dims=1), 1), dims=2)) abs.(std(rfft(d, 1), dims=2))], labels=["mean" "std"], title="Data Mean and Std"), legendfontsize=6, layout=(2, 1))
nDat = [abs.(rfft(reshape(dat1, (641,481*46)), 1)) abs.(rfft(reshape(dat2, (641,481*46)), 1))]
labels = [zeros(481*46); ones(481*46)]
(trainX, trainY), (testX, testY) = splitobs(shuffleobs((nDat, labels)), at=.5)
pca = fit(PCA, trainX, maxoutdim=80)
aTree = KDTree(trainX)
idxs, dists = knn(aTree, testX, 10)
inds = idxs[1]
classifyInds(inds) = sum(trainY[inds])/10
classified = classifyInds.(idxs)
sum(abs.(classified .- testY))/length(testY) #probabilistic estimate
count(round.(classified) .== testY)/length(testY)

dat2 = reshape(loadData(shape="triangle", normalize=true, onGpu=false, noise=true, speed=2500), (641, 1, 481, 46))
poolBy = 2; Q = (8, 6.0, 1.0); extraOctaves = (-2,0.5,0); outputPool=10; β=(1.5, 1, 1.0); cw=[dog2, dog2]; aveLen = (-1, 0, 2)
St = gpu(scatteringTransform((641, 1, chunkSize), 2; poolBy=poolBy, Q=Q, extraOctaves = extraOctaves, outputPool=outputPool, β=β, cw=cw, averagingLength=aveLen))

St2 = scatteringTransform((641, 1, chunkSize), 2; poolBy=2, Q=(6, 8.0, 1.0), extraOctaves = (-1,.5,0),outputPool=10, β=(1.5, 1, 1.0), cw=dog2, averagingLength=(-1, 0, 4))
a,b, c = getWavelets(St2)
# comparing the filters and some data
plot(heatmap(abs.(a[1:100, :]),title="$(size(a))"),
     heatmap(abs.(b),title="$(size(b))"),
     heatmap(abs.(reshape(c, (:,1))),title="$(size(c))"),
     heatmap(abs.(rfft(dat1[:,1,341,11:11]))[1:50,:]), legend=false)
plot(plot(abs.(a),title="$(size(a))"), plot(abs.(b),title="$(size(b))"), plot(abs.(c),title="$(size(c))"), plot(abs.(rfft(dat1[:,1,341,11]))), legend=false)
using FFTW
@time ex41 = St(cu(dat1[:, :, 150 .+ (0:(chunkSize-1)), 4]))
@time ex42 = St(cu(dat2[:, :, 150 .+ 10*(0:(chunkSize-1)), 4]))
exDiff = ex41-ex42
sum([exDiff[:,i:i] for i=1:10])
justI = cpu(sum([exDiff[:,i:i] for i=1:10]))
jointPlot(cpu(sum([exDiff[:,i:i] for i=1:10])), nothing, "Ave Diff of Examples", :redsblues, cpu(St), 156)
jointPlot(cpu(exDiff),1, "Diff of Examples", :redsblues, cpu(St), 156)
plotSecondLayer(cpu(ex41[:, 1]), cpu(St))
sz = size(ex41[2])
k = 9; plot(heatmap(ex41[1][:,:,k]',c=:viridis,title="first 1"), heatmap(ex42[1][:,:,k]',c=:viridis,title="first 2"), heatmap([norm(ex41[2][:,i,j, k]) for i in 1:sz[2], j in 1:sz[3]], c=cgrad(:viridis, scale=:log10),title="second 1"), heatmap([norm(ex42[2][:,i,j, k]) for i in 1:sz[2], j in 1:sz[3]], c=cgrad(:viridis, scale=:log10),title="second 2"),layout=(2,2))
plot(heatmap(abs.(St.mainChain[1](dat1[:, :, 150 .+ 10*(0:(chunkSize-1)), 4])[:,:,1,k]')), heatmap(abs.(St.mainChain[1](dat2[:, :, 150 .+ 10*(0:(chunkSize-1)), 4])[:,:,1,k]')))
plot([dat1[:,1,150+k*10,4] dat2[:,1,150+k*10,4]])
specificEx = gpu(dat1[:,:,1:10,4])
plot(abs.(rfft(specificEx[:,1,1], 1)))
gr(size=2.5 .* (480, 180))
plot(specificEx[:, 1, 241])
plot(res[0][:,1,241],legend=false,title="Layer zero")
savefig(saveDir * "tri2000simpleEx_lay0.pdf")
gr(size=2 .* (340, 150))
heatmap(res[1][:,:,241,1]', title="Layer one", xlabel="time", ylabel="frequency", c=:viridis)
savefig(saveDir * "tri2000simpleEx_lay1.pdf")
plotSecondLayer(res[:,241])
savefig(saveDir * "tri2000simpleEx_lay2.pdf")


specificEx = gpu(dat1[:,:,1:10,4])
nEx = size(dat1)[end]
@time res1 = St(specificEx)
flattened = ScatteringTransform.flatten(res1)
output1 = zeros(size(flattened,1), 481, nEx);
output2 = ones(size(flattened,1), 481, nEx);
for ii = 1:nEx
    println("at $ii out of $nEx")
    output1[:,:, ii] = ScatteringTransform.flatten(St(dat1[:,:,:,ii]))
    output2[:,:, ii] = ScatteringTransform.flatten(St(dat2[:,:,:,ii]))
end
count(output1 .==0) # they all worked
o1 = reshape(output1, (:, 481 * nEx))
o2 = reshape(output2, (:, 481 * nEx))
d = [o1 o2]
y = [zeros(481*nEx); ones(481*nEx)]
λ = 10. .^range(-6,0,length=50)
rng=MersenneTwister(2)
@time cvp = glmnetcv(d', y, lambda=λ, rng=rng)
cvp
"""
    indLambda = chooseBestLambda(cv)
the best lambda is the one which is has the largest value of lambda still within
a stdloss of the minimal value. This returns the index, not the value of lambda
"""
function chooseBestLambda(cv)
    lambdamin(cv)
    argmin(cv.meanloss)
    minimum(cv.meanloss)
    findlast((cv.meanloss .- cv.stdloss) .< minimum(cv.meanloss))
end
iλ = chooseBestLambda(cvp)
map(length, (cvp.lambda, cvp.meanloss, cvp.stdloss))
nFit = minimum(map(length, (cvp.lambda, cvp.meanloss, cvp.stdloss)))
gr()
plot(cvp.lambda[1:nFit], cvp.meanloss .+ cvp.stdloss, fillcolor=:blue, fill=cvp.meanloss .- cvp.stdloss, legend=:bottomright, scale=:log10, label="std")
plot!(cvp.lambda[1:nFit], cvp.meanloss,scale=:log10, label="mean loss")
vline!([cvp.lambda[chooseBestLambda(cvp)]],label=L"best\ \mu", title="CV Lasso convergence")
probs = GLMNet.predict(cvp.path, d', iλ, outtype=:prob)
norm(y - probs)
predictions = GLMNet.predict(cvp.path, d', iλ)
count(y .!== round.(predictions)) / length(y) # misclassification rate
size(cvp.path.betas)
βRoll = roll(relu.(cvp.path.betas), St)

roc = sonarProject.makeROC(probs, [y (y .- 1)])
sonarProject.AUC(roc[:,1], roc[:,2])
plot(roc[:,1], roc[:,2])

rollβ = βRoll; i = iλ; thingName = "Speed"; cSymbol = :redsblues; δt=1000
function jointPlot(rollβ,i,thingName,cSymbol, St, δt=1000, freqsigdigits=3)
    clims = (min(minimum.(justI)...), max(maximum.(justI)...))
    toHeat = sum(justI[2], dims=1)[1,:,:]
    firstLay = justI[1]
    zeroLay = justI[0]
    toHeat[toHeat.==0] .= -Inf    # we would like zeroes to not actually render
    firstLay[firstLay.==0] .=-Inf # for either layer

    zeroAt = -clims[1]/(clims[2]-clims[1]) # set the mid color switch to zero
    c = cgrad(cSymbol, [0,zeroAt])
    p2 = plotSecondLayer(justI, cpu(St), title="Second Layer", toHeat=toHeat, logPower=false, c=c, clims=clims, subClims=clims,cbar=false,xVals=(.000,.993),yVals=(0.0,0.994), )
    freqs = getMeanFreq(St, δt)
    freqs = map(x -> round.(x, sigdigits=freqsigdigits), freqs)
    p1 = heatmap(firstLay, c=c, title="$(thingName) First Layer", ylabel="location",clims=clims, cbar=false,xticks=false)
    p0 = heatmap(zeroLay, c=c, title="Zeroth Layer", ylabel="location", clims=clims, cbar=false,xticks=nothing)
    colorbarOnly = scatter([0,0], [0,1], zcolor=[0,3], clims=clims, xlims=(1,1.1), xshowaxis=false, yshowaxis=false, label="", c=c, grid=false, framestyle=:none)
    lay = @layout [a{.1w} grid(2,1) b{.04w}]
    plot(p0,p1,p2, colorbarOnly,layout=lay)
end
ScatteringTransform.jointPlot(βRoll[:, iλ], "Speed Classification", :redsblues, cpu(St); δt = 156)
savefig(saveDir * "../figures/SpeedCoeffsSubsample100Dog2.pdf")
save("../results/SpeedFittingSubsample100Dog2.jld2", "cvp", cvp, "poolBy", poolBy, "Q", Q, "extraOctaves", extraOctaves, "outputPool", outputPool, "cw", cw, "aveLen", aveLen)



using MLDatasets, MLDataUtils
using HDF5, JLD2, FileIO
using Base.GC
using Revise
using sonarProject, JLD2
using collatingTransform
using LinearAlgebra, Statistics, Plots, Printf
using Distributed, ScatteringTransform, KymatioWrapper
