using Revise
using Plots
using Wavelets, FFTW
using Shearlab

t = (-5:.01:5) .+ .00000001
f1 = randn(length(t))

plot([icwt(cwt(f1)) * 100 .1 * f1])
f1
-f1

n1 = length(f1)
c = CFW(WT.Morlet())
WT.getScales(n1, c)


J = 11; n = 2^J
x = testfunction(n, "Bumps")
plot(x)
heatmap(abs.(cwt(x)'))
plot([icwt(cwt(x)) x / 100])
size(cwt(max.(0, t) ./ t + sin.(10 * t) .- 10))
heatmap(abs.(cwt(max.(0, t) ./ t + sin.(10 * t) .- 10))')
plot(t, max.(0, t) ./ t + sin.(10 * t))
plot(t, exp.(-(t .- 3).^2 * 4) + exp.(-(t .+ 2).^2 * 4))
ψ = exp.(im .* randn(1001, 24)) .* [zeros(1001, 10) exp.(-(t .- 3).^2 * 4) zeros(1001, 10) exp.(-(t .+ 2).^2 * 4) zeros(1001, 2)]
heatmap(randn(10, 10), seriesalpha=randn(10, 10))


x = .1 * randn(1001) .+ .5 * sin.(2π * t) .* max.(0, t .- 2) ./ (t .- 2) + exp.(-(t .+ 3).^2 * 2^(10)) .* max.(0, -t) ./ (-t)
heatmap(abs.(cwt(x)))
plot(t, x,grid=false,label="",framestyle=:none,color="black")
savefig("separatedFrameCoefficients.pdf")
ψx = cwt(x)
plot(abs.(ψx[1:2:1001,5]),linewidth=5, ylim=(0, maximum(abs.(ψx[:,5]) * 1.01)), framestyle=:none,label="",color="black")
savefig("sineSignalFrame5.pdf")
plot(abs.(ψx[1:2:1001,9]), linewidth=5, ylim=(0, maximum(abs.(ψx[:,5]) * 1.01)), framestyle=:none, label="",color="black")
savefig("expSignalFrame9.pdf")
ψ, ω = computeWavelets(1001, CFW(WT.Morlet()))
plot(ifftshift(irfft(ψ[:,1], 2002)))
plot(ifftshift(irfft(ψ[:,5], 2002))[500:1500],label="", linewidth=5, color="black",framestyle=:none)
savefig("psi5.pdf")
plot(ifftshift(irfft(ψ[:,9], 2002))[500:1500],label="", linewidth=2, color="black",framestyle=:none)
savefig("psi9.pdf")
plot(ψ[:,9])


joined = abs.(ψx[1:2:1001,9]) + abs.(ψx[1:2:1001,5])
plot(joined,linewidth=5, ylim=(0, maximum(abs.(ψx[:,5]) * 1.01)), framestyle=:none,label="",color="black")
savefig("summed.pdf")

plot(irfft(rfft(abs.(ψx[1:2:1001, 9])) .* rfft(ϕ), 501))
ϕ = exp.(-(t[1:2:1001]).^2)
plot(ϕ, linewidth=5, framestyle=:none,label="",color="black")
savefig("averaging.pdf")
250 + 250 + 501
averaged5 = ifftshift(irfft(rfft(abs.([ψx[1:2:1001, 5]; reverse(abs.(ψx[1:2:1001, 5]))])) .* rfft([zeros(250); ϕ; zeros(251)]), 1002))[1:501]
averaged9 = ifftshift(irfft(rfft([abs.(ψx[1:2:1001, 9]); reverse(abs.(ψx[1:2:1001, 9]))]) .* rfft([zeros(250); ϕ; zeros(251)]), 1002))[1:501]
averagedJoined = ifftshift(irfft(rfft([joined; reverse(joined)]) .* rfft([zeros(250); ϕ; zeros(251)]), 1002))[1:501]
maxAll = max(maximum(averaged5), maximum(averaged9), maximum(averagedJoined))
plot(averaged5, linewidth=5, ylim=(0, maxAll * 1.01), framestyle=:none,label="",color="black")
savefig("averaged5.pdf")
plot(averaged9, linewidth=5, ylim=(0, maxAll * 1.01), framestyle=:none,label="",color="black")
savefig("averaged9.pdf")
plot(averagedJoined, linewidth=5, ylim=(0, maxAll * 1.01), framestyle=:none,label="",color="black")
savefig("averagedJoined.pdf")
plot(abs.(cwt(joined))[:,2])

plot(conv(exp.(-(t .+ 3).^2 * 2^2), joined), label="", linewidth=2, color="black",framestyle=:none)



# shearlet figures

using collatingTransform, CUDA, Flux
x = gpu(zeros(Float32, 32, 32, 3, 1)); x[19:23, 16:20, 1,:] .= 1; x[19:20, 16:20,
                                                              2,:] .= 1;
x[19:23, 20:25, 3,:] .= 1;
x = CuArray(x);
@time cL = collatingLayer(size(x), 3 => 49)
cL(x)
using Colorant
cL.layers[1].weight[:,:,]
plot(heatmap(cL.layers[1], dispReal=true, apply=identity, vis=36, colorbar=false, restrict=((35:62) .- 5, 35:62)), heatmap(cL.layers[1], dispReal=true, apply=identity, vis=11, restrict=((35:62) .- 5, (35:62) .- 1)),colorbar=false,framestyle=:none,background_color_outside=RGB(.294, 0, .388)) # .412, 0, .545
savefig("orthoShearlets.pdf")
heatmap(cL.layers[1],dispReal=true, apply=identity,vis=41)
# 35
