using Revise
using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, FFTW, LinearAlgebra, FourierFilterFlux, Wavelets
using Optim, LineSearches
using MLJ, DataFrames, MLJLinearModels
using Plots
using Revise
using CUDA, Zygote, Flux
using Test
using LinearAlgebra
using ScatteringInterpretation
using PerceptualColourMaps
gr(dpi = 400)
saveDir = "../figures/introduction/"
J = 11;
n = 2^J;
function HeaviSine(n, ω = 1)
    t = 0:1/n:1-eps()
    return 2 * sin.(ω * 4π .* t) - sign.(t .- 0.3) - sign.(0.72 .- t)
end
x = HeaviSine(n, 8)
bumps = testfunction(n, "Bumps")
doppler = testfunction(n, "Doppler")
blocks = testfunction(n, "Blocks")
heavysine = HeaviSine(n, 8)
plot(HeaviSine(n, 4))
y = testfunction(n, "Bumps")
plot(x)
t = range(-5, 5, length = n)
using Interpolations

itp = interpolate([3; 4; -3; -2; 3], BSpline(Cubic(Interpolations.Periodic(OnGrid()))))
plot(plot([itp[range(1, 5, length = n)] x], legend = false, title = "Raw signals", linewidth = 2),
    plot(max.([abs.(rfft(itp[range(1, 5, length = n)])) abs.(rfft(x))], 1e-5), yscale = :log10, legend = :best, labels = ["smooth" "HeaviSine"], title = "Absolute value of Positive Fourier Coefficients", linewidth = 2),
    layout = (2, 1))
savefig(joinpath(saveDir, "CoefficientDecay.pdf"))
plot(plot([itp[range(1, 5, length = n)] x], legend = false, title = "Raw signals", linewidth = 2, xlabel = "time"),
    plot(max.([abs.(rfft(itp[range(1, 5, length = n)])) abs.(rfft(x))], 1e-5), yscale = :log10, legend = :best, labels = ["smooth" "HeaviSine"], title = "Absolute value of Positive Fourier Coefficients", linewidth = 2, xlabel = "frequency", ylabel = "log magnitude"),
    layout = (2, 1))
savefig(joinpath(saveDir, "CoefficientDecayLabeled.pdf"))
function plotScalogram(x, cw, title = "", coloring = cgrad(:viridis, scale = :exp))
    freqs = getMeanFreq(ContinuousWavelets.computeWavelets(n, cw)[1], 1000)
    dispFreqs = round.(freqs[2:end], sigdigits = 3)
    CWTx = ContinuousWavelets.cwt(x, cw)
    CWTx = (abs.(CWTx[:, 2:end]) .^ 2)'
    sortedFFT = sort(abs.(rfft(x)), rev = true)
    plot(sortedFFT[sortedFFT.!=0], yscale = :log10)
    plot!(sort(CWTx[:], rev = true), yscale = :log10)
    yspacing = 2:4:(size(CWTx, 1)-1)
    scalogram = heatmap(CWTx, c = coloring, yticks = (yspacing, dispFreqs[yspacing]), ylabel = "Frequency (Hz)", xlabel = "time (ms)", colorbar = false, top_margin = -10Plots.px)
    pltX = plot(x, legend = false, title = title, xticks = (0:500:2048, ""), bottom_margin = -5Plots.px, xlims = (1, 2048))
    colorbar = Plots.scatter([0, 0], [0, 1], xlims = (1, 1.1), marker_z = 3, xshowaxis = false, label = "", colorbar = true, grid = false, xticks = false, yticks = false, framestyle = :grid, left_margin = -8Plots.px, right_margin = 0Plots.px, clims = (minimum(CWTx), maximum(CWTx)), c = coloring)
    blankSpot = Plots.scatter([0, 0], [0, 1], xlims = (1, 1.1), xshowaxis = false, label = "", colorbar = false, grid = false, xticks = false, yticks = false, framestyle = :grid, bottom_margin = -10Plots.px, top_margin = -10Plots.px, left_margin = -42Plots.px, right_margin = -22Plots.px)
    lay = @layout [a{0.15h}; c]
    p1 = plot(pltX, scalogram, layout = lay)
    lay = @layout [a{0.15h}; c]
    pc = plot(blankSpot, colorbar, layout = lay)
    lay0 = @layout [a b{0.11w}]
    plot(p1, pc, layout = lay0)
end
cw = wavelet(Morlet(π), β = 1, boundary = ContinuousWavelets.Periodic, frameBound = 100, averagingLength = -2)
plotScalogram(HeaviSine(n, 8), cw, "")
savefig(joinpath(saveDir, "heavisineScalogram.pdf"))
savefig(joinpath(saveDir, "heavisineScalogram.png"))
plotScalogram(HeaviSine(n, 8), wavelet(dog2, β = 1, boundary = ContinuousWavelets.Periodic, frameBound = 100, averagingLength = -2), "")
savefig(joinpath(saveDir, "heavisineScalogramDog2.pdf"))
savefig(joinpath(saveDir, "heavisineScalogramDog2.png"))
plotScalogram(testfunction(n, "Bumps"), cw, "")
savefig(joinpath(saveDir, "bumpsScalogram.pdf"))
savefig(joinpath(saveDir, "bumpsScalogram.png"))
plotScalogram(testfunction(n, "Bumps"), wavelet(dog2, β = 1, boundary = ContinuousWavelets.Periodic, frameBound = 100, averagingLength = -2), "")
savefig(joinpath(saveDir, "bumpsScalogramDog2.pdf"))
savefig(joinpath(saveDir, "bumpsScalogramDog2.png"))
plotScalogram(testfunction(n, "Bumps"), wavelet(cDb2, β = 1, boundary = ContinuousWavelets.Periodic, frameBound = 100, averagingLength = -2), "")
savefig(joinpath(saveDir, "BumpsScalogramDaubechies.pdf"))
savefig(joinpath(saveDir, "BumpsScalogramDaubechies.png"))
plotScalogram(testfunction(n, "Doppler"), cw, "")
savefig(joinpath(saveDir, "DopplerScalogram.pdf"))
savefig(joinpath(saveDir, "DopplerScalogram.png"))
plotScalogram(testfunction(n, "Doppler"), wavelet(dog2, β = 1, boundary = ContinuousWavelets.Periodic, frameBound = 100, averagingLength = -2), "")
savefig(joinpath(saveDir, "DopplerScalogramDog2.pdf"))
savefig(joinpath(saveDir, "DopplerScalogramDog2.png"))
plotScalogram(testfunction(n, "Blocks"), cw, "")
savefig(joinpath(saveDir, "BlocksScalogram.pdf"))
savefig(joinpath(saveDir, "BlocksScalogram.png"))
plotScalogram(testfunction(n, "Blocks"), wavelet(dog2, β = 1, boundary = ContinuousWavelets.Periodic, frameBound = 100, averagingLength = -2), "")
savefig(joinpath(saveDir, "BlocksScalogramDog2.pdf"))
savefig(joinpath(saveDir, "BlocksScalogramDog2.png"))
plotScalogram(testfunction(n, "Blocks"), wavelet(dog1, β = 1, boundary = ContinuousWavelets.Periodic, frameBound = 100, averagingLength = -2), "")
savefig(joinpath(saveDir, "BlocksScalogramDog1.pdf"))
savefig(joinpath(saveDir, "BlocksScalogramDog1.png"))
freqs = getMeanFreq(n, cw, 1000)
dispFreqs = round.(freqs[2:end], sigdigits = 3)
heavisineCWT = ContinuousWavelets.cwt(x, cw)
bumpsCWT = ContinuousWavelets.cwt(y, cw)
Ŵ, ω = ContinuousWavelets.computeWavelets(n, cw)
[norm(w, Inf) for w in eachslice(Ŵ, dims = 2)]






function mapTo(waveType, isReal = true, window = 1:2047; d = 1, kwargs...)
    if isReal
        c = wavelet(waveType; β = d, kwargs...)
        waves, ω = ContinuousWavelets.computeWavelets(n, c)
        return circshift(irfft(waves, 2 * n, 1), (1024, 0))[window, :]
    else
        c = wavelet(waveType; β = d, kwargs...)
        waves, ω = ContinuousWavelets.computeWavelets(n, c)
        waves = cat(waves, zeros(2047, size(waves, 2)), dims = 1)
        return circshift(ifft(waves, 1), (1024, 0))[window, :]
    end
end
tmp = mapTo(Morlet(π), false; averagingLength = -0.2)[:, 2]
p1 = plot([real.(tmp) imag.(tmp)], title = "Morlet π", labels = ["real" "imaginary"], ticks = nothing, linewidth = 5)
tmp = mapTo(paul2, false, averagingLength = -0.5)[:, 2]
p2 = plot([real.(tmp) imag.(tmp)], title = "Paul 2", labels = ["real" "imaginary"], ticks = nothing, linewidth = 5)
p3 = plot(mapTo(dog2; averagingLength = -1.5)[:, 2], title = "DoG2", legend = false, ticks = nothing, linewidth = 5)
p4 = plot(mapTo(cHaar, true; averagingLength = 1)[:, 2], title = "Haar", legend = false, ticks = nothing, linewidth = 5)
p5 = plot(mapTo(cBeyl, true; d = 1, averagingLength = -0)[:, 2], title = "Beylkin", legend = false, ticks = nothing, linewidth = 5)
p6 = plot(mapTo(cVaid, true; d = 1, averagingLength = -0)[:, 2], title = "Vaidyanathan", legend = false, ticks = nothing, linewidth = 5)
p7 = plot(mapTo(cDb2; d = 1, averagingLength = -0)[:, 2], title = "Daubechies 2", legend = false, ticks = nothing, linewidth = 5)
p8 = plot(mapTo(cCoif2, true; d = 1, averagingLength = -0)[:, 2], title = "Coiflet 2", legend = false, ticks = nothing, linewidth = 5)
p9 = plot(mapTo(cSym4, true; d = 1, averagingLength = -0)[:, 2], title = "Symlet 4", legend = false, ticks = nothing, linewidth = 5)
k = 0600;
p10 = plot(mapTo(cBatt4, true, 1024-k:1024+k; d = 1, averagingLength = -1)[:, 2], title = "Battle-Lemarie, 4", legend = false, ticks = nothing, linewidth = 5);
plot(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, layout = (2, 5), size = 300 .* (5, 2.2))
savefig(joinpath(saveDir, "mothers.pdf"))




cw = wavelet(Morlet(π), β = 1, boundary = ContinuousWavelets.Periodic, frameBound = 100, averagingLength = -2)
bumps = reshape(testfunction(n, "Bumps"), (n, 1, 1))
doppler = reshape(testfunction(n, "Doppler"), (n, 1, 1))
blocks = reshape(testfunction(n, "Blocks"), (n, 1, 1))
heavysine = reshape(HeaviSine(n, 8), (n, 1, 1))
plot(plot(heavysine[:, 1, 1], label = false, title = "HeaviSine"), plot(bumps[:, 1, 1], label = false, title = "Bumps"), plot(doppler[:, 1, 1], label = false, title = "Doppler"), plot(blocks[:, 1, 1], label = false, title = "Blocks"), layout = (4, 1))
savefig(joinpath(saveDir, "allExamples.pdf"))
st = scatteringTransform((n, 1, 1), 2, cw = Morlet(π), convBoundary = FourierFilterFlux.Periodic(), boundary = ContinuousWavelets.Periodic, β = 3, averagingLength = [-2, -1], normalize = false)
bumpSt = st(bumps)
dopplerSt = st(doppler)
blocksSt = st(blocks)
heavysineSt = st(heavysine)
freqsSt = getMeanFreq(st, 1000)
jointPlot(heavysineSt, "", cmap("L20"), st, sharedColorScaling = :linear, allPositive = true, logPower = true, frameTypes = :none, secondFreqSpacing = 1:2:size(bumpSt[2], 2), xlabel = "")
savefig(joinpath(saveDir, "heaviSineScatteredMorlet.pdf"))
savefig(joinpath(saveDir, "heaviSineScatteredMorlet.png"))

jointPlot(bumpSt, "", cmap("L20"), st, sharedColorScaling = :linear, allPositive = true, logPower = true, frameTypes = :none, secondFreqSpacing = 1:2:size(bumpSt[2], 2), xlabel = "")
savefig(joinpath(saveDir, "bumpsScatteredMorlet.pdf"))
savefig(joinpath(saveDir, "bumpsScatteredMorlet.png"))
jointPlot(dopplerSt, "", cmap("L20"), st, sharedColorScaling = :linear, allPositive = true, logPower = true, frameTypes = :none, secondFreqSpacing = 1:2:size(bumpSt[2], 2), xlabel = "")
savefig(joinpath(saveDir, "dopplerScatteredMorlet.pdf"))
savefig(joinpath(saveDir, "dopplerScatteredMorlet.png"))
jointPlot(blocksSt, "", cmap("L20"), st, sharedColorScaling = :linear, allPositive = true, logPower = true, frameTypes = :none, secondFreqSpacing = 1:2:size(bumpSt[2], 2), xlabel = "")
savefig(joinpath(saveDir, "blocksScatteredMorlet.pdf"))
savefig(joinpath(saveDir, "blocksScatteredMorlet.png"))

outPool = 8
st = scatteringTransform((n, 1, 1), 2, cw = dog2, convBoundary = FourierFilterFlux.Periodic(), boundary = ContinuousWavelets.Periodic, β = 1.5, Q = 4, outPool = outPool, normalize = false)
bumpDogSt = st(bumps)
dopplerDogSt = st(doppler)
blocksDogSt = st(blocks)
heavysineDogSt = st(heavysine)
freqsSt = getMeanFreq(st, 1000)
jointPlot(heavysineDogSt, "", cmap("L20"), st, sharedColorScaling = :linear, allPositive = true, logPower = true, frameTypes = :none, secondFreqSpacing = 1:2:size(bumpDogSt[2], 2), xlabel = "")
savefig(joinpath(saveDir, "heaviSineScatteredDoG2.pdf"))
savefig(joinpath(saveDir, "heaviSineScatteredDoG2.png"))

jointPlot(bumpDogSt, "", cmap("L20"), st, sharedColorScaling = :linear, allPositive = true, logPower = true, frameTypes = :none, secondFreqSpacing = 1:2:size(bumpDogSt[2], 2), xlabel = "")
savefig(joinpath(saveDir, "bumpsScatteredDoG2.pdf"))
savefig(joinpath(saveDir, "bumpsScatteredDoG2.png"))
jointPlot(dopplerDogSt, "", cmap("L20"), st, sharedColorScaling = :linear, allPositive = true, logPower = true, frameTypes = :none, secondFreqSpacing = 1:2:size(bumpDogSt[2], 2), xlabel = "")
savefig(joinpath(saveDir, "dopplerScatteredDoG2.pdf"))
savefig(joinpath(saveDir, "dopplerScatteredDoG2.png"))
jointPlot(blocksDogSt, "", cmap("L20"), st, sharedColorScaling = :linear, allPositive = true, logPower = true, frameTypes = :none, secondFreqSpacing = 1:2:size(blocksDogSt[2], 2), xlabel = "")
savefig(joinpath(saveDir, "blocksScatteredDoG2.pdf"))
savefig(joinpath(saveDir, "blocksScatteredDoG2.png"))
ψ1, ψ2 = getWavelets(st, spaceDomain = true)
plot(ψ1[:, end])
plot(ψ2[:, end])
plot(ψ2[:, 1])
heatmap(real.(ψ1))
originalDomain(st)
sizePlot = 1.5 .* (700, 100)
plot(bumps[:, 1, 1], legend = false, title = "Bumps function", xticks = 0:250:n, xlims = (0, n), size = sizePlot)
savefig(joinpath(saveDir, "bumpsFunction.pdf"))
plot(heavysine[:, 1, 1], legend = false, title = "HeaviSine function", xticks = 0:250:n, xlims = (0, n), size = sizePlot)
savefig(joinpath(saveDir, "HeaviSineFunction.pdf"))
plot(doppler[:, 1, 1], legend = false, title = "doppler function", xticks = 0:250:n, xlims = (0, n), size = sizePlot)
savefig(joinpath(saveDir, "dopplerFunction.pdf"))
plot(blocks[:, 1, 1], legend = false, title = "blocks function", xticks = 0:250:n, xlims = (0, n), size = sizePlot)
savefig(joinpath(saveDir, "blocksFunction.pdf"))
