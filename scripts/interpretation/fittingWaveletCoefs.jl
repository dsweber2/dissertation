using Revise
using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, LinearAlgebra
using FFTW, FourierFilterFlux
using Optim, LineSearches, GLMNet
using Plots
using CUDA, Zygote, Flux
using LinearAlgebra
using BlackBoxOptim
using BlackBoxOptim:num_func_evals
using ScatteringInterpretation
NDims = 128
N = 100
popSize = 500
nMinsBBO = 3
perturbRate = 1 / 5
totalMins = 1 * 60
homeDir = "../../results/appendix/fittingCWT/"
figDir = "../../figures/appendix/fittingCWT/"
St = scatteringTransform((NDims, 1, 1), cw=Morlet(π), 2, Q=4, β=[1,1,1], extraOctaves=(0, 0, 0), averagingLength=[-1.5,-1.5,2], outputPool=2 * 4, convBoundary=FourierFilterFlux.Periodic(), boundary=PerBoundary(), normalize=false)
WT = St.mainChain[1]
freqs = getMeanFreq(St, 1000)



println("-----------------------------------------------------------------------------------------")
println("-----------------------------------------------------------------------------------------")
println("------------------------------- starting maxi-mini -------------------------------------")
println("-----------------------------------------------------------------------------------------")
println("-----------------------------------------------------------------------------------------")
if !isdir(homeDir)
    mkpath(homeDir)
end
locDir = joinpath(homeDir, "maxMin")
j = 5
extraName = ""
# if it already exists, append the appropriate count so we get multiple copies
if isfile(locDir * "data.jld2")
    nRepeat = 1
    while isfile(locDir * "data$(nRepeat).jld2")
        nRepeat += 1
    end
    extraName = "$(nRepeat)"
end
saveName = joinpath(locDir, "data$(extraName).jld2")
saveNameDict = joinpath(locDir, "dictionary")
saveSerialName = joinpath(locDir, "data$(extraName)")

if !isdir(locDir)
    mkpath(locDir)
end

function objMaxMinDCT(x̂)
    x = idct(x̂, 1)
    t = St.mainChain[1](x)[:,1:end - 1]
    1.1f0^(-(300 * abs(t[64,j]) - sum(abs.(t))) + 1f-5 * norm(x)^2)
end
setProbW = bbsetup(objMaxMinDCT; SearchRange=(-1000000., 1000000.), NumDimensions=NDims, PopulationSize=popSize, MaxTime=10.0, TraceInterval=5, TraceMode=:silent)
println("pretraining")
bboptimize(setProbW)

pop = dct(pinkStart(NDims, -1, popSize), 1)
adjustPopulation!(setProbW, pop)

nIters = ceil(Int, totalMins / nMinsBBO)
for ii in 1:nIters
    println("----------------------------------------------------------")
    println("round $ii: GO")
    println("----------------------------------------------------------")
    bboptimize(setProbW, MaxTime=nMinsBBO * 60.0) # take some black box steps for the whole pop
    # for jj in 1:NDescents
    #     println("on run $jj of gradient descent")
    #     pathOfDescent, err = ScatteringTransformExperiments.pullAndTrain(setProbW, objSPC; normDiscount=1e-1, N=1000)
    # end
    pop = setProbW.runcontrollers[end].optimizer.population.individuals
    scores = [objMaxMinDCT(x) for x in eachslice(pop, dims=2)]
    iScores = sortperm(scores)

    relScoreDiff = abs(log(scores[iScores[1]]) - log(scores[iScores[round(Int, 4 * popSize / 5 - 1)]])) / abs(log(scores[iScores[1]]))
    if ii > 3 && relScoreDiff .< 0.01 # the relative difference between the best and the worst signals is less than 1%
        println("finished after $(ii) rounds")
        break
    end

    # perturb a fifth to sample the full space if it hasn't converged yet
    perturbWorst(setProbW, scores, perturbRate)
end
# NDescents = 30
pop = setProbW.runcontrollers[end].optimizer.population.individuals
scores = [objMaxMinDCT(x) for x in eachslice(pop, dims=2)]

saveSerial(saveSerialName, setProbW)
save(saveName, "pop", pop, "scores", scores)

histogram(log.(scores) / log(1.1), legend=false, xlabel="coordinate value + norm of input", title="Histogram of log scores\nMaximizing freq $(freqs[1][5]) while minimizing all others")
savefig(joinpath(locDir, "histogramPop$(extraName).pdf"))


if !isdir(joinpath(figDir, "maxMin"))
    mkpath(joinpath(figDir, "maxMin"))
end

# examples
iScores = sortperm(scores)
bestExMaxMin = idct(pop[:, iScores[1]], 1)
# comparing with original wavelet
p1 = plot(bestExMaxMin, legend=false, title="Optimal Fit example", linewidth=2)
ψ = originalDomain(St.mainChain[1])
p2 = plot([circshift(real.(ψ)[:,5], 64) circshift(imag.(ψ)[:,5], 64)], linewidth=2, labels=["Real" "Imag"], title="Target Wavelet")
plot(p1,p2,layout=(2, 1))
savefig(joinpath(figDir, "maxMin", "compareWithTargetWavelet$(extraName).pdf"))
plot([circshift(real.(ψ)[:,5], 64) sign(bestExMaxMin[64]) * (bestExMaxMin * norm(real.(ψ)[:,5]) / norm(bestExMaxMin))], labels=["target" "fit"],linewidth=3,title="Maximizing WT at $(round(freqs[1][j], sigdigits=3))Hz and 64ms,\n Minimize elsewhere")
savefig(joinpath(figDir, "maxMin", "compareDirectWithTargetWavelet$(extraName).pdf"))
# resulting wavelet transform
heatmap(abs.(WT(bestExMaxMin)[:,1:end - 1,1,1]').^2; c=cgrad(:viridis, scale=:exp), framestyle=:box, title="Scalogram from fitting $(round(freqs[1][j], sigdigits=3))Hz",  yticks=(1:length(freqs[1]) - 1, round.(freqs[1], sigdigits=3)), titlefontsize=13, guidefontsize=9, tickfontsize=7, bottom_margin=-3Plots.px, ylabel="Frequency (Hz)", xlabel="Time (ms)")
savefig(joinpath(figDir, "maxMin", "Scalogram$(extraName).pdf"))






println("-----------------------------------------------------------------------------------------")
println("-----------------------------------------------------------------------------------------")
println("------------------------------- starting unnormalzed max -------------------------------------")
println("-----------------------------------------------------------------------------------------")
println("-----------------------------------------------------------------------------------------")
if !isdir(homeDir)
    mkpath(homeDir)
end
locDirMax = joinpath(homeDir, "maximizeUnnormal")
j = 5
extraName = ""
# if it already exists, append the appropriate count so we get multiple copies
if isfile(locDirMax * "data.jld2")
    nRepeat = 1
    while isfile(locDirMax * "data$(nRepeat).jld2")
        nRepeat += 1
    end
    extraName = "$(nRepeat)"
end
saveNameMax = joinpath(locDirMax, "data$(extraName).jld2")
saveNameMaxDict = joinpath(locDirMax, "dictionary")
saveSerialNameMax = joinpath(locDirMax, "data$(extraName)")

if !isdir(locDirMax)
    mkpath(locDirMax)
end

function objMaxDCT(x̂)
    x = idct(x̂, 1)
    t = St.mainChain[1](x)[:,1:end - 1]
    1.1f0^(-abs(t[64,j]) + 1f-5 * norm(x)^2)
end
setProbWMax = bbsetup(objMaxDCT; SearchRange=(-1000., 1000.), NumDimensions=NDims, PopulationSize=popSize, MaxTime=10.0, TraceInterval=5, TraceMode=:silent)
println("pretraining")
bboptimize(setProbWMax)

popMax = dct(pinkStart(NDims, -1, popSize), 1)
adjustPopulation!(setProbWMax, popMax)

nIters = ceil(Int, totalMins / nMinsBBO)
for ii in 1:nIters
    println("----------------------------------------------------------")
    println("round $ii: GO")
    println("----------------------------------------------------------")
    bboptimize(setProbWMax, MaxTime=nMinsBBO * 60.0) # take some black box steps for the whole pop
    # for jj in 1:NDescents
    #     println("on run $jj of gradient descent")
    #     pathOfDescent, err = ScatteringTransformExperiments.pullAndTrain(setProbWMax, objSPC; normDiscount=1e-1, N=1000)
    # end
    popMax = setProbWMax.runcontrollers[end].optimizer.population.individuals
    scoresMax = [objMaxDCT(x) for x in eachslice(popMax, dims=2)]
    iScoresMax = sortperm(scoresMax)

    relScoreDiffMax = abs(log(scoresMax[iScoresMax[1]]) - log(scoresMax[iScoresMax[round(Int, 4 * popSize / 5 - 1)]])) / abs(log(scoresMax[iScoresMax[1]]))
    if ii > 3 && relScoreDiffMax .< 0.01 # the relative difference between the best and the worst signals is less than 1%
        println("finished after $(ii) rounds")
        break
    end

    # perturb a fifth to sample the full space if it hasn't converged yet
    perturbWorst(setProbWMax, scoresMax, perturbRate)
end
# NDescents = 30
popMax = setProbWMax.runcontrollers[end].optimizer.population.individuals
scoresMax = [objMaxDCT(x) for x in eachslice(pop, dims=2)]

saveSerial(saveSerialNameMax, setProbWMax)
save(saveNameMax, "pop", pop, "scoresMax", scoresMax)

histogram(log.(scoresMax) / log(1.1), legend=false, xlabel="coordinate value + norm of input", title="Histogram of log scoresMax\nMaximizing freq $(freqs[1][5]) while minimizing all others")
savefig(joinpath(locDirMax, "histogramPop$(extraName).pdf"))


# examples
iScoresMax = sortperm(scoresMax)
bestExMax = idct(popMax[:, iScoresMax[1]], 1)
plot(bestExMax)

if !isdir(joinpath(figDir, "max"))
    mkpath(joinpath(figDir, "max"))
end
p1 = plot(sign(bestExMax[64]) * bestExMax, legend=false, title="Optimal Fit example", linewidth=2)
ψ = originalDomain(St.mainChain[1])
p2 = plot([circshift(real.(ψ)[:,5], 64) circshift(imag.(ψ)[:,5], 64)], linewidth=2, labels=["Real" "Imag"], title="Target Wavelet")
plot(p1,p2,layout=(2, 1))
savefig(joinpath(figDir, "max", "compareWithTargetWavelet$(extraName).pdf"))
plot([circshift(real.(ψ)[:,5], 64) (sign(bestExMax[64]) * bestExMax * norm(real.(ψ)[:,5]) / norm(bestExMax))], labels=["target" "fit"],linewidth=3,title="Maximizing WT at $(round(freqs[1][j], sigdigits=3))Hz and 64ms, Unnormalized")
savefig(joinpath(figDir, "max", "compareDirectWithTargetWavelet$(extraName).pdf"))
# resulting wavelet transform
heatmap(abs.(WT(bestExMax)[:,1:end - 1,1,1]').^2; c=cgrad(:viridis, scale=:exp), framestyle=:box, title="Scalogram from fitting $(round(freqs[1][j], sigdigits=3))Hz",  yticks=(1:length(freqs[1]) - 1, round.(freqs[1], sigdigits=3)), titlefontsize=13, guidefontsize=9, tickfontsize=7, bottom_margin=-3Plots.px, ylabel="Frequency (Hz)", xlabel="Time (ms)")
savefig(joinpath(figDir, "max", "Scalogram$(extraName).pdf"))




StN = scatteringTransform((NDims, 1, 1), cw=Morlet(π), 2, Q=4, β=[1,1,1], extraOctaves=(0, 0, 0), averagingLength=[-1.5,-1.5,2], outputPool=2 * 4, convBoundary=FourierFilterFlux.Periodic(), boundary=PerBoundary(), normalize=true)
WTN = StN.mainChain[1]

println("-----------------------------------------------------------------------------------------")
println("-----------------------------------------------------------------------------------------")
println("------------------------------- starting normalzed max -------------------------------------")
println("-----------------------------------------------------------------------------------------")
println("-----------------------------------------------------------------------------------------")
if !isdir(homeDir)
    mkpath(homeDir)
end
locDirNor = joinpath(homeDir, "maximizeNormal")
j = 5
extraName = ""
# if it already exists, append the appropriate count so we get multiple copies
if isfile(locDirNor * "data.jld2")
    nRepeat = 1
    while isfile(locDirNor * "data$(nRepeat).jld2")
        nRepeat += 1
    end
    extraName = "$(nRepeat)"
end
saveNameNorm = joinpath(locDirNor, "data$(extraName).jld2")
saveNameNormDict = joinpath(locDirNor, "dictionary")
saveSerialNameNorm = joinpath(locDirNor, "data$(extraName)")

if !isdir(locDirNor)
    mkpath(locDirNor)
end

function objNormDCT(x̂)
    x = idct(x̂, 1)
    t = WTN(x)[:,1:end - 1]
    1.1f0^(-abs(t[64,j]) / norm(t) + 1f-5 * norm(x)^2)
end
setProbWNorm = bbsetup(objNormDCT; SearchRange=(-1000., 1000.), NumDimensions=NDims, PopulationSize=popSize, MaxTime=10.0, TraceInterval=5, TraceMode=:silent)
println("pretraining")
bboptimize(setProbWNorm)

popNorm = dct(pinkStart(NDims, -1, popSize), 1)
adjustPopulation!(setProbWNorm, popNorm)

nIters = ceil(Int, totalMins / nMinsBBO)
for ii in 1:nIters
    println("----------------------------------------------------------")
    println("round $ii: GO")
    println("----------------------------------------------------------")
    bboptimize(setProbWNorm, MaxTime=nMinsBBO * 60.0) # take some black box steps for the whole pop
    # for jj in 1:NDescents
    #     println("on run $jj of gradient descent")
    #     pathOfDescent, err = ScatteringTransformExperiments.pullAndTrain(setProbWNorm, objSPC; normDiscount=1e-1, N=1000)
    # end
    popNorm = setProbWNorm.runcontrollers[end].optimizer.population.individuals
    scoresNorm = [objNormDCT(x) for x in eachslice(popNorm, dims=2)]
    iScoresNorm = sortperm(scoresNorm)

    relScoreDiffNorm = abs(log(scoresNorm[iScoresNorm[1]]) - log(scoresNorm[iScoresNorm[round(Int, 4 * popSize / 5 - 1)]])) / abs(log(scoresNorm[iScoresNorm[1]]))
    if ii > 3 && relScoreDiffNorm .< 0.01 # the relative difference between the best and the worst signals is less than 1%
        println("finished after $(ii) rounds")
        break
    end

    # perturb a fifth to sample the full space if it hasn't converged yet
    perturbWorst(setProbWNorm, scoresNorm, perturbRate)
end
# NDescents = 30
popNorm = setProbWNorm.runcontrollers[end].optimizer.population.individuals
scoresNorm = [objNormDCT(x) for x in eachslice(popNorm, dims=2)]

saveSerial(saveSerialNameNorm, setProbWNorm)
save(saveNameNorm, "pop", pop, "scoresNorm", scoresNorm)

histogram(log.(scoresNorm) / log(1.1), legend=false, xlabel="coordinate value + norm of input", title="Histogram of log scoresNorm\nMaximizing freq $(freqs[1][5]) while minimizing all others")
savefig(joinpath(locDirNor, "histogramPop$(extraName).pdf"))


# examples
iScoresNorm = sortperm(scoresNorm)
bestExNorm = idct(popNorm[:, iScoresNorm[1]], 1)
plot(bestExNorm)

p1 = plot(sign(bestExNorm[64]) * bestExNorm, legend=false, title="Optimal Fit example", linewidth=2)
ψ = originalDomain(St.mainChain[1])
p2 = plot([circshift(real.(ψ)[:,5], 64) circshift(imag.(ψ)[:,5], 64)], linewidth=2, labels=["Real" "Imag"], title="Target Wavelet")
plot(p1,p2,layout=(2, 1))
savefig(joinpath(figDir, "normed", "compareWithTargetWavelet$(extraName).pdf"))
if !isdir(joinpath(figDir, "normed"))
    mkpath(joinpath(figDir, "normed"))
end
argmax(abs.(WT(bestExNorm)[:,1:end - 1,1,1]'))
plot([circshift(real.(ψ)[:,5], 64) (sign(bestExNorm[64]) * bestExNorm * norm(real.(ψ)[:,5]) / norm(bestExNorm))], labels=["target" "fit"],linewidth=3,title="Maximizing WT at $(round(freqs[1][j], sigdigits=3))Hz and 64ms, Normalized")
savefig(joinpath(figDir, "normed", "compareDirectlyWithTargetWavelet$(extraName).pdf"))
# resulting wavelet transform
heatmap(abs.(WT(bestExNorm)[:,1:end - 1,1,1]').^2; c=cgrad(:viridis, scale=:exp), framestyle=:box, title="Scalogram from fitting $(round(freqs[1][j], sigdigits=3))Hz, Normalized",  yticks=(1:length(freqs[1]) - 1, round.(freqs[1], sigdigits=3)), titlefontsize=13, guidefontsize=9, tickfontsize=7, bottom_margin=-3Plots.px, ylabel="Frequency (Hz)", xlabel="Time (ms)")
savefig(joinpath(figDir, "normed", "Scalogram$(extraName).pdf"))
joinpath(figDir, "normed", "Scalogram$(extraName).pdf")
