using Revise
using Distributed
addprocs(9)
@everywhere using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, FFTW, LinearAlgebra, FourierFilterFlux
@everywhere using Optim, LineSearches
@everywhere using MLJ, DataFrames, MLJLinearModels
@everywhere using Plots
@everywhere using Revise
@everywhere using CUDA, Zygote, Flux, FFTW
@everywhere using Test
@everywhere using LinearAlgebra
@everywhere using BlackBoxOptim
@everywhere using BlackBoxOptim: num_func_evals
@everywhere using ScatteringInterpretation

@everywhere N = 128
@everywhere NTrain = 1
@everywhere popSize = 500
@everywhere totalMins = 1 * 60
@everywhere nMinsBBO = 3
@everywhere perturbRate = 1 / 5
@everywhere refit = false

locationsAndWavelets = ((((((2, 5, 5), (5, 12, 14)), (1, 1)), (((4, 5, 5), (4, 12, 14)), (1, 1)), (((2, 5, 5), (5, 12, 14)), (1, 2)), (((3, 4), (7, 13)), (1, 1)), (((3, 4), (7, 13)), (2, 1))), Morlet(2π)),)
shortNames = ["morl2Pi"] # set the names for saving
#locNames = ("(2,5,5) and (5,12,14) equal weight", "(2,5,5) and double weight (5,12,14)", "(3, 3) and double weight (5,12,14)")
extraName = "mixed"
# listOfLocations, CW = locationsAndWavelets[1]
# ii = 1

for ii = 1:length(locationsAndWavelets)
    (listOfLocations, CW) = locationsAndWavelets[ii]
    namedCW = shortNames[ii]
    println("doing $CW, there are $(length(listOfLocations)) examples to do")
    # second layer setup begin
    # (trainLocs, trainVals) = listOfLocations[1]
    @sync @distributed for (trainLocs, trainVals) in listOfLocations
        β = [2, 2, 1]
        averagingLength = [-1, -1, 2]
        outputPool = 8
        normalize = false
        poolBy = 3 // 2
        NDescents = 100
        descentLength = 1000
        pNorm = 2
        NTrain = 5000
        extraOctaves = 0
        St = scatteringTransform((N, 1, 1), cw = CW, 2, poolBy = poolBy, β = β, averagingLength = averagingLength, outputPool = outputPool, normalize = normalize, σ = abs, p = pNorm, extraOctaves = extraOctaves)
        x = randn(N, 1, 1)
        Sx = St(x)
        freqs = getMeanFreq(St, 1000)
        f1, f2, f3 = freqs
        tFreqs = map(x -> round.(x, sigdigits = 3), freqs)
        usedFreqs = map(pathInd -> map((xii) -> tFreqs[length(pathInd)-xii[1]][xii[2]], enumerate(pathInd[2:end])), trainLocs)
        m = length(trainLocs[1]) - 1
        locs = [round(Int, 128 * x[1] / size(Sx[m], 1)) for (ii, x) in enumerate(trainLocs)]
        pathStrings = map(p -> "(" * reduce((x, y) -> x * ", " * y, ["$(λ)Hz" for λ in p]) * ")", usedFreqs)
        locName = ["$(trainVals[ii]) at $(pathStrings[ii]) and $(locs[ii])ms" for ii = 1:length(trainLocs)]
        locName = reduce((x, y) -> x * "; " * y, locName)
        # plotSecondLayer(Sx, St)
        aveLenStr = mapreduce(x -> "$x", *, averagingLength)
        saveDir = "../../results/multiFits/$(namedCW)/poolBy$(round(poolBy, sigdigits=3))_outPool$(outputPool)_pNorm$(pNorm)_aveLen$(aveLenStr)_extraOct$(extraOctaves)/"
        saveFigDir = "../../figures/interpret/multiFits/$(namedCW)/poolBy$(round(poolBy, sigdigits=3))_outPool$(outputPool)_pNorm$(pNorm)_aveLen$(aveLenStr)_extraOct$(extraOctaves)/"
        if !isdir(saveDir)
            mkpath(saveDir)
        end
        if !isdir(saveFigDir)
            mkpath(saveFigDir)
        end
        println("doing $trainVals at $trainLocs")
        target = ScatteredOut(map(x -> zeros(Float32, size(x)), Sx.output))
        paths = map(p -> pathLocs(length(p) - 1, p), trainLocs)
        for (p, v) in zip(trainLocs, trainVals)
            m = length(p) - 1
            pSet = pathLocs(m, p)
            target[pSet] = v
        end
        target[1][3, 4]
        target[1][7, 13]
        locDir = saveDir * "path$(trainLocs)Vals$(trainVals)/"
        locFigDir = saveFigDir * "path$(trainLocs)Vals$(trainVals)/"
        saveName = locDir * "data$(extraName).jld2"
        saveNameDict = locDir * "dictionary"
        saveSerialName = locDir * "data"
        if !isdir(locDir)
            mkpath(locDir)
        end
        if !isdir(locFigDir)
            mkpath(locFigDir)
        end
        # second layer setup end

        function objectiveDCT(x̂)
            x = idct(x̂)
            t = St(reshape(x, (N, 1, 1)))
            1.1f0^(-sum([sum(t[p] .* target[p]) for p in paths]) + 1f-5 * norm(x)^2)
        end

        fitnessCallsHistoryDCT = Array{Int,1}()
        fitnessHistoryDCT = Array{Float64,1}()
        callbackDCT = function recDCT(oc)
            push!(fitnessCallsHistoryDCT, num_func_evals(oc))
            push!(fitnessHistoryDCT, best_fitness(oc))
        end
        setProbDCT = bbsetup(objectiveDCT; SearchRange = (-1000.0, 1000.0), NumDimensions = N, PopulationSize = popSize, MaxTime = 1.0, TraceInterval = 5, CallbackFunction = callbackDCT, CallbackInterval = 0.5, TraceMode = :silent)
        println("pretraining freq")
        bboptimize(setProbDCT)

        pop = pinkStart(N, -1, popSize)
        popDCT = dct(pop, 1) # start from the same population
        adjustPopulation!(setProbDCT, popDCT)

        nIters = ceil(Int, totalMins / nMinsBBO)
        @time for ii = 1:nIters
            println("----------------------------------------------------------")
            println("round $ii, DCT: GO")
            println("----------------------------------------------------------")
            bboptimize(setProbDCT, MaxTime = nMinsBBO * 60.0) # take some black box steps for the whole pop
            popDCT = setProbDCT.runcontrollers[end].optimizer.population.individuals
            scoresDCT = [objectiveDCT(x) for x in eachslice(popDCT, dims = 2)]
            iScoresDCT = sortperm(scoresDCT)

            relScoreDiffDCT = abs(log(scoresDCT[iScoresDCT[1]]) - log(scoresDCT[iScoresDCT[round(Int, 4 * popSize / 5 - 1)]])) / abs(log(scoresDCT[iScoresDCT[1]]))
            if ii > 3 && relScoreDiffDCT .< 0.01
                println("finished after $(ii) rounds, relative Score diff= $(relScoreDiffDCT)")
                break
            end
            # perturb a fifth to sample the full space
            perturbWorst(setProbDCT, scoresDCT, perturbRate)
        end

        popDCT = setProbDCT.runcontrollers[end].optimizer.population.individuals
        scoresDCT = [objectiveDCT(x) for x in eachslice(popDCT, dims = 2)]
        betterPop = popDCT
        betterScores = scoresDCT
        betterSetProb = setProbDCT
        betterObjective = objectiveDCT
        mapToSpace = x -> idct(x, 1)

        saveSerial(saveSerialName, betterSetProb)
        save(saveName, "pop", betterPop, "scores", betterScores, "fitnessCallsHistoryDCT", fitnessCallsHistoryDCT, "fitnessHistoryDCT", fitnessHistoryDCT)

        histogram(log.(betterScores) / log(1.1), legend = false, xlabel = "coordinate value + norm of input", title = "Histogram of log betterScores\nlocation: $(locName)", titlefontsize = 12)
        savefig(locFigDir * "histogramPop$(extraName).pdf")

        # plot(mapToSpace(pop[:, [iBetterScores[1]; iBetterScores[end]]], 1))
        # examples
        iBetterScores = sortperm(betterScores)
        plotFirstXEx(mapToSpace(betterPop[:, iBetterScores]), betterScores[iBetterScores], "Targets: $(locName)")
        savefig(locFigDir * "exampleResults$(extraName).pdf")
        plot(mapToSpace(betterPop[:, iBetterScores[1]]), title = "Best Example log-score: $(round(log(objectiveDCT(betterPop[:,iBetterScores[1]])), sigdigits=3))\nTargets: $(locName)", c = :black, legend = false, titlefontsize = 10)
        savefig(locFigDir * "bestExample$(extraName).pdf")
        plot(abs.(rfft(mapToSpace(betterPop[:, iBetterScores[1]]))), title = "Fourier Domain,\n Targets: $(locName)", c = :black, legend = false, titlefontsize = 10)
        savefig(locFigDir * "rfftBestExample$(extraName).pdf")
        plot(betterPop[:, iBetterScores[1]], title = "DCT best example\n Targets: $(locName)", c = :black, legend = false, titlefontsize = 10)
        savefig(locFigDir * "dctBestExample$(extraName).pdf")


        # plots of the first
        firstLayerMats = cat([St(reshape(mapToSpace(betterPop[:, iBetterScores][:, ii]), (N, 1, 1)))[1][:, :, 1]' for ii = 1:20]..., dims = 3)
        colorMax = (minimum(firstLayerMats), maximum(firstLayerMats))
        plotOverTitle([heatmap(x, clims = colorMax, colorbar = false, c = cgrad(:viridis, scale = :log10), yticks = (1:3:size(x, 1), round.(f1[1:3:end-1], sigdigits = 3)), xticks = false) for x in eachslice(firstLayerMats, dims = 3)], "First results\n$(locName)", (4, 5))
        savefig(locFigDir * "ArrayFirstLayerOutput$(extraName).pdf")
        heatmap(firstLayerMats[:, :, 1], clims = colorMax, colorbar = false, c = cgrad(:viridis, scale = :log10), xlabel = "space", ylabel = "frequency", title = "First results\n$(locName)", yticks = (1:1:size(f1, 1)-1, round.(f1[1:1:end-1], sigdigits = 3)), titlefontsize = 10)
        savefig(locFigDir * "bestFirstLayerOutput$(extraName).pdf")
        heatmap(abs.(St.mainChain[1](mapToSpace(betterPop[:, iBetterScores[1]]))[:, 1:end-1, 1, 1]'), title = "internal first\n$(locName)", yticks = (1:1:size(f1, 1)-1, round.(f1[1:1:end-1], sigdigits = 3)), c = cgrad(:viridis), titlefontsize = 10)
        savefig(locFigDir * "ArrayInternalFirst$(extraName).pdf")
        #
        # second plot
        plotSecondLayer(St(reshape(mapToSpace(betterPop[:, iBetterScores[1]]), (128, 1, 1))), St; title = "SecondLayer: $(locName)", logPower = false, c = cgrad(:viridis, [0 0.9]), titlefontsize = 10)
        savefig(locFigDir * "SecondLayerOutput$(extraName).pdf")
    end
end
