using Revise
using Distributed
addprocs(9)
@everywhere using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, FFTW, LinearAlgebra, FourierFilterFlux
@everywhere using Optim, LineSearches
@everywhere using MLJ, DataFrames, MLJLinearModels
@everywhere using Plots
@everywhere using Revise
@everywhere using CUDA, Zygote, Flux, FFTW
@everywhere using Test
@everywhere using LinearAlgebra
@everywhere using BlackBoxOptim
@everywhere using BlackBoxOptim:num_func_evals
@everywhere using ScatteringInterpretation

@everywhere N = 128
@everywhere NTrain = 1
@everywhere popSize = 500
@everywhere totalMins = 1 * 60
@everywhere nMinsBBO = 3
@everywhere perturbRate = 1 / 5
@everywhere refit = false

locationsAndWavelets = ((:all, dog2),
                        (:all, dog1),
                        (:all, Morlet(π)),
                        (:all, Morlet(2π)),)
shortNames = ["dog2", "dog1", "morl", "morl2Pi"] # set the names for saving
extraName = "full1"
# listOfLocations, CW = locationsAndWavelets[1]
# (w2, w1) = listOfLocations[3]
# ii = 1

for ii = 1:length(locationsAndWavelets)
    (listOfLocations, CW) = locationsAndWavelets[ii]
    namedCW = shortNames[ii]
    if listOfLocations == :all
        β = [2,2,1]; averagingLength = [-1,-1,2]; outputPool = 8; normalize = false; poolBy = 3 // 2; NDescents = 100; descentLength = 1000; pNorm = 2; NTrain = 5000; extraOctaves = 0
        St = scatteringTransform((N, 1, 1), cw=CW, 2, poolBy=poolBy, β=β, averagingLength=averagingLength, outputPool=outputPool, normalize=normalize, σ=abs, p=pNorm, extraOctaves=extraOctaves)
        J1, J2 = map(x -> size(x, 2), getWavelets(St))
        listOfLocations = ([(j2, j1) for j1 = 1:(J1 - 1), j2 = 1:(J2 - 1)]...,)
    end
    println("doing $CW, there are $(length(listOfLocations)) examples to do")
    # second layer setup begin
    # (w2,w1) = listOfLocations[15]
    @sync @distributed for (w2, w1) in listOfLocations
        β = [2,2,1]; averagingLength = [-1,-1,2]; outputPool = 8; normalize = false; poolBy = 3 // 2; NDescents = 100; descentLength = 1000; pNorm = 2; NTrain = 5000; extraOctaves = 0
        St = scatteringTransform((N, 1, 1), cw=CW, 2, poolBy=poolBy, β=β, averagingLength=averagingLength, outputPool=outputPool, normalize=normalize, σ=abs, p=pNorm, extraOctaves=extraOctaves)
        x = randn(N, 1, 1)
        Sx = St(x)
    # plotSecondLayer(Sx, St)
        aveLenStr = mapreduce(x -> "$x", *, averagingLength)
        saveDir = "../../results/singleFits/$(namedCW)/poolBy$(round(poolBy, sigdigits=3))_outPool$(outputPool)_pNorm$(pNorm)_aveLen$(aveLenStr)_extraOct$(extraOctaves)/lay2/"
        saveFigDir = "../../figures/interpret/singleFits/$(namedCW)/poolBy$(round(poolBy, sigdigits=3))_outPool$(outputPool)_pNorm$(pNorm)_aveLen$(aveLenStr)_extraOct$(extraOctaves)/lay2/"
        if !isdir(saveDir)
            mkpath(saveDir)
        end
        if !isdir(saveFigDir)
            mkpath(saveFigDir)
        end
        println("doing ($w2,$w1)")
        l = floor(Int, 1 / 2 * size(Sx[2], 1))
        p = pathLocs(2, (l, w2, w1))
        locDir = saveDir * "wf$(w1)ws$(w2)/"
        locFigDir = saveFigDir * "wf$(w1)ws$(w2)/"
        saveName = locDir * "data$(extraName).jld2"
        saveNameDict = locDir * "dictionary"
        saveSerialName = locDir * "data"
        if !isdir(locDir)
            mkpath(locDir)
        end
        if !isdir(locFigDir)
            mkpath(locFigDir)
        end
        # second layer setup end
        # plot the wavelets used
        W = getWavelets(St);
        f1, f2, f3 = getMeanFreq(St)
        tf1 = round(f1[w1], sigdigits=3)
        tf2 = round(f2[w2], sigdigits=3)
        δt = 1000
        l1Waves = plot(range(0, δt / 2, length=N + 1), abs.(W[1][:,[end, (1:end - 1)...]]), labels=["ave" round.(f1[1:end - 1]', sigdigits=3)], title="First layer", palette=palette(:viridis, size(W[1], 2) + 5), xlabel="Frequency Hz")
        l2Waves = plot(range(0, δt / 2 * size(W[2], 1) / size(W[1], 1), length=size(W[2], 1)), abs.(W[2][:,[end, (1:end - 1)...]]), labels=["ave" round.(f2[1:end - 1]', sigdigits=3)], title="Second layer", palette=palette(:viridis, size(W[2], 2) + 5), xlabel="Frequency Hz")
        plot(l1Waves, l2Waves, layout=(2, 1))
        savefig(locFigDir * "waveletsRfft.pdf")

        # what about the wavelets themselves?
        wavs = circshift(originalDomain(St.mainChain[1]), (64, 0))[1:128,:]
        wavs2 = circshift(originalDomain(St.mainChain[4]), (43, 0))[1:85,:]
        if eltype(wavs) <: Real
            plot(plot(wavs[1:N,w1], legend=false, title="Layer 1, w= $(w1), Mean Freq $(tf1)Hz", c=:black), plot(wavs2[1:85,w2], legend=false, title="Layer 2, w= $(w2), Mean Freq $(tf2)Hz", c=:black), layout=(2, 1))
        else
            pl1 = plot([real.(wavs[1:N,w1]) imag.(wavs[1:N,w1]) ], linewidth=2, labels=["real" "imaginary"], title="Layer 1, w= $(w1), Mean Freq $(tf1)Hz")
            plot!([abs.(wavs[1:N,w1]) -abs.(wavs[1:N,w1])], labels=["envelope" ""], c=:black)
            pl2 = plot([real.(wavs2[1:85,w2]) imag.(wavs2[1:85,w2]) ], linewidth=2, legend=false, title="Layer 2, w= $(w2), Mean Freq $(tf2)Hz")
            plot!([abs.(wavs2[1:85,w2]) -abs.(wavs2[1:85,w2])], labels=["" ""], c=:black)
            plot(pl1, pl2, layout=(2, 1))
        end
        savefig(locFigDir * "waveletsTargeted.pdf")

        function objectiveDCT(x̂)
            x = idct(x̂)
            t = St(reshape(x, (N, 1, 1)))
            1.1f0^(-t[p][1] + 1f-5 * norm(x)^2)
        end

        fitnessCallsHistoryDCT = Array{Int,1}()
        fitnessHistoryDCT = Array{Float64,1}()
        callbackDCT = function recDCT(oc)
            push!(fitnessCallsHistoryDCT, num_func_evals(oc))
            push!(fitnessHistoryDCT, best_fitness(oc))
        end
        setProbDCT = bbsetup(objectiveDCT; SearchRange=(-1000., 1000.), NumDimensions=N, PopulationSize=popSize, MaxTime=1.0, TraceInterval=5,CallbackFunction=callbackDCT, CallbackInterval=0.5, TraceMode=:silent)
        println("pretraining freq")
        bboptimize(setProbDCT)

        pop = pinkStart(N, -1, popSize)
        popDCT = dct(pop, 1) # start from the same population
        adjustPopulation!(setProbDCT, popDCT)

        nIters = ceil(Int, totalMins / nMinsBBO)
        @time for ii in 1:nIters
            println("----------------------------------------------------------")
            println("round $ii, DCT: GO")
            println("----------------------------------------------------------")
            bboptimize(setProbDCT, MaxTime=nMinsBBO * 60.0) # take some black box steps for the whole pop
            popDCT = setProbDCT.runcontrollers[end].optimizer.population.individuals
            scoresDCT = [objectiveDCT(x) for x in eachslice(popDCT, dims=2)]
            iScoresDCT = sortperm(scoresDCT)

            relScoreDiffDCT = abs(log(scoresDCT[iScoresDCT[1]]) - log(scoresDCT[iScoresDCT[round(Int, 4 * popSize / 5 - 1)]])) / abs(log(scoresDCT[iScoresDCT[1]]))
            if ii > 3 && relScoreDiffDCT .< 0.01
                println("finished after $(ii) rounds, relative Score diff= $(relScoreDiffDCT)")
                break
            end
            # perturb a fifth to sample the full space
            perturbWorst(setProbDCT, scoresDCT, perturbRate)
        end

        popDCT = setProbDCT.runcontrollers[end].optimizer.population.individuals
        scoresDCT = [objectiveDCT(x) for x in eachslice(popDCT, dims=2)]
        betterPop = popDCT
        betterScores = scoresDCT
        betterSetProb = setProbDCT
        betterObjective = objectiveDCT
        mapToSpace = x -> idct(x, 1)

        saveSerial(saveSerialName, betterSetProb)
        save(saveName, "pop", betterPop, "scores", betterScores, "fitnessCallsHistoryDCT", fitnessCallsHistoryDCT, "fitnessHistoryDCT", fitnessHistoryDCT)

        histogram(log.(betterScores) / log(1.1), legend=false, xlabel="coordinate value + norm of input", title="Histogram of log betterScores\nlocation: $(l), first layer: $(tf1)Hz, second layer: $(tf2)Hz")
        savefig(locFigDir * "histogramPop$(extraName).pdf")

        # plot(mapToSpace(pop[:, [iBetterScores[1]; iBetterScores[end]]], 1))
        # examples
        iBetterScores = sortperm(betterScores)
        plotFirstXEx(mapToSpace(betterPop[:,iBetterScores]), betterScores[iBetterScores], "Target location: $(l), first: $(tf1)Hz, second: $(tf2)Hz")
        savefig(locFigDir * "exampleResults$(extraName).pdf")
        plot(mapToSpace(betterPop[:,iBetterScores[1]]), title="Best Example log-score: $(round(log(objectiveDCT(betterPop[:,iBetterScores[1]])), sigdigits=3))\nTarget location: $(l), first: $(tf1)Hz, second: $(tf2)Hz", c=:black, legend=false)
        savefig(locFigDir * "bestExample$(extraName).pdf")
        plot(abs.(rfft(mapToSpace(betterPop[:,iBetterScores[1]]))), title="Fourier Domain,\n Target location: $(l), first: $(tf1)Hz, second: $(tf2)Hz", c=:black, legend=false)
        savefig(locFigDir * "rfftBestExample$(extraName).pdf")
        plot(betterPop[:,iBetterScores[1]], title="DCT best example\n Target location: $(l), first: $(tf1)Hz, second: $(tf2)Hz", c=:black, legend=false)
        savefig(locFigDir * "dctBestExample$(extraName).pdf")


    # plots of the first
        firstLayerMats = cat([St(reshape(mapToSpace(betterPop[:,iBetterScores][:,ii]), (N, 1, 1)))[1][:,:,1]' for ii = 1:20]..., dims=3)
        colorMax = (minimum(firstLayerMats), maximum(firstLayerMats))
        plotOverTitle([heatmap(x, clims=colorMax, colorbar=false, c=cgrad(:viridis, scale=:log10), yticks=(1:3:size(x, 1), round.(f1[1:3:end - 1], sigdigits=3)), xticks=false) for x in eachslice(firstLayerMats, dims=3)], "First results\nlocation: $(l) first: $(tf1)Hz, second: $(tf2)Hz", (4, 5))
        savefig(locFigDir * "ArrayFirstLayerOutput$(extraName).pdf")
        heatmap(firstLayerMats[:,:,1], clims=colorMax, colorbar=false, c=cgrad(:viridis, scale=:log10), xlabel="space", ylabel="frequency", title="First results\nlocation: $(l), first: $(tf1)Hz, second: $(tf2)Hz", yticks=(1:1:size(f1, 1) - 1, round.(f1[1:1:end - 1], sigdigits=3)))
        savefig(locFigDir * "bestFirstLayerOutput$(extraName).pdf")
        heatmap(abs.(St.mainChain[1](mapToSpace(betterPop[:,iBetterScores[1]]))[:,1:end - 1,1,1]'), title="internal first\nlocation: $(l), first: $(tf1)Hz, second: $(tf2)Hz", yticks=(1:1:size(f1, 1) - 1, round.(f1[1:1:end - 1], sigdigits=3)), c=cgrad(:viridis))
        savefig(locFigDir * "ArrayInternalFirst$(extraName).pdf")
        plot(abs.(St.mainChain[1](mapToSpace(betterPop[:,iBetterScores[1]]))[:,w1,1,1]), title="first $(w1) for fitting at\nlocation: $(l) first: $(tf1)Hz, second: $(tf2)Hz", legend=false)
        savefig(locFigDir * "targetWaveInternalFirst$(extraName).pdf")
    #
    # second plot
        plotSecondLayer(St(reshape(mapToSpace(betterPop[:, iBetterScores[1]]), (128, 1, 1))), St; title="SecondLayer: loc: $(l), first $(tf1)Hz, second $(tf2)Hz",logPower=false,c=cgrad(:viridis, [0 .9]))
        savefig(locFigDir * "SecondLayerOutput$(extraName).pdf")
    end
end
