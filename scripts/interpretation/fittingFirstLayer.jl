using Revise
using Distributed
addprocs(2)
@everywhere using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, FFTW, LinearAlgebra, FourierFilterFlux
@everywhere using Optim, LineSearches
@everywhere using MLJ, DataFrames, MLJLinearModels
@everywhere using Plots
@everywhere using Revise
@everywhere using CUDA, Zygote, Flux, FFTW
@everywhere using Test
@everywhere using LinearAlgebra
@everywhere using BlackBoxOptim
@everywhere using BlackBoxOptim: num_func_evals
@everywhere using ScatteringInterpretation

@everywhere N = 128
@everywhere NTrain = 1
@everywhere popSize = 500
@everywhere totalMins = 1 * 60
@everywhere nMinsBBO = 3
@everywhere perturbRate = 1 / 5
locationsAndWavelets = ((((1:15)...,), dog2),)
fitBoth = false

shortNames = ["dog2", "dog1", "morl", "morl2Pi", "cDb2"] # set the names for saving
# listOfLocations, CW = locationsAndWavelets[1]
# w1 = listOfLocations[1]
for ii = 1:length(locationsAndWavelets)
    (listOfLocations, CW) = locationsAndWavelets[ii]
    namedCW = shortNames[ii]
    println("----------------------------------------------------------------------------------------------")
    println("----------------------------------------------------------------------------------------------")
    println("-------------------------------starting $(CW)-------------------------------")
    println("----------------------------------------------------------------------------------------------")
    println("----------------------------------------------------------------------------------------------")
    @sync @distributed for w1 in listOfLocations
        β = [2,2,1]; averagingLength = [-1,-1,2]; outputPool = 8; normalize = false; poolBy = 3 // 2; NDescents = 100; descentLength = 1000; pNorm = 2; NTrain = 5000; extraOctaves = 0
        St = scatteringTransform((N, 1, 1), cw=CW, 2, poolBy=poolBy, β=β, averagingLength=averagingLength, outputPool=outputPool, normalize=normalize, σ=abs, p=pNorm, extraOctaves=extraOctaves)
        x = randn(N, 1, 1)
        Sx = St(x)
        # plotSecondLayer(Sx, St)
        extraName = "dctOnly"
        l = ceil(Int, 1 / 2 * size(Sx[1], 1))
        p = pathLocs(1, (l, w1))
        aveLenStr = mapreduce(x -> "$x", *, averagingLength)
        saveDir = "../../results/singleFits/$(namedCW)/poolBy$(round(poolBy, sigdigits=3))_outPool$(outputPool)_pNorm$(pNorm)_aveLen$(aveLenStr)_extraOct$(extraOctaves)/lay1/"
        saveFigDir = "../../figures/interpret/singleFits/$(namedCW)/poolBy$(round(poolBy, sigdigits=3))_outPool$(outputPool)_pNorm$(pNorm)_aveLen$(aveLenStr)_extraOct$(extraOctaves)/lay1/"
        if !isdir(saveDir)
            mkpath(saveDir)
        end
        if !isdir(saveFigDir)
            mkpath(saveFigDir)
        end
        locDir = saveDir * "wf$(w1)ws/"
        locFigDir = saveFigDir * "wf$(w1)ws/"
        saveName = locDir * "data$(extraName).jld2"
        saveNameDict = locDir * "dictionary"
        saveSerialName = locDir * "data"
        if !isdir(locDir)
            mkpath(locDir)
        end
        if !isdir(locFigDir)
            mkpath(locFigDir)
        end
        # plot the wavelets used
        W = getWavelets(St);
        f1, f2, f3 = getMeanFreq(St)
        tf1 = round(f1[w1], sigdigits=3)
        δt = 1000
        # l1Waves = plot(range(0, δt / 2, length=N + 1), abs.(W[1][:,[end, (1:end - 1)...]]), labels=["ave" round.(f1[1:end - 1]', sigdigits=3)], title="First layer", palette=palette(:viridis, size(W[1], 2) + 5), xlabel="Frequency Hz")
        # savefig(locFigDir * "waveletsRfft.pdf")

        function objectiveDCT(x̂)
            x = idct(x̂)
            t = St(reshape(x, (N, 1, 1)))
            1.1f0^(-t[p][1] + 1f-5 * norm(x)^2)
        end
        function objectiveSpace(x)
            t = St(reshape(x, (N, 1, 1)))
            1.1f0^(-t[p][1] + 1f-5 * norm(x)^2)
        end

        fitnessCallsHistoryDCT = Array{Int,1}()
        fitnessHistoryDCT = Array{Float64,1}()
        callbackDCT = function recDCT(oc)
            push!(fitnessCallsHistoryDCT, num_func_evals(oc))
            push!(fitnessHistoryDCT, best_fitness(oc))
        end
        setProbDCT = bbsetup(objectiveDCT; SearchRange=(-1000., 1000.), NumDimensions=N, PopulationSize=popSize, MaxTime=1.0, TraceInterval=5, CallbackFunction=callbackDCT, CallbackInterval=0.5, TraceMode=:silent)
        println("pretraining freq")
        bboptimize(setProbDCT)
        if fitBoth
            fitnessCallsHistory = Array{Int,1}()
            fitnessHistory = Array{Float64,1}()
            callback = function rec(oc)
                push!(fitnessCallsHistory, num_func_evals(oc))
                push!(fitnessHistory, best_fitness(oc))
            end
            setProb = bbsetup(objectiveSpace; SearchRange=(-1000., 1000.), NumDimensions=N, PopulationSize=popSize, MaxTime=1.0, TraceInterval=5, CallbackFunction=callback, CallbackInterval=0.5, TraceMode=:silent)
            println("pretraining Space")
            bboptimize(setProb)
        end

        pop = pinkStart(N, -1, popSize)
        popDCT = dct(pop, 1) # start from the same population
        adjustPopulation!(setProbDCT, popDCT)
        if fitBoth
            adjustPopulation!(setProb, pop)
        end

        nIters = ceil(Int, totalMins / nMinsBBO)
        for ii in 1:nIters
            println("----------------------------------------------------------")
            println("round $ii, DCT: GO")
            println("----------------------------------------------------------")
            bboptimize(setProbDCT, MaxTime=nMinsBBO * 60.0) # take some black box steps for the whole pop
            popDCT = setProbDCT.runcontrollers[end].optimizer.population.individuals
            scoresDCT = [objectiveDCT(x) for x in eachslice(popDCT, dims=2)]
            iScoresDCT = sortperm(scoresDCT)

            relScoreDiffDCT = abs(log(scoresDCT[iScoresDCT[1]]) - log(scoresDCT[iScoresDCT[round(Int, 4 * popSize / 5 - 1)]])) / abs(log(scoresDCT[iScoresDCT[1]]))
            if ii > 3 && relScoreDiffDCT .< 0.01
                println("finished after $(ii) rounds, relative Score diff= $(relScoreDiffDCT)")
                break
            end
            # perturb a fifth to sample the full space
            perturbWorst(setProbDCT, scoresDCT, perturbRate)
        end
        if fitBoth
            for ii in 1:nIters
                println("----------------------------------------------------------")
                println("round $ii: GO")
                println("----------------------------------------------------------")
                bboptimize(setProb, MaxTime=nMinsBBO * 60.0) # take some black box steps for the whole pop
                pop = setProb.runcontrollers[end].optimizer.population.individuals
                scores = [objectiveSpace(x) for x in eachslice(pop, dims=2)]
                iScores = sortperm(scores)

                relScoreDiff = abs(log(scores[iScores[1]]) - log(scores[iScores[round(Int, 4 * popSize / 5 - 1)]])) / abs(log(scores[iScores[1]]))
                if ii > 3 && relScoreDiff .< 0.01
                    println("finished after $(ii) rounds, relative Score diff= $(relScoreDiff)")
                    break
                end
            # perturb a fifth to sample the full space
                perturbWorst(setProb, scores, perturbRate, identity)
            end
            plot(fitnessCallsHistory[3:end], log.(fitnessHistory[3:end]), xscale=:log10, label="Space descent")
            plot!(fitnessCallsHistoryDCT[3:end], log.(fitnessHistoryDCT[3:end]), xscale=:log10, label="DCT descent", title="Comparing Convergence Rates", xlabel="Number of steps (log10 scale)", ylabel="Objective value (linear scale)")
            savefig(locFigDir * "convergenceSpaceDCT.pdf")
        end

        popDCT = setProbDCT.runcontrollers[end].optimizer.population.individuals
        scoresDCT = [objectiveDCT(x) for x in eachslice(pop, dims=2)]
        if fitBoth
            pop = setProb.runcontrollers[end].optimizer.population.individuals
            scores = [objectiveSpace(x) for x in eachslice(pop, dims=2)]
            if minimum(scoresDCT) < minimum(scores)
                betterPop = popDCT
                betterScores = scoresDCT
                betterSetProb = setProbDCT
                betterObjective = objectiveDCT
                mapToSpace = x -> idct(x, 1)
            else
                betterPop = pop
                betterScores = scores
                betterSetProb = setProb
                betterObjective = objectiveSpace
                mapToSpace = identity
            end
            save(saveName, "pop", mapToSpace(betterPop), "scores", betterScores, "fitnessCallsHistory", fitnessCallsHistory, "fitnessCallsDCT", fitnessCallsHistoryDCT, "fitnessHistoryDCT", fitnessHistoryDCT, "fitnessHistory", fitnessHistory)
        else
            betterPop = popDCT
            betterScores = scoresDCT
            betterSetProb = setProbDCT
            betterObjective = objectiveDCT
            mapToSpace = x -> idct(x, 1)
            save(saveName, "pop", mapToSpace(betterPop), "scores", betterScores)
        end
        saveSerial(saveSerialName, betterSetProb)

        histogram(log.(betterScores) / log(1.1), legend=false, xlabel="coordinate value + norm of input", title="Histogram of log betterScores\nlocation: $(l), first: $(tf1)Hz")
        savefig(locFigDir * "histogramPop$(extraName).pdf")

        # plot(mapToSpace(pop[:, [iBetterScores[1]; iBetterScores[end]]], 1))
        # examples
        iBetterScores = sortperm(betterScores)
        plotFirstXEx(mapToSpace(betterPop[:,iBetterScores]), betterScores[iBetterScores], "Target location: $(l), $(tf1)Hz")
        savefig(locFigDir * "exampleResults$(extraName).pdf")
        plot(mapToSpace(betterPop[:,iBetterScores[1]]), title="Best Example log-score: $(round(log(objectiveDCT(betterPop[:,iBetterScores[1]])), sigdigits=3))\nTarget location: $(l), first: $(tf1)Hz", c=:black, legend=false)
        savefig(locFigDir * "bestExample$(extraName).pdf")
        plot(abs.(rfft(mapToSpace(betterPop[:,iBetterScores[1]]))), title="Fourier Domain,\n Target location: $(l), first: $(tf1)Hz", c=:black, legend=false)
        savefig(locFigDir * "rfftBestExample$(extraName).pdf")
        plot(betterPop[:,iBetterScores[1]], title="DCT best example\n Target location: $(l), first: $(tf1)Hz", c=:black, legend=false)
        savefig(locFigDir * "dctBestExample$(extraName).pdf")


    # plots of the first
        firstLayerMats = cat([St(reshape(mapToSpace(betterPop[:,iBetterScores][:,ii]), (N, 1, 1)))[1][:,:,1]' for ii = 1:20]..., dims=3)
        colorMax = (minimum(firstLayerMats), maximum(firstLayerMats))
        plotOverTitle([heatmap(x, clims=colorMax, colorbar=false, c=cgrad(:viridis, scale=:log10), yticks=(1:3:size(x, 1), round.(f1[1:3:end - 1], sigdigits=3)), xticks=false) for x in eachslice(firstLayerMats, dims=3)], "First results\nlocation: $(l) first: $(tf1)Hz", (4, 5))
        savefig(locFigDir * "ArrayFirstLayerOutput$(extraName).pdf")
        heatmap(firstLayerMats[:,:,1], clims=colorMax, colorbar=false, c=cgrad(:viridis, scale=:log10), xlabel="space", ylabel="frequency", title="First results\nlocation: $(l), first: $(tf1)Hz", yticks=(1:1:size(f1, 1) - 1, round.(f1[1:1:end - 1], sigdigits=3)))
        savefig(locFigDir * "bestFirstLayerOutput$(extraName).pdf")
        heatmap(abs.(St.mainChain[1](mapToSpace(betterPop[:,iBetterScores[1]]))[:,1:end - 1,1,1]'), title="internal first\nlocation: $(l), first: $(tf1)Hz", yticks=(1:1:size(f1, 1) - 1, round.(f1[1:1:end - 1], sigdigits=3)), c=cgrad(:viridis))
        savefig(locFigDir * "ArrayInternalFirst$(extraName).pdf")
        plot(abs.(St.mainChain[1](mapToSpace(betterPop[:,iBetterScores[1]]))[:,w1,1,1]), title="first $(w1) for fitting at\nlocation: $(l) first: $(tf1)Hz", legend=false)
        savefig(locFigDir * "targetWaveInternalFirst$(extraName).pdf")
    #
    # second plot
        plotSecondLayer(St(reshape(mapToSpace(betterPop[:, iBetterScores[1]]), (128, 1, 1))), St; title="SecondLayer: loc: $(l), first $(tf1)Hz",logPower=false,c=cgrad(:viridis, [0 .9]))
        savefig(locFigDir * "SecondLayerOutput$(extraName).pdf")
    end
end
