# from fittingSecondLayer in the examples folder of ScatteringTransformExperiments
using Revise
using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, LinearAlgebra
using FFTW, FourierFilterFlux
using Optim, LineSearches, GLMNet
using Plots
using CUDA, Zygote, Flux
using LinearAlgebra
using BlackBoxOptim
using BlackBoxOptim: num_func_evals
using ScatteringInterpretation
NDims = 128
N = 100
popSize = 500
nMinsBBO = 3
perturbRate = 1 / 5
totalMins = 1 * 60
homeDir = "../../results/bellCylinderFunnel/"
function chooseBestLambda(cv)
    effectiveRegion = (cv.meanloss .- cv.stdloss) .< minimum(cv.meanloss)
    λBest = maximum(cv.path.lambda[BitVector([effectiveRegion..., zeros(Bool, length(cv.path.lambda) - length(cv.meanloss))...])])
    findfirst(x -> x == λBest, cv.path.lambda)
end
St = scatteringTransform((NDims, 1, 1), cw=dog2, 2, Q=4, β=[1,1,1], extraOctaves=(0, 0, 0), averagingLength=[-1.5,-1.5,2], outputPool=2 * 4, convBoundary=FourierFilterFlux.Periodic(), boundary=PerBoundary())
specName = "stLay2_dog2" * "$N"
cvpSt = FileIO.load(joinpath(homeDir, specName * ".jld2"), "cvp")
cvpStRot = FileIO.load(joinpath(homeDir, specName * "Rot.jld2"), "cvpRot")
βSt2 = cvpSt.path.betas[:,:,chooseBestLambda(cvpSt)]
βSt2Rot = cvpStRot.path.betas[:,:,chooseBestLambda(cvpStRot)]

St12 = scatteringTransform((NDims, 1, 1), cw=[dog1, dog2, dog2], 2, Q=4, β=[1,1,1], extraOctaves=(-0, -0, 0), averagingLength=[-1.5,-1.5,2], outputPool=4 * 4, convBoundary=FourierFilterFlux.Periodic(), boundary=PerBoundary())
specName12 = "stLay2_dog2_dog1" * "$N"
cvpSt_dog12 = FileIO.load(joinpath(homeDir, specName12 * ".jld2"), "cvp")
cvpSt_dog12Rot = FileIO.load(joinpath(homeDir, specName12 * "Rot.jld2"), "cvpRot")
βSt12 = cvpSt_dog12.path.betas[:,:,chooseBestLambda(cvpSt_dog12)]
βSt12Rot = cvpSt_dog12Rot.path.betas[:,:,chooseBestLambda(cvpSt_dog12Rot)]


St1 = scatteringTransform((NDims, 1, 1), cw=dog1, 2, Q=4, β=[1,1,1], extraOctaves=(-0, -0, 0), averagingLength=[-1.5,-1.5,2], outputPool=4 * 4, convBoundary=FourierFilterFlux.Periodic(), boundary=PerBoundary())
specName1 = "stLay2_dog1" * "$N"
cvpSt_dog1 = FileIO.load(joinpath(homeDir, specName1 * ".jld2"), "cvp")
cvpSt_dog1Rot = FileIO.load(joinpath(homeDir, specName1 * "Rot.jld2"), "cvpRot")
βSt1 = cvpSt_dog1.path.betas[:,:,chooseBestLambda(cvpSt_dog1)]
βSt1Rot = cvpSt_dog1Rot.path.betas[:,:,chooseBestLambda(cvpSt_dog1Rot)]

listOfβs = ((βSt2[:,1], "Cyldog2", "Cylinder using 2nd Dog second layer only", "", St), (βSt2[:,2], "Belldog2", "Bell using 2nd Dog second layer only", "", St), (βSt2[:,3], "fundog2", "Funnel using 2nd Dog second layer only", "", St), (βSt2Rot[:,1], "Cyldog2", "Cylinder using 2nd Dog second layer only", "rotated", St), (βSt2Rot[:,2], "Belldog2", "Bell using 2nd Dog second layer only", "rotated", St), (βSt2Rot[:,3], "fundog2", "Funnel using 2nd Dog second layer only", "rotated", St),
            (βSt12[:,1], "Cyldog12", "Cylinder using 1st DoG then 2nd", "", St12), (βSt12[:,2], "Belldog12", "Bell using 1st DoG then 2nd", "", St12), (βSt12[:,3], "fundog12", "Funnel using 1st DoG then 2nd", "", St12), (βSt12Rot[:,1], "Cyldog12", "Cylinder using 1st DoG then 2nd", "rotated", St12), (βSt12Rot[:,2], "Belldog12", "Bell using 1st DoG then 2nd", "rotated", St12), (βSt12Rot[:,3], "fundog12", "Funnel using 1st DoG then 2nd", "rotated", St12),
            (βSt1[:,1], "Cyldog1", "Cylinder using 1st DoG", "", St1), (βSt1[:,2], "Belldog1", "Bell using 1st DoG", "", St1), (βSt1[:,3], "fundog1", "Funnel using 1st DoG", "", St1), (βSt1Rot[:,1], "Cyldog1", "Cylinder using 1st DoG", "rotated", St1), (βSt1Rot[:,2], "Belldog1", "Bell using 1st DoG", "rotated", St1), (βSt1Rot[:,3], "fundog1", "Funnel using 1st DoG", "rotated", St1))
# β, shortName, longName, rotated, StThis = listOfβs[1]
# to compare with the dog2, we need the center frequency to be at 144 and 24.7
# second layer setup begin
for (β, shortName, longName, rotated, StThis) in listOfβs
    println("-----------------------------------------------------------------------------------------")
    println("-----------------------------------------------------------------------------------------")
    println("------------------------------- starting $(longName) -------------------------------------")
    println("-----------------------------------------------------------------------------------------")
    println("-----------------------------------------------------------------------------------------")
    saveDir = joinpath(homeDir, "fitting")
    if !isdir(saveDir)
        mkpath(saveDir)
    end
    locDirPos = joinpath(saveDir, shortName * rotated * "pos")
    locDirNeg = joinpath(saveDir, shortName * rotated * "neg")
    extraName = ""
    # if it already exists, append the appropriate count so we get multiple copies
    if isfile(locDirPos * "data.jld2")
        nRepeat = 1
        while isfile(locDirPos * "data$(nRepeat).jld2")
            nRepeat += 1
        end
        extraName = "$(nRepeat)"
    end
    if isfile(locDirNeg * "data.jld2")
        nRepeat = 1
        while isfile(locDirNeg * "data$(nRepeat).jld2")
            nRepeat += 1
        end
        extraName = "$(nRepeat)"
    end
    saveNamePos = locDirPos * "data$(extraName).jld2"
    saveNameDictPos = locDirPos * "dictionary"
    saveSerialNamePos = locDirPos * "data$(extraName)"

    saveNameNeg = locDirNeg * "data$(extraName).jld2"
    saveNameDictNeg = locDirNeg * "dictionary"
    saveSerialNameNeg = locDirNeg * "data$(extraName)"

    if !isdir(locDirPos)
        mkpath(locDirPos)
    end
    if !isdir(locDirNeg)
        mkpath(locDirNeg)
    end
    # second layer setup end
    βPos = max.(0, β)
    βNeg = -min.(0, β)

    function objDCTPos(x̂)
        x = idct(x̂, 1)
        t = StThis(reshape(x, (NDims, 1, 1)))
        1.1f0^(-sum(reshape(t[2], (:,)) .* βPos) + 1f-5 * norm(x)^2)
    end
    function objDCTNeg(x̂)
        x = idct(x̂, 1)
        t = StThis(reshape(x, (NDims, 1, 1)))
        1.1f0^(-sum(reshape(t[2], (:,)) .* βNeg) + 1f-5 * norm(x)^2)
    end

    setProbPos = bbsetup(objDCTPos; SearchRange=(-1000., 1000.), NumDimensions=NDims, PopulationSize=popSize, MaxTime=10.0, TraceInterval=5, TraceMode=:silent)
    setProbNeg = bbsetup(objDCTNeg; SearchRange=(-1000., 1000.), NumDimensions=NDims, PopulationSize=popSize, MaxTime=10.0, TraceInterval=5, TraceMode=:silent)
    println("pretraining")
    bboptimize(setProbPos)
    bboptimize(setProbNeg)

    pop = dct(pinkStart(NDims, -1, popSize), 1)
    adjustPopulation!(setProbPos, pop)
    adjustPopulation!(setProbNeg, pop)

    nIters = ceil(Int, totalMins / nMinsBBO)
    for ii in 1:nIters
        println("----------------------------------------------------------")
        println("round $ii: GO")
        println("----------------------------------------------------------")
        bboptimize(setProbPos, MaxTime=nMinsBBO * 60.0) # take some black box steps for the whole pop
        # for jj in 1:NDescents
        #     println("on run $jj of gradient descent")
        #     pathOfDescent, err = ScatteringTransformExperiments.pullAndTrain(setProbPos, objSPC; normDiscount=1e-1, N=1000)
        # end
        pop = setProbPos.runcontrollers[end].optimizer.population.individuals
        scores = [objDCTPos(x) for x in eachslice(pop, dims=2)]
        iScores = sortperm(scores)

        relScoreDiff = abs(log(scores[iScores[1]]) - log(scores[iScores[round(Int, 4 * popSize / 5 - 1)]])) / abs(log(scores[iScores[1]]))
        if ii > 3 && relScoreDiff .< 0.01 # the relative difference between the best and the worst signals is less than 1%
            println("finished after $(ii) rounds")
            break
        end

        # perturb a fifth to sample the full space if it hasn't converged yet
        perturbWorst(setProbPos, scores, perturbRate)
    end
    # NDescents = 30
    pop = setProbPos.runcontrollers[end].optimizer.population.individuals
    scores = [objDCTPos(x) for x in eachslice(pop, dims=2)]

    saveSerial(saveSerialNamePos, setProbPos)
    save(saveNamePos, "pop", pop, "scores", scores)
    for ii in 1:nIters
        println("----------------------------------------------------------")
        println("round $ii: GO")
        println("----------------------------------------------------------")
        bboptimize(setProbNeg, MaxTime=nMinsBBO * 60.0) # take some black box steps for the whole pop
        # for jj in 1:NDescents
        #     println("on run $jj of gradient descent")
        #     pathOfDescent, err = ScatteringTransformExperiments.pullAndTrain(setProbNeg, objSPC; normDiscount=1e-1, N=1000)
        # end
        pop = setProbNeg.runcontrollers[end].optimizer.population.individuals
        scores = [objDCTNeg(x) for x in eachslice(pop, dims=2)]
        iScores = sortperm(scores)

        relScoreDiff = abs(log(scores[iScores[1]]) - log(scores[iScores[round(Int, 4 * popSize / 5 - 1)]])) / abs(log(scores[iScores[1]]))
        if ii > 3 && relScoreDiff .< 0.01 # the relative difference between the best and the worst signals is less than 1%
            println("finished after $(ii) rounds")
            break
        end

        # perturb a fifth to sample the full space if it hasn't converged yet
        perturbWorst(setProbNeg, scores, perturbRate)
    end
    # NDescents = 30
    pop = setProbNeg.runcontrollers[end].optimizer.population.individuals
    scores = [objDCTNeg(x) for x in eachslice(pop, dims=2)]

    saveSerial(saveSerialNameNeg, setProbNeg)
    save(saveNameNeg, "pop", pop, "scores", scores)
end
