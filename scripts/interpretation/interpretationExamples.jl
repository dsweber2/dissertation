using Revise
using ScatteringTransform, ContinuousWavelets
using ApproxFun
using Plots, LaTeXStrings
using Roots, ForwardDiff, FFTW
using SpecialFunctions
saveLoc = "../figures/interpret/"
relu(x) = x > 0 ? x : 0
H(k) = Fun(Hermite(1 / (2)), [zeros(k); 2^(k / 2)]) # these are the physicists' Hermite Polynomials, whereas I'm using the probabilists
t = range(-5, stop=5, length=100)
plot(t, H(2).(t))
H(1)(1)
# the original function
function G(x, σ, l)
    1 / sqrt(2) ./ σ .* (exp.(-x.^2 ./ 2 ./ σ.^2) .+ exp.(-(x .- l).^2 ./ 2 ./ σ.^2))
end
# the net result function of convolution
function ψG(x, λ, σ, l, m)
    h = H(m)
    var = (λ.^2 .+ σ^2)
    firstTerm = [λ[ii] / gamma(m + 1 / 2) / sqrt(v) * exp(-1 / 2 / v * xx^2) * h(xx / sqrt(v)) for (ii, v) in enumerate(var), xx in x]
    secondTerm = [λ[ii] / gamma(m + 1 / 2) / sqrt(v) * exp(-1 / 2 / v * (xx - l)^2) * h((xx - l) / sqrt(v)) for (ii, v) in enumerate(var), xx in x]
    firstTerm .+ secondTerm
end
function Ψ(x, λ, σ, l, m)
    h = H(m)
    v = (λ^2 + σ^2)
    λ / sqrt(v) / gamma(m + 1 / 2) .* (exp(-1 / 2 / v * x^2) * h(x / sqrt(v)) + exp(-1 / 2 / v * (x - l)^2) * h((x - l) / sqrt(v)))
end
Ψ.(0:3, 1:4, 1.0, 1.0, 2)
ψG(t,Λ,1.0,2.0,2)
getZeros(x,toler=.1) = abs.(x) .< toler
t = range(-20, stop=20, length=1000); Λ = exp.(range(-5, stop=2.5, length=1000)); σ = .5; l = 8.0; m = 1
plot(G(t, σ, l))
heatmap(t, Λ, identity.(ψG(t, Λ, σ, l, m)), xlabel="location",ylabel="scale",c=:viridis)
savefig(saveLoc * "3IntersectionExample.pdf")
heatmap(t, Λ, getZeros.(ψG(t, Λ, σ, l, m), 0.2), xlabel="location",ylabel="scale")
savefig("3IntersectionExample_Zeros.png")
heatmap(t, Λ, abs.(ψG(t, Λ, σ, l, m)), xlabel="location",ylabel="scale")
heatmap(t, Λ, relu.(ψG(t, Λ, σ, l, m)), xlabel="location",ylabel="scale")

using ScatteringTransform, ContinuousWavelets
poolBy = 2; Q = (8, 6.0, 1.0); extraOctaves = (-1, -1, 0); outputPool = 1; β = (1, 1, 1.0); cw = [dog1 dog2 dog2]; aveLen = (-1, 0, 2)
St = scatteringTransform((1000, 1, 1), 2; poolBy=poolBy, Q=Q, extraOctaves=extraOctaves, outputPool=outputPool, β=β, cw=cw, averagingLength=aveLen)
size(G(t, σ, l))
gt = reshape(G(t, σ, l), (1000, 1, 1,))
plot(gt[:,1,1])
res = St(reshape(gt, (1000, 1, 1,)))
plot(res[0][:,1,1])
heatmap(res[1][:,:,1,1]')
plotSecondLayer(res, St)
mc = St.mainChain
heatmap(mc[2](mc[1](gt))[:,:,1,1]')
argmax(mc[5](mc[4](mc[3](mc[2](mc[1](gt))))))
heatmap(mc[5](mc[4](mc[3](mc[2](mc[1](gt)))))[:,:,23,1]')

# actually finding the roots
bound(λ,lims,slope) = (slope * λ) .* (lims .- sum(lims) / 2) .+ lims
function getZeroCurve(f, lims, Λ=1.5.^(range(-5, stop=2.5, length=1000));slope=1.0)
    zers = [find_zeros(x -> f(x, λ), bound(λ, lims, slope)..., no_pts=20) for λ in Λ]
    ys = cat([repeat([λ], length(zers[i])) for (i, λ) in enumerate(Λ)]..., dims=1)
    xs = cat(zers..., dims=1)
    boundary = reshape([bound(Λ[1], lims, slope)...; bound(Λ[end], lims, slope)...], (2, 2))
    return (xs, ys, (boundary, [Λ[1]; Λ[end]]))
end
Λ = 1.5.^(range(-5, stop=3, length=1000))
lims = (-1, 5)
mid = sum(lims) / 2
startLocs = (Λ[1] + 1) .* 1.5 .* (lims .- mid) .+ lims
Λ[1]
startLocs = bound(0, lims, 1.0)
endLocs = bound(Λ[end], lims, 1.0)
boundaries = reshape([startLocs...; endLocs...], (2, 2))
plot(boundaries', [Λ[1]; Λ[end]],color=:blue,labels=["boundary" ""], legend=:bottomright)
m = 2; n = 2
circ = Shape(Plots.partialcircle(0, 2π))
function boundaryPlot(Λ, σ, l, m, n, r=1.5; lims=(-1, 5), onBackground=false, nSamples=1000)
    f(x, λ) = ψG(x, λ, σ, l, m + n - 2)
    xs, ys, boundary = getZeroCurve(f, lims, Λ, slope=1.0)
    zMark = reshape(r^n .* Ψ.(xs, ys, σ, l, m + n), (:,))
    t = range(minimum(xs), stop=maximum(xs), length=nSamples)
    xs ./= r
    if onBackground
        sampledFrom = r^n .* ψG(t, Λ, σ, l, m + n)
        heatmap(t ./ r, Λ, sampledFrom, xlabel="location", ylabel="scale")
        fun = scatter!; clims = (minimum(sampledFrom), maximum(sampledFrom))
    else
        fun = scatter; clims = (minimum(zMark), maximum(zMark))
    end
    fun(xs, ys, marker_z=zMark, clims=clims, markerstrokecolor=nothing, markershape=circ, legend=false, title="Unsmoothed second layer " * L"\sigma=" * "$σ, l=$l, r=$r, m=$m, n=$n/Zero level set of $(n + m - 2)th derivative", xlabel="space", ylabel="scale", colorbar=true, colorbar_title="value of the $(m + n)th derivative")
end
nSamples = 1000
pyplot()
m = 2;n = 2; σ = .01; l = 2; Λ = 1.5.^(range(-5, stop=3, length=1000)); lims = (-1, 3); r = 1.5
boundaryPlot(Λ,σ,l,m,n, r, lims=lims)
savefig("SecondLayerDelta_m$(m)_n$(n)_l$(l)_sig$(σ).pdf")
savefig("SecondLayerDelta_m$(m)_n$(n)_l$(l)_sig$(σ).png")
boundaryPlot(Λ,σ,l,m,n, r, lims=lims, onBackground=true)
plot!(boundary[1]', boundary[2], color=:red)
scatter!(boundary..., legend=false)
plot(xs)

# zers = [[z for z in zer if !any(z .≈ (1+λ) .* lims)] for zer in zers] # get rid of anything coming from the very edge; scaling by 1+λ seems to have solved this













# for the real case, the kind of example I need is













# envelope functions
t = range(-π, stop=π, length=2048)
a(t) = 1 .+ t + t.^2 - 1 / 3 * t.^3
plot(t,a(t))
gauss(t,μ,σ) = exp.(-(t .- μ).^2 ./ σ^2)
b(t) = gauss(t, -.5, .5) + .5gauss(t, 2.5, 1)
a(t) = b(t)
plot(t,b(t))
ϕ(t) = π / 2 .+ 5 .* t.^2
φ(t) = 20 * t .+ 20 ./ t.^2# \varphi
plot(t, ϕ(t))
plot(t, φ(t))
plot(t,cos.(φ(t)))
plot(t,cos.(ϕ(t)))
x = reshape(a(t) .* cos.(ϕ(t)), (length(t), 1, 1))
y = reshape(a(t) .* cos.(φ(t)), (length(t), 1, 1))

# sum of several envelope functions, each roughly corresponding to a frequency
a(t) = 1 .+ t + t.^2 - 1 / 3 * t.^3
gauss(t,μ,σ) = exp.(-(t .- μ).^2 ./ σ^2)
b(t) = gauss(t, -.5, .5) + .5gauss(t, 2.5, 1)
ϕ_a = 300t + 50 * sin.(30t)
ϕ_b = 30t + 5 * sin.(30t .+ π / 2)
plot([ϕ_a ϕ_b])
mixed = a(t) .* cos.(ϕ_a) + b(t) .* cos.(ϕ_b)
plot(mixed)
St = stFlux((length(t), 1, 1), 2, averagingLength=1; cw=Morlet(2π))
mixedSt = St(reshape(mixed, length(t), 1, 1))
aSt = St(reshape(a(t), length(t), 1, 1))
ϕaSt = St(reshape(ϕ_a, length(t), 1, 1))
bSt = St(reshape(b(t), length(t), 1, 1))
ϕbSt = St(reshape(ϕ_b, length(t), 1, 1))
plot(aSt[0][:,1,1])
plot(bSt[0][:,1,1])
res
heatmap(res[2][:,:])
ScatteringTransform.jointPlot(mixedSt[:,1], "mixed signals", :viridis, St)
ScatteringTransform.jointPlot(aSt[:,1], "a", :viridis, St)
