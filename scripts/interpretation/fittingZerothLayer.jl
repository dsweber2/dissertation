using Revise
using Distributed
addprocs(2)
@everywhere using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, FFTW, LinearAlgebra, FourierFilterFlux
@everywhere using Optim, LineSearches
@everywhere using MLJ, DataFrames, MLJLinearModels
@everywhere using Plots
@everywhere using Revise
@everywhere using CUDA, Zygote, Flux, FFTW
@everywhere using Test
@everywhere using LinearAlgebra
@everywhere using BlackBoxOptim
@everywhere using BlackBoxOptim:num_func_evals
@everywhere using ScatteringInterpretation

@everywhere N = 128
@everywhere NTrain = 1
@everywhere popSize = 500
@everywhere totalMins = 1 * 60
@everywhere nMinsBBO = 3
@everywhere perturbRate = 1 / 5
locationsAndWavelets = ((((1:16)...,), dog2),
                        (((1:17)...,), dog1),
                        (((1:16)...,), Morlet(π)),
                        (((1:14)...,), Morlet(2π)),)
shortNames = ["dog2", "dog1", "morl", "morl2Pi"] # set the names for saving
# listOfLocations, CW = locationsAndWavelets[1]
# w1 = listOfLocations[1]
for ii = 1:length(locationsAndWavelets)
    (listOfLocations, CW) = locationsAndWavelets[ii]
    namedCW = shortNames[ii]
    @sync @distributed for w1 in listOfLocations
        β = [2,2,1]; averagingLength = [-1,-1,2]; outputPool = 8; normalize = false; poolBy = 3 // 2; NDescents = 100; descentLength = 1000; pNorm = 2; NTrain = 5000; extraOctaves = 0
        St = scatteringTransform((N, 1, 1), cw=CW, 2, poolBy=poolBy, β=β, averagingLength=averagingLength, outputPool=outputPool, normalize=normalize, σ=abs, p=pNorm, extraOctaves=extraOctaves)
        x = randn(N, 1, 1)
        Sx = St(x)
        # plotSecondLayer(Sx, St)
        extraName = ""
        l = ceil(Int, 1 / 2 * size(Sx[0], 1))
        p = pathLocs(0, (l,))
        aveLenStr = mapreduce(x -> "$x", *, averagingLength)
        saveDir = "../../results/singleFits/$(namedCW)/poolBy$(round(poolBy, sigdigits=3))_outPool$(outputPool)_pNorm$(pNorm)_aveLen$(aveLenStr)_extraOct$(extraOctaves)/lay0/"
        saveFigDir = "../../figures/interpret/singleFits/$(namedCW)/poolBy$(round(poolBy, sigdigits=3))_outPool$(outputPool)_pNorm$(pNorm)_aveLen$(aveLenStr)_extraOct$(extraOctaves)/lay0/"
        if !isdir(saveDir)
            mkpath(saveDir)
        end
        if !isdir(saveFigDir)
            mkpath(saveFigDir)
        end
        locDir = saveDir * "l$(l)/"
        locFigDir = saveFigDir * "l$(l)/"
        saveName = locDir * "data$(extraName).jld2"
        saveNameDict = locDir * "dictionary"
        saveSerialName = locDir * "data"
        if !isdir(locDir)
            mkpath(locDir)
        end
        if !isdir(locFigDir)
            mkpath(locFigDir)
        end
        # plot the wavelets used
        W = getWavelets(St);
        f1, f2, f3 = getMeanFreq(St)
        tf1 = round(f1[w1], sigdigits=3)
        δt = 1000
        # l1Waves = plot(range(0, δt / 2, length=N + 1), abs.(W[1][:,[end, (1:end - 1)...]]), labels=["ave" round.(f1[1:end - 1]', sigdigits=3)], title="First layer", palette=palette(:viridis, size(W[1], 2) + 5), xlabel="Frequency Hz")
        # savefig(locFigDir * "waveletsRfft.pdf")

        function objectiveDCT(x̂)
            x = idct(x̂)
            t = St(reshape(x, (N, 1, 1)))
            1.1f0^(-t[p][1] + 1f-5 * norm(x)^2)
        end
        function objectiveSpace(x)
            t = St(reshape(x, (N, 1, 1)))
            1.1f0^(-t[p][1] + 1f-5 * norm(x)^2)
        end

        fitnessCallsHistoryDCT = Array{Int,1}()
        fitnessCallsHistory = Array{Int,1}()
        fitnessHistoryDCT = Array{Float64,1}()
        fitnessHistory = Array{Float64,1}()
        callbackDCT = function recDCT(oc)
            push!(fitnessCallsHistoryDCT, num_func_evals(oc))
            push!(fitnessHistoryDCT, best_fitness(oc))
        end
        callback = function rec(oc)
            push!(fitnessCallsHistory, num_func_evals(oc))
            push!(fitnessHistory, best_fitness(oc))
        end
        setProbDCT = bbsetup(objectiveDCT; SearchRange=(-10000., 10000.), NumDimensions=N, PopulationSize=popSize, MaxTime=1.0, TraceInterval=5, CallbackFunction=callbackDCT, CallbackInterval=0.5, TraceMode=:silent)
        setProb = bbsetup(objectiveSpace; SearchRange=(-1000., 1000.), NumDimensions=N, PopulationSize=popSize, MaxTime=1.0, TraceInterval=5, CallbackFunction=callback, CallbackInterval=0.5, TraceMode=:silent)
        println("pretraining freq")
        bboptimize(setProb)
        println("pretraining Space")
        bboptimize(setProbDCT)

        pop = pinkStart(N, -1, popSize)
        popDCT = dct(pop, 1) # start from the same population
        adjustPopulation!(setProb, pop)
        adjustPopulation!(setProbDCT, popDCT)

        nIters = ceil(Int, totalMins / nMinsBBO)
        for ii in 1:nIters
            println("----------------------------------------------------------")
            println("round $ii, DCT: GO")
            println("----------------------------------------------------------")
            bboptimize(setProbDCT, MaxTime=nMinsBBO * 60.0) # take some black box steps for the whole pop
            popDCT = setProbDCT.runcontrollers[end].optimizer.population.individuals
            scoresDCT = [objectiveDCT(x) for x in eachslice(popDCT, dims=2)]
            iScoresDCT = sortperm(scoresDCT)

            relScoreDiffDCT = abs(log(scoresDCT[iScoresDCT[1]]) - log(scoresDCT[iScoresDCT[round(Int, 4 * popSize / 5 - 1)]])) / abs(log(scoresDCT[iScoresDCT[1]]))
            if ii > 3 && relScoreDiffDCT .< 0.01
                println("finished after $(ii) rounds, relative Score diff= $(relScoreDiffDCT)")
                break
            end
            # perturb a fifth to sample the full space
            perturbWorst(setProbDCT, scoresDCT, perturbRate)
        end
        for ii in 1:nIters
            println("----------------------------------------------------------")
            println("round $ii: GO")
            println("----------------------------------------------------------")
            bboptimize(setProb, MaxTime=nMinsBBO * 60.0) # take some black box steps for the whole pop
            pop = setProb.runcontrollers[end].optimizer.population.individuals
            scores = [objectiveSpace(x) for x in eachslice(pop, dims=2)]
            iScores = sortperm(scores)

            relScoreDiff = abs(log(scores[iScores[1]]) - log(scores[iScores[round(Int, 4 * popSize / 5 - 1)]])) / abs(log(scores[iScores[1]]))
            if ii > 3 && relScoreDiff .< 0.01
                println("finished after $(ii) rounds, relative Score diff= $(relScoreDiff)")
                break
            end
            # perturb a fifth to sample the full space
            perturbWorst(setProb, scores, perturbRate, identity)
        end
        plot(fitnessCallsHistory[3:end], log.(fitnessHistory[3:end]), xscale=:log10, label="Space descent")
        plot!(fitnessCallsHistoryDCT[3:end], log.(fitnessHistoryDCT[3:end]), xscale=:log10, label="DCT descent", title="Comparing Convergence Rates", xlabel="Number of steps (log10 scale)", ylabel="Objective value (linear scale)")
        savefig(locFigDir * "convergenceSpaceDCT.pdf")

        popDCT = setProbDCT.runcontrollers[end].optimizer.population.individuals
        scoresDCT = [objectiveDCT(x) for x in eachslice(pop, dims=2)]
        pop = setProb.runcontrollers[end].optimizer.population.individuals
        scores = [objectiveSpace(x) for x in eachslice(pop, dims=2)]
        if minimum(scoresDCT) < minimum(scores)
            betterPop = popDCT
            betterScores = scoresDCT
            betterSetProb = setProbDCT
            betterObjective = objectiveDCT
            mapToSpace = x -> idct(x, 1)
        else
            betterPop = pop
            betterScores = scores
            betterSetProb = setProb
            betterObjective = objectiveSpace
            mapToSpace = identity
        end

        saveSerial(saveSerialName, betterSetProb)
        save(saveName, "pop", betterPop, "scores", betterScores, "fitnessCallsHistory", fitnessCallsHistory, "fitnessCallsDCT", fitnessCallsHistoryDCT, "fitnessHistoryDCT", fitnessHistoryDCT, "fitnessHistory", fitnessHistory)

        histogram(log.(betterScores) / log(1.1), legend=false, xlabel="coordinate value + norm of input", title="Histogram of log betterScores\nlocation: $(l), first: $(tf1)Hz")
        savefig(locFigDir * "histogramPop$(extraName).pdf")
    end
end
