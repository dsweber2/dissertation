using Revise
using Distributed
using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, FFTW, LinearAlgebra, FourierFilterFlux
using Optim, LineSearches, Random, Distributions
using MLJ, DataFrames, MLJLinearModels
using Plots
using Revise
using CUDA, Zygote, Flux, FFTW
using Test
using LinearAlgebra
using BlackBoxOptim
using BlackBoxOptim: num_func_evals
using ScatteringInterpretation
gr(dpi=200)
# joint BCF plots
NDims = 128
N = 100
popSize = 500
nMinsBBO = 3
perturbRate = 1 / 5
totalMins = 1 * 60
homeDir = "../../results/bellCylinderFunnel/"
function chooseBestLambda(cv)
    effectiveRegion = (cv.meanloss .- cv.stdloss) .< minimum(cv.meanloss)
    λBest = maximum(cv.path.lambda[BitVector([effectiveRegion..., zeros(Bool, length(cv.path.lambda) - length(cv.meanloss))...])])
    findfirst(x -> x == λBest, cv.path.lambda)
end
St = scatteringTransform((NDims, 1, 1), cw=dog2, 2, Q=4, β=[1,1,1], extraOctaves=(0, 0, 0), averagingLength=[-1.5,-1.5,2], outputPool=2 * 4, convBoundary=FourierFilterFlux.Periodic(), boundary=PerBoundary())
specName = "stLay2_dog2" * "$N"
cvpSt = FileIO.load(joinpath(homeDir, specName * ".jld2"), "cvp")
cvpStRot = FileIO.load(joinpath(homeDir, specName * "Rot.jld2"), "cvpRot")
βSt2 = cvpSt.path.betas[:,:,chooseBestLambda(cvpSt)]
βSt2Rot = cvpStRot.path.betas[:,:,chooseBestLambda(cvpStRot)]
freqsSt = getMeanFreq(St, 1000)

St12 = scatteringTransform((NDims, 1, 1), cw=[dog1, dog2, dog2], 2, Q=4, β=[1,1,1], extraOctaves=(-0, -0, 0), averagingLength=[-1.5,-1.5,2], outputPool=4 * 4, convBoundary=FourierFilterFlux.Periodic(), boundary=PerBoundary())
specName12 = "stLay2_dog2_dog1" * "$N"
cvpSt_dog12 = FileIO.load(joinpath(homeDir, specName12 * ".jld2"), "cvp")
cvpSt_dog12Rot = FileIO.load(joinpath(homeDir, specName12 * "Rot.jld2"), "cvpRot")
βSt12 = cvpSt_dog12.path.betas[:,:,chooseBestLambda(cvpSt_dog12)]
βSt12Rot = cvpSt_dog12Rot.path.betas[:,:,chooseBestLambda(cvpSt_dog12Rot)]
freqsSt12 = getMeanFreq(St12, 1000)


St1 = scatteringTransform((NDims, 1, 1), cw=dog1, 2, Q=4, β=[1,1,1], extraOctaves=(-0, -0, 0), averagingLength=[-1.5,-1.5,2], outputPool=4 * 4, convBoundary=FourierFilterFlux.Periodic(), boundary=PerBoundary())
specName1 = "stLay2_dog1" * "$N"
cvpSt_dog1 = FileIO.load(joinpath(homeDir, specName1 * ".jld2"), "cvp")
cvpSt_dog1Rot = FileIO.load(joinpath(homeDir, specName1 * "Rot.jld2"), "cvpRot")
βSt1 = cvpSt_dog1.path.betas[:,:,chooseBestLambda(cvpSt_dog1)]
βSt1Rot = cvpSt_dog1Rot.path.betas[:,:,chooseBestLambda(cvpSt_dog1Rot)]
freqsSt1 = getMeanFreq(St1, 1000)
#
function generateData(N; rng=nothing, rotate=true)
    if rng == nothing
        rng = MersenneTwister()
    end
    d1 = DiscreteUniform(16, 32); d2 = DiscreteUniform(32, 96);

    # Making cylinder signals
    cylinderNew = zeros(128, N);
    a = rand(rng, d1, N); b = a + rand(rng, d2, N);
    η = randn(rng, N);
    for k = 1:N
        cylinderNew[a[k]:b[k],k] = (6 + η[k]) * ones(b[k] - a[k] + 1);
        if rotate
            cylinderNew[:,k] = circshift(cylinderNew[:,k], rand(rng, 0:(N - 1)))
        end
    end

    # Making bell signals
    bellNew = zeros(128, N);
    a = rand(rng, d1, N); b = a + rand(rng, d2, N);
    η = randn(rng, N);
    for k = 1:N
        bellNew[a[k]:b[k],k] = (6 + η[k]) * collect(0:(b[k] - a[k])) / (b[k] - a[k]);
        if rotate
            bellNew[:,k] = circshift(bellNew[:,k], rand(rng, 0:(N - 1)))
        end
    end

    # Making funnel signals
    funnelNew = zeros(128, N);
    a = rand(rng, d1, N); b = a + rand(rng, d2, N);
    η = randn(rng, N);
    for k = 1:N
        funnelNew[a[k]:b[k],k] = (6 + η[k]) * collect((b[k] - a[k]):-1:0) / (b[k] - a[k]);
        if rotate
            funnelNew[:,k] = circshift(funnelNew[:,k], rand(rng, 0:(N - 1)))
        end
    end

    test = cat(cylinderNew, bellNew, funnelNew, dims=2);
    return test + randn(rng, 128, 3N)        # adding noise
end
d = generateData(100)

function plotConst(cvp, colors=:default)
    iλ = chooseBestLambda(cvp)
    a0 = cvp.path.a0[:,iλ]
    limA0 = maximum(abs.(a0)) * 1.08
    p2 = scatter([0,0,0]', a0', labels=["Cylinder" "Bell" "Funnel"], xlims=(-.5, .5), ylims=(-limA0, limA0), framestyle=:origin, marker=([:hexagon :circle :diamond], 8, .9), title="Bias", legend=false, xticks=(0), palette=colors)
end
function getZeroFrac(X)
    if maximum(X) == minimum(X)
        return .5 # if both are zero, then just return the midpoint
    elseif maximum(X) <= 0
        return .9 # if the max is below zero, stick zero high (.9)
    elseif minimum(X) >= 0
        return .1 # if the min is below zero, stick zero low (.1)
    else
        abs(minimum(X)) / (maximum(X) - minimum(X)) # return the point between 0 and 1 that corresponds to the fraction of its distance between min and max
    end
end
function getRange(X)
    if maximum(X) <= 0 && minimum(X) < 0 # if the max is below zero, then we need to adjust a bit
        return (minimum(X), -(.1 / .9) * minimum(X))
    elseif minimum(X) >= 0 && maximum(X) > 0
        return (-(.1 / .9) * maximum(X), maximum(X))
    elseif minimum(X) == maximum(X)
        return (-1, 1)
    else
        return (minimum(X), maximum(X))
    end
end

getMostExtremeValue(X) = X[argmax(abs.(X), dims=1)][1,axes(X)[2:end - 1]...,1]
using PerceptualColourMaps
function multiClassPlot(cvp, St, title="Multinomial Coefficients"; firstFreqSpacing=2, secondFreqSpacing=nothing)
    iλ = chooseBestLambda(cvp)
    β = cvp.path.betas[:,:,iλ]
    a0 = cvp.path.a0[:,iλ]
    βWrap = reshape(β, (size(St(reshape(d[:,1], (128, 1, 1)))[2])[1:end - 1]..., 3))
    freqs = getMeanFreq(St, 1000)
    if firstFreqSpacing isa Integer
        firstFreqSpacing = 1:firstFreqSpacing:length(freqs[1])
    end
    if secondFreqSpacing isa Integer
        secondFreqSpacing = 1:secondFreqSpacing:length(freqs[2])
    end
    p1 = plotSecondLayer(βWrap[:,:,:,1:1], St, toHeat=getMostExtremeValue(βWrap[:,:,:,1:1]), transp = true, logPower=false, title="Cylinder", c=cgrad([:royalblue1,:white,:green], getZeroFrac(getMostExtremeValue(βWrap[:,:,:,1:1]))), xlabel="", ylabel="", xVals=(.000, .839), yVals=(.000, .973), bottom_margin=5Plots.px,  firstFreqSpacing=firstFreqSpacing, secondFreqSpacing=secondFreqSpacing, frameTypes=:zerolines, subClims=getRange(βWrap[:,:,:,1:1]))
    p2 = plotSecondLayer(βWrap[:,:,:,2:2], St, toHeat=getMostExtremeValue(βWrap[:,:,:,2:2]), transp = true, logPower=false, title="Bell", c=cgrad([:royalblue1,:white,:red], getZeroFrac(getMostExtremeValue(βWrap[:,:,:,2:2]))), xlabel="", xVals=(.000, .839), yVals=(.000, .973), bottom_margin=5Plots.px,  firstFreqSpacing=firstFreqSpacing, secondFreqSpacing=secondFreqSpacing, frameTypes=:zerolines, subClims=getRange(βWrap[:,:,:,2:2]))
    p3 = plotSecondLayer(βWrap[:,:,:,3:3], St, toHeat=getMostExtremeValue(βWrap[:,:,:,3:3]), transp = true, logPower=false, title="Funnel", c=cgrad([:royalblue1, :white, :orange], getZeroFrac(getMostExtremeValue(βWrap[:,:,:,3:3]))), ylabel="", xVals=(.000, .839), yVals=(.000, .973), firstFreqSpacing=firstFreqSpacing, secondFreqSpacing=secondFreqSpacing, frameTypes=:zerolines, subClims=getRange(βWrap[:,:,:,3:3]))
    p4 = plotConst(cvp, [:green,:red,:orange])
    l = @layout [b{.01h}; grid(3, 1) a{.2w}]
    titlePlot = plot(title=title, grid=false, xticks=nothing, yticks=nothing, showaxis=false, bottom_margin=-20Plots.px)
    plot(titlePlot, p1, p2, p3, p4, layout=l)
end
# dog 2
multiClassPlot(cvpSt, St, "Multinomial weights for the second layer")
savefig("../../figures/bcf/St2dog2Multinomial.pdf")
multiClassPlot(cvpStRot, St, "Multinomial weights for the second layer\n Rotated Signals")
savefig("../../figures/bcf/St2dog2MultinomialRot.pdf")
# dog 12
multiClassPlot(cvpSt_dog12, St12, "Multinomial weights for the second layer")
savefig("../../figures/bcf/St2dog12MultinomialRot.pdf")
multiClassPlot(cvpSt_dog12Rot, St12, "Multinomial weights for the second layer\n Rotated Signals")
savefig("../../figures/bcf/St2dog12MultinomialRot.pdf")
# dog 1
multiClassPlot(cvpSt_dog1, St1, "Multinomial weights for the second layer")
savefig("../../figures/bcf/St2dog1MultinomialRot.pdf")
multiClassPlot(cvpSt_dog1Rot, St1, "Multinomial weights for the second layer\n Rotated Signals")
savefig("../../figures/bcf/St2dog1MultinomialRot.pdf")


saveFigDir = "../../figures/bcf/interpret/"
if !isdir(saveFigDir)
    mkpath(saveFigDir)
end
listOfβs = ((βSt2[:,1], "Cyldog2", "Cylinder using DoG2 second layer only", "", St), (βSt2[:,2], "Belldog2", "Bell using DoG2 second layer only", "", St), (βSt2[:,3], "fundog2", "Funnel using DoG2 second layer only", "", St), (βSt2Rot[:,1], "Cyldog2", "Cylinder using DoG2 second layer only", "rotated", St), (βSt2Rot[:,2], "Belldog2", "Bell using DoG2 second layer only", "rotated", St), (βSt2Rot[:,3], "fundog2", "Funnel using DoG2 second layer only", "rotated", St),
            (βSt12[:,1], "Cyldog12", "Cylinder using DoG1 then DoG2", "", St12), (βSt12[:,2], "Belldog12", "Bell using DoG1 then DoG2", "", St12), (βSt12[:,3], "fundog12", "Funnel using DoG1 then DoG2", "", St12), (βSt12Rot[:,1], "Cyldog12", "Cylinder using DoG1 then DoG2", "rotated", St12), (βSt12Rot[:,2], "Belldog12", "Bell using DoG1 then DoG2", "rotated", St12), (βSt12Rot[:,3], "fundog12", "Funnel using DoG1 then DoG2", "rotated", St12),
            (βSt1[:,1], "Cyldog1", "Cylinder using DoG1", "", St1), (βSt1[:,2], "Belldog1", "Bell using DoG1", "", St1), (βSt1[:,3], "fundog1", "Funnel using DoG1", "", St1), (βSt1Rot[:,1], "Cyldog1", "Cylinder using DoG1", "rotated", St1), (βSt1Rot[:,2], "Belldog1", "Bell using DoG1", "rotated", St1), (βSt1Rot[:,3], "fundog1", "Funnel using DoG1", "rotated", St1))
listOfLists = (((βSt2[:,1], "Cyldog2", "Cylinder using DoG2 Dog second layer only", "", St), (βSt2[:,2], "Belldog2", "Bell using DoG2 Dog second layer only", "", St), (βSt2[:,3], "fundog2", "Funnel using DoG2 Dog second layer only", "", St),),
               ((βSt2Rot[:,1], "Cyldog2", "Cylinder using DoG2 Dog second layer only", "rotated", St), (βSt2Rot[:,2], "Belldog2", "Bell using DoG2 Dog second layer only", "rotated", St), (βSt2Rot[:,3], "fundog2", "Funnel using DoG2 Dog second layer only", "rotated", St)),
               ((βSt12[:,1], "Cyldog12", "Cylinder using DoG1 then DoG2", "", St12), (βSt12[:,2], "Belldog12", "Bell using DoG1 then DoG2", "", St12), (βSt12[:,3], "fundog12", "Funnel using DoG1 then DoG2", "", St12)),
               ((βSt12Rot[:,1], "Cyldog12", "Cylinder using DoG1 then DoG2", "rotated", St12), (βSt12Rot[:,2], "Belldog12", "Bell using DoG1 then DoG2", "rotated", St12), (βSt12Rot[:,3], "fundog12", "Funnel using DoG1 then DoG2", "rotated", St12)),
               ((βSt1[:,1], "Cyldog1", "Cylinder using DoG1", "", St1), (βSt1[:,2], "Belldog1", "Bell using DoG1", "", St1), (βSt1[:,3], "fundog1", "Funnel using DoG1", "", St1)),
               ((βSt1Rot[:,1], "Cyldog1", "Cylinder using DoG1", "rotated", St1), (βSt1Rot[:,2], "Belldog1", "Bell using DoG1", "rotated", St1), (βSt1Rot[:,3], "fundog1", "Funnel using DoG1", "rotated", St1)))
longTitles = ("DoG2 best fits Classic bcf", "DoG2 best fits Rotated bcf", "DoG1 then DoG2 best fits, Classic bcf", "DoG1 then DoG2 best fits, Rotated bcf", "DoG1 best fits, Classic bcf", "DoG1 best fits, Rotated bcf")
shortSetNames = ("dog2", "dog2Rot", "dog12", "dog12Rot", "dog1", "dog1Rot")
listOfFreqs = (freqsSt, freqsSt, freqsSt12, freqsSt12, freqsSt1, freqsSt1)
# jj = 1
for jj = 1:6
    longTitle = longTitles[jj]
    listOfTraits = listOfLists[jj]
    shortSetName = shortSetNames[jj]
    freqs = listOfFreqs[jj]
    dog2Sols = zeros(128, 3)
    StThis = listOfTraits[end]
    for ii = 1:3
        (β, shortName, longName, rotated, StThis) = listOfTraits[ii]
        saveDir = joinpath(homeDir, "fitting")
        locDir = joinpath(saveDir, shortName * rotated)
        extraName = ""
    # unless otherwise specified, use the most recently fit version
        if isfile(locDir * "data.jld2") && extraName == ""
            nRepeat = 1
            while isfile(locDir * "data$(nRepeat).jld2")
                nRepeat += 1
            end
            extraName = "$(nRepeat - 1)"
        end
        saveName = locDir * "data$(extraName).jld2"
        saveNameDict = locDir * "dictionary"
        saveSerialName = locDir * "data$(extraName)"
        savedRes = FileIO.load(saveName)
        pop = savedRes["pop"]
        scores = savedRes["scores"]
        iScores = sortperm(scores)
        bestEx = idct(pop[:,iScores[1]], 1)
        dog2Sols[:,ii] = bestEx
    end
    locFigDir = joinpath(saveFigDir, shortSetName)
    if !isdir(locFigDir)
        mkpath(locFigDir)
    end
    overTitle = plot(randn(1, 2), xlims=(1, 1.1), xshowaxis=false, yshowaxis=false, label="", colorbar=false, grid=false, framestyle=:none, title=longTitle, top_margin=-25Plots.px, bottom_margin=-2Plots.px)
    actualPlots = plot(plot(dog2Sols[:,1], title="Cylinder", xticks=(0:20:120, ""), top_margin=10Plots.px), plot(dog2Sols[:,2], title="Bell", xticks=(0:20:120, "")), plot(dog2Sols[:,3], title="Funnel"), layout=(3, 1), legend=false, color=:black)
    l = @layout [a{.0001h}; b]
    plot(overTitle, actualPlots, layout=l)
    savefig(joinpath(locFigDir, "fitSignals.pdf"))
    savefig(joinpath(locFigDir, "fitSignals.png"))
    overTitle = plot(randn(1, 2), xlims=(1, 1.1), xshowaxis=false, yshowaxis=false, label="", colorbar=false, grid=false, framestyle=:none, title="$longTitle, FFT", top_margin=-25Plots.px, bottom_margin=-5Plots.px)
    actualPlots = plot(plot(abs.(fft(dog2Sols[:,1])), title="Cylinder", xticks=(0:20:120, ""), bottom_margin=-10Plots.px), plot(abs.(fft(dog2Sols[:,2])), title="Bell", xticks=(0:20:120, ""), bottom_margin=-10Plots.px), plot(abs.(fft(dog2Sols[:,3])), title="Funnel"), layout=(3, 1), legend=false)
    l = @layout [a{.0001h}; b]
    plot(overTitle, actualPlots, layout=l)
    savefig(joinpath(locFigDir, "fitSignalsFFT.pdf"))
    savefig(joinpath(locFigDir, "fitSignalsFFT.png"))
    actualScalograms = plot(heatmap(1:128, freqs[1][1:end - 1], abs.(circshift(StThis.mainChain[1](dog2Sols[:,1])[:,1:(end - 1),1,1]', (0, 0))).^2; c=cgrad(:viridis, scale=:exp), framestyle=:box, title="Cylinder",  titlefontsize=8, guidefontsize=5, tickfontsize=4, colorbar=false, link=:all, xticks=(1:20:120, ""), bottom_margin=-3Plots.px), heatmap(1:128, freqs[1][1:end - 1], abs.(circshift(StThis.mainChain[1](dog2Sols[:,2])[:,1:(end - 1),1,1]', (0, 0))).^2; c=cgrad(:viridis, scale=:exp), framestyle=:box, title="Bell",  titlefontsize=8, guidefontsize=5, tickfontsize=4, colorbar=false, link=:all, xticks=(1:20:120, ""), bottom_margin=-3Plots.px), heatmap(1:128, freqs[1][1:end - 1], abs.(circshift(StThis.mainChain[1](dog2Sols[:,3])[:,1:(end - 1),1,1]', (0, 0))).^2; c=cgrad(:viridis, scale=:exp), framestyle=:box, title="Funnel",  titlefontsize=8, guidefontsize=7, tickfontsize=4, colorbar=false, link=:all, bottom_margin=-4Plots.px,xlabel="Time (ms)"), layout=(3, 1))
    overTitle = plot(randn(1, 2), xlims=(1, 1.1), xshowaxis=false, yshowaxis=false, label="", colorbar=false, grid=false, framestyle=:none, title="$(longTitle), Scalograms", top_margin=-25Plots.px, bottom_margin=-5Plots.px)
    fauxYLabel = Plots.scatter([0,0], [0,1], xlims=(1, 1.1), xshowaxis=false, label="", colorbar=false, grid=false, ylabel="Frequency (Hz)", xticks=false, yticks=false, framestyle=:grid, left_margin=13Plots.px, right_margin=-38Plots.px)
    lay = @layout [a{.01h}; [b{.00001w} c]]
    plot(overTitle, fauxYLabel, actualScalograms, layout=lay)
    savefig(joinpath(locFigDir, "fitSignalsScalograms.pdf"))
    savefig(joinpath(locFigDir, "fitSignalsScalograms.png"))

# second layer plots for the best fits
    p1 = plotSecondLayer(St(reshape(dog2Sols[:,1], (128, 1, 1))), St, logPower=false, title="Cylinder", yVals=(.000, .973), frameTypes=:zerolines, xlabel="", ylabel="", bottom_margin=1Plots.px, xticks=nothing, firstFreqSpacing=1:2:length(freqs[2]))
    p2 = plotSecondLayer(St(reshape(dog2Sols[:,2], (128, 1, 1))), St, logPower=false, title="Bell", yVals=(.000, .973), frameTypes=:zerolines, xlabel="", ylabel="", bottom_margin=1Plots.px, xticks=nothing, firstFreqSpacing=1:2:length(freqs[2]))
    p3 = plotSecondLayer(St(reshape(dog2Sols[:,3], (128, 1, 1))), St, logPower=false, title="Funnel", yVals=(.000, .973), frameTypes=:zerolines, ylabel="", firstFreqSpacing=1:2:length(freqs[2]))
    overTitle = plot(randn(1, 2), xlims=(1, 1.1), xshowaxis=false, yshowaxis=false, label="", colorbar=false, grid=false, framestyle=:none, title="$(longTitle), Second Layer", top_margin=-25Plots.px, bottom_margin=-5Plots.px)
    l = @layout [b{.01h}; grid(3, 1)]
    yaxis = plot(grid=false,right_margin=-40Plots.px,left_margin=15Plots.px, xticks=nothing, yticks=nothing, showaxis=false,ylabel="Layer 1 Frequency (Hz)")
    l = @layout [a{.0001h}; b{.0001w} grid(3,1 )]
    plot(overTitle, yaxis, p1, p2, p3, layout=l)
    savefig(joinpath(locFigDir, "fitSignalsSecondLayer.pdf"))
    savefig(joinpath(locFigDir, "fitSignalsSecondLayer.png"))
end
