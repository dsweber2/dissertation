using ScatteringTransform, Flux, Zygote, Plots, FourierFilterFlux, ContinuousWavelets, Wavelets, FFTW, LinearAlgebra, CUDA
using PerceptualColourMaps
CUDA.allowscalar(true)
redoing = Dict(:cylinder => true, :delta => true, :sinusoid => true, :noise => true, :bumps => true)
saveDir = "../../figures/interpret/gradientAlone/"
N = 2048
outPool = 2 * 4
St =    stFlux((N, 1, 1), cw=dog2, 2, Q=4, β=[1,1,1], extraOctaves=(-0, -0, 0), averagingLength=[-1.5,-1.5,0], outputPool=outPool, convBoundary=FourierFilterFlux.Periodic(), boundary=PerBoundary(), poolBy=2, normalize=true) # |> gpu
StMorl =    stFlux((N, 1, 1), 2, Q=4, β=[1,1,1], extraOctaves=(-0, -0, 0), averagingLength=[-1.5,-1.5,0], outputPool=outPool, convBoundary=FourierFilterFlux.Periodic(), boundary=PerBoundary(), poolBy=2, normalize=true) # |> gpu
StUnnorm =    stFlux((N, 1, 1), cw=dog2, 2, Q=4, β=[1,1,1], extraOctaves=(-0, -0, 0), averagingLength=[-1.5,-1.5,0], outputPool=outPool, convBoundary=FourierFilterFlux.Periodic(), boundary=PerBoundary(), poolBy=2, normalize=false) # |> gpu
Stcpu = stFlux((N, 1, 1), cw=dog2, 2, Q=4, β=[1,1,1], extraOctaves=(-0, -0, 0), averagingLength=[-1.5,-1.5,0], outputPool=outPool, convBoundary=FourierFilterFlux.Periodic(), boundary=PerBoundary(), poolBy=2)
StcpuUnnorm = stFlux((N, 1, 1), cw=dog2, 2, Q=4, β=[1,1,1], extraOctaves=(-0, -0, 0), averagingLength=[-1.5,-1.5,0], outputPool=outPool, convBoundary=FourierFilterFlux.Periodic(), boundary=PerBoundary(), poolBy=2, normalize=false)
x = randn(N, 1, 1) # |> gpu
freqs = getMeanFreq(Stcpu, 1000)
freqs = map(x -> round.(x, sigdigits=4), freqs)
St(x)
j2 = 1; j1 = 3
@time ∇x, = gradient(x -> St(x)[2][round(Int, N / 2 / 2 / outPool), j2,j1,1], x)
@time ∇x, = gradient(x -> Stcpu(x)[2][round(Int, N / 2 / 2 / outPool), j2,j1,1], randn(2048, 1, 1))
plot(∇x[:,1,:], legend=false)
ψ1, ψ2, ψ3 = getWavelets(St, spaceDomain=true)
plot(-ψ2[:,3])
plot(ψ2[:,end])
size(ψ2)
halfSupportSize = count((ψ2[:,end] ./ maximum(ψ2[:,end])) .> .5)
println("father wavelet size = $(1024 .+ (-halfSupportSize, halfSupportSize))")
println("father wavelet size = $((1024 .+ (-halfSupportSize, halfSupportSize)) ./ 2056)")
2 * halfSupportSize

# much larger cylinder example
_, J2, J1, _ = size(St(x)[2])
function generateAllTheGradients(StThis, signal, spaceLoc=round(Int, N / 2 / 2 / outPool / 2))
    _, J2, J1, _ = size(StThis(signal)[2])
    allTheGradients = [gradient(x -> StThis(x)[2][spaceLoc, j2,j1,1], signal)[1] for j2 = 1:J2, j1 = 1:J1]
end
zeroAtOutOf(k,n,N) = range(-(k) / n, (n - k) / n + ((N - 1) % 2) / N, length=N)

# allTheGrads = zeros(N, J2, J1)
# for j2 in 1:J2, j1 in 1:J1
#     allTheGrads[:,j2,j1] = allTheGradients[j2,j1]
# end
function generateTitle(title; kwargs...)
    titlePlot = plot(;title=title, grid=false, xticks=nothing, yticks=nothing, showaxis=false, bottom_margin=-20Plots.px, kwargs...)
    return titlePlot
end
function generateTitleAndLayout(title, otherLayout)
    titlePlot = plot(title=title, grid=false, xticks=nothing, yticks=nothing, showaxis=false, bottom_margin=-20Plots.px)
    l = @layout [a{.0001h}; otherLayout]
    return titlePlot, l
end
function plotAllTheGradients(allTheGradients, spacing=2, title="Gradient"; normalize=false, preproc=identity, start=-1, kwargs...)
    if preproc isa Symbol
        if preproc == :rfft
            preproc = x -> abs.(rfft(x, 1))
        elseif preproc == :sameWavelet
            preproc = x -> circshift(cpu(St.mainChain[1](x))[:,:,1,1]', (1, 0))
        else
            error("$(preproc) is not a defined symbol")
        end
    end
    justTheFirst = preproc(allTheGradients[1,1][:,1,1])
    p1 = plot(justTheFirst)
    NSpace = size(justTheFirst, 1)
    allThePlots = Array{typeof(p1),2}(undef, size(allTheGradients)...)
    J2, J1 = size(allTheGradients)
    procced = [preproc(allTheGradients[j2,j1][:,1,1]) for j1=1:size(allTheGradients,2), j2=1:size(allTheGradients,1)]
    for j2 in 1:J2, j1 in 1:J1
        if normalize
            ylims = (minimum(procced), maximum(procced))
        else
            ylims = (minimum(preproc(allTheGradients[j2,j1][:,1,1])), maximum(preproc(allTheGradients[j2,j1][:,1,1])))
        end

        if j1 == 1 && j2 != 1
            allThePlots[j2, J1 - j1 + 1] = plot(range(start, 1, length=NSpace), preproc(allTheGradients[j2,j1][:,1,1]); legend=false, xlabel="$(freqs[2][j2])", guidefontsize=8, xticks=false, yticks=false, framestyle=:zerolines, top_margin=-5Plots.px, left_margin=-5Plots.px, right_margin=-5Plots.px, yguidefontrotation=-90, ylims=ylims, kwargs...)
        elseif j2 == 1 && j1 != 1
            allThePlots[j2, J1 - j1 + 1] = plot(range(start, 1, length=NSpace), preproc(allTheGradients[j2,j1][:,1,1]); legend=false, ylabel="$(freqs[1][j1])", guidefontsize=8, xticks=false, yticks=false, framestyle=:zerolines, bottom_margin=-5Plots.px, top_margin=-5Plots.px, left_margin=10Plots.px, right_margin=-5Plots.px, yguidefontrotation=-90, ylims=ylims, kwargs...)
        elseif j2 == 1 && j1 == 1
            allThePlots[j2, J1 - j1 + 1] = plot(range(start, 1, length=NSpace), preproc(allTheGradients[j2,j1][:,1,1]); legend=false, ylabel="$(freqs[2][j2])", xlabel="$(freqs[1][j1])", guidefontsize=8, xticks=false, yticks=false, framestyle=:zerolines, top_margin=-5Plots.px, left_margin=10Plots.px, right_margin=-5Plots.px, yguidefontrotation=-90, ylims=ylims, kwargs...)
        else
            allThePlots[j2, J1 - j1 + 1] = plot(range(start, 1, length=NSpace), preproc(allTheGradients[j2,j1][:,1,1]); legend=false, xticks=false, yticks=false, framestyle=:zerolines, bottom_margin=-5Plots.px, top_margin=-5Plots.px, left_margin=-5Plots.px, right_margin=-5Plots.px, yguidefontrotation=-90, ylims=ylims, kwargs...)
        end
    end
    toBePlotted = allThePlots[1:spacing:end, 1:spacing:end]
    titlePlot = plot(title=title, grid=false, xticks=nothing, yticks=nothing, showaxis=false, bottom_margin=-20Plots.px)
    yaxis = plot(grid=false, left_margin=20Plots.px, xticks=nothing, yticks=nothing, showaxis=false, ylabel="First Layer Frequency (Hz)")
    xaxis = plot(grid=false, xticks=nothing, yticks=nothing, showaxis=false, bottom_margin=20Plots.px, xlabel="Second Layer Frequency (Hz)")
    l = @layout [a{.0001h}; b{.0001w} grid(reverse(size(toBePlotted))...); c{.0001h}]
    return plot(titlePlot, yaxis, toBePlotted..., xaxis, layout=l)
end
function plotAllTheGradientsHeatmap(allTheGradients, spacing=2, title="Gradient"; StThis=St, normalize=false, MetaXLabel=(freqs[2], ""), MetaYLabel=(freqs[1], ""), preproc=identity, start=-1, kwargs...)
    if preproc isa Symbol
        if preproc == :rfft
            preproc = x -> abs.(rfft(x, 1))
        elseif preproc == :sameWavelet
            preproc = x -> circshift(cpu(StThis.mainChain[1](x))[:,:,1,1]', (1, 0))
        else
            error("$(preproc) is not a defined symbol")
        end
    end
    justTheFirst = preproc(allTheGradients[1,1])[:,:,1,1]
    p1 = heatmap(justTheFirst; kwargs...)
    allThePlots = Array{typeof(p1),2}(undef, size(allTheGradients)...)
    J1, J2
    for j2 in 1:J2, j1 in 1:J1
        if j1 == 1 && j2 != 1
            allThePlots[j2, J1 - j1 + 1] = heatmap(preproc(allTheGradients[j2,j1])[:,:,1,1]; legend=false, xlabel="$(MetaXLabel[1][j2])" * MetaXLabel[2], guidefontsize=6, xticks=false, yticks=false, framestyle=:zerolines, top_margin=-5Plots.px, left_margin=-5Plots.px, right_margin=-5Plots.px, guidefontrotation=-30, kwargs...)
        elseif j2 == 1 && j1 != 1
            allThePlots[j2, J1 - j1 + 1] = heatmap(preproc(allTheGradients[j2,j1])[:,:,1,1]; legend=false, ylabel="$(MetaYLabel[1][j1])" * MetaYLabel[2], guidefontsize=6, xticks=false, yticks=false, framestyle=:zerolines, bottom_margin=-5Plots.px, top_margin=-5Plots.px, left_margin=10Plots.px, right_margin=-5Plots.px, guidefontrotation=-30, kwargs...)
        elseif j2 == 1 && j1 == 1
            allThePlots[j2, J1 - j1 + 1] = heatmap(preproc(allTheGradients[j2,j1])[:,:,1,1]; legend=false, ylabel="$(MetaYLabel[1][j1])" * MetaYLabel[2], xlabel="$(MetaXLabel[1][j2])" * MetaXLabel[2], guidefontsize=6, xticks=false, yticks=false, framestyle=:zerolines, top_margin=-5Plots.px, left_margin=10Plots.px, right_margin=-5Plots.px, guidefontrotation=-30, kwargs...)
        else
            allThePlots[j2, J1 - j1 + 1] = heatmap(preproc(allTheGradients[j2,j1])[:,:,1,1]; legend=false, xticks=false, yticks=false, framestyle=:zerolines, bottom_margin=-5Plots.px, top_margin=-5Plots.px, left_margin=-5Plots.px, right_margin=-5Plots.px, guidefontrotation=-30, kwargs...)
        end
    end
    toBePlotted = allThePlots[1:spacing:end, 1:spacing:end]
    titlePlot = plot(title=title, grid=false, xticks=nothing, yticks=nothing, showaxis=false, bottom_margin=-20Plots.px)
    yaxis = plot(grid=false, left_margin=20Plots.px, xticks=nothing, yticks=nothing, showaxis=false, ylabel="First Layer Frequency (Hz)")
    xaxis = plot(grid=false, xticks=nothing, yticks=nothing, showaxis=false, bottom_margin=20Plots.px, xlabel="Second Layer Frequency (Hz)")
    l = @layout [a{.0001h}; b{.0001w} grid(reverse(size(toBePlotted))...); c{.0001h}]
    return plot(titlePlot, yaxis, toBePlotted..., xaxis, layout=l)
end
function firstLayerPlotWavelets(∇Array, label, loc, St, sendData=cpu, isNormalized="unnormalized")
    ∇Tensor = zeros(N, size(∇Array)...)
    J1 = size(∇Array, 2)
    for ii in 1:size(∇Array, 1), j1 in 1:J1
        ∇Tensor[:,ii,j1] = ∇Array[ii,j1]
    end
    t = generateTitle("First Layer Gradients $(isNormalized) $(loc)/$(size(∇Array, 1))\n wavelet transformed")
    ylabel = plot(; ylabel="Frequency", grid=false, xticks=nothing, yticks=nothing, showaxis=false, right_margin=-30Plots.px, left_margin=20Plots.px, bottom_margin=-20Plots.px)
    xlabel = plot(; xlabel="Input time", grid=false, xticks=nothing, framestyle=nothing, yticks=nothing, showaxis=false, right_margin=-30Plots.px, left_margin=20Plots.px, top_margin=0Plots.px)
    wavecylinder = cat([circshift(cpu(St.mainChain[1](sendData(∇Tensor[:, loc:loc, j1:j1]))[:,:,1,1])', (1, 0)) for j1 = 1:J1]..., dims=3)
    clims = (minimum(wavecylinder), maximum(wavecylinder))
    hmaps = [heatmap(wavecylinder[:,:,j1], clims=clims, xticks=false, yticks=false, color=:viridis, colorbar=false, titlefontsize=6, title="$(freqs[1][j1]) Hz", bottom_margin=-5Plots.px, top_margin=-10Plots.px, left_margin=-5Plots.px, right_margin=-5Plots.px) for j1 = 1:J1]
    l = @layout [t{.03h}; ylab{.01w} [a a a a a; a a a a a; a a a a a; a a a a a;a a a a a; a a a a _]; xlabel{.01h}]
    p = plot(t, ylabel, hmaps..., xlabel, layout=l)
    savefig(joinpath(saveDir, "firstLayer$(label)$(isNormalized)$(loc)WaveletTransformed.pdf"))
    return p
end
function firstLayerPlotSpecificLocs(∇Array, label, loc, freqs, isNormalized="unnormalized")
    ∇Tensor = zeros(N, size(∇Array)...)
    J1 = size(∇Array, 2)
    for ii in 1:size(∇Array, 1), j1 in 1:J1
        ∇Tensor[:,ii,j1] = ∇Array[ii,j1]
    end
    t = generateTitle("First Layer gradients $(isNormalized) $loc/$(size(∇Array, 1))")
    l = @layout [t{.02h}; [a a a a a; a a a a a; a a a a a; a a a a a;a a a a a; a a a a _]]
    ylims = (minimum(∇Tensor[:, loc, :]), maximum(∇Tensor[:, loc, :]))
    p = plot(t, [plot(zeroAtOutOf(loc, 128, N), ∇Tensor[:, loc, j1], xticks=false, yticks=false, color=:viridis, colorbar=false, titlefontsize=6, title="$(freqs[1][j1]) Hz", legend=false, framestyle=:zerolines, ylims=ylims) for j1 = 1:J1]..., layout=l)
    savefig(joinpath(saveDir, "firstLayer$(label)$(isNormalized)Loc$(loc).pdf"))
    return p
end
function firstLayerPlotHeatmap(∇Array, label, freqs, isNormalized="unnormalized")
    ∇Tensor = zeros(N, size(∇Array)...)
    J1 = size(∇Array, 2)
    for ii in 1:size(∇Array, 1), j1 in 1:J1
        ∇Tensor[:,ii,j1] = ∇Array[ii,j1]
    end
    clims = (minimum(∇Tensor), maximum(∇Tensor))
    hmaps = [heatmap(∇Tensor[:,:, j1]', xticks=false, yticks=false, color=:viridis, colorbar=false, titlefontsize=6, title="$(freqs[1][j1]) Hz", clims=clims) for j1 = 1:J1]
    t = generateTitle("First Layer Gradients $(isNormalized) $(label)")
    ylabel = plot(; ylabel="Output time varies", grid=false, xticks=nothing, yticks=nothing, showaxis=false, right_margin=-30Plots.px, left_margin=20Plots.px, bottom_margin=-20Plots.px)
    xlabel = plot(; xlabel="Input time varies", grid=false, xticks=nothing, yticks=nothing, showaxis=false, right_margin=-30Plots.px, left_margin=20Plots.px, top_margin=-20Plots.px)
    l = @layout [t{.02h}; ylab{.01w} [a a a a a; a a a a a; a a a a a; a a a a a;a a a a a; a a a a _]; xlabel{.01h}]
    p = plot(t, ylabel, hmaps..., xlabel, layout=l)
    savefig(joinpath(saveDir, "firstLayer$(label)$(isNormalized)AllLocationsHeatmap.pdf"))
    return p
end
function zerothLayerDualPlot(St, input, label, loc, sendData=cpu, isNormalized="unnormalized", longLabel=label)
    tmp = St(sendData(input))
    @time  ∇ = [cpu(gradient(x -> St(x)[0][ii,1,1], sendData(input))[1][:,1,1]) for ii = 1:size(tmp[0], 1)]
    ∇cat = cat(∇..., dims=2)'
    p1CylNorm = heatmap(∇cat, xticks=false, ylabel="output time", title="Zeroth layer derivative for $(longLabel)\n layer-wise $(isNormalized)", color=:viridis, colorbar=false)
    p2CylNorm = plot(∇cat[loc,:], legend=false, xlabel="input time")
    l = @layout [a;b{.3h}]
    p = plot(p1CylNorm, p2CylNorm, layout=l)
    savefig(joinpath(saveDir, "zerothLayer$(label)$(loc)$(isNormalized).pdf"))
    return p
end

println("-------------------------------------------------------------------")
println("starting cylinder")
println("-------------------------------------------------------------------")
n = round(Int, N / 2)
cylinder = reshape(circshift([ones(n); zeros(N - n)], round(Int, N / 2 - n / 2)), (N, 1, 1)); plot(cylinder[:,1,1])
exStResult = St(cpu(cylinder))

zerothTimeLabel = "Subsampled Time ($(Int(N / size(exStResult[0], 1)))ms)"
firstTimeLabel = "Subsampled Time ($(Int(N / size(exStResult[1], 1)))ms)"
secondTimeLabel = "Subsampled Time ($(Int(N / size(exStResult[2], 1)))ms)"

if redoing[:cylinder]
    heatmap(cpu(St(cpu(cylinder)))[1][:,:,1]', yticks=(1:length(freqs[1][1:end - 1]), freqs[1][1:end - 1]), title="Cylinder first layer", color=cgrad(:viridis), xlabel=firstTimeLabel, ylabel="Frequency (Hz)")
    savefig(joinpath(saveDir, "firstLayerCylinderOutput.pdf"))

    @time  ∇cyl, = gradient(x -> St(x)[2][round(Int, N / 2 / 2 / outPool), j2,j1,1], cpu(cylinder))
    plot(∇cyl[:,1,:], title="Derivative of a cylinder, \nalong path (j1,j2) = $((freqs[1][j1], freqs[2][j2]))", legend=false)
    @time allTheGradients = cpu(generateAllTheGradients(St, cpu(cylinder)))
    plotAllTheGradients(allTheGradients, 2, "Cylinder input Gradient, 2nd DoG wavelets, normalized")
    savefig(joinpath(saveDir, "dog2CylinderInputGradientNormalized.pdf"))
    plotSecondLayer(cpu(St(cpu(cylinder))), Stcpu, title="Cylinder Second Layer normalized", c=cgrad(cmap("L20"), scale=:log), frameTypes=:none, logPower=true)
    savefig(joinpath(saveDir, "dog2CylinderSecondLayerOutputNormalized.pdf"))
    plotAllTheGradientsHeatmap(cpu(allTheGradients), 2, "Cylinder input Gradient, 2nd DoG wavelets, scalograms, normalized"; preproc=:sameWavelet, colorbar=false, color=:viridis, StThis=St)
    savefig(joinpath(saveDir, "dog2CylinderSecondLayerScalogramNormalized.pdf"))
    @time allTheGradientsUnnorm = cpu(generateAllTheGradients(StcpuUnnorm, cpu(cylinder)))
    plotAllTheGradients(allTheGradientsUnnorm, 2, "Cylinder input Gradient, 2nd DoG wavelets")
    savefig(joinpath(saveDir, "dog2CylinderInputGradient.pdf"))
    savefig(joinpath(saveDir, "dog2CylinderInputGradient.png"))
    plotSecondLayer(cpu(StcpuUnnorm(cpu(cylinder))), Stcpu, title="Cylinder Second Layer", frameTypes=:none, c=cgrad(cmap("L20"), scale=:log), logPower=true)
    savefig(joinpath(saveDir, "dog2CylinderSecondLayerOutput.pdf"))
    plotAllTheGradientsHeatmap(allTheGradientsUnnorm, 2, "Cylinder input Gradient, 2nd DoG wavelets, scalograms"; preproc=:sameWavelet, colorbar=false, color=:viridis, StThis=StcpuUnnorm)
    savefig(joinpath(saveDir, "dog2CylinderSecondLayerScalogramNormalized.pdf"))

# shifted cylinder
    shiftCyl = circshift(cylinder, 256)
    plot(shiftCyl[:,1,1])
    @time allTheGradientsShift = cpu(generateAllTheGradients(St, cpu(shiftCyl)))
    plotAllTheGradients(cpu.(allTheGradientsShift), 2, "Shifted Cylinder input Gradient, 2nd DoG wavelets")
    savefig(joinpath(saveDir, "dog2ShiftedCylinderInputGradient.pdf"))
    plotAllTheGradientsHeatmap(cpu(allTheGradientsShift), 2, "Shifted Cylinder input Gradient, 2nd DoG wavelets, scalograms"; preproc=:sameWavelet, colorbar=false, color=:viridis)

# zeroth layer cylinder
    plot(cylinder[:,1,1], label="cylinder")
    plot!(range(1, N, length=length(cpu(St(cpu(cylinder))[0])[:,1,1])), cpu(St(cpu(cylinder))[0])[:,1,1] * norm(cylinder[:,1,1]) / norm(cpu(St(cpu(cylinder))[0])))
    zerothLayerDualPlot(St, cylinder, "cylinder", 128, cpu, "normalized")

# first layer cylinder
    @time ∇cylinder1Unnorm = [cpu(gradient(x -> StcpuUnnorm(x)[1][ii,j1,1], cpu(cylinder))[1][:,1,1]) for ii = 1:128, j1 = 1:J1]
    @time ∇cylinder1 = [cpu(gradient(x -> St(x)[1][ii,j1,1], cpu(cylinder))[1][:,1,1]) for ii = 1:128, j1 = 1:J1]

    firstLayerPlotWavelets(∇cylinder1Unnorm, "cylinder", 64, StcpuUnnorm, identity)
    firstLayerPlotWavelets(∇cylinder1, "cylinder", 64, St, cpu, "normalized")
# plotting the individual signals by frequency unnormalized
    firstLayerPlotSpecificLocs(∇cylinder1Unnorm, "cylinder", 64, freqs, "unnormalized")
    firstLayerPlotSpecificLocs(∇cylinder1, "cylinder", 32, freqs, "normalized")
# Jacobian heatmaps
    firstLayerPlotHeatmap(∇cylinder1, "cylinder", freqs, "normalized")
    firstLayerPlotHeatmap(∇cylinder1Unnorm, "cylinder", freqs, "unnormalized")
end



# plotting the wavelet transform of the actual wavelets themselves
t = generateTitle("Wavelet transformed Wavelets")
ylabel = plot(; ylabel="Frequency", grid=false, xticks=nothing, yticks=nothing, showaxis=false, right_margin=-30Plots.px, left_margin=20Plots.px, bottom_margin=-20Plots.px)
xlabel = plot(; xlabel="Input time", grid=false, xticks=nothing, framestyle=nothing, yticks=nothing, showaxis=false, right_margin=-30Plots.px, left_margin=20Plots.px, top_margin=0Plots.px)
origWavelets = real((ifftshift(irfft(St.mainChain[1].weight, N, 1), 1)))
heatmap(cpu(origWavelets))
wavecylinder = cat([circshift(cpu(St.mainChain[1](origWavelets[:,j1])[:,:,1,1])', (1, 0)) for j1 = 1:J1]..., dims=3)
hmaps = [heatmap(wavecylinder[:,:,j1], xticks=false, yticks=false, color=:viridis, colorbar=false, titlefontsize=6, title="$(freqs[1][j1]) Hz", bottom_margin=-5Plots.px, top_margin=-10Plots.px, left_margin=-5Plots.px, right_margin=-5Plots.px) for j1 = 1:J1]
l = @layout [t{.03h}; ylab{.01w} [a a a a a; a a a a a; a a a a a; a a a a a;a a a a a; a a a a _]; xlabel{.01h}]
p = plot(t, ylabel, hmaps..., xlabel, layout=l)
savefig(joinpath(saveDir, "dog2WaveletsWaveletTransformed.pdf"))









println("-------------------------------------------------------------------")
println("starting Delta")
println("-------------------------------------------------------------------")
# delta function
δ = [1; zeros(2047)]
circshiftAmount = 1024
δ = reshape(circshift(δ, circshiftAmount), (N, 1, 1))
if redoing[:delta]
    heatmap(cpu(St(cpu(δ)))[1][:,:,1]', yticks=(1:length(freqs[1][1:end - 1]), freqs[1][1:end - 1]), title="Delta first layer", color=cgrad(:viridis), xlabel=firstTimeLabel, ylabel="Frequency (Hz)")
    savefig(joinpath(saveDir, "firstLayerDeltaOutput.pdf"))
    @time allTheGradientsDelta = cpu(generateAllTheGradients(St, cpu(δ)))
    plotAllTheGradients(allTheGradientsDelta, 2, "Delta spike gradient, 2nd DoG wavelets normalized")
    savefig(joinpath(saveDir, "dog2DeltaInputGradientNormalized.pdf"))
    plotSecondLayer(cpu(St(cpu(δ))), Stcpu, title="Delta Spike Second Layer normalized", frameTypes=:none, c=cgrad(cmap("L20"), scale=:log), logPower=true)
    savefig(joinpath(saveDir, "dog2DeltaSecondLayerOutputNormalized.pdf"))
    plotAllTheGradients(allTheGradientsDelta, 2, "Delta Spike Gradient, 2nd DoG wavelet normalized rfft", preproc=x -> abs.(rfft(x, 1)), start=0)
    savefig(joinpath(saveDir, "dog2DeltaRfftGradientNormalized.pdf"))
    plotAllTheGradientsHeatmap(cpu(allTheGradientsDelta), 2, "Delta input Gradient, 2nd DoG wavelets normalized, scalograms"; preproc=:sameWavelet, colorbar=false, color=:viridis)
    savefig(joinpath(saveDir, "dog2DeltaScalogramGradientNormalized.pdf"))
    @time allTheGradientsDelta = cpu(generateAllTheGradients(StcpuUnnorm, cpu(δ)))
    plotAllTheGradients(allTheGradientsDelta, 2, "Delta spike gradient, 2nd DoG wavelets")
    savefig(joinpath(saveDir, "dog2DeltaInputGradient.pdf"))
    savefig(joinpath(saveDir, "dog2DeltaInputGradient.png"))
    plotSecondLayer(cpu(StcpuUnnorm(cpu(δ))), StcpuUnnorm, title="Delta Spike Second Layer", frameTypes=:none, c=cgrad(cmap("L20"), scale=:log), logPower=true)
    savefig(joinpath(saveDir, "dog2DeltaSecondLayerOutput.pdf"))
    plotAllTheGradients(allTheGradientsDelta, 2, "Delta Spike Gradient, 2nd DoG wavelet rfft", preproc=x -> abs.(rfft(x, 1)), start=0)
    savefig(joinpath(saveDir, "dog2DeltaRfftGradient.pdf"))
    plotAllTheGradientsHeatmap(cpu(allTheGradientsDelta), 2, "Delta input Gradient, 2nd DoG wavelets, scalograms"; preproc=:sameWavelet, colorbar=false, color=:viridis, StThis=StcpuUnnorm)
    savefig(joinpath(saveDir, "dog2DeltaScalogramGradient.pdf"))

# the derivative of the zeroth layer; complicated by averaging, subsampling and normalization within the layer
    zerothLayerDualPlot(St, δ, "Delta", 128, cpu, "normalized")
    zerothLayerDualPlot(St, δ, "Delta", 128, cpu, "normalized")
# compare with the unnormalied version, which is just a translated father.
    zerothLayerDualPlot(StcpuUnnorm, δ, "Delta", 128, cpu, "unnormalized")
# for comparison, the gradient of just the wavelet transform (exactly the averaging wavelet)
    tmp = Stcpu.mainChain[1]
    justAveragingDerivative = gradient(x -> tmp(x)[1024,30,1,1], cpu(δ))[1]
    plot(justAveragingDerivative[:,1,1])

# the derivative of the first layer
    @time ∇δ1Unnorm = [cpu(gradient(x -> StcpuUnnorm(x)[1][ii,j1,1], cpu(δ))[1][:,1,1]) for ii = 1:128, j1 = 1:J1]
    @time  ∇δ1 = [cpu(gradient(x -> St(x)[1][ii,j1,1], cpu(δ))[1][:,1,1]) for ii = 1:128, j1 = 1:J1]

# Jacobian heatmaps
    firstLayerPlotHeatmap(∇δ1, "Delta", freqs, "normalized")
    firstLayerPlotHeatmap(∇δ1Unnorm, "Delta", freqs, "unnormalized")

# wavelet transformed
    firstLayerPlotWavelets(∇δ1Unnorm, "Delta", 64, StcpuUnnorm, identity)
    firstLayerPlotWavelets(∇δ1, "Delta", 64, St, cpu, "normalized")

# plotting the individual signals by frequency
    firstLayerPlotSpecificLocs(∇δ1Unnorm, "Delta", 64, freqs)
    firstLayerPlotSpecificLocs(∇δ1Unnorm, "Delta", 32, freqs)
    firstLayerPlotSpecificLocs(∇δ1, "Delta", 64, freqs, "normalized")
    firstLayerPlotSpecificLocs(∇δ1, "Delta", 32, freqs, "normalized")
end

println("-------------------------------------------------------------------")
println("starting pure sinusoid")
println("-------------------------------------------------------------------")
δFreq = [1; zeros(1024)]
circFreqshiftAmount = 64
δFreq = reshape(circshift(δFreq, circFreqshiftAmount), (1025, 1, 1))
# calculating the frequency
omega = range(0, 1000 / 2, length=1025)
# eachNorm = [norm(w, 1) for w in eachslice(Ŵ, dims=ndims(Ŵ))]'
sum(omega .* abs.(δFreq[:,1,1]), dims=1) ./ norm(δFreq)
δFreq = irfft(δFreq, N, 1)

if redoing[:sinusoid]
    heatmap(cpu(St(cpu(δFreq)))[1][:,:,1]', yticks=(1:length(freqs[1][1:end - 1]), freqs[1][1:end - 1]), title="Sinusoid first layer", color=cgrad(:viridis), xlabel=firstTimeLabel, ylabel="Frequency (Hz)")
    savefig(joinpath(saveDir, "firstLayerSinusoidOutput.pdf"))
    @time allTheGradientsSinusoid = cpu(generateAllTheGradients(St, cpu(δFreq)))
    plotAllTheGradients(allTheGradientsSinusoid, 2, "Sinusoid gradient, 2nd DoG wavelets normalized", normalized=true)
    savefig(joinpath(saveDir, "dog2SineInputGradientNormalized.pdf"))
    plotSecondLayer(cpu(St(cpu(δFreq))), Stcpu, title="Sinusoid Second Layer normalized", frameTypes=:none, c=cgrad(cmap("L20"), scale=:log), logPower=true)
    savefig(joinpath(saveDir, "dog2SineSecondLayerOutputNormalized.pdf"))
    plotAllTheGradients(allTheGradientsSinusoid, 2, "Sinusoid Gradient, 2nd DoG wavelet normalized rfft", preproc=x -> abs.(rfft(x, 1)), start=0)
    savefig(joinpath(saveDir, "dog2SineRfftGradientNormalized.pdf"))
    plotAllTheGradientsHeatmap(cpu(allTheGradientsSinusoid), 2, "Sinusoid input Gradient, 2nd DoG wavelets normalized, scalograms"; preproc=:sameWavelet, colorbar=false, color=:viridis)
    savefig(joinpath(saveDir, "dog2SineScalogramGradientNormalized.pdf"))
    @time allTheGradientsSinusoid = cpu(generateAllTheGradients(StcpuUnnorm, cpu(δFreq)))
    heatmap([norm(allTheGradientsSinusoid[ii, jj]) for ii=1:size(allTheGradientsSinusoid,1), jj=1:size(allTheGradientsSinusoid,1)])
    plotAllTheGradients(allTheGradientsSinusoid, 2, "Sinusoid gradient, 2nd DoG wavelets")
    savefig(joinpath(saveDir, "dog2SineInputGradient.pdf"))
    savefig(joinpath(saveDir, "dog2SineInputGradient.png"))
    plotSecondLayer(cpu(StcpuUnnorm(cpu(δFreq))), StcpuUnnorm, title="Sinusoid Second Layer", frameTypes=:none, c=cgrad(cmap("L20"), scale=:log), logPower=true)
    savefig(joinpath(saveDir, "dog2SineSecondLayerOutput.pdf"))
    plotAllTheGradients(allTheGradientsSinusoid, 2, "Sinusoid Gradient, 2nd DoG wavelet rfft", preproc=x -> abs.(rfft(x, 1)), start=0)
    savefig(joinpath(saveDir, "dog2SineRfftGradient.pdf"))
    plotAllTheGradientsHeatmap(cpu(allTheGradientsSinusoid), 2, "Sinusoid input Gradient, 2nd DoG wavelets, scalograms"; preproc=:sameWavelet, colorbar=false, color=:viridis, StThis=StcpuUnnorm)
    savefig(joinpath(saveDir, "dog2SineScalogramGradient.pdf"))
    savefig(joinpath(saveDir, "dog2SineScalogramGradient.png"))


# the derivative of the zeroth layer; complicated by averaging, subsampling and normalization within the layer
    zerothLayerDualPlot(St, δFreq, "Sinusoid", 128, cpu, "normalized")
# compare with the unnormalied version, which is just a translated father.
    zerothLayerDualPlot(StcpuUnnorm, δFreq, "Sinusoid", 128, cpu, "unnormalized")
# the derivative of the first layer
    @time ∇δFreq1Unnorm = [cpu(gradient(x -> StcpuUnnorm(x)[1][ii,j1,1], cpu(δFreq))[1][:,1,1]) for ii = 1:128, j1 = 1:J1]
    @time  ∇δFreq1 = [cpu(gradient(x -> St(x)[1][ii,j1,1], cpu(δFreq))[1][:,1,1]) for ii = 1:128, j1 = 1:J1]

# Jacobian heatmaps
    firstLayerPlotHeatmap(∇δFreq1, "Sinusoid", freqs, "normalized")
    firstLayerPlotHeatmap(∇δFreq1Unnorm, "Sinusoid", freqs, "unnormalized")

# wavelet transformed
    firstLayerPlotWavelets(∇δFreq1Unnorm, "Sinusoid", 64, StcpuUnnorm, identity)
    firstLayerPlotWavelets(∇δFreq1, "Sinusoid", 64, St, cpu, "normalized")

# plotting the individual signals by frequency
    firstLayerPlotSpecificLocs(∇δFreq1Unnorm, "Sinusoid", 64, freqs)
    firstLayerPlotSpecificLocs(∇δFreq1Unnorm, "Sinusoid", 32, freqs)
    firstLayerPlotSpecificLocs(∇δFreq1, "Sinusoid", 64, freqs, "normalized")
    firstLayerPlotSpecificLocs(∇δFreq1, "Sinusoid", 32, freqs, "normalized")

# for comparison, the gradient of just the wavelet transform (exactly the averaging wavelet)
    tmp = Stcpu.mainChain[1]
    justAveragingDerivative = gradient(x -> tmp(x)[1024,30,1,1], cpu(δFreq))[1]
    plot(justAveragingDerivative[:,1,1])

# the derivative of the first layer
    @time ∇δFreq1Unnorm = [cpu(gradient(x -> StcpuUnnorm(x)[1][ii,j1,1], cpu(δFreq))[1][:,1,1]) for ii = 1:128, j1 = 1:J1]
    @time  ∇δ1 = [cpu(gradient(x -> St(x)[1][ii,j1,1], cpu(δFreq))[1][:,1,1]) for ii = 1:128, j1 = 1:J1]

# Jacobian heatmaps
    firstLayerPlotHeatmap(∇δFreq1, "Sinusoid", freqs, "normalized")
    firstLayerPlotHeatmap(∇δFreq1Unnorm, "Sinusoid", freqs, "unnormalized")

# wavelet transformed
    firstLayerPlotWavelets(∇δFreq1Unnorm, "Sinusoid", 64, StcpuUnnorm, identity)
    firstLayerPlotWavelets(∇δFreq1Unnorm, "Sinusoid", 32, StcpuUnnorm, identity)
    firstLayerPlotWavelets(∇δFreq1, "Sinusoid", 64, St, cpu, "normalized")

# plotting the individual signals by frequency
    firstLayerPlotSpecificLocs(∇δFreq1Unnorm, "Sinusoid", 64, freqs)
    firstLayerPlotSpecificLocs(∇δFreq1Unnorm, "Sinusoid", 32, freqs)
    firstLayerPlotSpecificLocs(∇δFreq1, "Sinusoid", 64, freqs, "normalized")
    firstLayerPlotSpecificLocs(∇δFreq1, "Sinusoid", 32, freqs, "normalized")
end




println("-------------------------------------------------------------------")
println("starting White Noise")
println("-------------------------------------------------------------------")

# white noise
x = cpu(randn(N, 1, 1))
if redoing[:noise]
    heatmap(cpu(St(cpu(x)))[1][:,:,1]', yticks=(1:length(freqs[1][1:end - 1]), freqs[1][1:end - 1]), title="White noise first layer", color=cgrad(:viridis), xlabel=firstTimeLabel, ylabel="Frequency (Hz)")
    savefig(joinpath(saveDir, "firstLayerNoiseOutput.pdf"))
    @time allTheGradientsRandn = cpu(generateAllTheGradients(St, x))
    plotAllTheGradients(allTheGradientsRandn, 2, "White Noise Gradient, 2nd DoG wavelet normalized")
    savefig(joinpath(saveDir, "dog2NoiseInputGradientNormalized.pdf"))
    plotSecondLayer(cpu(St(cpu(x))), Stcpu, title="Noise Second Layer normalized", frameTypes=:none, c=cgrad(cmap("L20"), scale=:log), logPower=true)
    savefig(joinpath(saveDir, "dog2NoiseSecondLayerOutputNormalized.pdf"))
    plotSecondLayer(cpu(Stcpu(cpu(x)))[2][:,:,:,1], Stcpu, title="Noise Second Layer", frameTypes=:none, c=cgrad(cmap("L20"), scale=:log), logPower=true)
    savefig(joinpath(saveDir, "dog2NoiseSecondLayerOutput.pdf"))
    plotAllTheGradients(allTheGradientsRandn, 2, "White Noise Gradient, 2nd DoG wavelet rfft normalized", preproc=x -> abs.(rfft(x, 1)), start=0)
    savefig(joinpath(saveDir, "dog2NoiseRfftGradientNormalized.pdf"))
    @time allTheGradientsRandnUnnorm = cpu(generateAllTheGradients(StcpuUnnorm, cpu(x)))
    plotAllTheGradients(allTheGradientsRandnUnnorm, 2, "White Noise input Gradient, 2nd DoG wavelets")
    savefig(joinpath(saveDir, "dog2whiteNoiseInputGradient.pdf"))
    savefig(joinpath(saveDir, "dog2whiteNoiseInputGradient.png"))
    plotSecondLayer(cpu(StcpuUnnorm(cpu(x))), StcpuUnnorm, title="White Noise Second Layer", frameTypes=:none, c=cgrad(cmap("L20"), scale=:log), logPower=true)
    savefig(joinpath(saveDir, "dog2whiteNoiseSecondLayerOutput.pdf"))
    plotAllTheGradientsHeatmap(allTheGradientsRandnUnnorm, 2, "White Noise input Gradient, 2nd DoG wavelets, scalograms"; preproc=:sameWavelet, colorbar=false, color=:viridis, StThis=StcpuUnnorm)
    savefig(joinpath(saveDir, "dog2whiteNoiseSecondLayerScalogram.pdf"))
end







println("-------------------------------------------------------------------")
println("starting Bumps")
println("-------------------------------------------------------------------")
# bumps function
bumps = reshape(testfunction(N, "Bumps"), (N, 1, 1)) # HeaviSine, Doppler, Blocks
if redoing[:bumps]
    heatmap(cpu(St((bumps)))[1][:,:,1]', yticks=(1:length(freqs[1][1:end - 1]), freqs[1][1:end - 1]), title="Bumps first layer", color=cgrad(:viridis), xlabel=firstTimeLabel, ylabel="Frequency (Hz)")
    savefig(joinpath(saveDir, "firstLayerBumpsOutput.pdf"))
    @time allTheGradientsBumps = cpu(generateAllTheGradients(St, cpu(bumps)))
    plotAllTheGradients(allTheGradientsBumps, 2, "Bumps Gradient, 2nd DoG wavelets normalized")
    savefig(joinpath(saveDir, "dog2BumpsInputGradientNormalized.pdf"))
    plotSecondLayer(cpu(St(cpu(bumps))), Stcpu, title="Bumps Second Layer normalized", frameTypes=:none, c=cgrad(cmap("L20"), scale=:log), logPower=true)
    savefig(joinpath(saveDir, "dog2BumpsSecondLayerOutput.pdf"))
    plotAllTheGradients(allTheGradientsBumps, 2, "Bumps Gradient, 2nd DoG wavelets normalized rfft", preproc=x -> abs.(rfft(x, 1)), start=0)
    savefig(joinpath(saveDir, "dog2BumpsRfftGradientNormalized.pdf"))
    @time allTheGradientsUnnormBumps = cpu(generateAllTheGradients(StcpuUnnorm, cpu(bumps)))
    plotAllTheGradients(allTheGradientsUnnormBumps, 2, "Bumps Gradient, 2nd DoG wavelets ")
    savefig(joinpath(saveDir, "dog2BumpsInputGradient.pdf"))
    savefig(joinpath(saveDir, "dog2BumpsInputGradient.png"))
    plotSecondLayer(cpu(St(cpu(bumps))), StcpuUnnorm, title="Bumps Second Layer", frameTypes=:none, c=cgrad(cmap("L20"), scale=:log), logPower=true)
    savefig(joinpath(saveDir, "dog2BumpsSecondLayerOutput.pdf"))
    plotAllTheGradients(allTheGradientsUnnormBumps, 2, "Bumps Gradient, 2nd DoG wavelets rfft", preproc=x -> abs.(rfft(x, 1)), start=0)
    savefig(joinpath(saveDir, "dog2BumpsRfftGradient.pdf"))
    plotAllTheGradientsHeatmap(allTheGradientsUnnormBumps, 2, "Bumps input Gradient, 2nd DoG wavelets, scalograms"; preproc=:sameWavelet, colorbar=false, color=:viridis, StThis=StcpuUnnorm)
    savefig(joinpath(saveDir, "dog2BumpsSecondLayerScalogram.pdf"))

# zeroth layer bumps
    plot(bumps[:,1,1])
    plot!(range(1, N, length=length(cpu(St(cpu(bumps))[0])[:,1,1])), cpu(St(cpu(bumps))[0])[:,1,1] * norm(bumps[:,1,1]) / norm(cpu(St(cpu(bumps))[0])))
    zerothLayerDualPlot(St, bumps, "bumps", 128, cpu, "normalized")
    zerothLayerDualPlot(StcpuUnnorm, bumps, "bumps", 128, cpu, "normalized")


    plot(plot(x[:,1,1], legend=false, title="noise", color=:black), plot(cylinder[:,1,1], legend=false, title="Cylinder", color=:black), plot(δ[:,1,1], legend=false, title="Delta function", color=:black), plot(bumps[:,1,1], legend=false, title="Bumps", color=:black))
    plot(plot(cylinder[:,1,1], legend=false, title="Cylinder", color=:black), plot(δ[:,1,1], legend=false, title="Delta function", color=:black), plot(bumps[:,1,1], legend=false, title="Bumps", color=:black), layout=(3, 1))
    plot(plot(δ[:,1,1], legend=false, title="Delta function", color=:black), plot(cylinder[:,1,1], legend=false, title="Cylinder", color=:black), plot(bumps[:,1,1], legend=false, title="Bumps", color=:black), plot(δFreq[:,1,1], legend=false, title="Sinusoid", color=:black), xlims=(0, 2049), layout=(4, 1))
    savefig(joinpath(saveDir, "gradientInputs.pdf"))

    @time ∇bumps1Unnorm = [cpu(gradient(x -> StcpuUnnorm(x)[1][ii,j1,1], cpu(bumps))[1][:,1,1]) for ii = 1:128, j1 = 1:J1]
    @time  ∇bumps1 = [cpu(gradient(x -> St(x)[1][ii,j1,1], cpu(bumps))[1][:,1,1]) for ii = 1:128, j1 = 1:J1]
    ∇bumps1Un = zeros(N, size(∇bumps1Unnorm)...)
    for ii in 1:128, j1 in 1:J1
        ∇bumps1Un[:,ii,j1] = ∇bumps1Unnorm[ii,j1]
    end
# wavelet transform
    firstLayerPlotWavelets(∇bumps1Unnorm, "bumps", 64, StcpuUnnorm, identity)
    firstLayerPlotWavelets(∇bumps1, "bumps", 64, St, cpu, "normalized")


# plotting the individual signals by frequency unnormalized
    firstLayerPlotSpecificLocs(∇bumps1Unnorm, "Bumps", 64, freqs)
    firstLayerPlotSpecificLocs(∇bumps1Unnorm, "Bumps", 32, freqs)
    firstLayerPlotSpecificLocs(∇bumps1, "Bumps", 64, freqs, "normalized")
    firstLayerPlotSpecificLocs(∇bumps1, "Bumps", 32, freqs, "normalized")

# Jacobian heatmaps
    firstLayerPlotHeatmap(∇bumps1, "Bumps", freqs, "normalized")
    firstLayerPlotHeatmap(∇bumps1Unnorm, "Bumps", freqs, "unnormalized")
end

# wavelet normalization example
size(St.mainChain[1](x))
(St.mainChain[1](x)[1024,3,1,1])
testEx = 50δ
j = 7
g = gradient(x -> St.mainChain[1](x)[1024,j,1,1], testEx)[1][:,1,1]
gnorm = gradient(x -> (St.mainChain[1](x)[1024,j,1,1]) / norm(St.mainChain[1](x)), testEx)[1][:,1,1]
plot([gnorm g],labels=["normalized" "unnormalized"],ticks=false,size=1.5 .* (300, 100),legend=:bottomright,title="WT Gradient at a Delta function", linewidth=2)
savefig(joinpath(saveDir, "WTDeltaNormalizedBroken.pdf"))
St.mainChain[1](x)
plot(gradient(x -> norm(St.mainChain[1](x)), x)[1][:,1,1])
