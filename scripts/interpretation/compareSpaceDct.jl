using Revise
using Distributed
using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, FFTW, LinearAlgebra, FourierFilterFlux
using Optim, LineSearches
using MLJ, DataFrames, MLJLinearModels
using Plots
using Revise
using CUDA, Zygote, Flux, FFTW
using Test
using LinearAlgebra
using BlackBoxOptim
using BlackBoxOptim: num_func_evals
using ScatteringInterpretation

N = 128
NTrain = 1
popSize = 500
totalMins = 1 * 60
nMinsBBO = 3
perturbRate = 1 / 5
w2, w1 = (8, 15)
CW = dog2
# to compare with the dog2, we need the center frequency to be at 144 and 24.7
# second layer setup begin
β = [2,2,1]; averagingLength = [-1,-1,2]; outputPool = 8; normalize = false; poolBy = 3 // 2; NDescents = 100; descentLength = 1000; pNorm = 2; NTrain = 5000; extraOctaves = 0
namedCW = ContinuousWavelets.name(CW) # * "pi"
St = scatteringTransform((N, 1, 1), cw=CW, 2, poolBy=poolBy, β=β, averagingLength=averagingLength, outputPool=outputPool, normalize=normalize, σ=abs, p=pNorm, extraOctaves=extraOctaves)
x = randn(N, 1, 1)
Sx = St(x)
# plotSecondLayer(Sx, St)
extraName = "compareFreqSpace"
l = floor(Int, 1 / 2 * size(Sx[2], 1))
p = pathLocs(2, (l, w2, w1))
aveLenStr = mapreduce(x -> "$x", *, averagingLength)
saveDir = "../../results/singleFits/"
saveFigDir = "../../figures/interpret/singleFits/"
if !isdir(saveDir)
    mkpath(saveDir)
end
if !isdir(saveFigDir)
    mkpath(saveFigDir)
end
locDir = saveDir * "wf$(w1)ws$(w2)/"
locFigDir = saveFigDir * "wf$(w1)ws$(w2)/"
saveName = locDir * "data$(extraName).jld2"
saveNameDict = locDir * "dictionary"
saveSerialName = locDir * "data"
if !isdir(locDir)
    mkpath(locDir)
end
if !isdir(locFigDir)
    mkpath(locFigDir)
end
# second layer setup end
# plot the wavelets used

function objectiveDCT(x̂)
    x = idct(x̂)
    t = St(reshape(x, (N, 1, 1)))
    1.1f0^(-t[p][1] + 1f-5 * norm(x)^2)
end
function objectiveSpace(x)
    t = St(reshape(x, (N, 1, 1)))
    1.1f0^(-t[p][1] + 1f-5 * norm(x)^2)
end

fitnessCallsHistoryDCT = Array{Int,1}()
fitnessCallsHistory = Array{Int,1}()
fitnessHistoryDCT = Array{Float64,1}()
fitnessHistory = Array{Float64,1}()
callbackDCT = function recDCT(oc)
    push!(fitnessCallsHistoryDCT, num_func_evals(oc))
    push!(fitnessHistoryDCT, best_fitness(oc))
end
callback = function rec(oc)
    push!(fitnessCallsHistory, num_func_evals(oc))
    push!(fitnessHistory, best_fitness(oc))
end
setProbDCT = bbsetup(objectiveDCT; SearchRange=(-1000., 1000.), NumDimensions=N, PopulationSize=popSize, MaxTime=1.0, TraceInterval=5,CallbackFunction=callbackDCT, CallbackInterval=0.5)
setProb = bbsetup(objectiveSpace; SearchRange=(-1000., 1000.), NumDimensions=N, PopulationSize=popSize, MaxTime=1.0, TraceInterval=5,CallbackFunction=callback, CallbackInterval=0.5)
println("pretraining freq")
bboptimize(setProb)
println("pretraining Space")
bboptimize(setProbDCT)

pop = pinkStart(N, -1, popSize)
popDCT = dct(pop, 1) # start from the same population
adjustPopulation!(setProb, pop)
adjustPopulation!(setProbDCT, popDCT)

nIters = ceil(Int, totalMins / nMinsBBO)
for ii in 1:nIters
    println("----------------------------------------------------------")
    println("round $ii, DCT: GO")
    println("----------------------------------------------------------")
    bboptimize(setProbDCT, MaxTime=nMinsBBO * 60.0) # take some black box steps for the whole pop
    popDCT = setProbDCT.runcontrollers[end].optimizer.population.individuals
    scoresDCT = [objectiveDCT(x) for x in eachslice(popDCT, dims=2)]
    iScoresDCT = sortperm(scoresDCT)

    relScoreDiffDCT = abs(log(scoresDCT[iScoresDCT[1]]) - log(scoresDCT[iScoresDCT[round(Int, 4 * popSize / 5 - 1)]])) / abs(log(scoresDCT[iScoresDCT[1]]))
    if ii > 3 && relScoreDiffDCT .< 0.01
        println("finished after $(ii) rounds, relative Score diff= $(relScoreDiffDCT)")
        break
    end
    # perturb a fifth to sample the full space
    perturbWorst(setProbDCT, scoresDCT, perturbRate)
end
for ii in 1:nIters
    println("----------------------------------------------------------")
    println("round $ii: GO")
    println("----------------------------------------------------------")
    bboptimize(setProb, MaxTime=nMinsBBO * 60.0) # take some black box steps for the whole pop
    pop = setProb.runcontrollers[end].optimizer.population.individuals
    scores = [objectiveSpace(x) for x in eachslice(pop, dims=2)]
    iScores = sortperm(scores)

    relScoreDiff = abs(log(scores[iScores[1]]) - log(scores[iScores[round(Int, 4 * popSize / 5 - 1)]])) / abs(log(scores[iScores[1]]))
    if ii > 3 && relScoreDiff .< 0.01
        println("finished after $(ii) rounds, relative Score diff= $(relScoreDiff)")
        break
    end
    # perturb a fifth to sample the full space
    perturbWorst(setProb, scores, perturbRate, identity)
end
plot(fitnessCallsHistory[3:end], log.(fitnessHistory[3:end]),xscale=:log10,label="Space descent")
plot!(fitnessCallsHistoryDCT[3:end], log.(fitnessHistoryDCT[3:end]),xscale=:log10,label="DCT descent", title="Comparing Convergence of Space and DCT representations",xlabel="Number of steps (log10 scale)",ylabel="Objective value (linear scale)")

# NDescents = 30
pop = setProb.runcontrollers[end].optimizer.population.individuals
scores = [objectiveDCT(x) for x in eachslice(pop, dims=2)]
plot(abs.(rfft(pop[:,2])))
plot(idct(popDCT[:,2], 1))
ScatteringTransformExperiments.saveSerial(saveSerialName, setProb)
save(saveName, "pop", pop, "scores", scores)

histogram(log.(scores) / log(1.1), legend=false, xlabel="coordinate value + norm of input", title="Histogram of log scores\nlocation: $(l), first layer: $(tf1)Hz, second layer: $(tf2)Hz")
savefig(locFigDir * "histogramPop$(extraName).pdf")

# plot(idct(pop[:, [iScores[1]; iScores[end]]], 1))
# examples
iScores = sortperm(scores)
plotFirstXEx(idct(pop[:,iScores], 1), scores[iScores], "Target location: $(l), first layer: $(tf1)Hz, second layer: $(tf2)Hz")
savefig(locFigDir * "exampleResults$(extraName).pdf")
plot(idct(pop[:,iScores[1]]), title="Best Example log-score: $(round(log(objectiveDCT(pop[:,iScores[1]])), sigdigits=3))\nTarget location: $(l), first: $(tf1)Hz, second: $(tf2)Hz", c=:black, legend=false)
savefig(locFigDir * "bestExample$(extraName).pdf")
plot(abs.(rfft(idct(pop[:,iScores[1]]))), title="Fourier Domain,\n Target location: $(l), first: $(tf1)Hz, second: $(tf2)Hz", c=:black, legend=false)
savefig(locFigDir * "rfftBestExample$(extraName).pdf")
plot(pop[:,iScores[1]], title="DCT best example\n Target location: $(l), first: $(tf1)Hz, second: $(tf2)Hz", c=:black, legend=false)
savefig(locFigDir * "dctBestExample$(extraName).pdf")


# plots of the first layer
firstLayerMats = cat([St(reshape(idct(pop[:,iScores][:,ii], 1), (N, 1, 1)))[1][:,:,1]' for ii = 1:20]..., dims=3)
colorMax = (minimum(firstLayerMats), maximum(firstLayerMats))
plotOverTitle([heatmap(x, clims=colorMax, colorbar=false, c=cgrad(:viridis, scale=:log10), yticks=(1:3:size(x, 1), round.(f1[1:3:end - 1], sigdigits=3)), xticks=false) for x in eachslice(firstLayerMats, dims=3)], "First Layer results\nlocation: $(l) first: $(tf1)Hz, second: $(tf2)Hz", (4, 5))
savefig(locFigDir * "ArrayFirstLayerOutput$(extraName).pdf")
heatmap(firstLayerMats[:,:,1], clims=colorMax, colorbar=false, c=cgrad(:viridis, scale=:log10), xlabel="space", ylabel="frequency", title="First Layer results\nlocation: $(l), first: $(tf1)Hz, second: $(tf2)Hz", yticks=(1:1:size(f1, 1) - 1, round.(f1[1:1:end - 1], sigdigits=3)))
savefig(locFigDir * "bestFirstLayerOutput$(extraName).pdf")
heatmap(abs.(St.mainChain[1](idct(pop[:,iScores[1]]))[:,1:end - 1,1,1]'), title="internal first layer\nlocation: $(l), first: $(tf1)Hz, second: $(tf2)Hz", yticks=(1:1:size(f1, 1) - 1, round.(f1[1:1:end - 1], sigdigits=3)), c=cgrad(:viridis))
savefig(locFigDir * "ArrayInternalFirst$(extraName).pdf")
plot(abs.(St.mainChain[1](idct(pop[:,iScores[1]]))[:,w1,1,1]), title="first layer $(w1) for fitting at\nlocation: $(l) first: $(tf1)Hz, second: $(tf2)Hz", legend=false)
savefig(locFigDir * "targetWaveInternalFirst$(extraName).pdf")
#
# second layer plot
plotSecondLayer(St(reshape(idct(pop[:, iScores[1]]), (128, 1, 1))), St; title="SecondLayer: loc: $(l), first layer $(tf1)Hz, second layer $(tf2)Hz",logPower=false,c=cgrad(:viridis, [0 .9]))
savefig(locFigDir * "SecondLayerOutput$(extraName).pdf")
