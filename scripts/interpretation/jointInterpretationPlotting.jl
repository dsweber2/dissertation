using Revise
using Distributed
using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, FFTW, LinearAlgebra, FourierFilterFlux
using Optim, LineSearches
using MLJ, DataFrames, MLJLinearModels
using Plots
using CUDA, Zygote, Flux, FFTW
using Test
using LinearAlgebra
using BlackBoxOptim
using BlackBoxOptim: num_func_evals
using ScatteringInterpretation
gr(dpi = 200)
N = 128
NTrain = 1
popSize = 500
totalMins = 1 * 60
nMinsBBO = 3
locationsAndWavelets = ((:all, dog2),
    (:all, dog1),
    (:all, Morlet(π)),
    (:all, Morlet(2π)),)
longNames = ("DoG2", "DoG1", "Morlet π", "Morlet 2π")
shortNames = ["dog2", "dog1", "morl", "morl2Pi"] # set the names for saving
# ii = 4; jj = 3
# ii = 2
# listOfLocations, CW = locationsAndWavelets[ii]
# (w2, w1) = listOfLocations[jj]
# name = names[ii]
# second layer plots
for ii = 1:length(locationsAndWavelets)
    (listOfLocations, CW) = locationsAndWavelets[ii]
    if listOfLocations == :all
        β = [2, 2, 1]
        averagingLength = [-1, -1, 2]
        outputPool = 8
        normalize = false
        poolBy = 3 // 2
        NDescents = 100
        descentLength = 1000
        pNorm = 2
        NTrain = 5000
        extraOctaves = 0
        St = scatteringTransform((N, 1, 1), cw = CW, 2, poolBy = poolBy, β = β, averagingLength = averagingLength, outputPool = outputPool, normalize = normalize, σ = abs, p = pNorm, extraOctaves = extraOctaves)
        J1, J2 = map(x -> size(x, 2), getWavelets(St))
        listOfLocations = ([(j2, j1) for j1 = 1:(J1-1), j2 = 1:(J2-1)]...,)
    end
    longName = longNames[ii]
    namedCW = shortNames[ii]
    println("doing $CW")
    β = [2, 2, 1]
    averagingLength = [-1, -1, 2]
    outputPool = 8
    normalize = false
    poolBy = 3 // 2
    NDescents = 100
    descentLength = 1000
    pNorm = 2
    NTrain = 5000
    extraOctaves = 0
    St = scatteringTransform((N, 1, 1), cw = CW, 2, poolBy = poolBy, β = β, averagingLength = averagingLength, outputPool = outputPool, normalize = normalize, σ = abs, p = pNorm, extraOctaves = extraOctaves)
    x = randn(N, 1, 1)
    Sx = St(x)
    targets = Dict(map(x -> (x => Array{Float64,1}(undef, 128)), listOfLocations)...)
    aveLenStr = mapreduce(x -> "$x", *, averagingLength)
    saveDir = "../../results/singleFits/$(namedCW)/poolBy$(round(poolBy, sigdigits=3))_outPool$(outputPool)_pNorm$(pNorm)_aveLen$(aveLenStr)_extraOct$(extraOctaves)/lay2/"
    saveFigDir = "../../figures/interpret/singleFits/$(namedCW)/poolBy$(round(poolBy, sigdigits=3))_outPool$(outputPool)_pNorm$(pNorm)_aveLen$(aveLenStr)_extraOct$(extraOctaves)/lay2/"
    # second layer setup begin
    l = floor(Int, 1 / 2 * size(Sx[2], 1))
    p = pathLocs(2, (l, w2, w1))
    for (w2, w1) in listOfLocations
        # plotSecondLayer(Sx, St)
        extraName = "full"
        aveLenStr = mapreduce(x -> "$x", *, averagingLength)
        locDir = saveDir * "wf$(w1)ws$(w2)/"
        locFigDir = saveFigDir * "wf$(w1)ws$(w2)/"
        saveName = locDir * "data$(extraName).jld2"
        saveNameDict = locDir * "dictionary"
        saveSerialName = locDir * "data"
        tmpDictionary = FileIO.load(saveName)
        scores = tmpDictionary["scores"]
        pop = tmpDictionary["pop"]
        iScores = sortperm(scores)
        bestEx = idct(pop[:, iScores[1]])
        targets[(w2, w1)] = bestEx
    end
    W = getWavelets(St, spaceDomain = true)
    f1, f2, f3 = getMeanFreq(St)
    firstLayerFrequencies = map(f -> round(f, sigdigits = 3), f1)[1:end-1]
    secondLayerFrequencies = map(f -> round(f, sigdigits = 3), f2)[1:end-1]
    targets
    lenFreq2 = length(secondLayerFrequencies)
    lenFreq1 = length(firstLayerFrequencies)
    exWidth = 1 / lenFreq2
    exHeight = 1 / lenFreq1
    pltt = scatter(0, 0, tickdirection = :out, rotation = 30, xlims = (0.5, lenFreq2 + 0.5), xticks = (1:lenFreq2, secondLayerFrequencies), xlabel = "Second layer frequency (Hz)", ylims = (0.5, lenFreq1 + 0.5), yticks = (1:lenFreq1, firstLayerFrequencies), ylabel = "First layer frequency (Hz)", legend = false, tickfontsize = 4, guidefontsize = 7)
    plot!(title = "Best Fit Solutions for $(longNames[ii]) Second Layer", titlefontsize = 12)
    for (plotNumber, key) in enumerate(keys(targets))
        i2, i1 = key
        pltt = plot!(targets[key], inset = (1, bbox((i2 - 1) * exWidth, (i1 - 1) * exHeight, exWidth, exHeight, :bottom, :left)), legend = false, framestyle = :box, subplot = plotNumber + 1, ticks = false)
    end
    pltt
    savefig(saveFigDir * "multiBestPlot.pdf")
    savefig(saveFigDir * "multiBestPlot.png")

    w2 = 1
    w1 = J1 - 1
    plot(range(1, stop = N, length = size(W[2], 1)), norm(targets[(w2, w1)], Inf) / norm(W[2][:, w2], Inf) * circshift(W[2][:, w2], round(Int, range(1, stop = size(W[2], 1), length = size(Sx[2], 1))[l])), label = "Second Layer Wavelet")
    plot!(targets[(w2, w1)], label = "Result")
    title!("$((secondLayerFrequencies[w2], firstLayerFrequencies[w1]))")


    pltt = scatter(0, 0, tickdirection = :out, rotation = 30, xlims = (0.5, lenFreq2 + 0.5), xticks = (1:lenFreq2, secondLayerFrequencies), xlabel = "Second layer frequency (Hz)", ylims = (0.5, lenFreq1 + 0.5), yticks = (1:lenFreq1, firstLayerFrequencies), ylabel = "First layer frequency (Hz)", legend = false,)
    plot!(title = "Scalograms of Best Fit Solutions for $(longNames[ii]) Second Layer", titlefontsize = 12)
    for (plotNumber, key) in enumerate(keys(targets))
        i2, i1 = key
        pltt = heatmap!(abs.(circshift(St.mainChain[1](targets[key])[:, 1:(end-1), 1, 1]', (0, 0))) .^ 2, c = cgrad(:viridis, scale = :exp), colorbar = false, inset = (1, bbox((i2 - 1) * exWidth + 0.001, (i1 - 1) * exHeight + 0.001, exWidth, exHeight, :bottom, :left)), legend = false, subplot = plotNumber + 1, ticks = false, framestyle = :none)
    end
    pltt
    savefig(saveFigDir * "multiBestScalogramPlot.pdf")
    savefig(saveFigDir * "multiBestScalogramPlot.png")
    # run(`convert -density 100 $(saveFigDir)multiBestScalogramPlot.pdf -quality 70 $(saveFigDir)multiBestScalogramPlot.png`)
end


locationsAndWavelets = ((((1:16)...,), dog2),
    (((1:17)...,), dog1),
    (((1:16)...,), Morlet(π)),
    (((1:14)...,), Morlet(2π)),)
shortNames = ["dog2", "dog1", "morl", "morl2Pi"] # set the names for saving
# listOfLocations, CW = locationsAndWavelets[3]
# ii =  1
for ii = 1:length(locationsAndWavelets)
    println(ii)
    (listOfLocations, CW) = locationsAndWavelets[ii]
    w1 = listOfLocations[1]
    namedCW = shortNames[ii]
    β = [2, 2, 1]
    averagingLength = [-1, -1, 2]
    outputPool = 8
    normalize = false
    poolBy = 3 // 2
    NDescents = 100
    descentLength = 1000
    pNorm = 2
    NTrain = 5000
    extraOctaves = 0
    St = scatteringTransform((N, 1, 1), cw = CW, 2, poolBy = poolBy, β = β, averagingLength = averagingLength, outputPool = outputPool, normalize = normalize, σ = abs, p = pNorm, extraOctaves = extraOctaves)
    x = randn(N, 1, 1)
    Sx = St(x)
    aveLenStr = mapreduce(x -> "$x", *, averagingLength)
    saveDir = "../../results/singleFits/$(namedCW)/poolBy$(round(poolBy, sigdigits=3))_outPool$(outputPool)_pNorm$(pNorm)_aveLen$(aveLenStr)_extraOct$(extraOctaves)/lay1/"
    saveFigDir = "../../figures/interpret/singleFits/$(namedCW)/poolBy$(round(poolBy, sigdigits=3))_outPool$(outputPool)_pNorm$(pNorm)_aveLen$(aveLenStr)_extraOct$(extraOctaves)/lay1/"
    extraName = "dctOnly"
    l = ceil(Int, 1 / 2 * size(Sx[1], 1))
    solutions = zeros(128, length(listOfLocations))
    W = getWavelets(St)
    f1, f2, f3 = getMeanFreq(St)
    firstLayerFrequencies = map(f -> round(f, sigdigits = 3), f1)[1:end-1]
    secondLayerFrequencies = map(f -> round(f, sigdigits = 3), f2)[1:end-1]
    for w1 in listOfLocations
        # p = pathLocs(1, (l, w1))
        # w1 = 7
        locDir = saveDir * "wf$(w1)ws/"
        locFigDir = saveFigDir * "wf$(w1)ws/"
        saveName = locDir * "data$(extraName).jld2"
        tmpDictionary = FileIO.load(saveName)
        scores = tmpDictionary["scores"]
        pop = idct(tmpDictionary["pop"], 1)
        iScores = sortperm(scores)
        bestEx = pop[:, iScores[1]]
        solutions[:, w1] = bestEx
        heatmap(1:128, f1[1:end-1], abs.(circshift(St.mainChain[1](bestEx)[:, 1:(end-1), 1, 1]', (0, 0))) .^ 2, c = cgrad(:viridis, scale = :exp), framestyle = :box, title = "Scalogram of fit for $(longNames[ii]) at $(firstLayerFrequencies[w1]) Hz", ylabel = "Frequency (Hz)", xlabel = "time (ms)")
        savefig(joinpath(locFigDir, "solutionScalogram.pdf"))
        run(`convert -density 100 $(locFigDir)solutionScalogram.pdf -quality 70 $(locFigDir)solutionScalogram.png`)
    end
    heatmap(1:128, firstLayerFrequencies[1:size(solutions, 2)], solutions', title = "Fitting First Layer, $(longNames[ii])", c = :viridis, xlabel = "Time (ms)", ylabel = "Frequency (Hz)")
    savefig(joinpath(saveFigDir, "multiBestPlotHeatmap.pdf"))
    run(`convert -density 100 $(saveFigDir)multiBestPlotHeatmap.pdf -quality 70 $(saveFigDir).png`)


    # plotting the best solutions compared with the original wavelets
    validEntries = range(1, stop = 128, length = size(Sx[1], 1))
    wavShift = floor(Int, validEntries[l])
    wavs = circshift(originalDomain(St.mainChain[1]), (wavShift, 0))[1:128, :]
    wavs = real.(wavs)
    tt = [(0:127) .- wavShift for x in eachslice(wavs, dims = 2)]

    # check whether or not solutions needs to be flipped to match the sign of the original wavelets
    signs = [(sign(solutions[argmax(abs.(wavs[:, jj])), jj]) * sign(real.(wavs)[argmax(abs.(wavs[:, jj])), jj])) for jj = 1:size(solutions, 2)]
    signedSols = signs' .* real.(solutions)
    renormedWavs = cat([real.(wavs)[:, jj] * norm(solutions[:, jj]) / norm(wavs[:, jj]) for jj = 1:size(solutions, 2)]..., dims = 2)
    ytickSizes = [(range(min(minimum(signedSols[:, jj]), minimum(real.(renormedWavs)[:, jj])), max(maximum(signedSols[:, jj]), maximum(real.(renormedWavs)[:, jj])), length = 5), "") for jj = 1:size(solutions, 2)]
    kurtFirstLayFreq = round.(Int, firstLayerFrequencies)

    sharedArgs = (gridalpha = 0.3, frame = :box, linewidth = 1.5, guidefontrotation = -0, left_margin = -10Plots.px, guidefontvalign = :center, yguidefontsize = 5, guidefonthalign = :center, tickfontsize = 5,)
    # first plot needs more vertical space
    plt1 = plot(tt[end-1], [renormedWavs[:, end] signedSols[:, end]]; xticks = (range(tt[end-1][1], tt[end-1][end], length = 7), ""), bottom_margin = -12Plots.px, top_margin = 0Plots.px, yticks = ytickSizes[end], ylabel = "$(kurtFirstLayFreq[end])", xlims = (tt[end-1][1], tt[end-1][end]), legend = false, sharedArgs...)
    # last plot needs horizontal ticks
    pltLast = plot(tt[1], [renormedWavs[:, 1] signedSols[:, 1]]; xticks = (range(tt[1][1], tt[1][end], length = 7), round.(Int, range(1, 128, length = 7))), top_margin = -12Plots.px, yticks = ytickSizes[1], ylabel = "$(kurtFirstLayFreq[1])", xlabel = "Time (ms)", xlims = (tt[1][1], tt[1][end]), legend = false, sharedArgs...)
    # every other plot
    plts = [plot(tt[jj], [renormedWavs[:, jj] signedSols[:, jj]]; xticks = (range(tt[end][1], tt[end][end], length = 7), ""), legend = false, bottom_margin = -12Plots.px, top_margin = -12Plots.px, frame = :box, yticks = ytickSizes[jj], ylabel = "$(kurtFirstLayFreq[jj])", xlims = (tt[jj][1], tt[jj][end]), sharedArgs...) for jj = (size(solutions, 2)-1):-1:2]
    l = @layout grid(1, 16)
    title = "Fitting First Layer, $(longNames[ii])"
    yLabel = "Frequency (Hz)"
    gridSizes = (size(solutions, 2), 1)
    setOfPlots = [plt1, plts..., pltLast]
    overTitle = plot(randn(1, 2), xlims = (1, 1.1), xshowaxis = false, yshowaxis = false, label = "", colorbar = false, grid = false, framestyle = :none, title = title, top_margin = -25Plots.px, bottom_margin = -13Plots.px, legend = :outertopright, labels = ["original wavelet" "coordinate maximizer"], legendfontsize = 4,)
    fauxYLabel = Plots.scatter([0, 0], [0, 1], xlims = (1, 1.1), xshowaxis = false, label = "", colorbar = false, grid = false, ylabel = yLabel, xticks = false, yticks = false, framestyle = :grid, left_margin = 12Plots.px, right_margin = -12Plots.px)
    lay = @layout [a{0.01h}; [b{0.00001w} grid(gridSizes...)]]
    plot(overTitle, fauxYLabel, setOfPlots..., layout = lay)
    savefig(joinpath(saveFigDir, "multiBestPlot.pdf"))


    numPlts = size(signedSols, 2)
    height = round(Int, sqrt(numPlts))
    width = ceil(Int, numPlts / height)
    tmpPlt = heatmap(1:128, f1[1:end-1], abs.(circshift(St.mainChain[1](signedSols[:, 1])[:, 1:(end-1), 1, 1]', (0, 0))) .^ 2, c = cgrad(:viridis, scale = :exp), framestyle = :box, title = "$(firstLayerFrequencies[1]) Hz", titlefontsize = 7, guidefontsize = 5, tickfontsize = 4, colorbar = false, link = :all)
    listOfPlots = Array{typeof(tmpPlt),1}(undef, size(solutions, 2))
    for w1 = 1:size(solutions, 2)
        if w1 % width == 1
            if w1 >= (height - 1) * width + 1
                # on the left edge and the bottom
                extraArgs = (ylabel = "Frequency (Hz)", xlabel = "time (ms)")
            else
                # on the left edge but not the bottom
                extraArgs = (ylabel = "Frequency (Hz)", xticks = false)
            end
        else
            if w1 >= (height - 1) * width + 1
                # on the bottom and not the left edge
                extraArgs = (xlabel = "time (ms)", yticks = false)
            else
                # in the middle
                extraArgs = (ticks = false,)
            end
        end
        listOfPlots[w1] = heatmap(1:128, f1[1:end-1], abs.(circshift(St.mainChain[1](signedSols[:, w1])[:, 1:(end-1), 1, 1]', (0, 0))) .^ 2; c = cgrad(:viridis, scale = :exp), framestyle = :box, title = "$(firstLayerFrequencies[w1]) Hz", titlefontsize = 5, guidefontsize = 5, tickfontsize = 4, colorbar = false, link = :all, bottom_margin = -4Plots.px, extraArgs...)
    end
    plt = plot(listOfPlots..., plot_title = "Scalogram of fit for $(longNames[ii])", link = :all)
    title = "Scalograms of First Layer Fits, $(longNames[ii])"
    overTitle = Plots.scatter([0, 0], [0, 1], xlims = (1, 1.1), xshowaxis = false, yshowaxis = false, label = "", colorbar = false, grid = false, framestyle = :none, title = title, bottom_margin = -30Plots.px)
    l = @layout [a{0.0001h}; b]
    plot(overTitle, plt, layout = l)
    savefig(joinpath(saveFigDir, "allScalograms.pdf"))
    savefig(joinpath(saveFigDir, "allScalograms.png"))
end




# zeroth layer joint plots
locationsAndWavelets = ((((1:16)...,), dog2),
    (((1:17)...,), dog1),
    (((1:16)...,), Morlet(π)),
    (((1:14)...,), Morlet(2π)),)
shortNames = ["dog2", "dog1", "morl", "morl2Pi"] # set the names for saving
# listOfLocations, CW = locationsAndWavelets[1]
# w1 = listOfLocations[1]
ii = 2
solutions = zeros(128, length(locationsAndWavelets))
wavs = zeros(128, length(locationsAndWavelets))
exactOutputLocations = (range(1, stop = N, length = 16)...,)
wavShift = floor(Int, exactOutputLocations[8])
for ii = 1:length(locationsAndWavelets)
    (listOfLocations, CW) = locationsAndWavelets[ii]
    namedCW = shortNames[ii]
    β = [2, 2, 1]
    averagingLength = [-1, -1, 2]
    outputPool = 8
    normalize = false
    poolBy = 3 // 2
    NDescents = 100
    descentLength = 1000
    pNorm = 2
    NTrain = 5000
    extraOctaves = 0
    St = scatteringTransform((N, 1, 1), cw = CW, 2, poolBy = poolBy, β = β, averagingLength = averagingLength, outputPool = outputPool, normalize = normalize, σ = abs, p = pNorm, extraOctaves = extraOctaves)
    x = randn(N, 1, 1)
    Sx = St(x)
    extraName = ""
    l = ceil(Int, 1 / 2 * size(Sx[0], 1))
    p = pathLocs(0, (l,))
    aveLenStr = mapreduce(x -> "$x", *, averagingLength)
    saveDir = "../../results/singleFits/$(namedCW)/poolBy$(round(poolBy, sigdigits=3))_outPool$(outputPool)_pNorm$(pNorm)_aveLen$(aveLenStr)_extraOct$(extraOctaves)/lay0/"
    saveFigDir = "../../figures/interpret/singleFits/$(namedCW)/poolBy$(round(poolBy, sigdigits=3))_outPool$(outputPool)_pNorm$(pNorm)_aveLen$(aveLenStr)_extraOct$(extraOctaves)/lay0/"
    locDir = saveDir * "l$(l)/"
    locFigDir = saveFigDir * "l$(l)/"
    saveName = locDir * "data$(extraName).jld2"
    saveNameDict = locDir * "dictionary"
    saveSerialName = locDir * "data"
    tmpDictionary = FileIO.load(saveName)
    scores = tmpDictionary["scores"]
    pop = tmpDictionary["pop"]
    iScores = sortperm(scores)
    bestEx = pop[:, iScores[1]]
    solutions[:, ii] = bestEx
    wavs[:, ii] = real.(circshift(originalDomain(St.mainChain[1]), (wavShift, 0))[1:128, end])
end
renormedWavs = cat([real.(wavs)[:, jj] * norm(solutions[:, jj]) / norm(wavs[:, jj]) for jj = 1:size(solutions, 2)]..., dims = 2)
renormedWavs
argmax(solutions, dims = 1)
peakDiff = argmax(renormedWavs, dims = 1) - argmax(solutions, dims = 1)
shiftingObj = [objectiveDCT(dct(circshift(renormedWavs[:, 1], (j,)))) for j = 0:128]
argmin(shiftingObj)
objectiveDCT(dct(solutions[:, 1]))
println("the distance from the peak is $(peakDiff)")
tt = [(0:127) .- wavShift for x in eachslice(wavs, dims = 2)]
ytickSizes = [(range(min(minimum(solutions[:, jj]), minimum(real.(renormedWavs)[:, jj])), max(maximum(solutions[:, jj]), maximum(real.(renormedWavs)[:, jj])), length = 5), "") for jj = 1:size(solutions, 2)]
sharedArgs = (gridalpha = 0.3, frame = :box, linewidth = 1.5, guidefontrotation = -0, left_margin = -10Plots.px, guidefontvalign = :center, yguidefontsize = 7, guidefonthalign = :center, tickfontsize = 5,)
# first plot needs more vertical space
plt1 = plot(tt[end-1], [renormedWavs[:, end] solutions[:, end]]; xticks = (range(tt[end-1][1], tt[end-1][end], length = 7), ""), bottom_margin = -12Plots.px, top_margin = 0Plots.px, yticks = ytickSizes[end], ylabel = "$(longNames[end])", xlims = (tt[end-1][1], tt[end-1][end]), legend = false, sharedArgs...)
# last plot needs horizontal ticks
pltLast = plot(tt[1], [renormedWavs[:, 1] solutions[:, 1]]; xticks = (range(tt[1][1], tt[1][end], length = 7), round.(Int, range(1, 128, length = 7))), top_margin = -12Plots.px, yticks = ytickSizes[1], ylabel = "$(longNames[1])", xlabel = "Time (ms)", xlims = (tt[1][1], tt[1][end]), legend = false, sharedArgs...)
# every other plot
plts = [plot(tt[jj], [renormedWavs[:, jj] solutions[:, jj]]; xticks = (range(tt[end][1], tt[end][end], length = 7), ""), legend = false, bottom_margin = -12Plots.px, top_margin = -12Plots.px, frame = :box, yticks = ytickSizes[jj], ylabel = "$(longNames[jj])", xlims = (tt[jj][1], tt[jj][end]), sharedArgs...) for jj = (size(solutions, 2)-1):-1:2]
title = "Fitting Zeroth Layer"
yLabel = "Frequency (Hz)"
gridSizes = (size(solutions, 2), 1)
setOfPlots = [plt1, plts..., pltLast]
overTitle = plot(randn(1, 2), xlims = (1, 1.1), xshowaxis = false, yshowaxis = false, label = "", colorbar = false, grid = false, framestyle = :none, title = title, top_margin = -25Plots.px, bottom_margin = -13Plots.px, legend = :outertopright, labels = ["original wavelet" "coordinate maximizer"], legendfontsize = 4,)
fauxYLabel = Plots.scatter([0, 0], [0, 1], xlims = (1, 1.1), xshowaxis = false, label = "", colorbar = false, grid = false, ylabel = "Wavelet type", xticks = false, yticks = false, framestyle = :grid, left_margin = 12Plots.px, right_margin = -12Plots.px)
lay = @layout [a{0.01h}; [b{0.00001w} grid(gridSizes...)]]
plot(overTitle, fauxYLabel, setOfPlots..., layout = lay)
savefig(joinpath("../../figures/interpret/singleFits/", "zerothMultiBestPlot.pdf"))
savefig(joinpath("../../figures/interpret/singleFits/", "zerothMultiBestPlot.png"))
