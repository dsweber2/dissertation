# from fittingSecondLayer in the examples folder of ScatteringTransformExperiments
using Revise
using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, LinearAlgebra
using FFTW, FourierFilterFlux
using Optim, LineSearches, GLMNet
using Plots
using CUDA, Zygote, Flux
using LinearAlgebra
using BlackBoxOptim
using BlackBoxOptim: num_func_evals
using ScatteringInterpretation
NDims = 128
N = 100
popSize = 500
nMinsBBO = 3
perturbRate = 1 / 5
totalMins = 1 * 60
homeDir = "../../results/bellCylinderFunnel/"
function chooseBestLambda(cv)
    effectiveRegion = (cv.meanloss .- cv.stdloss) .< minimum(cv.meanloss)
    λBest = maximum(cv.path.lambda[BitVector([effectiveRegion..., zeros(Bool, length(cv.path.lambda) - length(cv.meanloss))...])])
    findfirst(x -> x == λBest, cv.path.lambda)
end
St = scatteringTransform((NDims, 1, 1), cw=dog2, 2, Q=4, β=[1,1,1], extraOctaves=(0, 0, 0), averagingLength=[-1.5,-1.5,2], outputPool=2 * 4, convBoundary=FourierFilterFlux.Periodic(), boundary=PerBoundary())
specName = "stLay2_dog2" * "$N"
cvpSt = FileIO.load(joinpath(homeDir, specName * ".jld2"), "cvp")
cvpStRot = FileIO.load(joinpath(homeDir, specName * "Rot.jld2"), "cvpRot")
βSt2 = cvpSt.path.betas[:,:,chooseBestLambda(cvpSt)]
βSt2Rot = cvpStRot.path.betas[:,:,chooseBestLambda(cvpStRot)]

St12 = scatteringTransform((NDims, 1, 1), cw=[dog1, dog2, dog2], 2, Q=4, β=[1,1,1], extraOctaves=(-0, -0, 0), averagingLength=[-1.5,-1.5,2], outputPool=4 * 4, convBoundary=FourierFilterFlux.Periodic(), boundary=PerBoundary())
specName12 = "stLay2_dog2_dog1" * "$N"
cvpSt_dog12 = FileIO.load(joinpath(homeDir, specName12 * ".jld2"), "cvp")
cvpSt_dog12Rot = FileIO.load(joinpath(homeDir, specName12 * "Rot.jld2"), "cvpRot")
βSt12 = cvpSt_dog12.path.betas[:,:,chooseBestLambda(cvpSt_dog12)]
βSt12Rot = cvpSt_dog12Rot.path.betas[:,:,chooseBestLambda(cvpSt_dog12Rot)]


St1 = scatteringTransform((NDims, 1, 1), cw=dog1, 2, Q=4, β=[1,1,1], extraOctaves=(-0, -0, 0), averagingLength=[-1.5,-1.5,2], outputPool=4 * 4, convBoundary=FourierFilterFlux.Periodic(), boundary=PerBoundary())
specName1 = "stLay2_dog1" * "$N"
cvpSt_dog1 = FileIO.load(joinpath(homeDir, specName1 * ".jld2"), "cvp")
cvpSt_dog1Rot = FileIO.load(joinpath(homeDir, specName1 * "Rot.jld2"), "cvpRot")
βSt1 = cvpSt_dog1.path.betas[:,:,chooseBestLambda(cvpSt_dog1)]
βSt1Rot = cvpSt_dog1Rot.path.betas[:,:,chooseBestLambda(cvpSt_dog1Rot)]

listOfβs = ((βSt2[:,1], "Cyldog2", "Cylinder using 2nd Dog second layer only", "", St), (βSt2[:,2], "Belldog2", "Bell using 2nd Dog second layer only", "", St), (βSt2[:,3], "fundog2", "Funnel using 2nd Dog second layer only", "", St), (βSt2Rot[:,1], "Cyldog2", "Cylinder using 2nd Dog second layer only", "rotated", St), (βSt2Rot[:,2], "Belldog2", "Bell using 2nd Dog second layer only", "rotated", St), (βSt2Rot[:,3], "fundog2", "Funnel using 2nd Dog second layer only", "rotated", St),
            (βSt12[:,1], "Cyldog12", "Cylinder using 1st DoG then 2nd", "", St12), (βSt12[:,2], "Belldog12", "Bell using 1st DoG then 2nd", "", St12), (βSt12[:,3], "fundog12", "Funnel using 1st DoG then 2nd", "", St12), (βSt12Rot[:,1], "Cyldog12", "Cylinder using 1st DoG then 2nd", "rotated", St12), (βSt12Rot[:,2], "Belldog12", "Bell using 1st DoG then 2nd", "rotated", St12), (βSt12Rot[:,3], "fundog12", "Funnel using 1st DoG then 2nd", "rotated", St12),
            (βSt1[:,1], "Cyldog1", "Cylinder using 1st DoG", "", St1), (βSt1[:,2], "Belldog1", "Bell using 1st DoG", "", St1), (βSt1[:,3], "fundog1", "Funnel using 1st DoG", "", St1), (βSt1Rot[:,1], "Cyldog1", "Cylinder using 1st DoG", "rotated", St1), (βSt1Rot[:,2], "Belldog1", "Bell using 1st DoG", "rotated", St1), (βSt1Rot[:,3], "fundog1", "Funnel using 1st DoG", "rotated", St1))
# β, shortName, longName, rotated, StThis = listOfβs[1]
# to compare with the dog2, we need the center frequency to be at 144 and 24.7
# second layer setup begin
for (β, shortName, longName, rotated, StThis) in listOfβs
    println("-----------------------------------------------------------------------------------------")
    println("-----------------------------------------------------------------------------------------")
    println("------------------------------- starting $(longName) -------------------------------------")
    println("-----------------------------------------------------------------------------------------")
    println("-----------------------------------------------------------------------------------------")
    saveDir = joinpath(homeDir, "fitting")
    if !isdir(saveDir)
        mkpath(saveDir)
    end
    locDir = joinpath(saveDir, shortName * rotated)
    extraName = ""
    # if it already exists, append the appropriate count so we get multiple copies
    if isfile(locDir * "data.jld2")
        nRepeat = 1
        while isfile(locDir * "data$(nRepeat).jld2")
            nRepeat += 1
        end
        extraName = "$(nRepeat)"
    end
    saveName = locDir * "data$(extraName).jld2"
    saveNameDict = locDir * "dictionary"
    saveSerialName = locDir * "data$(extraName)"

    if !isdir(locDir)
        mkpath(locDir)
    end
    # second layer setup end

    function objDCT(x̂)
        x = idct(x̂, 1)
        t = StThis(reshape(x, (NDims, 1, 1)))
        1.1f0^(-sum(reshape(t[2], (:,)) .* β) + 1f-5 * norm(x)^2)
    end

    setProbW = bbsetup(objDCT; SearchRange=(-1000., 1000.), NumDimensions=NDims, PopulationSize=popSize, MaxTime=10.0, TraceInterval=5, TraceMode=:silent)
    println("pretraining")
    bboptimize(setProbW)

    pop = dct(pinkStart(NDims, -1, popSize), 1)
    adjustPopulation!(setProbW, pop)

    nIters = ceil(Int, totalMins / nMinsBBO)
    for ii in 1:nIters
        println("----------------------------------------------------------")
        println("round $ii: GO")
        println("----------------------------------------------------------")
        bboptimize(setProbW, MaxTime=nMinsBBO * 60.0) # take some black box steps for the whole pop
        # for jj in 1:NDescents
        #     println("on run $jj of gradient descent")
        #     pathOfDescent, err = ScatteringTransformExperiments.pullAndTrain(setProbW, objSPC; normDiscount=1e-1, N=1000)
        # end
        pop = setProbW.runcontrollers[end].optimizer.population.individuals
        scores = [objDCT(x) for x in eachslice(pop, dims=2)]
        iScores = sortperm(scores)

        relScoreDiff = abs(log(scores[iScores[1]]) - log(scores[iScores[round(Int, 4 * popSize / 5 - 1)]])) / abs(log(scores[iScores[1]]))
        if ii > 3 && relScoreDiff .< 0.01 # the relative difference between the best and the worst signals is less than 1%
            println("finished after $(ii) rounds")
            break
        end

        # perturb a fifth to sample the full space if it hasn't converged yet
        perturbWorst(setProbW, scores, perturbRate)
    end
    # NDescents = 30
    pop = setProbW.runcontrollers[end].optimizer.population.individuals
    scores = [objDCT(x) for x in eachslice(pop, dims=2)]

    saveSerial(saveSerialName, setProbW)
    save(saveName, "pop", pop, "scores", scores)

    histogram(log.(scores) / log(1.1), legend=false, xlabel="coordinate value + norm of input", title="Histogram of log scores\n$(longName) $(rotated)")
    savefig(joinpath(locDir, "histogramPop$(extraName).pdf"))

    # examples
    iScores = sortperm(scores)
    plotFirstXEx(idct(pop[:,iScores], 1), scores[iScores], "Fit solutions: $(longName) $(rotated)")
    savefig(joinpath(locDir, "exampleResults$(extraName).pdf"))
    plot(idct(pop[:,iScores[1]]), title="Best Example log-score: $(round(log(objDCT(pop[:,iScores[1]])), sigdigits=3))\n$(longName) $(rotated)", c=:black, legend=false)
    savefig(joinpath(locDir, "bestExample$(extraName).pdf"))
    plot(abs.(rfft(idct(pop[:,iScores[1]]))), title="Fourier Domain,\n $(longName) $(rotated)", c=:black, legend=false)
    savefig(joinpath(locDir, "rfftBestExample$(extraName).pdf"))
    plot(pop[:,iScores[1]], title="dct best example,\n $(longName) $(rotated)", c=:black, legend=false)
    savefig(joinpath(locDir, "dctBestExample$(extraName).pdf"))


    # plots of the first layer
    f1, f2, f3 = getMeanFreq(StThis)
    firstLayerMats = cat([StThis(reshape(idct(pop[:,iScores][:,ii], 1), (NDims, 1, 1)))[1][:,:,1]' for ii = 1:20]..., dims=3)
    colorMax = (minimum(firstLayerMats), maximum(firstLayerMats))
    p = plotOverTitle([heatmap(x, clims=colorMax, colorbar=false, c=cgrad(:viridis, scale=:log10), yticks=(1:3:size(x, 1), round.(f1[1:3:end - 1], sigdigits=3)), xticks=false) for x in eachslice(firstLayerMats, dims=3)], "First Layer results for the best example\n$(longName) $(rotated)", (4, 5))
    savefig(joinpath(locDir, "ArrayFirstLayerOutput$(extraName).pdf"))
    heatmap(firstLayerMats[:,:,1], clims=colorMax, colorbar=false, c=cgrad(:viridis, scale=:log10), xlabel="space", ylabel="frequency", title="First Layer results\n$(longName) $(rotated)", yticks=(1:1:size(firstLayerMats, 1), round.(f1[1:1:end - 1], sigdigits=3)))
    savefig(joinpath(locDir, "bestFirstLayerOutput$(extraName).pdf"))
    heatmap(abs.(StThis.mainChain[1](idct(pop[:,iScores[1]]))[:,1:end - 1,1,1]'), title="internal first layer\n$(longName) $(rotated)", yticks=(1:1:size(firstLayerMats, 1), round.(f1[1:1:end - 1], sigdigits=3)))
    savefig(joinpath(locDir, "ArrayInternalFirst$(extraName).pdf"))
    # second layer plot
    plotSecondLayer(StThis(reshape(idct(pop[:, iScores[1]]), (NDims, 1, 1))), StThis; title="SecondLayer: $(longName) $(rotated)",logPower=false,c=cgrad(:viridis, [0 .9]))
    savefig(joinpath(locDir, "SecondLayerOutput$(extraName).pdf"))
end
