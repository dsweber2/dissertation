addprocs(5)
using MNIST
# make an upsampled version of MNIST
using JLD
try
    trainingDataTot, trainingClassesTot = traindata()
    nTot = length(trainingClassesTot)
    trainingDataTot = reshape(trainingDataTot, (28, 28, 60000))

    @everywhere using shatteringTransform
    @everywhere using Images
# using Plots
# using LIBSVM

    trainingDataTot, trainingClassesTot = traindata()
    nTot = length(trainingClassesTot)
    trainingDataTot = reshape(trainingDataTot, (28, 28, :))
# let's *not* use the gpu, since construction is trivial at this stage
    @everywhere layers = layeredTransform(2, 200, 200, [4, 2, 2], [1.25, 2, 2], gpu=0)
# wef = svmtrain(trainingDataTot[1:30,1:3000],trainingClasses[1:3000],kernel=Kernel.Polynomial)
    @time trainDataTmp = shatter(imresize(trainingDataTot[:,:,1], (200, 200)), layers, absType(), thin=true)
# COLUMN-MAJOR
    dimension = length(trainDataTmp)
    typeBase = typeof(trainDataTmp[1])
# dimension = 10; nTot= 1000; typeBase = Float64
    s = open("/fasterHome/workingDataDir/shattering/shatteredMNISTabs200x200.bin", "w+")
# We'll write the dimensions of the array as the first two Ints in the file
    write(s, dimension)
    write(s, nTot)
    shatteredTraining = SharedArray{typeBase,2}(dimension, nprocs() * 10)
    for i = 1:Int(nTot / nprocs() / 10)
        println("on run $i out of $(Int(nTot / nprocs() / 10))")
        @sync @parallel for j = 1:nprocs() * 10
            shatteredTraining[:, j] = shatter(imresize(trainingDataTot[:, :, (i - 1) * nprocs() * 10 + j], (200, 200)), layers, absType(), thin=true)
            if i == 1
                println("made it through for $j")
            end
        # shatteredTraining[:,j] = A[:,(i-1)*nprocs()*10+j]
        end
        write(s, shatteredTraining)
    end
    close(s)


    testingDataTot, testingClassesTot = testdata()
    nTot = length(testingClassesTot)
    testingDataTot = reshape(testingDataTot, (28, 28, :))
# dimension = 10; nTot= 1000; typeBase = Float64
    s = open("/fasterHome/workingDataDir/shattering/testSets/shatteredMNISTabs200x200Test.bin", "w+")
# We'll write the dimensions of the array as the first two Ints in the file
    write(s, dimension)
    write(s, nTot)
    shatteredTraining = SharedArray{typeBase,2}(dimension, nprocs() * 10)
    for i = 1:Int(nTot / nprocs() / 10)
        println("on run $i out of $(Int(nTot / nprocs() / 10))")
        @sync @parallel for j = 1:nprocs() * 10
            shatteredTraining[:, j] = shatter(imresize(testingDataTot[:, :, (i - 1) * nprocs() * 10 + j], (200, 200)), layers, absType(), thin=true)
        # shatteredTraining[:,j] = A[:,(i-1)*nprocs()*10+j]
        end
        write(s, shatteredTraining)
    end
    save("shatteredMNISTabs200x200.jld",layers)
    close(s)
catch e
    yo()
    throw(e)
end
yo()
