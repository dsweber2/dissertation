using Revise, Plots
using ScatteringTransform, FileIO, Images, Interpolations, Random
imageNetLocation = "/Elysium/dsweber/data/slim/ImageNet/raw-data"
listOfDirs = [(root, dirs, filenames) for (root, dirs, filenames) in walkdir(joinpath(imageNetLocation, "train"))];
allTheFiles = [[joinpath(listOfDirs[i][1], listOfDirs[i][3][j]) for j in 1:length(listOfDirs[i][3])] for i in 2:length(listOfDirs)]
# the category labels are from https://gist.github.com/yrevar/942d3a0ac09ec9e5eb3a note that this is zero indexed.
labelFile = open(joinpath(imageNetLocation, "labels.txt"))
labels = map(newline -> split(newline, "'")[2], readlines(labelFile)) # first is number, 3rd is a comma
close(labelFile)
Random.seed!(3425)
labs = rand(labels, 10)
ii = 30; tmp = load(allTheFiles[ii][7]); heatmap(tmp, title="Example of $(labels[ii])")

# ways to deal with uneven sizes:
# whitespace fill (potental variable to pick up on)  doing this one
# trim images (potentially drop the subject of interest)
# resample so they're the same (skew shapes)
function toFloat32(x)
    x = channelview(x)
    Float32.(permutedims(x, (2, 3, 1)))
end
function adjustSizesTo(x;n=500)
    itp = interpolate(x, BSpline(Linear()))
    xS, yS, _ = size(itp)
    newResult = zeros(eltype(itp), n, n, 3)
    if xS > yS
        newSize = round(Int, n * yS / xS)
        offset = round(Int, (n - newSize) / 2)
        newResult[:, offset .+ (1:newSize), :] = itp[range(1, xS, length=n), range(1, yS, length=newSize), 1:3]
    else
        newSize = round(Int, n * xS / yS)
        offset = round(Int, (n - newSize) / 2)
        newResult[offset .+ (1:newSize), :, :] = itp[range(1, xS, length=newSize), range(1, yS, length=n), 1:3]
    end
    return newResult
end
function adjustForClassification(x;n=500)
    xp = toFloat32(x)
    xp = adjustSizesTo(xp; n=500)
end
heatmap(reverse(adjustForClassification(load(allTheFiles[ii][1234])), dims=1)[:,:,1])

tmpEx = adjustForClassification(load(allTheFiles[ii][1234]))
@time St = scatteringTransform((500, 500, 3, 1), 2, scale=4, poolBy=4, outputPool=16)# |> cu
@time res = St(tmpEx)

StG = scatteringTransform((500, 500, 3, 1), 2, scale=4, poolBy=4, outputPool=16) |> cu
@time res = StG(gpu(tmpEx))
