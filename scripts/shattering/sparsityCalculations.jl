# this is calculating something they never actually tested. The things they did test are somewhat outside of reach
using Revise
using Plots, LinearAlgebra
using ScatteringTransform
using Shearlab, FFTW
using DataFrames, Plots, Statistics, Flux, GLM
using Distributed, SharedArrays, Distributions
using cartoonLike
# MNIST examples
using MLDatasets, FileIO
train_x, train_y = MNIST.traindata()
imageNetLocation = "/Elysium/dsweber/data/slim/ImageNet/raw-data/train"
folderName = "n01514668" # roosters
folderName = "n03457902" # roofs?
listOfDirs = [(root, dirs, filenames) for (root, dirs, filenames) in walkdir(imageNetLocation)];
allTheNames = [[joinpath(listOfDirs[i][1], listOfDirs[i][3][j]) for j in 1:length(listOfDirs[i][3])] for i in 2:length(listOfDirs)]
length.(allTheNames)

isfile(joinpath(imageNetLocation, folderName, "$(folderName)_10688.JPEG"))
# 1 caught fish
# 2 goldfish?
# 3 great white sharks
# 4
# 30 Axolotl
# 31 Toad
# 100 canadian geese
tmp = load(allTheNames[102][10]); heatmap(tmp)
abs.(tmp[1])
heatmap(reverse(abs.(tmp), dims=1))
ideal(n) = (log.(1 .+ (1:n)) ./ (1:n)).^(3 / 2)
reorderCoeff(A) = sort(abs.(A), dims=1, rev=true) # sort from smallest to largest
"""
    (coeffs, stderrs) = fitPolyDecay(A)
`A` is a dim×nEx matrix. tests log log (linear decay) fit and log (exponential) fit
"""
function fitPolyDecay(A; discardRate=1,initOffset=1000)
    coeffs = zeros(size(A, 2), 2)
    stderrs = zeros(size(coeffs)...)
    linBetter = falses(size(A, 2))
    residual = zeros(size(A, 2))
    lastCoord = zeros(Int64, size(A, 2))
    for i = 1:size(A, 2)
        minFitInd = findfirst(map(x -> x / A[1,i][1] < 1e-16, A[:,i]))
        if minFitInd == nothing
            minFitInd = size(A, 1)
        end
        AnZ = Float64.(A[1:minFitInd,i]) # the non-zero entries
        lastCoord[i] = lastPoint
        df = DataFrame(X=1:lastPoint, logX=log.(1:minFitInd), Y=log.(AnZ));
        olsLinear = lm(@formula(Y ~ logX), df)# ; wts=collect((1:lastPoint).^(-1)))
        olsExp = lm(@formula(Y ~ X), df)# ; wts=collect((1:lastPoint).^(-1)))
        linBetter[i] = r2(olsLinear) > r2(olsExp) # true when linear is better
        if linBetter[i]
            println("the linear model is better")
            coeffs[i,:] = coef(olsLinear)
            stderrs[i, :] = stderror(olsLinear)
            residual[i] = r2(olsLinear)
        else
            coeffs[i,:] = coef(olsExp)
            stderrs[i, :] = stderror(olsExp)
            residual[i] = r2(olsExp)
        end
    end
    return (coeffs, stderrs, linBetter, residual, lastCoord)
end
function fitLogPolyDecay(A; discardRate=3 / 4)
    coeffs = zeros(size(A, 2), 3)
    stderrs = zeros(size(coeffs)...)
    for i = 1:size(A, 2)
        AnZ = Float64.(A[:,i][A[:,i] .> 0]) # the non-zero entries
        lastPoint = round(Int, discardRate * length(AnZ))
        df = DataFrame(X1=log.(2:lastPoint + 1), X2=log.(log.(2:lastPoint + 1)), Y=log.(AnZ[1:lastPoint]))
        ols = lm(@formula(Y ~ X1 + X2), df; wts=collect((1:lastPoint).^(-1)))
        coeffs[i, :] = coef(ols)
        stderrs[i, :] = stderror(ols)
    end
    return (coeffs, stderrs)
end
function fitLog(coeffs, A)
    x = log.(2:size(A, 1) + 1)
    netLines = coeffs[:,2] * x' .+ coeffs[:,3] * log.(x)' .+ coeffs[:,1]
    return netLines'
end
function fitLine(coeffs, A;initOffset=1000)
    x = log.(initOffset .+ (1:size(A, 1)))
    netLines = coeffs[:,2] * x' .+ coeffs[:,1]
    return netLines'
end
function fitExp(coeffs, A;initOffset=000)
    x = initOffset .+ (1:size(A, 1))
    netLines = exp.(coeffs[:,2] * x' .+ coeffs[:,1])
    return netLines'
end
# ideal case
plot((log.(1 .+ (1:n)) ./ (1:n)).^(3 / 2),scale=:log10)
plot(ideal(length(truncCl)),scale=:log10)
plot(exp.(-(1:n) ./ 16)[exp.(-(1:n) ./ 16) .> 1e-20], yscale=:log10)
# on a actually cartoon-like image
c = cartoonLikeImage{Float64}(xSize=400, ySize=400)
image, xLoc, yLoc = given_points([0 0; 0 .2; .5 .3; .7 0; -.7 -.3; -.5 .4; .5 .5], cart=c); heatmap(image)
using FFTW
δ = zeros(size(image)); δ[1:7,1:7] = 1 .+ randn(7, 7)
δ = FFTW.r2r(δ, FFTW.RODFT00)
δ ./= norm(δ, Inf)
image = max.((image * 2) .- 1, .5δ) # set it so the background is smoothly varying but not really zero (except smoothly decaying to the boundaries, since there's zero padding)
heatmap(image)
# the decay of the coefficients themselves
reorderCartoonLike = reorderCoeff(reshape(image, (:, 1)))
minFitInd = findfirst(reorderCartoonLike / reorderCartoonLike[1] .< 1e-8)
if minFitInd == nothing
   minFitInd = size(reorderCartoonLike, 1)
end
truncCl = reorderCartoonLike[1:minFitInd[1]]
plot(truncCl, scale=:log10)
plot(truncCl, yscale=:log10)
coeffsCl, stderrsCl = fitPolyDecay(truncCl, discardRate=1.0)
netLineCl = fitLine(coeffsCl, truncCl)
plot(log.(1:size(netLineCl, 1)), netLineCl); plot!(log.(1:size(truncCl, 1)), log.(truncCl[:,1:1]), legend=:bottomleft)
# moving on to shearlet/shattering
St = scatteringTransform((size(image)[1:2]..., 1, 1), 2, normalize=false, scale=4)# |> cu
heatmap(St.mainChain[1], vis=28, dispReal=true, apply=real, restrict=(100:200, 100:200))
heatmap(St.mainChain[1], vis=2, dispReal=true, apply=real, restrict=(300:500, 300:500))
heatmap(St.mainChain[1].weight)
@time tmp = cpu(St(cu(reshape(image, (size(image)..., 1)))))
@time tmp = cpu(St(reshape(image, (size(image)..., 1))))
plot([heatmap(tmp[1][:,:,i,1], xticks=false, yticks=false, colorbar=false) for i = 1:32]...,layout=(4, 8))
allThePlots = [heatmap(tmp[2][:,:,i,j,1], xticks=false, yticks=false, colorbar=false) for i = 1:32,j = 1:32]
plot(allThePlots[1:4,1:4]..., layout=(4, 4))
@time res = cpu(ScatteringTransform.flatten(St(cu(reshape(image, (size(image)..., 1))))))
@time sheared = cpu(reshape(St.mainChain[1](reshape(image, (size(image)..., 1))), (:, 1)))
# decay of the shearlet coefficients
reorderSheared = reorderCoeff(sheared)
10000 ./ size(reorderSheared)
reorderSheared[reorderSheared .> 0]
plot(reorderSheared[reorderSheared .> 0], scale=:log10)
plot(heatmap(image, title="original image"), plot(reorderSheared[reorderSheared .> 0][1:10:end], yscale=:log10, title="Log-linear shearlet coefficient decay", legend=false),layout=(2, 1))
coeffs, stderrs, linBetter, residual, lastCoord = fitPolyDecay(reorderSheared, discardRate=1.0)
netLine = fitExp(coeffs, reorderSheared)
function visualCompare(;title="Experimental decay",i=1)
    labelCoeffs = round.([exp.(coeffs[i,1]) coeffs[i,2]], sigdigits=3)
    plot(1:length(netLine), netLine[1:end], yscale=:log10, label="exp fit $(labelCoeffs[1])e^($(labelCoeffs[2]) × t)"); plot!(reorderSheared, yscale=:log10, label="data", title=title)
end

plot(10000 .+ (1:length(reorderSheared)), reorderSheared, scale=:log10)
plot(1000 .+ (1:length(reorderSheared)), exp.(netLine),scale=:log10); plot!(1000 .+ (1:length(reorderSheared)), reorderSheared, scale=:log10)
dropZeros = reorderSheared[reorderSheared .> 0][10000:end]
coeffs, stderrs, linBetter, residual, lastCoord = fitPolyDecay(reorderSheared[10000:minFitInd], discardRate=1.0)
lin = fitLine(coefEnd, reorderSheared[10000:minFitInd])
plot(reorderSheared[10000:minFitInd],scale=:log10); plot!(exp.(lin), scale=:log10)
netLine = fitLine(coeffs, dropZeros)
plot(log.(1:size(netLine, 1)), netLine); plot!(log.(1:size(dropZeros, 1)), log.(dropZeros[:,1:1]), legend=:bottomleft)

St = scatteringTransform((size(train_x)[1:2]..., 1, 100), 2, normalize=false) |> cu
@time res = cpu(ScatteringTransform.flatten(St(cu(train_x[:,:,1:100]))))
@time sheared = cpu(reshape(St.mainChain[1](cu(train_x[:,:,1:100])), (:, 100)))
reorderA = reorderCoeff(res)
reorderSheared = reorderCoeff(sheared)
coeffs, stderrs = fitPolyDecay(reorderSheared[:,1:1])
netLine = fitLine(coeffs, reorderSheared[:, 1:1])
plot(log.(1:size(netLine, 1)), netLine); plot!(log.(1:size(reorderSheared, 1)), log.(reorderSheared[:,1:1]), legend=:bottomleft)
plot(1:size(netLine, 1), netLine); plot!(1:size(reorderSheared, 1), log.(reorderSheared[:,1:3]), legend=:bottomleft)
scatter(1:size(netLine, 1), size(netLine, 1):-1:1,scale=:log10)
scatter(1:size(netLine, 1), (1:size(netLine, 1)).^(-2),scale=:log10)
