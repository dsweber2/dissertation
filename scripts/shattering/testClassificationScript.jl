inputFile = "/fasterHome/workingDataDir/shattering/shatteredMNISTabs2_4shearLevels.bin"
resultsFolder = "abs2Layer4shearLevels/"
nOfCVs = 30
script=true
using JLD, Plots, LaTeXStrings, PyCall, PyCallJLD, #Rsvg
using ScikitLearn
if script
    @sk_import linear_model: SGDClassifier
    _, testingClassesTot = testdata()
    s = open(inputFile)
    m = read(s, Int64)
    n = read(s, Int64)
    if contains(inputFile,"abs")
        A = Mmap.mmap(s, Matrix{Float64}, (m, n))
    else
        A = Mmap.mmap(s, Matrix{ComplexF64}, (m, n))
    end
end
println("loaded modules and data")

# first, figure out which is the best classifier
ValuesOfLambda = zeros(Float64, 50)
curIndexInValues = 1
validAccuracies = zeros(Float64, 50, nOfCVs) # λ × trial
totalAccuracies = zeros(Float64, 50, nOfCVs, 1000) # λ × cv × trial
batchAccuracies = zeros(Float64, 50, nOfCVs, 6000) # λ × cv × trial
for (root, dirs, files) in walkdir(resultsFolder)
    for file in files
        if file[end-2:end]=="jld" && file[1:4]=="2018" && parse(Int64,file[9:10])>8
            afterLambda = search(file, "lambda")[end] + 1 # the filenames are somewhat awkward for searching through
            beforeCv = search(file, "cv")[1]-1
            λ = parse(file[afterLambda:beforeCv])
            if !(λ in ValuesOfLambda)
                ValuesOfLambda[curIndexInValues] = λ
                curIndexInValues+=1
            end
            λIndex = indexin([λ], ValuesOfLambda)
            cvN = parse(Int64,file[(beforeCv+3):end-4])
            actualFile = load(joinpath(root,file))
            validAccuracies[λIndex, cvN+1] = actualFile["validAccuracy"]
            totalAccuracies[λIndex, cvN+1,1:length(actualFile["totalAccuracy"])] = actualFile["totalAccuracy"]
            batchAccuracies[λIndex, cvN+1,1:length(actualFile["batchAccuracy"])] = actualFile["batchAccuracy"]
        else
            println(file)
            println(file[9:10])
        end
    end
end
if !script
    heatmap(validAccuracies)
end
println("loaded the accuracies")


# plot the cross validation curve and find the best index

plotlyjs()
lastInd = findin(ValuesOfLambda,0)[1]-1
plot(ValuesOfLambda[1:lastInd], 100-100*sum(validAccuracies[1:lastInd,:], 2)/size(validAccuracies,2),xscale=:log10,yscale=:log10,yerror=std(validAccuracies[1:lastInd,:],2), title="Soft-Plus 2 layer shattering 30-fold CV, 100 examples", label="", xlabel="λ", ylabel="% validation set error")
savefig(joinpath(resultsFolder,"CVPlotabs2Layer.pdf"))

bestInd = indmin(100-100*sum(validAccuracies[1:lastInd,:], 2)/size(validAccuracies,2))
println("saved the plot")

# see how that one did on the test set
preds = zeros(Float64, nOfCVs, length(testingClassesTot))
for (root, dirs, files) in walkdir(resultsFolder)
    for file in files
        if file[end-2:end]=="jld" && file[1:4]=="2018" && parse(Int64,file[9:10])>8
            afterLambda = search(file, "lambda")[end] + 1 # the filenames are somewhat awkward for searching through
            beforeCv = search(file, "cv")[1]-1
            λ = parse(file[afterLambda:beforeCv])
            if λ==ValuesOfLambda[bestInd]
                cvN = parse(Int64,file[(beforeCv+3):end-4])
                actualFile = load(joinpath(root,file))
                model = actualFile["model"]
                for i=0:9
                    preds[cvN+1,(1000*i+1):((i+1)*1000)] = predict(model, A[:,(1000*i+1):((i+1)*1000)]')
                end
            end
        else
            println(file)
            println(file[9:10])
        end
    end
end

# wef = load(joinpath(resultsFolder,"TestSetPredictions.jld"))
# predictions = wef["preds"]
predictions = preds
_, testingClassesTot = testdata()
accuracies = Array{Bool}(size(predictions))
for i=1:size(accuracies,1)
    accuracies[i,:] = predictions[i,:].==testingClassesTot
end
percentage = sum(accuracies[:,1:6000],2)./size(accuracies[:,1:6000],2)
μ = Base.mean(percentage)
σ = std(percentage)
println("μ=$μ, σ=$(σ)")
save(joinpath(resultsFolder,"TestSetPredictions.jld"),"preds",preds,"λ",ValuesOfLambda[bestInd],"μ",μ,"σ",σ,"percentage",percentage)
# save(joinpath(resultsFolder,"TestSetPredictions.jld"),"preds",preds,"λ",wef["λ"],"μ",μ,"σ",σ,"percentage",percentage)

# pyplot()
# # let's see what convergence overall looks like
# λi = 31; cvI = 1 ;lastConvergeInd = findin(totalAccuracies[λi,cvI,:],0)[1] - 1; plot(100-100*sum(totalAccuracies[λi,:,1:lastConvergeInd],1)'/10,yerror=std(totalAccuracies[λi,:,1:lastConvergeInd],1)', title=string("Convergence (whole dataset) for ", L"λ_{31} = ", "$(@sprintf("%.2e", ValuesOfLambda[λi]))  (warm start for $(L"i>1"))"), yscale=:log10, label="", xlabel="number of times through dataset", ylabel="% error total dataset")
# savefig("ShatteringExamples/Abs2LayerClassifiershatteredMNIST/ConvergenceFullCyclePlotabs2Layer.pdf")
# # what's convergece by batch
# λi = 31; cvI = 1 ;lastConvergeInd = findin(batchAccuracies[λi,cvI,:],0)[1] - 1
# plot(max.(100-100*sum(batchAccuracies[λi,:,1:lastConvergeInd],1)'/10,1/1000),yerror=std(batchAccuracies[λi,:,1:lastConvergeInd],1)', title=string("Convergence (whole dataset) for ", L"λ_{31} = ", "$(@sprintf("%.2e", ValuesOfLambda[λi]))  (warm start for $(L"i>1"))"), yscale=:log10, label="", xlabel="batch #", ylabel="% error given batch")
# savefig("ShatteringExamples/Abs2LayerClassifiershatteredMNIST/ConvergenceByBatchPlotabs2Layer.pdf")

# if !(λ in ValuesOfLambda)
#     ValuesOfLambda[curIndexInValues] = λ
#     curIndexInValues+=1
# end
# λIndex = indexin([λ], ValuesOfLambda)[1]
#
# wef["totalAccuracy"]
# actualFile = load("ShatteringExamples/SoftPlus2LayerClassifiershatteredMNIST/2018-03-29T21:12:47.221lambda0.5736152510448679cv0.jld")
# wef = load("/home/dsweber/projects/shatteringTransform/ShatteringExamples/SoftPlus2SoftPlus2LayerClassifiershatteredMNIST/")
# shatteredTraining = wef["shatteredTraining"]
# layers = wef["layers"]
# trainingDataTot,trainingClassesTot = traindata()
# nTot = length(trainingClassesTot)
# validSize=Int(.3*length(trainingClassesTot))
# validData,validClasses = trainingDataTot[:,end+1-validSize:end], trainingClassesTot[(end+1-validSize):end]
# trainingData, trainingClasses = trainingDataTot[:,1:end-validSize],trainingClassesTot[1:(end-validSize)]
# SVM1000 = JLD.load("/home/dsweber/projects/shatteringTransform/ShatteringExamples/classifiers.jld","SVM1000")
# trainAccuracy = sum(predict(SVM1000,shatteredTraining[1001:end,:]).==trainingClasses[1001:end])/length(trainingClasses[1001:end])
# println(trainAccuracy)
#
# predictions = zeros(42000-1000)
# for i=1:(Int64(length(predictions)/10))
#     predictions[1+(i-1)*10:i*10] = predict(SVM1000,shatteredTraining[1000+(((i-1)*10+1):i*10),:])
# end
# 42000/10
# using JLD
# wef = load("/fasterHome/workingDataDir/shattering/shatteredMNISTReLU.jld")
# wef["shatteredTraining"]
# wef = load("/home/dsweber/projects/shatteringTransform/ShatteringExamples/shatteredMNIST.jld")
#
# tmp =
#
#
#
#
# X = [randn(100,30)+im*randn(100,30) randn(100,30)+300*im*randn(100,30)]
