using Shearlab
using Plots, FFTW, LaTeXStrings
plotlyjs()
saveFolder = "../../figures/"

system = getshearletsystem2D(500, 500, 5)
fieldnames(typeof(system))
(maximum(system.dualFrameWeights), minimum(system.dualFrameWeights))
systemTiny = getshearletsystem2D(28, 28, 2)
maximum(systemTiny.dualFrameWeights)
minimum(systemTiny.dualFrameWeights)
realShears = real.(ifftshift(ifft(system.shearlets, (1, 2))))
imagShears = imag.(ifftshift(ifft(system.shearlets, (1, 2))))
system.shearletIdxs
heatmap(abs.(system.shearlets[:,:,4]))
println("new one")
for i = 1:17
    println(system.shearletIdxs[i,:])
end
for i = 1:33
    println(system.shearletIdxs[i,:])
end
sampleat = [200:300, 200:300]
plotly(size=(1000, 1000))
plot([heatmap(real.(system.shearlets[:,:,i])) for i = 1:32]...,layout=(4, 8))
heatmap(sampleat[1], sampleat[2], realShears[sampleat[1], sampleat[2], 1])
heatmap(sampleat[1], sampleat[2], realShears[sampleat[1], sampleat[2], 2])
system.shearletIdxs



plotlyjs()
nScales = 3
shearLevels = 2
n = 500
@time shearletSystem = getshearletsystem2D(Int(n), Int(n), nScales, padded=false)
shearletSystem.shearletIdxs
thingToPlot = shearletSystem.shearlets
thingsIds = shearletSystem.shearletIdxs

func(x) = real.(ifftshift(ifft(fftshift(x)))) # space domain, real part
# func(x) = abs.(ifftshift(ifft(fftshift(x)))) # space domain, abs value
# func(x) = abs.(x) # frequency domain, abs value
# func(x) = real.(x) # frequency domain, real part
# func(x) = imag.(x) # frequency domain, imaginary part
# organized first by scale
toBePlat = plotRelevant(func(thingToPlot[:,:,ai]), ϵ=1.0)
ex1 = heatmap(toBePlat[1], toBePlat[2], toBePlat[3], subplot=1, yaxis=:flip, axis=false,  title=latexstring("\$\\psi_{$(thingsIds[ai,1]), $(thingsIds[ai,2]), $(thingsIds[ai,3])}\$"), colorbar=false, ratio=1, clims=(-0.02, 0.02), c=:viridis)
gr()
descToRow(row) = max(1 + row[2] + (row[1] - 1) * 3, 1)
pltList = Array{typeof(ex1),2}(undef, 9, 7)
function descToRow(row)
    if row[1] == 1
        return 2 * row[2] + 1
    elseif row[1] == 2
        return 2 * row[2]
    end
    return 1
end
for (ii, row) = enumerate(eachrow(thingsIds))
    vertLoc = descToRow(row)
    horiLoc = row[3] + 5
    println("$row,  ($vertLoc, $horiLoc)")
    toBePlat = plotRelevant(func(thingToPlot[:,:,ii]), ϵ=200.0)
    pltList[horiLoc, vertLoc] = heatmap(toBePlat..., xticks=false, yticks=false, yaxis=:flip, axis=false,  title=latexstring("\$\\psi_{$(row[1]), $(row[2]), $(row[3])}\$"), colorbar=false, ratio=1, clims=(-0.02, 0.02), c=:viridis)
end
pltList
# make the rest blank plots
for ii in 1:length(pltList)
    if !isassigned(pltList, ii)
        pltList[ii] = plot(legend=false, grid=false, foreground_color_subplot=:white)
    else
        println(ii)
    end
end
t = plot(title="Shearlet System: 3 scales", grid=false, showaxis=false, bottom_margin=-30Plots.px, ticks=false)
l = @layout [t{.01h}; grid(7, 9)]
plot(t, pltList..., layout=l)
savefig(joinpath(saveFolder, "shearletExamples.pdf"))
"""
    plotRelevant(x::Array{T,2};ϵ::Float64=1,square::Bool=true) where T<:Number

given a 2D image, returns the minimal box containing coefficients which are ϵ different from zero. ϵ is the factor to multiply the box selection by, which is otherwise just based on the average value of the array.
"""
function plotRelevant(x::Array{T,2}; ϵ::Float64=1.0,square::Bool=true) where T <: Number
  ϵ = ϵ * sum(abs.(x)) / size(x, 1) / size(x, 2)
  comp = abs.(x) .> ϵ # an array of booleans determining whether it's larger than
  # these will be the extremal indices
  left, top = (0, 0)
  right, bottom = (Inf, Inf)
  sum(comp[:,4])
  comp[:,4]
  n = size(comp, 2)
  m = size(comp, 1)
  for i = 1:n
    if sum(comp[:,i]) > 0
      left = i
      break
    end
    if sum(comp[:,n - i]) > 0
      right = n - i
      break
    end
  end
  for i = 1:m
    if sum(comp[i,:]) > 0
      top = i
      break
    end
    if sum(comp[m - i,:]) > 0
      bottom = m - i
      break
    end
  end
  if square
    left = top = min(top, left)
    right = bottom = max(right, bottom)
  end
  return (top:bottom, left:right, x[top:bottom, left:right])
end
