# classifying the transformed Data using svm trained via sgd on the first n examples
using ScikitLearn
using MNIST
using PyCall, JLD, PyCallJLD
@sk_import linear_model:SGDClassifier
# inputFile = "/fasterHome/workingDataDir/shattering/shatteredMNISTReLU.bin"
# saveDirectory = "/home/dsweber/projects/shatteringTransform/ShatteringExamples/ReLU2LayerClassifiershatteredMNIST/300"
inputFile = "/fasterHome/workingDataDir/shattering/shatteredMNISTabs2_4shearLevels.bin"
saveDirectory = "/home/dsweber/projects/shatteringTransform/ShatteringExamples/AbsTest2LayerClassifiershatteredMNIST/300"

# inputFile = "/fasterHome/workingDataDir/shattering/shatteredMNISTsoftplus.bin"
# saveDirectory = "/home/dsweber/projects/shatteringTransform/ShatteringExamples/SoftPlus2LayerClassifiershatteredMNIST/300"

mkpath(saveDirectory)
# testFile = "/fasterHome/workingDataDir/shattering/testSets/shatteredMNISTabs.bin"

segfault = true
if segfault
    initCV = 74 # if we're recovering from a segfault, set this to the cv round
    initλ = 17 # if we're recovering from a segfault, set this to the λ index we're at
else
    initCV = 1
    initλ = 1
end

dataSize = 300 # the number of samples to use to train the model. If zero, use Cross-validation. Otherwise, make sure it's divisible by nCV (generally 10). Also make sure that dataSize<.5 total size
nCV = 30 # if dataSize is non-zero, this is just the number of times to sample, and we are performing a monte carlo cross validation. Otherwise, we're holding out 1/nCV of the data as a validation set
cvSize = 1 / nCV
nIterations = 1 # the number of times to run over the data
batchSize = 1000
# see if we can store everything in memory
if batchSize >= dataSize && dataSize > 0
    batchSize = dataSize
end
initλ = 1
nλ = 100
λminratio = 1e-14
λmax = 1000
s = open(inputFile)
m = read(s, Int64)
n = read(s, Int64)
if contains(inputFile, "abs")
    A = Mmap.mmap(s, Matrix{Float64}, (m, n))
else
    A = Mmap.mmap(s, Matrix{ComplexF64}, (m, n))
end
# set up the values of λ
function computeλ(λmax, λminratio, nλ)
    logλmax = log(λmax)
    λ = exp.(range(logλmax, stop=logλmax + log(λminratio), length=nλ))
end
λpath = computeλ(λmax, λminratio, nλ)

if dataSize == 0
    # randomize the indicies and hold out some indices for cross validation
    totalIterations = ceil(Int64, (nCV - 1) / nCV * n / batchSize)
    indexes = shuffle(1:n)
    cvs = copy(indexes)
    OverAll = totalIterations * nIterations
else
    # pull out a random subset of size dataSize to be used for training
    totalIterations = ceil(Int64, dataSize / batchSize)
    indexes = shuffle(1:n)
    cvs = copy(indexes)
    OverAll = totalIterations * nIterations
end

# get the labels for the training set
_, trainingClassesTot = traindata()
classes = unique(trainingClassesTot)

# simple way of getting the correct indices
function rangeFromIndex(k::Int64, totalIterations::Int64, batchSize::Int64)
    if totalIterations == 1
        return 1:batchSize
    end
    # the last bit might not have exactly the same number of entries
    if mod(k + 1, totalIterations) != 0
        return currRange = 1 + mod(k, totalIterations) * batchSize:mod(k + 1, totalIterations) * batchSize
    else
        # if dataSize is in use, it caps the range size
        return currRange = (1 + mod(k, totalIterations) * batchSize):(dataSize == 0 ? floor(Int64, n * (1 - cvSize)) : dataSize)
    end
end
model = 3

# solve starting from the most sparse and use warm starts to move to the next layer
for λ in λpath[initλ:end]
    if λ == λpath[initλ]
        model = SGDClassifier(loss="log", penalty="l1", n_jobs=10, warm_start=true, alpha=λ, max_iter=100, tol=1e-5)
    else
        model = set_params!(model, alpha=λ)
    end
    for cvn = initCV - 1:nCV - 1
        if dataSize == 0
            # set the cv variables
            indexes = [cvs[1:floor(Int64, cvn / nCV * n)]; cvs[floor(Int64, (1 + cvn) / nCV * n) + 1:n]]
            cv = cvs[floor(Int64, 1 + cvn / nCV * n):floor(Int64, (1 + cvn) / nCV * n)]
        else
            indexes = cvs[1:dataSize]
            cv = cvs[1 + dataSize:2 * dataSize]
        end

        times = zeros(OverAll)
        totalTimes = zeros(OverAll)
        batchAccuracy = zeros(OverAll)
        totalAccuracy = zeros(floor(Int64, OverAll / totalIterations) + 1)


        println("============================================= Now for $(findin(λpath, λ)) λ=$(λ), cross-validation round $(cvn + 1) =============================================")
        println("Iteration       | Time (classification)    |  Time (total)   |   Batch Accuracy     |   First set accuracy")
        for k = 1:OverAll
            tic()
            currRange = rangeFromIndex(k, totalIterations, batchSize)
            _, t, _, _, _ = @timed partial_fit!(model, A[:,indexes[currRange]]', trainingClassesTot[indexes[currRange]], classes=classes)
            times[k] = t
            batchAccuracy[k] = score(model, A[:, indexes[currRange]]', trainingClassesTot[indexes[currRange]])
            totalTimes[k] = toq()
            println("$k              |         $(times[k])s     | $(totalTimes[k])s   | $(batchAccuracy[k])%              |    $(score(model, A[:, 1:1000]', trainingClassesTot[1:1000]))%  ")
            if mod(k, totalIterations) == 0
                tmpScores = zeros(totalIterations)
                for j = 1:totalIterations
                    currRange = rangeFromIndex(j, totalIterations, batchSize)
                    tmpScores[j] = score(model, A[:, indexes[currRange]]', trainingClassesTot[indexes[currRange]])
                end
                totalAccuracy[floor(Int64, k / totalIterations)] = sum(tmpScores) / length(tmpScores)
                indexes = shuffle(indexes)
                println("$k -------------------------------------------------- $(totalAccuracy[floor(Int64, k / totalIterations)]) --------------------------------------------------")
                if totalAccuracy[floor(Int64, k / totalIterations)] > .999
                    println("No reasonable way this is going to get better")
                    break
                end

            end
        end
        tmpScores = zeros(totalIterations)
        for j = 1:totalIterations
            currRange = rangeFromIndex(j, totalIterations, batchSize)
            tmpScores[j] = score(model, A[:, indexes[currRange]]', trainingClassesTot[indexes[currRange]])
        end
        totalAccuracy[end] = sum(tmpScores) / length(tmpScores)
        # just in case cv is also too large to fit in memory, also break it into batches
        validAccuracy = 0.0
        for k = 0:floor(Int64, length(cv) / batchSize - 1)
            validAccuracy += score(model, A[:, cv[(1 + k * batchSize):(k + 1) * batchSize]]', trainingClassesTot[cv[(1 + k * batchSize):(k + 1) * batchSize]])
        end

        validAccuracy = score(model, A[:,cv[(1 + end - batchSize):end]]', trainingClassesTot[cv[(1 + end - batchSize):end]])
        JLD.save(joinpath(saveDirectory, "$(now())lambda$(λ)cv$(cvn).jld"), "model", model, "totalAccuracy", totalAccuracy, "batchAccuracy", batchAccuracy, "times", times, "validAccuracy", validAccuracy, "cv", cv)
        # let's say it's running too long, but you want to kill it gracefully. Make a file named break in the appropriate folder, then it will break immediately after saving
        if isfile("/home/dsweber/projects/shatteringTransform/break")
            close(s)
            yo()
            break
        end
        # if we're doing the Monte Carlo method, we need to shuffle cvs all the time
        if dataSize > 0
            cvs = shuffle(cvs)
        end
    end
    # remix cvs for the next λ
    cvs = shuffle(cvs)
end

close(s)
