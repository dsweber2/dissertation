(TeX-add-style-hook
 "Abstract_Only"
 (lambda ()
   (TeX-run-style-hooks
    "../LayoutPreamble"
    "../MacrosPreamble"
    "../Abstract"))
 :latex)

