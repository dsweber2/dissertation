using Interpolations, FFTW
using ContinuousWavelets, Wavelets
using Plots, LaTeXStrings

gr(dpi = 400)
saveDir = "../figures/introduction/"
J = 11;
n = 2^J;
function HeaviSine(n, ω = 1)
    t = 0:1/n:1-eps()
    return 2 * sin.(ω * 4π .* t) - sign.(t .- 0.3) - sign.(0.72 .- t)
end
x = HeaviSine(n, 8)

# Figure 2.1
itp = interpolate([3; 4; -3; -2; 3], BSpline(Cubic(Interpolations.Periodic(OnGrid()))))
plot(plot([itp[range(1, 5, length = n)] x], legend = false, title = "Raw signals", linewidth = 2),
    plot(max.([abs.(rfft(itp[range(1, 5, length = n)])) abs.(rfft(x))], 1e-5), yscale = :log10, legend = :best, labels = ["smooth" "HeaviSine"], title = "Absolute value of Positive Fourier Coefficients", linewidth = 2),
    layout = (2, 1))
savefig(joinpath(saveDir, "CoefficientDecay.pdf"))
plot(plot([itp[range(1, 5, length = n)] x], legend = false, title = "Raw signals", linewidth = 2, xlabel = "time"),
    plot(max.([abs.(rfft(itp[range(1, 5, length = n)])) abs.(rfft(x))], 1e-5), yscale = :log10, legend = :best, labels = ["smooth" "HeaviSine"], title = "Absolute value of Positive Fourier Coefficients", linewidth = 2, xlabel = "frequency", ylabel = "log magnitude"),
    layout = (2, 1))
savefig(joinpath(saveDir, "CoefficientDecayLabeled.pdf"))

# Figure 2.2
function mapTo(waveType, isReal = true, window = 1:2047; d = 1, kwargs...)
    if isReal
        c = wavelet(waveType; β = d, kwargs...)
        waves, ω = ContinuousWavelets.computeWavelets(n, c)
        return circshift(irfft(waves, 2 * n, 1), (1024, 0))[window, :]
    else
        c = wavelet(waveType; β = d, kwargs...)
        waves, ω = ContinuousWavelets.computeWavelets(n, c)
        waves = cat(waves, zeros(2047, size(waves, 2)), dims = 1)
        return circshift(ifft(waves, 1), (1024, 0))[window, :]
    end
end
tmp = mapTo(Morlet(π), false; averagingLength = -0.2)[:, 2]
p1 = plot([real.(tmp) imag.(tmp)], title = "Morlet π", labels = ["real" "imaginary"], ticks = nothing, linewidth = 5)
tmp = mapTo(paul2, false, averagingLength = -0.5)[:, 2]
p2 = plot([real.(tmp) imag.(tmp)], title = "Paul 2", labels = ["real" "imaginary"], ticks = nothing, linewidth = 5)
p3 = plot(mapTo(dog2; averagingLength = -1.5)[:, 2], title = "DoG2", legend = false, ticks = nothing, linewidth = 5)
p4 = plot(mapTo(cHaar, true; averagingLength = 1)[:, 2], title = "Haar", legend = false, ticks = nothing, linewidth = 5)
p5 = plot(mapTo(cBeyl, true; d = 1, averagingLength = -0)[:, 2], title = "Beylkin", legend = false, ticks = nothing, linewidth = 5)
p6 = plot(mapTo(cVaid, true; d = 1, averagingLength = -0)[:, 2], title = "Vaidyanathan", legend = false, ticks = nothing, linewidth = 5)
p7 = plot(mapTo(cDb2; d = 1, averagingLength = -0)[:, 2], title = "Daubechies 2", legend = false, ticks = nothing, linewidth = 5)
p8 = plot(mapTo(cCoif2, true; d = 1, averagingLength = -0)[:, 2], title = "Coiflet 2", legend = false, ticks = nothing, linewidth = 5)
p9 = plot(mapTo(cSym4, true; d = 1, averagingLength = -0)[:, 2], title = "Symlet 4", legend = false, ticks = nothing, linewidth = 5)
k = 0600;
p10 = plot(mapTo(cBatt4, true, 1024-k:1024+k; d = 1, averagingLength = -1)[:, 2], title = "Battle-Lemarie, 4", legend = false, ticks = nothing, linewidth = 5);
plot(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, layout = (2, 5), size = 300 .* (5, 2.2))
savefig(joinpath(saveDir, "mothers.pdf"))



# Figure 2.3
function plotScalogram(x, cw, title = "", coloring = cgrad(:viridis, scale = :exp))
    freqs = getMeanFreq(ContinuousWavelets.computeWavelets(n, cw)[1], 1000)
    dispFreqs = round.(freqs[2:end], sigdigits = 3)
    CWTx = ContinuousWavelets.cwt(x, cw)
    CWTx = (abs.(CWTx[:, 2:end]) .^ 2)'
    sortedFFT = sort(abs.(rfft(x)), rev = true)
    plot(sortedFFT[sortedFFT.!=0], yscale = :log10)
    plot!(sort(CWTx[:], rev = true), yscale = :log10)
    yspacing = 2:4:(size(CWTx, 1)-1)
    scalogram = heatmap(CWTx, c = coloring, yticks = (yspacing, dispFreqs[yspacing]), ylabel = "Frequency (Hz)", xlabel = "time (ms)", colorbar = false, top_margin = -10Plots.px)
    pltX = plot(x, legend = false, title = title, xticks = (0:500:2048, ""), bottom_margin = -5Plots.px, xlims = (1, 2048))
    colorbar = Plots.scatter([0, 0], [0, 1], xlims = (1, 1.1), marker_z = 3, xshowaxis = false, label = "", colorbar = true, grid = false, xticks = false, yticks = false, framestyle = :grid, left_margin = -8Plots.px, right_margin = 0Plots.px, clims = (minimum(CWTx), maximum(CWTx)), c = coloring)
    blankSpot = Plots.scatter([0, 0], [0, 1], xlims = (1, 1.1), xshowaxis = false, label = "", colorbar = false, grid = false, xticks = false, yticks = false, framestyle = :grid, bottom_margin = -10Plots.px, top_margin = -10Plots.px, left_margin = -42Plots.px, right_margin = -22Plots.px)
    lay = @layout [a{0.15h}; c]
    p1 = plot(pltX, scalogram, layout = lay)
    lay = @layout [a{0.15h}; c]
    pc = plot(blankSpot, colorbar, layout = lay)
    lay0 = @layout [a b{0.11w}]
    plot(p1, pc, layout = lay0)
end
cw = wavelet(Morlet(π), β = 1, boundary = ContinuousWavelets.Periodic, frameBound = 100, averagingLength = -2)
plotScalogram(HeaviSine(n, 8), cw, "")
savefig(joinpath(saveDir, "heavisineScalogram.pdf"))
savefig(joinpath(saveDir, "heavisineScalogram.png"))
plotScalogram(HeaviSine(n, 8), wavelet(dog2, β = 1, boundary = ContinuousWavelets.Periodic, frameBound = 100, averagingLength = -2), "")
savefig(joinpath(saveDir, "heavisineScalogramDog2.pdf"))
savefig(joinpath(saveDir, "heavisineScalogramDog2.png"))
plotScalogram(testfunction(n, "Bumps"), cw, "")
savefig(joinpath(saveDir, "bumpsScalogram.pdf"))
savefig(joinpath(saveDir, "bumpsScalogram.png"))
plotScalogram(testfunction(n, "Bumps"), wavelet(dog2, β = 1, boundary = ContinuousWavelets.Periodic, frameBound = 100, averagingLength = -2), "")
savefig(joinpath(saveDir, "bumpsScalogramDog2.pdf"))
savefig(joinpath(saveDir, "bumpsScalogramDog2.png"))


# figure 2.4
pyplot()
dRate = 4
Ψ1 = wavelet(Morlet(π), s = 8, decreasing = dRate)
locs = ContinuousWavelets.polySpacing(10, Ψ1)
scatter(1:length(locs), locs, legend = :bottomright, label = "mean log frequency", xlabel = "Wavelet Index (k)", ylabel = "log-Frequency (y)", color = :black)
scatter!(length(locs):length(locs), locs[end:end], markersize = 10, markershape = :x, color = :black, label = :none)
firstW, lastW, stepS = ContinuousWavelets.genSamplePoints(8, Ψ1)
fieldnames(typeof(Ψ1))
b = (dRate / Ψ1.Q)^(1 ./ dRate) * (8 + Ψ1.averagingLength)^((dRate - 1) / dRate)
t = range(1, stop = length(locs), step = 0.1)
curve = b .* (range(firstW, stop = (locs[end] / b)^dRate, length = length(t))) .^ (1 / dRate)
plot!(t, curve, color = :blue, line = :dash, label = L"y=a(mk+k_0)^{^1/_\beta}", legend = :bottomright, legendfontsize = 12, xrange = (0, length(locs) + 3), xticks = [1; 5:5:1+length(locs)...], yrange = (minimum(locs) - 1, maximum(locs) + 1), yticks = (2:2:10, [L"\alpha", (4:2:8)..., "N.Octaves"]))
x = range(15, stop = 28, step = 0.5)
ycord(x) = curve[end] .+ b / dRate * length(locs) .^ (1 / dRate - 1) .* (x .- length(locs))
plot!(x, ycord(x), c = :black, line = 2, label = :none)
annotate!(length(locs) - 1 / 8, locs[end] + 7.5 / 16, Plots.text(L"\frac{dy}{dk}=^{1}/_{Q}", 11, :black, :center))
savefig("../figures/plotOfLogCentralFrequencies.pdf")


# figure 2.5
Ψ0 = wavelet(Morlet(π), decreasing = 1)
d0, ξ = ContinuousWavelets.computeWavelets(n, Ψ0)
Ψ1 = wavelet(Morlet(π), s = 8, decreasing = dRate)
d1, ξ = ContinuousWavelets.computeWavelets(n, Ψ1)
println("How many filters are below frequency 50 when β = 1? $(count(sum(d0 .* ξ, dims=1).<50))")
println("How many filters are below frequency 50 when β = 4? $(count(sum(d1 .* ξ, dims=1).<50))")
xticksFewer = [1; 4; 7; 9; 12; 15; 18]
plot(heatmap(1:size(d0, 2), ξ, d0, color = :Greys,
        yaxis = (L"\omega", font(20, "sans-serif")),
        xaxis = ("wavelet index", font(12, "sans-serif")),
        title = L"Q=8, \beta=1", colorbar = false, tickfontsize = 10,
        clims = (minimum([d0 d1]), maximum([d0 d1]))),
    heatmap(1:size(d1, 2), ξ, d1, color = :Greys, yticks = [],
        xaxis = ("wavelet index", font(12, "sans-serif")), tickfontsize = 12, xticks = xticksFewer,
        title = L"Q=8, \beta=4"), layout = (1, 2),
    clims = (minimum([d0 d1]), maximum([d0 d1])),
    colorbar_title = L"\widehat{\psi_i}")
savefig("../figures/QDecreasingBeta0vs1.pdf")
