using ContinuousWavelets, Wavelets
using Plots, PerceptualColourMaps, LaTeXStrings
using FourierFilterFlux, ScatteringTransform

J = 11;
n = 2^J;
cw = wavelet(Morlet(π), β = 1, boundary = ContinuousWavelets.Periodic, frameBound = 100, averagingLength = -2)
st = scatteringTransform((n, 1, 1), 2, cw = Morlet(π), convBoundary = FourierFilterFlux.Periodic(), boundary = ContinuousWavelets.Periodic, β = 3, averagingLength = [-2, -1], normalize = false)

function HeaviSine(n, ω = 1)
    t = 0:1/n:1-eps()
    return 2 * sin.(ω * 4π .* t) - sign.(t .- 0.3) - sign.(0.72 .- t)
end
heavysine = reshape(HeaviSine(n, 8), (n, 1, 1))
bumps = reshape(testfunction(n, "Bumps"), (n, 1, 1))
heavysineSt = st(heavysine)
bumpSt = st(bumps)

freqsSt = getMeanFreq(st, 1000)
# figure 3.2 (a)
plot(heavysine[:, 1, 1], legend = false, title = "HeaviSine function", xticks = 0:250:n, xlims = (0, n), size = sizePlot)
savefig(joinpath(saveDir, "HeaviSineFunction.pdf"))
jointPlot(heavysineSt, "", cmap("L20"), st, sharedColorScaling = :linear, allPositive = true, logPower = true, frameTypes = :none, secondFreqSpacing = 1:2:size(bumpSt[2], 2), xlabel = "")
savefig(joinpath(saveDir, "heaviSineScatteredMorlet.pdf"))
savefig(joinpath(saveDir, "heaviSineScatteredMorlet.png"))

# figure 3.3
plot(bumps[:, 1, 1], legend = false, title = "Bumps function", xticks = 0:250:n, xlims = (0, n), size = sizePlot)
savefig(joinpath(saveDir, "bumpsFunction.pdf"))
jointPlot(bumpSt, "", cmap("L20"), st, sharedColorScaling = :linear, allPositive = true, logPower = true, frameTypes = :none, secondFreqSpacing = 1:2:size(bumpSt[2], 2), xlabel = "")
savefig(joinpath(saveDir, "bumpsScatteredMorlet.pdf"))
savefig(joinpath(saveDir, "bumpsScatteredMorlet.png"))


# figure 3.2 (b)
st = scatteringTransform((n, 1, 1), 2, cw = dog2, convBoundary = FourierFilterFlux.Periodic(), boundary = ContinuousWavelets.Periodic, β = 1.5, Q = 4, outPool = 8, normalize = false)
heavysineDogSt = st(heavysine)
freqsSt = getMeanFreq(st, 1000)
jointPlot(heavysineDogSt, "", cmap("L20"), st, sharedColorScaling = :linear, allPositive = true, logPower = true, frameTypes = :none, secondFreqSpacing = 1:2:size(heavysineDogSt[2], 2), xlabel = "")
savefig(joinpath(saveDir, "heaviSineScatteredDoG2.pdf"))
savefig(joinpath(saveDir, "heaviSineScatteredDoG2.png"))
