using Flux, Zygote
using ScatteringTransform, FourierFilterFlux, ContinuousWavelets, Wavelets, FFTW, LinearAlgebra, CUDA
using PerceptualColourMaps, Plots
CUDA.allowscalar(true)
toSave = false

# shared preamble utilities
saveDir = "../figures/interpret/gradientAlone/"

outPool = 8
N = 2048
St = stFlux((N, 1, 1), cw = dog2, 2, Q = 4, β = [1, 1, 1], extraOctaves = (-0, -0, 0), averagingLength = [-1.5, -1.5, 0], outputPool = outPool, convBoundary = FourierFilterFlux.Periodic(), boundary = PerBoundary(), poolBy = 2, normalize = true) # |> gpu
StcpuUnnorm = stFlux((N, 1, 1), cw = dog2, 2, Q = 4, β = [1, 1, 1], extraOctaves = (-0, -0, 0), averagingLength = [-1.5, -1.5, 0], outputPool = outPool, convBoundary = FourierFilterFlux.Periodic(), boundary = PerBoundary(), poolBy = 2, normalize = false)
x = randn(N, 1, 1) # |> gpu
freqs = getMeanFreq(StcpuUnnorm, 1000)
freqs = map(x -> round.(x, sigdigits = 4), freqs)
_, J2, J1, _ = size(St(x)[2])

function generateAllTheGradients(StThis, signal, spaceLoc = round(Int, N / 2 / 2 / outPool / 2))
    _, J2, J1, _ = size(StThis(signal)[2])
    allTheGradients = [gradient(x -> StThis(x)[2][spaceLoc, j2, j1, 1], signal)[1] for j2 = 1:J2, j1 = 1:J1]
end
zeroAtOutOf(k, n, N) = range(-(k) / n, (n - k) / n + ((N - 1) % 2) / N, length = N)

# allTheGrads = zeros(N, J2, J1)
# for j2 in 1:J2, j1 in 1:J1
#     allTheGrads[:,j2,j1] = allTheGradients[j2,j1]
# end
function generateTitle(title; kwargs...)
    titlePlot = plot(; title = title, grid = false, xticks = nothing, yticks = nothing, showaxis = false, bottom_margin = -20Plots.px, kwargs...)
    return titlePlot
end
function generateTitleAndLayout(title, otherLayout)
    titlePlot = plot(title = title, grid = false, xticks = nothing, yticks = nothing, showaxis = false, bottom_margin = -20Plots.px)
    l = @layout [a{0.0001h}; otherLayout]
    return titlePlot, l
end
function plotAllTheGradients(allTheGradients, spacing = 2, title = "Gradient"; normalize = false, preproc = identity, start = -1, kwargs...)
    if preproc isa Symbol
        if preproc == :rfft
            preproc = x -> abs.(rfft(x, 1))
        elseif preproc == :sameWavelet
            preproc = x -> circshift(cpu(St.mainChain[1](x))[:, :, 1, 1]', (1, 0))
        else
            error("$(preproc) is not a defined symbol")
        end
    end
    justTheFirst = preproc(allTheGradients[1, 1][:, 1, 1])
    p1 = plot(justTheFirst)
    NSpace = size(justTheFirst, 1)
    allThePlots = Array{typeof(p1),2}(undef, size(allTheGradients)...)
    J2, J1 = size(allTheGradients)
    procced = [preproc(allTheGradients[j2, j1][:, 1, 1]) for j1 = 1:size(allTheGradients, 2), j2 = 1:size(allTheGradients, 1)]
    for j2 in 1:J2, j1 in 1:J1
        if normalize
            ylims = (minimum(procced), maximum(procced))
        else
            ylims = (minimum(preproc(allTheGradients[j2, j1][:, 1, 1])), maximum(preproc(allTheGradients[j2, j1][:, 1, 1])))
        end

        if j1 == 1 && j2 != 1
            allThePlots[j2, J1-j1+1] = plot(range(start, 1, length = NSpace), preproc(allTheGradients[j2, j1][:, 1, 1]); legend = false, xlabel = "$(freqs[2][j2])", guidefontsize = 8, xticks = false, yticks = false, framestyle = :zerolines, top_margin = -5Plots.px, left_margin = -5Plots.px, right_margin = -5Plots.px, yguidefontrotation = -90, ylims = ylims, kwargs...)
        elseif j2 == 1 && j1 != 1
            allThePlots[j2, J1-j1+1] = plot(range(start, 1, length = NSpace), preproc(allTheGradients[j2, j1][:, 1, 1]); legend = false, ylabel = "$(freqs[1][j1])", guidefontsize = 8, xticks = false, yticks = false, framestyle = :zerolines, bottom_margin = -5Plots.px, top_margin = -5Plots.px, left_margin = 10Plots.px, right_margin = -5Plots.px, yguidefontrotation = -90, ylims = ylims, kwargs...)
        elseif j2 == 1 && j1 == 1
            allThePlots[j2, J1-j1+1] = plot(range(start, 1, length = NSpace), preproc(allTheGradients[j2, j1][:, 1, 1]); legend = false, ylabel = "$(freqs[2][j2])", xlabel = "$(freqs[1][j1])", guidefontsize = 8, xticks = false, yticks = false, framestyle = :zerolines, top_margin = -5Plots.px, left_margin = 10Plots.px, right_margin = -5Plots.px, yguidefontrotation = -90, ylims = ylims, kwargs...)
        else
            allThePlots[j2, J1-j1+1] = plot(range(start, 1, length = NSpace), preproc(allTheGradients[j2, j1][:, 1, 1]); legend = false, xticks = false, yticks = false, framestyle = :zerolines, bottom_margin = -5Plots.px, top_margin = -5Plots.px, left_margin = -5Plots.px, right_margin = -5Plots.px, yguidefontrotation = -90, ylims = ylims, kwargs...)
        end
    end
    toBePlotted = allThePlots[1:spacing:end, 1:spacing:end]
    titlePlot = plot(title = title, grid = false, xticks = nothing, yticks = nothing, showaxis = false, bottom_margin = -20Plots.px)
    yaxis = plot(grid = false, left_margin = 20Plots.px, xticks = nothing, yticks = nothing, showaxis = false, ylabel = "First Layer Frequency (Hz)")
    xaxis = plot(grid = false, xticks = nothing, yticks = nothing, showaxis = false, bottom_margin = 20Plots.px, xlabel = "Second Layer Frequency (Hz)")
    l = @layout [a{0.0001h}; b{0.0001w} grid(reverse(size(toBePlotted))...); c{0.0001h}]
    return plot(titlePlot, yaxis, toBePlotted..., xaxis, layout = l)
end
function plotAllTheGradientsHeatmap(allTheGradients, spacing = 2, title = "Gradient"; StThis = St, normalize = false, MetaXLabel = (freqs[2], ""), MetaYLabel = (freqs[1], ""), preproc = identity, start = -1, kwargs...)
    if preproc isa Symbol
        if preproc == :rfft
            preproc = x -> abs.(rfft(x, 1))
        elseif preproc == :sameWavelet
            preproc = x -> circshift(cpu(StThis.mainChain[1](x))[:, :, 1, 1]', (1, 0))
        else
            error("$(preproc) is not a defined symbol")
        end
    end
    justTheFirst = preproc(allTheGradients[1, 1])[:, :, 1, 1]
    p1 = heatmap(justTheFirst; kwargs...)
    allThePlots = Array{typeof(p1),2}(undef, size(allTheGradients)...)
    J1, J2
    for j2 in 1:J2, j1 in 1:J1
        if j1 == 1 && j2 != 1
            allThePlots[j2, J1-j1+1] = heatmap(preproc(allTheGradients[j2, j1])[:, :, 1, 1]; legend = false, xlabel = "$(MetaXLabel[1][j2])" * MetaXLabel[2], guidefontsize = 6, xticks = false, yticks = false, framestyle = :zerolines, top_margin = -5Plots.px, left_margin = -5Plots.px, right_margin = -5Plots.px, guidefontrotation = -30, kwargs...)
        elseif j2 == 1 && j1 != 1
            allThePlots[j2, J1-j1+1] = heatmap(preproc(allTheGradients[j2, j1])[:, :, 1, 1]; legend = false, ylabel = "$(MetaYLabel[1][j1])" * MetaYLabel[2], guidefontsize = 6, xticks = false, yticks = false, framestyle = :zerolines, bottom_margin = -5Plots.px, top_margin = -5Plots.px, left_margin = 10Plots.px, right_margin = -5Plots.px, guidefontrotation = -30, kwargs...)
        elseif j2 == 1 && j1 == 1
            allThePlots[j2, J1-j1+1] = heatmap(preproc(allTheGradients[j2, j1])[:, :, 1, 1]; legend = false, ylabel = "$(MetaYLabel[1][j1])" * MetaYLabel[2], xlabel = "$(MetaXLabel[1][j2])" * MetaXLabel[2], guidefontsize = 6, xticks = false, yticks = false, framestyle = :zerolines, top_margin = -5Plots.px, left_margin = 10Plots.px, right_margin = -5Plots.px, guidefontrotation = -30, kwargs...)
        else
            allThePlots[j2, J1-j1+1] = heatmap(preproc(allTheGradients[j2, j1])[:, :, 1, 1]; legend = false, xticks = false, yticks = false, framestyle = :zerolines, bottom_margin = -5Plots.px, top_margin = -5Plots.px, left_margin = -5Plots.px, right_margin = -5Plots.px, guidefontrotation = -30, kwargs...)
        end
    end
    toBePlotted = allThePlots[1:spacing:end, 1:spacing:end]
    titlePlot = plot(title = title, grid = false, xticks = nothing, yticks = nothing, showaxis = false, bottom_margin = -20Plots.px)
    yaxis = plot(grid = false, left_margin = 20Plots.px, xticks = nothing, yticks = nothing, showaxis = false, ylabel = "First Layer Frequency (Hz)")
    xaxis = plot(grid = false, xticks = nothing, yticks = nothing, showaxis = false, bottom_margin = 20Plots.px, xlabel = "Second Layer Frequency (Hz)")
    l = @layout [a{0.0001h}; b{0.0001w} grid(reverse(size(toBePlotted))...); c{0.0001h}]
    return plot(titlePlot, yaxis, toBePlotted..., xaxis, layout = l)
end
function firstLayerPlotWavelets(∇Array, label, loc, St, sendData = cpu, isNormalized = "unnormalized")
    ∇Tensor = zeros(N, size(∇Array)...)
    J1 = size(∇Array, 2)
    for ii in 1:size(∇Array, 1), j1 in 1:J1
        ∇Tensor[:, ii, j1] = ∇Array[ii, j1]
    end
    t = generateTitle("First Layer Gradients $(isNormalized) $(loc)/$(size(∇Array, 1))\n wavelet transformed")
    ylabel = plot(; ylabel = "Frequency", grid = false, xticks = nothing, yticks = nothing, showaxis = false, right_margin = -30Plots.px, left_margin = 20Plots.px, bottom_margin = -20Plots.px)
    xlabel = plot(; xlabel = "Input time", grid = false, xticks = nothing, framestyle = nothing, yticks = nothing, showaxis = false, right_margin = -30Plots.px, left_margin = 20Plots.px, top_margin = 0Plots.px)
    wavecylinder = cat([circshift(cpu(St.mainChain[1](sendData(∇Tensor[:, loc:loc, j1:j1]))[:, :, 1, 1])', (1, 0)) for j1 = 1:J1]..., dims = 3)
    clims = (minimum(wavecylinder), maximum(wavecylinder))
    hmaps = [heatmap(wavecylinder[:, :, j1], clims = clims, xticks = false, yticks = false, color = :viridis, colorbar = false, titlefontsize = 6, title = "$(freqs[1][j1]) Hz", bottom_margin = -5Plots.px, top_margin = -10Plots.px, left_margin = -5Plots.px, right_margin = -5Plots.px) for j1 = 1:J1]
    l = @layout [t{0.03h}; ylab{0.01w} [a a a a a; a a a a a; a a a a a; a a a a a; a a a a a; a a a a _]; xlabel{0.01h}]
    p = plot(t, ylabel, hmaps..., xlabel, layout = l)
    if toSave
        savefig(joinpath(saveDir, "firstLayer$(label)$(isNormalized)$(loc)WaveletTransformed.pdf"))
    end
    return p
end
function firstLayerPlotSpecificLocs(∇Array, label, loc, freqs, isNormalized = "unnormalized")
    ∇Tensor = zeros(N, size(∇Array)...)
    J1 = size(∇Array, 2)
    for ii in 1:size(∇Array, 1), j1 in 1:J1
        ∇Tensor[:, ii, j1] = ∇Array[ii, j1]
    end
    t = generateTitle("First Layer gradients $(isNormalized) $loc/$(size(∇Array, 1))")
    l = @layout [t{0.02h}; [a a a a a; a a a a a; a a a a a; a a a a a; a a a a a; a a a a _]]
    ylims = (minimum(∇Tensor[:, loc, :]), maximum(∇Tensor[:, loc, :]))
    p = plot(t, [plot(zeroAtOutOf(loc, 128, N), ∇Tensor[:, loc, j1], xticks = false, yticks = false, color = :viridis, colorbar = false, titlefontsize = 6, title = "$(freqs[1][j1]) Hz", legend = false, framestyle = :zerolines, ylims = ylims) for j1 = 1:J1]..., layout = l)
    if toSave
        savefig(joinpath(saveDir, "firstLayer$(label)$(isNormalized)Loc$(loc).pdf"))
    end
    return p
end





# Figure 4.1
plot(plot(δ[:, 1, 1], legend = false, title = "Delta function", color = :black), plot(cylinder[:, 1, 1], legend = false, title = "Cylinder", color = :black), plot(bumps[:, 1, 1], legend = false, title = "Bumps", color = :black), plot(δFreq[:, 1, 1], legend = false, title = "Sinusoid", color = :black), xlims = (0, 2049), layout = (4, 1))
savefig(joinpath(saveDir, "gradientInputs.pdf"))


# Figure 4.2
println("-------------------------------------------------------------------")
println("starting Delta")
println("-------------------------------------------------------------------")
δ = [1; zeros(2047)]
circshiftAmount = 1024
δ = reshape(circshift(δ, circshiftAmount), (N, 1, 1))
function zerothLayerDualPlot(St, input, label, loc, sendData = cpu, isNormalized = "unnormalized", longLabel = label)
    tmp = St(sendData(input))
    @time ∇ = [cpu(gradient(x -> St(x)[0][ii, 1, 1], sendData(input))[1][:, 1, 1]) for ii = 1:size(tmp[0], 1)]
    ∇cat = cat(∇..., dims = 2)'
    p1CylNorm = heatmap(∇cat, xticks = false, ylabel = "output time", title = "Zeroth layer derivative for $(longLabel)\n layer-wise $(isNormalized)", color = :viridis, colorbar = false)
    p2CylNorm = plot(∇cat[loc, :], legend = false, xlabel = "input time")
    l = @layout [a; b{0.3h}]
    p = plot(p1CylNorm, p2CylNorm, layout = l)
    if toSave
        savefig(joinpath(saveDir, "zerothLayer$(label)$(loc)$(isNormalized).pdf"))
    end
    return p
end
zerothLayerDualPlot(St, δ, "Delta", 128, cpu, "normalized")

# figure 4.4
@time ∇δ1Unnorm = [cpu(gradient(x -> StcpuUnnorm(x)[1][ii, j1, 1], cpu(δ))[1][:, 1, 1]) for ii = 1:128, j1 = 1:J1]
firstLayerPlotSpecificLocs(∇δ1Unnorm, "Delta", 64, freqs)
# figure 4.5 (a)
firstLayerPlotWavelets(∇δ1Unnorm, "Delta", 64, StcpuUnnorm, identity)
# figure 4.5 (b)
t = generateTitle("Wavelet transformed Wavelets")
ylabel = plot(; ylabel = "Frequency", grid = false, xticks = nothing, yticks = nothing, showaxis = false, right_margin = -30Plots.px, left_margin = 20Plots.px, bottom_margin = -20Plots.px)
xlabel = plot(; xlabel = "Input time", grid = false, xticks = nothing, framestyle = nothing, yticks = nothing, showaxis = false, right_margin = -30Plots.px, left_margin = 20Plots.px, top_margin = 0Plots.px)
origWavelets = real((ifftshift(irfft(St.mainChain[1].weight, N, 1), 1)))
wavecylinder = cat([circshift(cpu(St.mainChain[1](origWavelets[:, j1])[:, :, 1, 1])', (1, 0)) for j1 = 1:J1]..., dims = 3)
hmaps = [heatmap(wavecylinder[:, :, j1], xticks = false, yticks = false, color = :viridis, colorbar = false, titlefontsize = 6, title = "$(freqs[1][j1]) Hz", bottom_margin = -5Plots.px, top_margin = -10Plots.px, left_margin = -5Plots.px, right_margin = -5Plots.px) for j1 = 1:J1]
l = @layout [t{0.03h}; ylab{0.01w} [a a a a a; a a a a a; a a a a a; a a a a a; a a a a a; a a a a _]; xlabel{0.01h}]
p = plot(t, ylabel, hmaps..., xlabel, layout = l)
savefig(joinpath(saveDir, "dog2WaveletsWaveletTransformed.pdf"))

# figure 4.10
@time allTheGradientsDelta = cpu(generateAllTheGradients(St, cpu(δ)))
plotAllTheGradients(allTheGradientsDelta, 2, "Delta spike gradient, 2nd DoG wavelets normalized")



println("-------------------------------------------------------------------")
println("starting Bumps")
println("-------------------------------------------------------------------")

bumps = reshape(testfunction(N, "Bumps"), (N, 1, 1)) # HeaviSine, Doppler, Blocks


# figure 4.6
@time ∇bumps1Unnorm = [cpu(gradient(x -> StcpuUnnorm(x)[1][ii, j1, 1], cpu(bumps))[1][:, 1, 1]) for ii = 1:128, j1 = 1:J1]
firstLayerPlotSpecificLocs(∇bumps1Unnorm, "Bumps", 64, freqs)

# figure 4.8
@time allTheGradientsUnnormBumps = cpu(generateAllTheGradients(StcpuUnnorm, cpu(bumps)))
plotAllTheGradients(allTheGradientsUnnormBumps, 2, "Bumps Gradient, 2nd DoG wavelets ")
savefig(joinpath(saveDir, "dog2BumpsInputGradient.pdf"))
savefig(joinpath(saveDir, "dog2BumpsInputGradient.png"))


println("-------------------------------------------------------------------")
println("starting Cylinder")
println("-------------------------------------------------------------------")
cylinder = reshape(circshift([ones(n); zeros(N - n)], round(Int, N / 2 - n / 2)), (N, 1, 1));

# figure 4.7
@time ∇cylinder1Unnorm = [cpu(gradient(x -> StcpuUnnorm(x)[1][ii, j1, 1], cpu(cylinder))[1][:, 1, 1]) for ii = 1:128, j1 = 1:J1]
firstLayerPlotWavelets(∇cylinder1Unnorm, "cylinder", 64, StcpuUnnorm, identity)

# figure 4.11
@time allTheGradientsUnnorm = cpu(generateAllTheGradients(StcpuUnnorm, cpu(cylinder)))
plotAllTheGradients(allTheGradientsUnnorm, 2, "Cylinder input Gradient, 2nd DoG wavelets")
savefig(joinpath(saveDir, "dog2CylinderInputGradient.pdf"))
savefig(joinpath(saveDir, "dog2CylinderInputGradient.png"))



println("-------------------------------------------------------------------")
println("starting pure sinusoid")
println("-------------------------------------------------------------------")
δFreq = [1; zeros(1024)]
circFreqshiftAmount = 64
δFreq = reshape(circshift(δFreq, circFreqshiftAmount), (1025, 1, 1))
# calculating the frequency
omega = range(0, 1000 / 2, length = 1025)
# eachNorm = [norm(w, 1) for w in eachslice(Ŵ, dims=ndims(Ŵ))]'
sum(omega .* abs.(δFreq[:, 1, 1]), dims = 1) ./ norm(δFreq)
δFreq = irfft(δFreq, N, 1)

# figure 4.3
@time ∇δFreq1Unnorm = [cpu(gradient(x -> StcpuUnnorm(x)[1][ii, j1, 1], cpu(δFreq))[1][:, 1, 1]) for ii = 1:128, j1 = 1:J1]
firstLayerPlotWavelets(∇δFreq1Unnorm, "Sinusoid", 64, StcpuUnnorm, identity)

# figure 4.9
@time allTheGradientsSinusoid = cpu(generateAllTheGradients(StcpuUnnorm, cpu(δFreq)))
heatmap([norm(allTheGradientsSinusoid[ii, jj]) for ii = 1:size(allTheGradientsSinusoid, 1), jj = 1:size(allTheGradientsSinusoid, 1)])
plotAllTheGradients(allTheGradientsSinusoid, 2, "Sinusoid gradient, 2nd DoG wavelets")
savefig(joinpath(saveDir, "dog2SineInputGradient.pdf"))
savefig(joinpath(saveDir, "dog2SineInputGradient.png"))


println("-------------------------------------------------------------------")
println("starting White Noise")
println("-------------------------------------------------------------------")

# white noise
x = cpu(randn(N, 1, 1))
# figure 4.12
@time allTheGradientsRandnUnnorm = cpu(generateAllTheGradients(StcpuUnnorm, cpu(x)))
plotAllTheGradients(allTheGradientsRandnUnnorm, 2, "White Noise input Gradient, 2nd DoG wavelets")
savefig(joinpath(saveDir, "dog2whiteNoiseInputGradient.pdf"))
savefig(joinpath(saveDir, "dog2whiteNoiseInputGradient.png"))

#     \includegraphics[width=\textwidth]{figures/interpret/gradientAlone/firstLayerSinusoidunnormalized32WaveletTransformed.pdf}
#     \includegraphics[width=\textwidth]{figures/interpret/gradientAlone/firstLayerDeltaunnormalizedLoc64.pdf}
# check     \includegraphics[width=\textwidth]{figures/interpret/gradientAlone/firstLayerDeltaunnormalized64WaveletTransformed.pdf}
#     \includegraphics[width=\textwidth]{figures/interpret/gradientAlone/firstLayerBumpsunnormalizedLoc64.pdf}
#     \includegraphics[width=\textwidth]{figures/interpret/gradientAlone/firstLayercylinderunnormalized64WaveletTransformed.pdf}
