using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, LinearAlgebra
using FFTW, FourierFilterFlux
using Optim, LineSearches, GLMNet
using Plots
using CUDA, Zygote, Flux
using LinearAlgebra
using BlackBoxOptim
using BlackBoxOptim: num_func_evals
using ScatteringInterpretation
NDims = 128
N = 100
popSize = 500
nMinsBBO = 3
perturbRate = 1 / 5
totalMins = 1 * 60
homeDir = "../../results/bellCylinderFunnel/"

function chooseBestLambda(cv)
    effectiveRegion = (cv.meanloss .- cv.stdloss) .< minimum(cv.meanloss)
    λBest = maximum(cv.path.lambda[BitVector([effectiveRegion..., zeros(Bool, length(cv.path.lambda) - length(cv.meanloss))...])])
    findfirst(x -> x == λBest, cv.path.lambda)
end

St = scatteringTransform((NDims, 1, 1), cw = dog2, 2, Q = 4, β = [1, 1, 1], extraOctaves = (0, 0, 0), averagingLength = [-1.5, -1.5, 2], outputPool = 2 * 4, convBoundary = FourierFilterFlux.Periodic(), boundary = PerBoundary())
specName = "stLay2_dog2" * "$N"
cvpSt = FileIO.load(joinpath(homeDir, specName * ".jld2"), "cvp")
cvpStRot = FileIO.load(joinpath(homeDir, specName * "Rot.jld2"), "cvpRot")
βSt2 = cvpSt.path.betas[:, :, chooseBestLambda(cvpSt)]
βSt2Rot = cvpStRot.path.betas[:, :, chooseBestLambda(cvpStRot)]

listOfβs = ((βSt2[:, 1], "Cyldog2", "Cylinder using 2nd Dog second layer only", "", St), (βSt2[:, 2], "Belldog2", "Bell using 2nd Dog second layer only", "", St), (βSt2[:, 3], "fundog2", "Funnel using 2nd Dog second layer only", "", St), (βSt2Rot[:, 1], "Cyldog2", "Cylinder using 2nd Dog second layer only", "rotated", St), (βSt2Rot[:, 2], "Belldog2", "Bell using 2nd Dog second layer only", "rotated", St), (βSt2Rot[:, 3], "fundog2", "Funnel using 2nd Dog second layer only", "rotated", St))
for (β, shortName, longName, rotated, StThis) in listOfβs
    println("-----------------------------------------------------------------------------------------")
    println("-----------------------------------------------------------------------------------------")
    println("------------------------------- starting $(longName) -------------------------------------")
    println("-----------------------------------------------------------------------------------------")
    println("-----------------------------------------------------------------------------------------")
    saveDir = joinpath(homeDir, "fitting")
    if !isdir(saveDir)
        mkpath(saveDir)
    end
    locDir = joinpath(saveDir, shortName * rotated)
    extraName = ""
    # if it already exists, append the appropriate count so we get multiple copies
    if isfile(locDir * "data.jld2")
        nRepeat = 1
        while isfile(locDir * "data$(nRepeat).jld2")
            nRepeat += 1
        end
        extraName = "$(nRepeat)"
    end
    saveName = locDir * "data$(extraName).jld2"
    saveNameDict = locDir * "dictionary"
    saveSerialName = locDir * "data$(extraName)"

    if !isdir(locDir)
        mkpath(locDir)
    end
    # second layer setup end

    function objDCT(x̂)
        x = idct(x̂, 1)
        t = StThis(reshape(x, (NDims, 1, 1)))
        1.1f0^(-sum(reshape(t[2], (:,)) .* β) + 1.0f-5 * norm(x)^2)
    end

    setProbW = bbsetup(objDCT; SearchRange = (-1000.0, 1000.0), NumDimensions = NDims, PopulationSize = popSize, MaxTime = 10.0, TraceInterval = 5, TraceMode = :silent)
    println("pretraining")
    bboptimize(setProbW)

    pop = dct(pinkStart(NDims, -1, popSize), 1)
    adjustPopulation!(setProbW, pop)

    nIters = ceil(Int, totalMins / nMinsBBO)
    for ii in 1:nIters
        println("----------------------------------------------------------")
        println("round $ii: GO")
        println("----------------------------------------------------------")
        bboptimize(setProbW, MaxTime = nMinsBBO * 60.0) # take some black box steps for the whole pop
        # for jj in 1:NDescents
        #     println("on run $jj of gradient descent")
        #     pathOfDescent, err = ScatteringTransformExperiments.pullAndTrain(setProbW, objSPC; normDiscount=1e-1, N=1000)
        # end
        pop = setProbW.runcontrollers[end].optimizer.population.individuals
        scores = [objDCT(x) for x in eachslice(pop, dims = 2)]
        iScores = sortperm(scores)

        relScoreDiff = abs(log(scores[iScores[1]]) - log(scores[iScores[round(Int, 4 * popSize / 5 - 1)]])) / abs(log(scores[iScores[1]]))
        if ii > 3 && relScoreDiff .< 0.01 # the relative difference between the best and the worst signals is less than 1%
            println("finished after $(ii) rounds")
            break
        end

        # perturb a fifth to sample the full space if it hasn't converged yet
        perturbWorst(setProbW, scores, perturbRate)
    end
    # NDescents = 30
    pop = setProbW.runcontrollers[end].optimizer.population.individuals
    scores = [objDCT(x) for x in eachslice(pop, dims = 2)]

    saveSerial(saveSerialName, setProbW)
    save(saveName, "pop", pop, "scores", scores)
end
