using DSP
using Pkg
using Revise
using Plots, LaTeXStrings
@everywhere using Wavelets, ContinuousWavelets, FFTW
@everywhere using sonarProject, CUDA, Flux, ContinuousWavelets
@everywhere using ScatteringTransform, Statistics, FourierFilterFlux
@everywhere using Random, Distributions, LinearAlgebra
@everywhere using JLD2, FileIO
using Latexify, MLJBase
using GLMNet, MLJ
using NearestNeighbors, MLDataUtils, MultivariateStats
@everywhere using GLMNet
# interpretation cyl-bell-fun examples
# Setting up the random shift distributions
figLocation = "../../figures/bcf/"
rng = MersenneTwister(2)
function generateData(N; rng = nothing, rotate = true)
    if rng == nothing
        rng = MersenneTwister()
    end
    d1 = DiscreteUniform(16, 32)
    d2 = DiscreteUniform(32, 96)

    # Making cylinder signals
    cylinderNew = zeros(128, N)
    a = rand(rng, d1, N)
    b = a + rand(rng, d2, N)
    η = randn(rng, N)
    for k = 1:N
        cylinderNew[a[k]:b[k], k] = (6 + η[k]) * ones(b[k] - a[k] + 1)
        if rotate
            cylinderNew[:, k] = circshift(cylinderNew[:, k], rand(rng, 0:(N-1)))
        end
    end

    # Making bell signals
    bellNew = zeros(128, N)
    a = rand(rng, d1, N)
    b = a + rand(rng, d2, N)
    η = randn(rng, N)
    for k = 1:N
        bellNew[a[k]:b[k], k] = (6 + η[k]) * collect(0:(b[k]-a[k])) / (b[k] - a[k])
        if rotate
            bellNew[:, k] = circshift(bellNew[:, k], rand(rng, 0:(N-1)))
        end
    end

    # Making funnel signals
    funnelNew = zeros(128, N)
    a = rand(rng, d1, N)
    b = a + rand(rng, d2, N)
    η = randn(rng, N)
    for k = 1:N
        funnelNew[a[k]:b[k], k] = (6 + η[k]) * collect((b[k]-a[k]):-1:0) / (b[k] - a[k])
        if rotate
            funnelNew[:, k] = circshift(funnelNew[:, k], rand(rng, 0:(N-1)))
        end
    end

    test = cat(cylinderNew, bellNew, funnelNew, dims = 2)
    return test + randn(rng, 128, 3N)        # adding noise
end
d = generateData(100, rng = rng)
d = generateData(100, rng = rng, rotate = false)

y = cat([[1 0 0] for i = 1:100]..., [[0 1 0] for i = 1:100]..., [[0 0 1] for i = 1:100]..., dims = 1)

"""
    indLambda = chooseBestLambda(cv)
the best lambda is the one which is has the largest value of lambda still within
a stdloss of the minimal value. This returns the index, not the value of lambda
"""
function chooseBestLambda(cv)
    effectiveRegion = (cv.meanloss .- cv.stdloss) .< minimum(cv.meanloss)
    λBest = maximum(cv.path.lambda[BitVector([effectiveRegion..., zeros(Bool, length(cv.path.lambda) - length(cv.meanloss))...])])
    findfirst(x -> x == λBest, cv.path.lambda)
end
function backgroundProb(cv)
    iλ = chooseBestLambda(cv)
    softmax(cv.path.a0[:, iλ])
end
function confusionMatrix(predMat)
    ŷtest = map(eachslice(predMat, dims = 1)) do x
        ii = argmax(x)
        ii == 1 ? "cylinder" : (ii == 2 ? "Bell" : "Funnel")
    end
    ŷtest = coerce(ŷtest, OrderedFactor)
    testSize = Int(size(predMat, 1) / 3)
    yTest = cat(["Cylinder" for i = 1:testSize], ["Bell" for i = 1:testSize], ["Funnel" for i = 1:testSize], dims = 1)
    yTest = coerce(yTest, OrderedFactor)
    conf = confusion_matrix(ŷtest, yTest)
end

getAccuracy(conf) = sum(diag(conf.mat)) / sum(conf.mat)
function loadTestResults(saveName)
    FileIO.load(joinpath("../../results/bellCylinderFunnel/", saveName * ".jld2"), "cvp")
end
function latexify_conf(conf; kwargs...)
    names = conf.labels
    topName = string("\\diagbox{Predict}{Actual} & ", string(["$name & " for name in names]...)[1:end-2], "\\\\")
    l = latexify(conf.mat; env = :table)#, kwargs...)
    lines = split(l, "\n")
    lines[1] = lines[1][1:16] * "c|" * lines[1][17:end]
    middleAdd = map(x -> x * " & ", names) .* lines[2:end-2]
    res = mapreduce(x -> x * "\n", *, (lines[1], topName, "\\hline", middleAdd..., lines[end-1:end]...))
    res[1:end-2]
end
function latexFriendlyTableName(tableName)
    # 2-> Sec
    strTable = replace(tableName, "2" => "Sec")
    # _ -> ""
    strTable = replace(strTable, "_" => "")
    # 100 -> ""
    strTable = replace(strTable, "100" => "")
    # 10 -> Small
    strTable = replace(strTable, "10" => "Small")
    # 1->First
    strTable = replace(strTable, "1" => "First")
    return strTable
end

function makeTable(conf, tableName, tableDirs = ""; sigDigAcc = 3, kwargs...)
    table = latexify_conf(conf; kwargs...)
    mkpath("../../figures/tables/" * tableDirs)
    fObj = open(joinpath("../../figures/tables/", tableDirs, tableName * ".tex"), "w+")
    write(fObj, table)
    close(fObj)
    # record the accuracy in a variable
    value = round(round(getAccuracy(conf), sigdigits = sigDigAcc) * 100, sigdigits = sigDigAcc)
    strTable = latexFriendlyTableName(tableName)
    println(strTable)
    println("acc=$value")
    alreadyRecorded = String(read("../../programmaticTex.tex"))
    previousLine = Regex("\\\\newcommand\\{\\\\$(strTable)Accuracy\\}\\{.*\\}")
    if match(previousLine, alreadyRecorded) != nothing
        toSave = replace(alreadyRecorded, previousLine => "\\newcommand{\\$(strTable)Accuracy}{$(value)\\%}")
    else
        toSave = alreadyRecorded * "\\newcommand{\\$(strTable)Accuracy}{$(value)\\%}\n"
    end
    write("../../programmaticTex.tex", toSave)
end
checkResults(saveName) = isfile(joinpath("../../results/bellCylinderFunnel/", saveName * ".jld2"))
function getResults(name, transform; N = 100, rng = nothing, maxit = Int(1e7), λ = nothing, λRot = nothing, nλ = 100, nλRot = 100, λRatio = 1e-5, λRatioRot = 1e-5, refit = false, refitRot = false, tol = 1e-7, tolRot = 1e-7, kwargs...)
    specName = name * "$N"
    cvp, cvpRot = nothing, nothing
    y = cat([[1 0 0] for i = 1:N]..., [[0 1 0] for i = 1:N]..., [[0 0 1] for i = 1:N]..., dims = 1)
    d = generateData(N, rotate = false, rng = rng)
    dRot = generateData(N, rng = rng)
    if rng == nothing
        rng = MersenneTwister()
    end
    dT = transform(d)
    dRotT = transform(dRot)
    # either generate or load the previous results for the normal data
    if checkResults(specName) && !refit
        cvp = loadTestResults(specName)
    else
        if !(λ isa Nothing)
            cvp = glmnetcv(dT', y, Multinomial(); rng = rng, parallel = true, verbose = true, maxit = maxit, lambda = λ, tol = tol, kwargs...)
        elseif λRatio isa Nothing
            cvp = glmnetcv(dT', y, Multinomial(); rng = rng, parallel = true, verbose = true, maxit = maxit, tol = tol, kwargs...)
        else
            cvp = glmnetcv(dT', y, Multinomial(); rng = rng, parallel = true, verbose = true, maxit = maxit, nlambda = nλ, lambda_min_ratio = λRatio, tol = tol, kwargs...)
        end
        save(joinpath("../../results/bellCylinderFunnel/", specName * ".jld2"), "cvp", cvp)
    end

    # either generate or load the previous results for the rotated data
    if checkResults(specName * "Rot") && !refitRot
        cvpRot = FileIO.load(joinpath("../../results/bellCylinderFunnel/", specName * "Rot.jld2"), "cvpRot")
    else
        if !(λRot isa Nothing)
            #λRot = sort(λRot, rev=true)
            cvpRot = glmnetcv(dRotT', y, Multinomial(); rng = rng, parallel = true, verbose = true, maxit = maxit, lambda = λRot, tol = tolRot, kwargs...)
        elseif λRatioRot isa Nothing
            cvpRot = glmnetcv(dRotT', y, Multinomial(); rng = rng, parallel = true, verbose = true, maxit = maxit, tol = tolRot, kwargs...)
        else
            cvpRot = glmnetcv(dRotT', y, Multinomial(); rng = rng, parallel = true, verbose = true, nlambda = nλRot, maxit = maxit, lambda_min_ratio = λRatioRot, tol = tolRot, kwargs...)
        end
        save(joinpath("../../results/bellCylinderFunnel/", specName * "Rot.jld2"), "cvpRot", cvpRot)
    end
    return cvp, cvpRot
end
function genTestResults(cvp, transform = identity, convergenceTitle = "CV Lasso convergence", testSize = 1000; legend = :bottomright, rotated = false)
    iλ = chooseBestLambda(cvp)
    println("the lengths of lambda, meanloss and stdloss $(map(length, (cvp.lambda, cvp.meanloss, cvp.stdloss)))")
    nFit = minimum(map(length, (cvp.lambda, cvp.meanloss, cvp.stdloss)))
    pConvergence = plot(cvp.lambda[1:nFit], cvp.meanloss .+ cvp.stdloss, fill = cvp.meanloss .- cvp.stdloss, legend = legend, scale = :log10, label = "std")
    pConvergence = plot!(cvp.lambda[1:nFit], cvp.meanloss, scale = :log10, label = "mean loss")
    pConvergence = vline!([cvp.lambda[chooseBestLambda(cvp)]], label = L"best\ \mu", title = convergenceTitle)
    yTest = cat([[1 0 0] for i = 1:testSize]..., [[0 1 0] for i = 1:testSize]..., [[0 0 1] for i = 1:testSize]..., dims = 1)
    testData = generateData(testSize, rotate = rotated)
    testD = transform(testData)
    testD = reshape(testD, (:, 3 * testSize))
    println(size(testD))
    testŷ = GLMNet.predict(cvp.path, testD', iλ; outtype = :prob)
    ŷtest = map(eachslice(testŷ, dims = 1)) do x
        ii = argmax(x)
        ii == 1 ? "cylinder" : (ii == 2 ? "Bell" : "Funnel")
    end
    ŷtest = coerce(ŷtest, OrderedFactor)
    yTest = cat(["cylinder" for i = 1:testSize], ["Bell" for i = 1:testSize], ["Funnel" for i = 1:testSize], dims = 1)
    yTest = coerce(yTest, OrderedFactor)
    conf = confusion_matrix(ŷtest, yTest)
    overAcc = getAccuracy(conf)
    println("overall $(rotated ? "rotated " : "")accuracy: $(overAcc)")
    return iλ, pConvergence, conf, overAcc
end

function plotRaw(β, title; legend = :bottomright)
    plot(β, line = ([:dash :solid :dashdot], 2), labels = ["Cylinder" "Bell" "Funnel"], markercolor = [:green :red :orange], marker = ([:hex :d :c], min.(8, max.(2, 20 .* abs.(β) .^ (1 / 2))), 1.0, stroke(3.0 .* abs.(β) .^ (1 / 2))), title = title, legend = legend)
end
function plotConst(cvp, colors = :default)
    iλ = chooseBestLambda(cvp)
    a0 = cvp.path.a0[:, iλ]
    limA0 = maximum(abs.(a0)) * 1.08
    p2 = scatter([0, 0, 0]', a0', labels = ["Cylinder" "Bell" "Funnel"], xlims = (-0.5, 0.5), ylims = (-limA0, limA0), framestyle = :origin, marker = ([:hexagon :diamond :circle], 8, 0.9), title = "Bias", legend = false, xticks = (0), palette = colors, markercolor = [:green :red :orange])
end
function plotCoeffs(cvp, title = "Multinomial weights for raw data", plotβ = plotRaw; legend = :bottomright)
    iλ = chooseBestLambda(cvp)
    β = cvp.path.betas[:, :, iλ]
    a0 = cvp.path.a0[:, iλ]
    limA0 = maximum(abs.(a0)) * 1.08
    colors = [:green :red :orange]
    p2 = scatter([0, 0, 0]', a0', labels = ["Cylinder" "Bell" "Funnel"], xlims = (-0.5, 0.5), ylims = (-limA0, limA0), framestyle = :origin, marker = ([:hexagon :diamond :circle], 8, 0.9), title = "Bias", legend = false, xticks = (0), markercolor = colors)
    p1 = plotβ(β, title; legend = legend)
    l = @layout [a b{0.2w}]
    plot(p1, p2, layout = l)
end
function getZeroFrac(X)
    if maximum(X) == minimum(X)
        return 0.5 # if both are zero, then just return the midpoint
    elseif maximum(X) <= 0
        return 0.9 # if the max is below zero, stick zero high (.9)
    elseif minimum(X) >= 0
        return 0.1 # if the min is below zero, stick zero low (.1)
    else
        abs(minimum(X)) / (maximum(X) - minimum(X)) # return the point between 0 and 1 that corresponds to the fraction of its distance between min and max
    end
end
getMostExtremeValue(X) = X[argmax(abs.(X), dims = 1)][1, axes(X)[2:end-1]..., 1]
function getRange(X)
    if maximum(X) <= 0 && minimum(X) < 0 # if the max is below zero, then we need to adjust a bit
        return (minimum(X), -(0.1 / 0.9) * minimum(X))
    elseif minimum(X) >= 0 && maximum(X) > 0
        return (-(0.1 / 0.9) * maximum(X), maximum(X))
    elseif minimum(X) == maximum(X)
        return (-1, 1)
    else
        return (minimum(X), maximum(X))
    end
end
function multiClassPlot(cvp, St, title = "Multinomial Coefficients"; firstFreqSpacing = nothing, secondFreqSpacing = nothing)
    iλ = chooseBestLambda(cvp)
    β = cvp.path.betas[:, :, iλ]
    a0 = cvp.path.a0[:, iλ]
    βWrap = reshape(β, (size(St(reshape(d[:, 1], (128, 1, 1)))[2])[1:end-1]..., 3))
    freqs = getMeanFreq(St, 1000)
    if firstFreqSpacing isa Integer
        firstFreqSpacing = 1:firstFreqSpacing:length(freqs[1])
    end
    if secondFreqSpacing isa Integer
        secondFreqSpacing = 1:secondFreqSpacing:length(freqs[2])
    end
    p1 = plotSecondLayer(βWrap[:, :, :, 1:1], St, toHeat = getMostExtremeValue(βWrap[:, :, :, 1:1]), transp = true, logPower = false, title = "Cylinder", c = cgrad([:royalblue1, :white, :green], getZeroFrac(getMostExtremeValue(βWrap[:, :, :, 1:1]))), xlabel = "", ylabel = "", xVals = (0.000, 0.839), yVals = (0.000, 0.973), bottom_margin = -10Plots.px, firstFreqSpacing = firstFreqSpacing, secondFreqSpacing = secondFreqSpacing, subClims = getRange(βWrap[:, :, :, 1:1]), frameTypes = :none)
    p2 = plotSecondLayer(βWrap[:, :, :, 2:2], St, toHeat = getMostExtremeValue(βWrap[:, :, :, 2:2]), transp = true, logPower = false, title = "Bell", c = cgrad([:royalblue1, :white, :red], getZeroFrac(getMostExtremeValue(βWrap[:, :, :, 2:2]))), xlabel = "", xVals = (0.000, 0.839), yVals = (0.000, 0.973), bottom_margin = -10Plots.px, firstFreqSpacing = firstFreqSpacing, secondFreqSpacing = secondFreqSpacing, subClims = getRange(βWrap[:, :, :, 2:2]), frameTypes = :none)
    p3 = plotSecondLayer(βWrap[:, :, :, 3:3], St, toHeat = getMostExtremeValue(βWrap[:, :, :, 3:3]), transp = true, logPower = false, title = "Funnel", c = cgrad([:royalblue1, :white, :orange], getZeroFrac(getMostExtremeValue(βWrap[:, :, :, 3:3]))), ylabel = "", xVals = (0.000, 0.839), yVals = (0.000, 0.973), firstFreqSpacing = firstFreqSpacing, secondFreqSpacing = secondFreqSpacing, subClims = getRange(βWrap[:, :, :, 3:3]), frameTypes = :none)
    p4 = plotConst(cvp, [:green, :red, :orange])
    l = @layout [b{0.01h}; grid(3, 1) a{0.2w}]
    titlePlot = plot(title = title, grid = false, xticks = nothing, yticks = nothing, showaxis = false, bottom_margin = -20Plots.px)
    plot(titlePlot, p1, p2, p3, p4, layout = l)
end

# classify raw data using GLMNet
λ = reverse(10.0 .^ range(-8, 0, length = 100)) #apparently  their default values really *don't* work, so I will need to choose λ manually
println("----------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------")
println("Classify raw bcf data")
println("----------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------")
cvpRaw, cvpRawRot = getResults("raw", identity, λ = λ, λRot = λ, maxit = Int(1e7), tolRot = 1e-11, tol = 1e-11, algorithm = :modifiednewtonraphson, refit = true, refitRot = true)
iλRaw, pConvergenceRaw, confRaw = genTestResults(cvpRaw, identity, "CV Lasso Convergence Raw Data", legend = :bottomleft)
iλRawRot, pConvergenceRawRot, confRawRot = genTestResults(cvpRawRot, identity, "CV Lasso Convergence Raw Rotated Data", legend = :bottomleft)
confRaw
makeTable(confRaw, "raw")
confRawRot
makeTable(confRawRot, "rawRot")
βRaw = cvpRaw.path.betas[:, :, iλRaw]
αRaw = cvpRaw.path.a0[:, iλRaw]
ci = [getAccuracy(confusionMatrix(GLMNet.predict(cvpRaw.path, generateData(1000, rng = rng, rotate = false)', ii, outtype = :prob))) for ii = 1:length(cvpRaw.path.lambda)]
plot(cvpRaw.lambda, ci, xscale = :log10, legend = false)
getAccuracy(confRaw), getAccuracy(confRawRot)
iλ = chooseBestLambda(cvpRaw)
β = cvpRaw.path.betas[:, :, iλ]
a0 = cvpRaw.path.a0[:, iλ]
rawProbs = exp.(a0) ./ sum(exp.(a0))
println("raw apriori probabilities are (cyl,bell, fun) = $(rawProbs)")
iλrot = chooseBestLambda(cvpRawRot)
βrot = cvpRawRot.path.betas[:, :, iλ]
a0rot = cvpRawRot.path.a0[:, iλ]
rawProbsRot = exp.(a0rot) ./ sum(exp.(a0rot))
println("raw apriori rotated probabilities are (cyl,bell, fun) = $(rawProbs)")
# how well did the raw classifier do on some test data?
println("generating figure 4.24(a)")
rawCoefPlt = plotCoeffs(cvpRaw)
savefig("../../figures/bcf/rawMultinomialbcf.pdf")
println("generating figure 4.24(b)")
rawRotCoefPlt = plotCoeffs(cvpRawRot, "Multinomial Weights for raw rotated data")
savefig("../../figures/bcf/rawMultinomialbcfRot.pdf")

println("----------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------")
println("Classify raw bcf data 10 examples")
println("----------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------")
λ = reverse(10.0 .^ range(-9, 0, length = 100)) #apparently  their default values really *don't* work, so I will need to choose λ manually
cvpRaw10, cvpRaw10Rot = getResults("raw", identity, λ = λ, λRot = λ, maxit = Int(1e7), tolRot = 1e-11, tol = 1e-11, algorithm = :modifiednewtonraphson, N = 10, refit = false, refitRot = false)
iλRaw10, pConvergenceRaw10, confRaw10 = genTestResults(cvpRaw10, identity, "CV Lasso Convergence Raw Data 30 Ex", legend = :bottomleft)
iλRaw10Rot, pConvergenceRaw10Rot, confRaw10Rot = genTestResults(cvpRaw10Rot, identity, "CV Lasso Convergence Raw Rotated Data 30 Ex", legend = :bottomleft, rotated = true)
confRaw10
makeTable(confRaw10, "raw10");
confRaw10Rot
makeTable(confRaw10Rot, "raw10Rot");
βRaw = cvpRaw10.path.betas[:, :, iλRaw];
αRaw = cvpRaw10.path.a0[:, iλRaw];
ci = [getAccuracy(confusionMatrix(GLMNet.predict(cvpRaw10.path, generateData(1000, rng = rng, rotated = false)', ii, outtype = :prob))) for ii = 1:length(cvpRaw10.path.lambda)]
plot(cvpRaw10.lambda, ci, xscale = :log10, legend = false)
ci = [getAccuracy(confusionMatrix(GLMNet.predict(cvpRaw10Rot.path, generateData(1000, rng = rng, rotated = true)', ii, outtype = :prob))) for ii = 1:length(cvpRaw10.path.lambda)]
plot(cvpRaw10.lambda, ci, xscale = :log10, legend = false)
getAccuracy(confRaw10), getAccuracy(confRaw10Rot)




println("----------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------")
println("FITTING avft")
println("----------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------")
avft(x) = abs.(rfft(x, 1))
λ = reverse(10.0 .^ range(-10, 0, length = 80)) #apparently  their default values really *don't* work, so I will need to choose λ manually
cvpAVFT, cvpAVFTRot = getResults("avft", avft, λ = λ, λRot = λ, maxit = Int(1e7), tolRot = 1e-11, tol = 1e-11, algorithm = :modifiednewtonraphson, refit = false, refitRot = false)
iλAVFT, pConvergenceAVFT, confAVFT = genTestResults(cvpAVFT, avft, "CV Lasso convergence AVFT", legend = :topright)
iλAVFTRot, pConvergenceAVFTRot, confAVFTRot = genTestResults(cvpAVFTRot, avft, "CV Lasso convergence AVFT Rotated", legend = :topright, rotated = true)
confAVFT
makeTable(confAVFT, "AVFT")
confAVFTRot
makeTable(confAVFTRot, "AVFTRot")

println("----------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------")
println("FITTING avft 10 examples")
println("----------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------")
λ = reverse(10.0 .^ range(-8, 0, length = 40)) #apparently  their default values really *don't* work, so I will need to choose λ manually
cvpAVFT10, cvpAVFT10Rot = getResults("avft", avft, λ = λ, λRot = λ, maxit = Int(1e7), tolRot = 1e-11, tol = 1e-11, algorithm = :modifiednewtonraphson, N = 10, refit = false, refitRot = false)
iλAVFT10, pConvergenceAVFT10, confAVFT10 = genTestResults(cvpAVFT10, avft, "CV Lasso convergence AVFT 30 Ex", legend = :topright)
iλAVFT10Rot, pConvergenceAVFT10Rot, confAVFT10Rot = genTestResults(cvpAVFT10Rot, avft, "CV Lasso convergence AVFT Rotated 30 Examples", legend = :topright, rotated = true)
confAVFT10
makeTable(confAVFT10, "AVFT10")
confAVFT10Rot
makeTable(confAVFT10Rot, "AVFT10Rot")



println("----------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------")
println("FITTING St dog2")
println("----------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------")
λ = reverse(10.0 .^ range(-8, 0, length = 30)) #apparently  their default values really *don't* work, so I will need to choose λ manually
cvpSt2, cvpSt2Rot = getResults("stLay2_dog2", St2, λ = λ, λRot = λ, maxit = Int(3e8), tolRot = 1e-11, tol = 1e-11, algorithm = :modifiednewtonraphson, refit = false, refitRot = false)
iλSt2, pConvergenceSt2, confSt2 = genTestResults(cvpSt2, St2, "CV Lasso convergence ST just second layer", legend = :bottomright)
iλSt2Rot, pConvergenceSt2Rot, confSt2Rot = genTestResults(cvpSt2Rot, St2, "CV Lasso convergence ST just second layer rotated", legend = :bottomright, rotated = true)
println(confSt2)
makeTable(confSt2, "stLay2_dog2", sigDigAcc = 4)
println(confSt2Rot)
makeTable(confSt2Rot, "stLay2_dog2Rot")

println("generating figure 4.26")
plotCoeffs(cvpSt2, "Multinomial weights for the second layer")
savefig(joinpath(figLocation, "St2dog2MultinomialLinePlot.pdf"))

println("generating figure 4.27")
multiClassPlot(cvpSt2Rot, St, "Multinomial weights for the second layer\n Rotated Signals"; firstFreqSpacing = 2)
savefig("../../figures/bcf/St2dog2MultinomialRot.pdf")

St = scatteringTransform((size(d, 1), 1, 30), cw = dog2, 2, Q = 4, β = [1, 1, 1], extraOctaves = (-0, -0, 0), averagingLength = [-1.5, -1.5, 2], outputPool = 2 * 4, convBoundary = FourierFilterFlux.Periodic(), boundary = PerBoundary())
St2(x) = reshape(St(reshape(x, (128, 1, :)))[2], (:, 30))
d = generateData(10)
Std = St(d)
# full thing is *very* good (one wrong in the tests). Doing just second layer. basically as good
println("----------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------")
println("FITTING St dog2 10ex")
println("----------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------")
λ = reverse(10.0 .^ range(-12, 0, length = 100)) #apparently  their default values really *don't* work, so I will need to choose λ manually
cvp30St2, cvp30St2Rot = getResults("stLay2_dog2", St2, λ = λ, λRot = λ, maxit = Int(3e8), tolRot = 1e-11, tol = 1e-11, algorithm = :modifiednewtonraphson, N = 10, refit = false, refitRot = false)
iλ30St2, pConvergence30St2, conf30St2 = genTestResults(cvp30St2, St2, "CV Lasso convergence ST just second layer", legend = :bottomright)
iλ30St2Rot, pConvergence30St2Rot, conf30St2Rot = genTestResults(cvp30St2Rot, St2, "CV Lasso convergence ST just second layer rotated", legend = :bottomright, rotated = true)
makeTable(conf30St2, "stLay2_dog2_10")
makeTable(conf30St2Rot, "stLay2_dog2Rot_10")





println("----------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------")
println("FITTING St dog1 dog2")
println("----------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------")
St1 = scatteringTransform((size(d, 1), 1, 300), cw = [dog1, dog2, dog2], 2, Q = 4, β = [1, 1, 1], extraOctaves = (-0, -0, 0), averagingLength = [-1.5, -1.5, 2], outputPool = 4 * 4, convBoundary = FourierFilterFlux.Periodic(), boundary = PerBoundary())
N = 100
St1_2(x) = reshape(St1(reshape(x, (128, 1, :)))[2], (:, 3N))
λ = reverse(10.0 .^ range(-11, 0, length = 50)) #apparently  their default values really *don't* work, so I will need to choose λ manually
cvpSt1_2, cvpSt1_2Rot = getResults("stLay2_dog2_dog1", λ = λ, λRot = λ, St1_2, maxit = Int(3e8), tolRot = 1e-11, tol = 1e-11, algorithm = :modifiednewtonraphson, refit = false, refitRot = false)
iλSt1_2, pConvergenceSt1_2, confSt1_2 = genTestResults(cvpSt1_2, St1_2, "CV Lasso convergence ST just second layer", legend = :topright)
iλSt1_2Rot, pConvergenceSt1_2Rot, confSt1_2Rot = genTestResults(cvpSt1_2Rot, St1_2, "CV Lasso convergence ST just second layer rotated", legend = :topright, rotated = true)
makeTable(confSt1_2, "stLay2_dog2_dog1")
makeTable(confSt1_2Rot, "stLay2_dog2_dog1Rot")

println("----------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------")
println("FITTING St dog1 dog2 10ex")
println("----------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------")
St1 = scatteringTransform((size(d, 1), 1, 30), cw = [dog1, dog2, dog2], 2, Q = 4, β = [1, 1, 1], extraOctaves = (-0, -0, 0), averagingLength = [-1.5, -1.5, 2], outputPool = 4 * 4, convBoundary = FourierFilterFlux.Periodic(), boundary = PerBoundary())
St1_2(x) = reshape(St1(reshape(x, (128, 1, :)))[2], (:, 30))
d = generateData(10)
cvp30St1_2, cvp30St1_2Rot = getResults("stLay2_dog2_dog1", λ = λ, λRot = λ, St1_2, maxit = Int(3e8), tolRot = 1e-11, tol = 1e-11, algorithm = :modifiednewtonraphson, refit = false, refitRot = false, N = 10)
iλSt1_2, pConvergence30St1_2, conf30St1_2 = genTestResults(cvp30St1_2, St1_2, "CV Lasso convergence ST just second layer", legend = :topright)
iλSt1_2Rot, pConvergence30St1_2Rot, conf30St1_2Rot = genTestResults(cvp30St1_2Rot, St1_2, "CV Lasso convergence ST just second layer rotated", legend = :topright, rotated = true)
makeTable(conf30St1_2, "stLay2_dog2_dog1_10")
makeTable(conf30St1_2Rot, "stLay2_dog2_dog1Rot_10")



println("----------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------")
println("FITTING St dog1 only")
println("----------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------")
St1 = scatteringTransform((size(d, 1), 1, 300), cw = dog1, 2, Q = 4, β = [1, 1, 1], extraOctaves = (-0, -0, 0), averagingLength = [-1.5, -1.5, 2], outputPool = 4 * 4, convBoundary = FourierFilterFlux.Periodic(), boundary = PerBoundary())
St1_2(x) = reshape(St1(reshape(x, (128, 1, :)))[2], (:, 3N))
λ = reverse(10.0 .^ range(-9, 0, length = 50)) #apparently  their default values really *don't* work, so I will need to choose λ manually
cvpSt1_2, cvpSt1_2Rot = getResults("stLay2_dog1", St1_2, λ = λ, λRot = λ, maxit = Int(3e8), tolRot = 1e-11, tol = 1e-11, algorithm = :modifiednewtonraphson, refit = false, refitRot = false)
iλSt1_2, pConvergenceSt1_2, confSt1_2 = genTestResults(cvpSt1_2, St1_2, "CV Lasso convergence ST just second layer", legend = :topright)
iλSt1_2Rot, pConvergenceSt1_2Rot, confSt1_2Rot = genTestResults(cvpSt1_2Rot, St1_2, "CV Lasso convergence ST dog1, just second layer rotated", legend = :topright, rotated = true)
makeTable(confSt1_2, "stLay2_dog1")
makeTable(confSt1_2Rot, "stLay2_dog1Rot")

println("----------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------")
println("FITTING St dog1 only, 10 ex")
println("----------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------")
d = generateData(10)
St1 = scatteringTransform((size(d, 1), 1, 30), cw = dog1, 2, Q = 4, β = [1, 1, 1], extraOctaves = (-0, -0, 0), averagingLength = [-1.5, -1.5, 2], outputPool = 4 * 4, convBoundary = FourierFilterFlux.Periodic(), boundary = PerBoundary())
St1_2(x) = reshape(St1(reshape(x, (128, 1, :)))[2], (:, 30))
cvpSt1_2, cvpSt1_2Rot = getResults("stLay2_dog1", St1_2, λ = λ, λRot = λ, maxit = Int(3e8), tolRot = 1e-11, tol = 1e-11, algorithm = :modifiednewtonraphson, refit = false, refitRot = false, N = 10)
iλSt1_2, pConvergenceSt1_2, confSt1_2 = genTestResults(cvpSt1_2, St1_2, "CV Lasso convergence ST just second layer", legend = :topright)
iλSt1_2Rot, pConvergenceSt1_2Rot, confSt1_2Rot = genTestResults(cvpSt1_2Rot, St1_2, "CV Lasso convergence ST dog1, just second layer rotated", legend = :topright, rotated = true)
makeTable(confSt1_2, "stLay2_dog1_10")
makeTable(confSt1_2Rot, "stLay2_dog1Rot_10")
