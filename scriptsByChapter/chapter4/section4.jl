using DSP
using Pkg
using Revise
using Plots, LaTeXStrings
using Wavelets, ContinuousWavelets, FFTW
using sonarProject, CUDA, Flux, ContinuousWavelets
using ScatteringTransform, Statistics, FourierFilterFlux
using Random, Distributions, LinearAlgebra
using JLD2, FileIO
using Distributed
using Latexify, MLJBase
using GLMNet, MLJ
using NearestNeighbors, MLDataUtils, MultivariateStats
using BlackBoxOptim
using ScatteringInterpretation
using GLMNet
# interpretation cyl-bell-fun examples
# Setting up the random shift distributions
figLocation = "../../figures/bcf/"
rng = MersenneTwister(2)
function generateData(N; rng = nothing, rotate = true)
    if rng == nothing
        rng = MersenneTwister()
    end
    d1 = DiscreteUniform(16, 32)
    d2 = DiscreteUniform(32, 96)

    # Making cylinder signals
    cylinderNew = zeros(128, N)
    a = rand(rng, d1, N)
    b = a + rand(rng, d2, N)
    η = randn(rng, N)
    for k = 1:N
        cylinderNew[a[k]:b[k], k] = (6 + η[k]) * ones(b[k] - a[k] + 1)
        if rotate
            cylinderNew[:, k] = circshift(cylinderNew[:, k], rand(rng, 0:(N-1)))
        end
    end

    # Making bell signals
    bellNew = zeros(128, N)
    a = rand(rng, d1, N)
    b = a + rand(rng, d2, N)
    η = randn(rng, N)
    for k = 1:N
        bellNew[a[k]:b[k], k] = (6 + η[k]) * collect(0:(b[k]-a[k])) / (b[k] - a[k])
        if rotate
            bellNew[:, k] = circshift(bellNew[:, k], rand(rng, 0:(N-1)))
        end
    end

    # Making funnel signals
    funnelNew = zeros(128, N)
    a = rand(rng, d1, N)
    b = a + rand(rng, d2, N)
    η = randn(rng, N)
    for k = 1:N
        funnelNew[a[k]:b[k], k] = (6 + η[k]) * collect((b[k]-a[k]):-1:0) / (b[k] - a[k])
        if rotate
            funnelNew[:, k] = circshift(funnelNew[:, k], rand(rng, 0:(N-1)))
        end
    end

    test = cat(cylinderNew, bellNew, funnelNew, dims = 2)
    return test + randn(rng, 128, 3N)        # adding noise
end
d = generateData(100, rng = rng)
d = generateData(100, rng = rng, rotate = false)

println("generating figure 4.23")
plot(plot(d[:, 2], title = "Cylinder", legend = false, c = :green, linewidth = 3), plot(d[:, 103], title = "Bell", legend = false, c = :red, linewidth = 3), plot(d[:, 270], title = "Funnel", legend = false, c = :orange, linewidth = 3), layout = (3, 1))
savefig("../../figures/bcf/cbf.pdf")



println("generating figure 4.25")
St = scatteringTransform((size(d, 1), 1, 300), cw = dog2, 2, Q = 4, β = [1, 1, 1], extraOctaves = (-0, -0, 0), averagingLength = [-1.5, -1.5, 0], outputPool = 2 * 4, convBoundary = FourierFilterFlux.Periodic(), boundary = PerBoundary())
a, b, c = getWavelets(St)
p1 = plot(plot(abs.(a), legend = false, title = "First Layer Wavelets"), plot([abs.(mean(rfft(d .- mean(d, dims = 1), 1), dims = 2)) abs.(std(rfft(d, 1), dims = 2))], labels = ["mean" "std"], title = "Data Mean and Std"), legendfontsize = 6, layout = (2, 1))
firstLayRes = St.mainChain[3](St.mainChain[2](St.mainChain[1](d)))
p2 = plot(plot(abs.(b), legend = false, title = "Second Layer Wavelets"), plot([abs.(mean(rfft(firstLayRes .- mean(firstLayRes, dims = 1), 1), dims = (2, 3))[:, 1, 1]) abs.(std(rfft(firstLayRes, 1), dims = (2, 3))[:, 1, 1])], labels = ["mean" "std"], title = "Internal First Layer Data Mean and Std", xlabel = "Positive Frequency Index"), legendfontsize = 6, layout = (2, 1))
plot(p1, p2, layout = (2, 1))
savefig(joinpath(figLocation, "chosingDog2Wavelets.pdf"))



gr(dpi = 200)
homeDir = "../../results/bellCylinderFunnel/"
function chooseBestLambda(cv)
    effectiveRegion = (cv.meanloss .- cv.stdloss) .< minimum(cv.meanloss)
    λBest = maximum(cv.path.lambda[BitVector([effectiveRegion..., zeros(Bool, length(cv.path.lambda) - length(cv.meanloss))...])])
    findfirst(x -> x == λBest, cv.path.lambda)
end
St = scatteringTransform((NDims, 1, 1), cw = dog2, 2, Q = 4, β = [1, 1, 1], extraOctaves = (0, 0, 0), averagingLength = [-1.5, -1.5, 2], outputPool = 2 * 4, convBoundary = FourierFilterFlux.Periodic(), boundary = PerBoundary())
specName = "stLay2_dog2" * "$N"
cvpSt = FileIO.load(joinpath(homeDir, specName * ".jld2"), "cvp")
cvpStRot = FileIO.load(joinpath(homeDir, specName * "Rot.jld2"), "cvpRot")
βSt2 = cvpSt.path.betas[:, :, chooseBestLambda(cvpSt)]
βSt2Rot = cvpStRot.path.betas[:, :, chooseBestLambda(cvpStRot)]
freqsSt = getMeanFreq(St, 1000)

function plotConst(cvp, colors = :default)
    iλ = chooseBestLambda(cvp)
    a0 = cvp.path.a0[:, iλ]
    limA0 = maximum(abs.(a0)) * 1.08
    p2 = scatter([0, 0, 0]', a0', labels = ["Cylinder" "Bell" "Funnel"], xlims = (-0.5, 0.5), ylims = (-limA0, limA0), framestyle = :origin, marker = ([:hexagon :circle :diamond], 8, 0.9), title = "Bias", legend = false, xticks = (0), palette = colors)
end
function getZeroFrac(X)
    if maximum(X) == minimum(X)
        return 0.5 # if both are zero, then just return the midpoint
    elseif maximum(X) <= 0
        return 0.9 # if the max is below zero, stick zero high (.9)
    elseif minimum(X) >= 0
        return 0.1 # if the min is below zero, stick zero low (.1)
    else
        abs(minimum(X)) / (maximum(X) - minimum(X)) # return the point between 0 and 1 that corresponds to the fraction of its distance between min and max
    end
end
function getRange(X)
    if maximum(X) <= 0 && minimum(X) < 0 # if the max is below zero, then we need to adjust a bit
        return (minimum(X), -(0.1 / 0.9) * minimum(X))
    elseif minimum(X) >= 0 && maximum(X) > 0
        return (-(0.1 / 0.9) * maximum(X), maximum(X))
    elseif minimum(X) == maximum(X)
        return (-1, 1)
    else
        return (minimum(X), maximum(X))
    end
end

function getMostExtremeValue(X)
    X[argmax(abs.(X), dims = 1)][1, axes(X)[2:end-1]..., 1]
end
using PerceptualColourMaps

saveFigDir = "../../figures/bcf/interpret/"
if !isdir(saveFigDir)
    mkpath(saveFigDir)
end
listOfβs = ((βSt2[:, 1], "Cyldog2", "Cylinder using DoG2 second layer only", "", St), (βSt2[:, 2], "Belldog2", "Bell using DoG2 second layer only", "", St), (βSt2[:, 3], "fundog2", "Funnel using DoG2 second layer only", "", St), (βSt2Rot[:, 1], "Cyldog2", "Cylinder using DoG2 second layer only", "rotated", St), (βSt2Rot[:, 2], "Belldog2", "Bell using DoG2 second layer only", "rotated", St), (βSt2Rot[:, 3], "fundog2", "Funnel using DoG2 second layer only", "rotated", St))
listOfLists = (((βSt2[:, 1], "Cyldog2", "Cylinder using DoG2 Dog second layer only", "", St), (βSt2[:, 2], "Belldog2", "Bell using DoG2 Dog second layer only", "", St), (βSt2[:, 3], "fundog2", "Funnel using DoG2 Dog second layer only", "", St),),
    ((βSt2Rot[:, 1], "Cyldog2", "Cylinder using DoG2 Dog second layer only", "rotated", St), (βSt2Rot[:, 2], "Belldog2", "Bell using DoG2 Dog second layer only", "rotated", St), (βSt2Rot[:, 3], "fundog2", "Funnel using DoG2 Dog second layer only", "rotated", St)))
longTitles = ("DoG2 best fits Classic bcf", "DoG2 best fits Rotated bcf")
shortSetNames = ("dog2", "dog2Rot")
listOfFreqs = (freqsSt, freqsSt)
println("generating figures 4.28-30(a) and 4.31-33(a)")
for jj = 1:2
    longTitle = longTitles[jj]
    listOfTraits = listOfLists[jj]
    shortSetName = shortSetNames[jj]
    freqs = listOfFreqs[jj]
    dog2Sols = zeros(128, 3)
    StThis = listOfTraits[end]
    for ii = 1:3
        (β, shortName, longName, rotated, StThis) = listOfTraits[ii]
        saveDir = joinpath(homeDir, "fitting")
        locDir = joinpath(saveDir, shortName * rotated)
        extraName = ""
        # unless otherwise specified, use the most recently fit version
        if isfile(locDir * "data.jld2") && extraName == ""
            nRepeat = 1
            while isfile(locDir * "data$(nRepeat).jld2")
                nRepeat += 1
            end
            extraName = "$(nRepeat - 1)"
        end
        saveName = locDir * "data$(extraName).jld2"
        saveNameDict = locDir * "dictionary"
        saveSerialName = locDir * "data$(extraName)"
        savedRes = FileIO.load(saveName)
        pop = savedRes["pop"]
        scores = savedRes["scores"]
        iScores = sortperm(scores)
        bestEx = idct(pop[:, iScores[1]], 1)
        dog2Sols[:, ii] = bestEx
    end
    locFigDir = joinpath(saveFigDir, shortSetName)
    if !isdir(locFigDir)
        mkpath(locFigDir)
    end
    overTitle = plot(randn(1, 2), xlims = (1, 1.1), xshowaxis = false, yshowaxis = false, label = "", colorbar = false, grid = false, framestyle = :none, title = longTitle, top_margin = -25Plots.px, bottom_margin = -2Plots.px)
    actualPlots = plot(plot(dog2Sols[:, 1], title = "Cylinder", xticks = (0:20:120, ""), top_margin = 10Plots.px), plot(dog2Sols[:, 2], title = "Bell", xticks = (0:20:120, "")), plot(dog2Sols[:, 3], title = "Funnel"), layout = (3, 1), legend = false, color = :black)
    l = @layout [a{0.0001h}; b]
    plot(overTitle, actualPlots, layout = l)
    savefig(joinpath(locFigDir, "fitSignals.pdf"))
    savefig(joinpath(locFigDir, "fitSignals.png"))
    actualScalograms = plot(heatmap(1:128, freqs[1][1:end-1], abs.(circshift(StThis.mainChain[1](dog2Sols[:, 1])[:, 1:(end-1), 1, 1]', (0, 0))) .^ 2; c = cgrad(:viridis, scale = :exp), framestyle = :box, title = "Cylinder", titlefontsize = 8, guidefontsize = 5, tickfontsize = 4, colorbar = false, link = :all, xticks = (1:20:120, ""), bottom_margin = -3Plots.px), heatmap(1:128, freqs[1][1:end-1], abs.(circshift(StThis.mainChain[1](dog2Sols[:, 2])[:, 1:(end-1), 1, 1]', (0, 0))) .^ 2; c = cgrad(:viridis, scale = :exp), framestyle = :box, title = "Bell", titlefontsize = 8, guidefontsize = 5, tickfontsize = 4, colorbar = false, link = :all, xticks = (1:20:120, ""), bottom_margin = -3Plots.px), heatmap(1:128, freqs[1][1:end-1], abs.(circshift(StThis.mainChain[1](dog2Sols[:, 3])[:, 1:(end-1), 1, 1]', (0, 0))) .^ 2; c = cgrad(:viridis, scale = :exp), framestyle = :box, title = "Funnel", titlefontsize = 8, guidefontsize = 7, tickfontsize = 4, colorbar = false, link = :all, bottom_margin = -4Plots.px, xlabel = "Time (ms)"), layout = (3, 1))
    overTitle = plot(randn(1, 2), xlims = (1, 1.1), xshowaxis = false, yshowaxis = false, label = "", colorbar = false, grid = false, framestyle = :none, title = "$(longTitle), Scalograms", top_margin = -25Plots.px, bottom_margin = -5Plots.px)
    fauxYLabel = Plots.scatter([0, 0], [0, 1], xlims = (1, 1.1), xshowaxis = false, label = "", colorbar = false, grid = false, ylabel = "Frequency (Hz)", xticks = false, yticks = false, framestyle = :grid, left_margin = 13Plots.px, right_margin = -38Plots.px)
    lay = @layout [a{0.01h}; [b{0.00001w} c]]
    plot(overTitle, fauxYLabel, actualScalograms, layout = lay)
    savefig(joinpath(locFigDir, "fitSignalsScalograms.pdf"))
    savefig(joinpath(locFigDir, "fitSignalsScalograms.png"))

    # second layer plots for the best fits
    p1 = plotSecondLayer(St(reshape(dog2Sols[:, 1], (128, 1, 1))), St, logPower = false, title = "Cylinder", yVals = (0.000, 0.973), frameTypes = :zerolines, xlabel = "", ylabel = "", bottom_margin = 1Plots.px, xticks = nothing, firstFreqSpacing = 1:2:length(freqs[2]))
    p2 = plotSecondLayer(St(reshape(dog2Sols[:, 2], (128, 1, 1))), St, logPower = false, title = "Bell", yVals = (0.000, 0.973), frameTypes = :zerolines, xlabel = "", ylabel = "", bottom_margin = 1Plots.px, xticks = nothing, firstFreqSpacing = 1:2:length(freqs[2]))
    p3 = plotSecondLayer(St(reshape(dog2Sols[:, 3], (128, 1, 1))), St, logPower = false, title = "Funnel", yVals = (0.000, 0.973), frameTypes = :zerolines, ylabel = "", firstFreqSpacing = 1:2:length(freqs[2]))
    overTitle = plot(randn(1, 2), xlims = (1, 1.1), xshowaxis = false, yshowaxis = false, label = "", colorbar = false, grid = false, framestyle = :none, title = "$(longTitle), Second Layer", top_margin = -25Plots.px, bottom_margin = -5Plots.px)
    l = @layout [b{0.01h}; grid(3, 1)]
    yaxis = plot(grid = false, right_margin = -40Plots.px, left_margin = 15Plots.px, xticks = nothing, yticks = nothing, showaxis = false, ylabel = "Layer 1 Frequency (Hz)")
    l = @layout [a{0.0001h}; b{0.0001w} grid(3, 1)]
    plot(overTitle, yaxis, p1, p2, p3, layout = l)
    savefig(joinpath(locFigDir, "fitSignalsSecondLayer.pdf"))
    savefig(joinpath(locFigDir, "fitSignalsSecondLayer.png"))
end
