using Distributed
addprocs(12)
@everywhere using ScatteringTransform, ContinuousWavelets, Plots, JLD, JLD2, FileIO, FFTW, LinearAlgebra, FourierFilterFlux
@everywhere using Optim, LineSearches
@everywhere using MLJ, DataFrames, MLJLinearModels
@everywhere using Plots
@everywhere using Revise
@everywhere using CUDA, Zygote, Flux, FFTW
@everywhere using Test
@everywhere using LinearAlgebra
@everywhere using BlackBoxOptim
@everywhere using BlackBoxOptim: num_func_evals
@everywhere using ScatteringInterpretation


N = 128
popSize = 500
totalMins = 1 * 60
nMinsBBO = 3
perturbRate = 1 / 5
β = [2, 2, 1]
averagingLength = [-1, -1, 2]
outputPool = 8
normalize = false
poolBy = 3 // 2
pNorm = 2
extraOctaves = 0
waveletExamples = (dog2, dog1, Morlet(π), Morlet(2π))
shortNames = ["dog2", "dog1", "morl", "morl2Pi"] # set the names for saving



println("----------------------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------")
println("----------------------------------------------------------------------------")
println("----------------------------------------------------------------------------")
println("Generating the data for plot 4.13 (the zeroth layer)")
println("----------------------------------------------------------------------------")
println("----------------------------------------------------------------------------")
println("----------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------------------")
for (ii, CW) = enumerate(waveletExamples)
    namedCW = shortNames[ii]
    St = scatteringTransform((N, 1, 1), cw = CW, 2, poolBy = poolBy, β = β, averagingLength = averagingLength, outputPool = outputPool, normalize = normalize, σ = abs, p = pNorm, extraOctaves = extraOctaves)
    x = randn(N, 1, 1)
    Sx = St(x)
    # plotSecondLayer(Sx, St)
    extraName = ""
    l = ceil(Int, 1 / 2 * size(Sx[0], 1))
    p = pathLocs(0, (l,))
    aveLenStr = mapreduce(x -> "$x", *, averagingLength)
    saveDir = "../../results/singleFits/$(namedCW)/poolBy$(round(poolBy, sigdigits=3))_outPool$(outputPool)_pNorm$(pNorm)_aveLen$(aveLenStr)_extraOct$(extraOctaves)/lay0/"
    if !isdir(saveDir)
        mkpath(saveDir)
    end
    locDir = saveDir * "l$(l)/"
    saveName = locDir * "data$(extraName).jld2"
    saveSerialName = locDir * "data"
    if !isdir(locDir)
        mkpath(locDir)
    end
    function objectiveDCT(x̂)
        x = idct(x̂)
        t = St(reshape(x, (N, 1, 1)))
        1.1f0^(-t[p][1] + 1.0f-5 * norm(x)^2)
    end
    fitnessCallsHistoryDCT = Array{Int,1}()
    fitnessHistoryDCT = Array{Float64,1}()
    callbackDCT = function recDCT(oc)
        push!(fitnessCallsHistoryDCT, num_func_evals(oc))
        push!(fitnessHistoryDCT, best_fitness(oc))
    end
    setProbDCT = bbsetup(objectiveDCT; SearchRange = (-10000.0, 10000.0), NumDimensions = N, PopulationSize = popSize, MaxTime = 1.0, TraceInterval = 5, CallbackFunction = callbackDCT, CallbackInterval = 0.5, TraceMode = :silent)
    println("pretraining")
    bboptimize(setProb)

    pop = pinkStart(N, -1, popSize)
    popDCT = dct(pop, 1) # start from the same population
    adjustPopulation!(setProbDCT, popDCT)

    nIters = ceil(Int, totalMins / nMinsBBO)
    for ii in 1:nIters
        println("----------------------------------------------------------")
        println("round $ii, DCT: GO")
        println("----------------------------------------------------------")
        bboptimize(setProbDCT, MaxTime = nMinsBBO * 60.0) # take some black box steps for the whole pop
        popDCT = setProbDCT.runcontrollers[end].optimizer.population.individuals
        scoresDCT = [objectiveDCT(x) for x in eachslice(popDCT, dims = 2)]
        iScoresDCT = sortperm(scoresDCT)

        relScoreDiffDCT = abs(log(scoresDCT[iScoresDCT[1]]) - log(scoresDCT[iScoresDCT[round(Int, 4 * popSize / 5 - 1)]])) / abs(log(scoresDCT[iScoresDCT[1]]))
        if ii > 3 && relScoreDiffDCT .< 0.01
            println("finished after $(ii) rounds, relative Score diff= $(relScoreDiffDCT)")
            break
        end
        # perturb a fifth to sample the full space
        perturbWorst(setProbDCT, scoresDCT, perturbRate)
    end

    popDCT = setProbDCT.runcontrollers[end].optimizer.population.individuals
    scoresDCT = [objectiveDCT(x) for x in eachslice(pop, dims = 2)]
    if minimum(scoresDCT) < minimum(scores)
        betterPop = popDCT
        betterScores = scoresDCT
        betterSetProb = setProbDCT
        betterObjective = objectiveDCT
        mapToSpace = x -> idct(x, 1)
    else
        betterPop = pop
        betterScores = scores
        betterSetProb = setProb
        betterObjective = objectiveSpace
        mapToSpace = identity
    end

    saveSerial(saveSerialName, setProbDCT)
    save(saveName, "pop", popDCT, "scores", scoresDCT, "fitnessCallsDCT", fitnessCallsHistoryDCT, "fitnessHistoryDCT", fitnessHistoryDCT)
end


println("----------------------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------")
println("----------------------------------------------------------------------------")
println("----------------------------------------------------------------------------")
println("Generating the data for plots 14-17 (the first layer)")
println("----------------------------------------------------------------------------")
println("----------------------------------------------------------------------------")
println("----------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------------------")
for (ii, CW) in enumerate(waveletExamples)
    println("----------------------------------------------------------------------------------------------")
    println("-------------------------------starting $(CW)-------------------------------")
    println("----------------------------------------------------------------------------------------------")
    namedCW = shortNames[ii]
    J1, J2 = map(x -> size(x, 2), getWavelets(St))
    @sync @distributed for w1 in 1:J1-1
        St = scatteringTransform((N, 1, 1), cw = CW, 2, poolBy = poolBy, β = β, averagingLength = averagingLength, outputPool = outputPool, normalize = normalize, σ = abs, p = pNorm, extraOctaves = extraOctaves)
        x = randn(N, 1, 1)
        Sx = St(x)
        extraName = ""
        l = ceil(Int, 1 / 2 * size(Sx[1], 1))
        p = pathLocs(1, (l, w1))
        aveLenStr = mapreduce(x -> "$x", *, averagingLength)
        saveDir = "../../results/singleFits/$(namedCW)/poolBy$(round(poolBy, sigdigits=3))_outPool$(outputPool)_pNorm$(pNorm)_aveLen$(aveLenStr)_extraOct$(extraOctaves)/lay1/"
        if !isdir(saveDir)
            mkpath(saveDir)
        end
        locDir = saveDir * "wf$(w1)ws/"
        saveName = locDir * "data$(extraName).jld2"
        saveNameDict = locDir * "dictionary"
        saveSerialName = locDir * "data"
        if !isdir(locDir)
            mkpath(locDir)
        end
        if !isdir(locFigDir)
            mkpath(locFigDir)
        end
        function objectiveDCT(x̂)
            x = idct(x̂)
            t = St(reshape(x, (N, 1, 1)))
            1.1f0^(-t[p][1] + 1.0f-5 * norm(x)^2)
        end
        function objectiveSpace(x)
            t = St(reshape(x, (N, 1, 1)))
            1.1f0^(-t[p][1] + 1.0f-5 * norm(x)^2)
        end

        fitnessCallsHistoryDCT = Array{Int,1}()
        fitnessHistoryDCT = Array{Float64,1}()
        callbackDCT = function recDCT(oc)
            push!(fitnessCallsHistoryDCT, num_func_evals(oc))
            push!(fitnessHistoryDCT, best_fitness(oc))
        end
        setProbDCT = bbsetup(objectiveDCT; SearchRange = (-1000.0, 1000.0), NumDimensions = N, PopulationSize = popSize, MaxTime = 1.0, TraceInterval = 5, CallbackFunction = callbackDCT, CallbackInterval = 0.5, TraceMode = :silent)
        println("pretraining")
        bboptimize(setProbDCT)
        pop = pinkStart(N, -1, popSize)
        popDCT = dct(pop, 1) # start from the same population
        adjustPopulation!(setProbDCT, popDCT)
        nIters = ceil(Int, totalMins / nMinsBBO)
        for ii in 1:nIters
            println("----------------------------------------------------------")
            println("round $ii, DCT: GO")
            println("----------------------------------------------------------")
            bboptimize(setProbDCT, MaxTime = nMinsBBO * 60.0) # take some black box steps for the whole pop
            popDCT = setProbDCT.runcontrollers[end].optimizer.population.individuals
            scoresDCT = [objectiveDCT(x) for x in eachslice(popDCT, dims = 2)]
            iScoresDCT = sortperm(scoresDCT)

            relScoreDiffDCT = abs(log(scoresDCT[iScoresDCT[1]]) - log(scoresDCT[iScoresDCT[round(Int, 4 * popSize / 5 - 1)]])) / abs(log(scoresDCT[iScoresDCT[1]]))
            if ii > 3 && relScoreDiffDCT .< 0.01
                println("finished after $(ii) rounds, relative Score diff= $(relScoreDiffDCT)")
                break
            end
            # perturb a fifth to sample the full space
            perturbWorst(setProbDCT, scoresDCT, perturbRate)
        end
        popDCT = setProbDCT.runcontrollers[end].optimizer.population.individuals
        scoresDCT = [objectiveDCT(x) for x in eachslice(pop, dims = 2)]
        save(saveName, "pop", idct(popDCT, 1), "scores", scoresDCT)
        saveSerial(saveSerialName, setProbDCT)
    end
end




println("----------------------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------")
println("----------------------------------------------------------------------------")
println("----------------------------------------------------------------------------")
println("Generating the data for plots 18-22 (the second layer)")
println("----------------------------------------------------------------------------")
println("----------------------------------------------------------------------------")
println("----------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------------------")
println("----------------------------------------------------------------------------------------------")


extraName = "full1"
for (ii, CW) = enumerate(waveletExamples)
    namedCW = shortNames[ii]
    St = scatteringTransform((N, 1, 1), cw = CW, 2, poolBy = poolBy, β = β, averagingLength = averagingLength, outputPool = outputPool, normalize = normalize, σ = abs, p = pNorm, extraOctaves = extraOctaves)
    J1, J2 = map(x -> size(x, 2), getWavelets(St))
    listOfLocations = ([(j2, j1) for j1 = 1:(J1-1), j2 = 1:(J2-1)]...,)
    println("doing $CW, there are $(length(listOfLocations)) examples to do")
    # second layer setup begin
    # (w2,w1) = listOfLocations[15]
    @sync @distributed for (w2, w1) in listOfLocations
        St = scatteringTransform((N, 1, 1), cw = CW, 2, poolBy = poolBy, β = β, averagingLength = averagingLength, outputPool = outputPool, normalize = normalize, σ = abs, p = pNorm, extraOctaves = extraOctaves)
        x = randn(N, 1, 1)
        Sx = St(x)
        aveLenStr = mapreduce(x -> "$x", *, averagingLength)
        saveDir = "../../results/singleFits/$(namedCW)/poolBy$(round(poolBy, sigdigits=3))_outPool$(outputPool)_pNorm$(pNorm)_aveLen$(aveLenStr)_extraOct$(extraOctaves)/lay2/"
        if !isdir(saveDir)
            mkpath(saveDir)
        end
        println("doing ($w2,$w1)")
        l = floor(Int, 1 / 2 * size(Sx[2], 1))
        p = pathLocs(2, (l, w2, w1))
        locDir = saveDir * "wf$(w1)ws$(w2)/"
        saveName = locDir * "data$(extraName).jld2"
        saveNameDict = locDir * "dictionary"
        saveSerialName = locDir * "data"
        if !isdir(locDir)
            mkpath(locDir)
        end
        # second layer setup end

        function objectiveDCT(x̂)
            x = idct(x̂)
            t = St(reshape(x, (N, 1, 1)))
            1.1f0^(-t[p][1] + 1.0f-5 * norm(x)^2)
        end

        fitnessCallsHistoryDCT = Array{Int,1}()
        fitnessHistoryDCT = Array{Float64,1}()
        callbackDCT = function recDCT(oc)
            push!(fitnessCallsHistoryDCT, num_func_evals(oc))
            push!(fitnessHistoryDCT, best_fitness(oc))
        end
        setProbDCT = bbsetup(objectiveDCT; SearchRange = (-1000.0, 1000.0), NumDimensions = N, PopulationSize = popSize, MaxTime = 1.0, TraceInterval = 5, CallbackFunction = callbackDCT, CallbackInterval = 0.5, TraceMode = :silent)
        println("pretraining freq")
        bboptimize(setProbDCT)

        pop = pinkStart(N, -1, popSize)
        popDCT = dct(pop, 1) # start from the same population
        adjustPopulation!(setProbDCT, popDCT)

        nIters = ceil(Int, totalMins / nMinsBBO)
        @time for ii in 1:nIters
            println("----------------------------------------------------------")
            println("round $ii, DCT: GO")
            println("----------------------------------------------------------")
            bboptimize(setProbDCT, MaxTime = nMinsBBO * 60.0) # take some black box steps for the whole pop
            popDCT = setProbDCT.runcontrollers[end].optimizer.population.individuals
            scoresDCT = [objectiveDCT(x) for x in eachslice(popDCT, dims = 2)]
            iScoresDCT = sortperm(scoresDCT)

            relScoreDiffDCT = abs(log(scoresDCT[iScoresDCT[1]]) - log(scoresDCT[iScoresDCT[round(Int, 4 * popSize / 5 - 1)]])) / abs(log(scoresDCT[iScoresDCT[1]]))
            if ii > 3 && relScoreDiffDCT .< 0.01
                println("finished after $(ii) rounds, relative Score diff= $(relScoreDiffDCT)")
                break
            end
            # perturb a fifth to sample the full space
            perturbWorst(setProbDCT, scoresDCT, perturbRate)
        end

        popDCT = setProbDCT.runcontrollers[end].optimizer.population.individuals
        scoresDCT = [objectiveDCT(x) for x in eachslice(popDCT, dims = 2)]
        betterPop = popDCT
        betterScores = scoresDCT
        betterSetProb = setProbDCT
        betterObjective = objectiveDCT
        mapToSpace = x -> idct(x, 1)

        saveSerial(saveSerialName, betterSetProb)
        save(saveName, "pop", betterPop, "scores", betterScores, "fitnessCallsHistoryDCT", fitnessCallsHistoryDCT, "fitnessHistoryDCT", fitnessHistoryDCT)
    end
end
